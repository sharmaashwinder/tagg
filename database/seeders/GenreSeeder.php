<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Genre;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Genre::updateOrCreate(['name' => 'Fiction']);
        Genre::updateOrCreate(['name' => 'Action']);
        Genre::updateOrCreate(['name' => 'Fantasy']);
        Genre::updateOrCreate(['name' => 'Adventure']);
        Genre::updateOrCreate(['name' => 'Role Playing']);
        Genre::updateOrCreate(['name' => 'Simulation']);
        Genre::updateOrCreate(['name' => 'Strategy']);
        Genre::updateOrCreate(['name' => 'Sports']);
        Genre::updateOrCreate(['name' => 'Racing']);
        Genre::updateOrCreate(['name' => 'Puzzle']);
    }
}

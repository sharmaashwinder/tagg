<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run() {
        $this->call(FormatSeeder::class);
        $this->call(GenreSeeder::class);
        $this->call(ModifierSeeder::class);
        $this->call(PlatformSeeder::class);
        $this->call(ReportRequestSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(SkillLevelSeeder::class);
        $this->call(UserSeeder::class);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $a=new User;
        $user1 = User::updateOrCreate(['name' => 'Super Admin', 'gamer_name' => 'superAdmin', 'email' => 'admin@admin.com', 'password' => bcrypt('123456'),'slug'=>$a->slug_creater()]);
        $user1->assignRole('admin');
        $user2 = User::updateOrCreate(['name' => 'User', 'gamer_name' => 'user', 'status' => '0', 'email' => 'user@user.com', 'password' => bcrypt('123456'),'slug'=>$a->slug_creater()]);
        $user2->assignRole('gamer');
    }
}
// yopmail

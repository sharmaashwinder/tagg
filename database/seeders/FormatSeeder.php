<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Format;

class FormatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Format::updateOrCreate(['name' => '1v1']);
        Format::updateOrCreate(['name' => '2v2']);
        Format::updateOrCreate(['name' => '3v3']);
        Format::updateOrCreate(['name' => '4v4']);
        Format::updateOrCreate(['name' => '5v5']);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Modifier;

class ModifierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Modifier::updateOrCreate(['name' => 'Boost Speed']);
        Modifier::updateOrCreate(['name' => 'Being Active']);
        Modifier::updateOrCreate(['name' => 'Highly Active']);
    }
}

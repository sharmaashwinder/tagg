<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Setting::truncate();
        Setting::updateOrCreate(['label' => '$1 is equal to ', 'description' => "$1 is equal to how many TG tokens.", 'key_name' => 'token_value', 'key_value' => '50']);
        Setting::updateOrCreate(['label' => 'Tagg fee', 'description' => "How much Tagg fee deduct form payouts.", 'key_name' => 'tagg_fee', 'key_value' => '10']);
        Setting::updateOrCreate(['label' => 'Affiliate fee', 'description' => "How much affiliate fee will be deducted form payouts.", 'key_name' => 'affiliate_fee', 'key_value' => '5']);
        Setting::updateOrCreate(['label' => 'Maximum amount to add in wallet (USD)', 'description' => "Maximum amount to add in the wallet in USD.", 'key_name' => 'max_token_add', 'key_value' => '500']);
        Setting::updateOrCreate(['label' => 'Minimum amount to add in wallet (USD)', 'description' => "Minimum amount to add in the wallet in USD.", 'key_name' => 'min_token_add', 'key_value' => '10']);
        Setting::updateOrCreate(['label' => 'Maximum TG tokens to withdraw', 'description' => "Maximum TG tokens to withdraw from wallet.", 'key_name' => 'max_token_withdraw', 'key_value' => '50000']);
        Setting::updateOrCreate(['label' => 'Minimum TG tokens to withdraw', 'description' => "Minimum TG tokens to withdraw from wallet.", 'key_name' => 'min_token_withdraw', 'key_value' => '500']);
        Setting::updateOrCreate(['label' => 'Maximum TG token to transfer', 'description' => "Maximum TG Tokens that can be transfer to another player form wallet.", 'key_name' => 'max_token_transfer', 'key_value' => '100']);
        Setting::updateOrCreate(['label' => 'Minimum TG token transfer', 'description' => "Minimum TG Tokens that can be transfer to another player form wallet.", 'key_name' => 'min_token_transfer', 'key_value' => '10']);
        Setting::updateOrCreate(['label' => 'Tagg fee on add token into wallet', 'description' => "Tagg fee when player add tokens in wallet in percentage.", 'key_name' => 'tagg_add_token_fee', 'key_value' => '1']);
        Setting::updateOrCreate(['label' => 'Tagg fee on withdraw token from wallet', 'description' => "Tagg fee when player withdraw tokens from wallet in percentage.", 'key_name' => 'tagg_withdraw_token_fee', 'key_value' => '3']);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Platform;

class PlatformSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Platform::updateOrCreate(['name' => 'XBOX']);
        Platform::updateOrCreate(['name' => 'PSN']);
        Platform::updateOrCreate(['name' => 'PC']);
        Platform::updateOrCreate(['name' => 'Cross Platform']);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ReportRequest;

class ReportRequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ReportRequest::updateOrCreate(['name' => 'Cheating']);
        ReportRequest::updateOrCreate(['name' => 'Hacking']);
        ReportRequest::updateOrCreate(['name' => 'Bug']);
        ReportRequest::updateOrCreate(['name' => 'Player']);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\SkillLevel;

class SkillLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        SkillLevel::updateOrCreate(['name' => 'Diamond']);
        SkillLevel::updateOrCreate(['name' => 'Gold']);
        SkillLevel::updateOrCreate(['name' => 'Silver']);
        SkillLevel::updateOrCreate(['name' => 'Bronze']);
        SkillLevel::updateOrCreate(['name' => 'Platinum']);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $role=Role::all();
        if(count($role)>1){
            $findSpecialUser=Role::find(2);
            $findSpecialUser->name='affiliate';
            $findSpecialUser->save();
            $findUser=Role::find(3);
            $findUser->name='gamer';
            $findUser->save();
        }
        else{
            $role1 = Role::updateOrCreate(['name' => 'admin']);
            $role3 = Role::updateOrCreate(['name' => 'affiliate']);
            $role2 = Role::updateOrCreate(['name' => 'gamer']);
            $permissions = [
            'ResetPassword',
            'All',
            'RoleEdit',
            'Games',
            'Tournament',
            'LeaderManagement',
            'OrderManagement',
            'ProductManagement',
            'GiveAwayManagement'
            ];

            foreach ($permissions as $permission) {
                Permission::create(['name' => $permission]);
            }
            $role3->givePermissionTo('ResetPassword');
            $role3->givePermissionTo('Games');
            $role1->givePermissionTo(['Games','Tournament','LeaderManagement','OrderManagement','ProductManagement']);
            $role1->givePermissionTo('All');
            $role1->givePermissionTo('RoleEdit');
        }
    }
}

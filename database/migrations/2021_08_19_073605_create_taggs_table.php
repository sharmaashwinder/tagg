<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaggsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('gamer_name')->nullable();
            $table->string('password')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('phone')->nullable();
            $table->string('gender')->nullable();
            $table->string('city')->nullable();
            $table->string('slug')->unique();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->dateTime('email_verified_at')->nullable();
            $table->text('profile_photo_path')->nullable();
            $table->text('bio')->nullable();
            $table->integer('status')->default(0);
            $table->string('remember_token')->nullable();
            $table->string('password_reset_token')->nullable();
            $table->string('provider')->nullable();
            $table->string('provider_id')->nullable();
            $table->string('verification_code')->nullable();
            $table->timestamps();
        });

        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->string('game_name');
            $table->text('image')->nullable();
            $table->longText('description')->nullable();
            $table->integer('total_tournaments')->nullable();
            $table->dateTime('expiration_date')->nullable();
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });

        Schema::create('notifications', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('type');
            $table->morphs('notifiable');
            $table->longText('data');
            $table->timestamp('read_at')->nullable();
            $table->timestamps();
        });

        Schema::create('tournaments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->bigInteger('prize_pool_id');
            $table->bigInteger('game_id');
            $table->bigInteger('platform_id')->nullable();
            $table->bigInteger('modifier_id')->nullable();
            $table->bigInteger('format_id');
            $table->string('name');
            $table->longText('descriptions')->nullable();
            $table->integer('player_numbers');
            $table->string('consol_tournament_id')->nullable();
            $table->string('consol_tournament_password');
            $table->integer('age_limit')->nullable();
            $table->string('entry_fee')->nullable();
            $table->date('start_date')->nullable();
            $table->time('start_time')->nullable();
            $table->date('end_date')->nullable();
            $table->time('end_time')->nullable();
            $table->longText('rules')->nullable();
            $table->enum('status', ['created', 'cancelled', 'finished', 'paid'])->default('created')->comment('status should be => created, cancelled, finished, paid');
            $table->timestamps();
        });

        Schema::create('tournament_purchases', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->bigInteger('tournament_id');
            $table->string('tournament_fee')->nullable();
            $table->enum('status', ['purchased', 'refunded'])->default('purchased')->comment('status should be => purchased, refunded');
            $table->timestamps();
        });

        Schema::create('tournament_funds', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->bigInteger('tournament_id');
            $table->string('admin_fee')->nullable();
            $table->string('admin_percentage')->nullable();
            $table->string('affiliate_fee')->nullable();
            $table->string('affiliate_percentage')->nullable();
            $table->string('amount')->nullable();
            $table->timestamps();
        });

        Schema::create('winner_player_logs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->bigInteger('platform_id');
            $table->bigInteger('game_id');
            $table->bigInteger('tournament_id');
            $table->string('platform_player_id')->nullable();
            $table->string('winning_amount')->nullable();
            $table->integer('position')->nullable();
            $table->enum('paid_status', ['paid', 'not_paid'])->default('not_paid')->comment('paid status should be => paid, not_paid');
            $table->timestamps();
        });

        Schema::create('user_social_profiles', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->boolean('profile_show')->default(0);
            $table->string('facebook_url')->nullable();
            $table->string('insta_url')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('linkedin_url')->nullable();
            $table->string('discord_url')->nullable();
            $table->string('twitch_url')->nullable();
            $table->timestamps();
        });

        Schema::create('player_earnings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->string('total_earning')->nullable();
            $table->integer('total_tournaments')->nullable();
            $table->string('most_played_game')->nullable();
            $table->timestamps();
        });

        Schema::create('admin_stats', function (Blueprint $table) {
            $table->id();
            $table->string('tournament_fee')->nullable();
            $table->string('add_funds_amount')->nullable();
            $table->string('withdraw_funds_amount')->nullable();
            $table->timestamps();
        });

        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('label')->nullable();
            $table->longText('description')->nullable();
            $table->string('key_name');
            $table->string('key_value')->nullable();
            $table->timestamps();
        });

        Schema::create('wallets', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->string('balance')->nullable();
            $table->timestamps();
        });

        Schema::create('follow_unfollows', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('following_id');
            $table->bigInteger('follower_id');
            $table->timestamps();
        });

        Schema::create('games_platforms', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('game_id');
            $table->bigInteger('platform_id');
            $table->timestamps();
        });

        Schema::create('skill_levels', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('game_platform_connections', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->string('provider_id');
            $table->string('provider_name');
            $table->string('username')->nullable();
            $table->longText('response_data');
            $table->timestamps();
        });

        Schema::create('paypal_transactions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->bigInteger('wallet_id')->nullable();
            $table->string('transaction_for')->nullable();
            $table->string('tg_token_rate')->nullable();
            $table->string('transaction_id')->nullable();
            $table->string('at_order_id')->nullable();
            $table->string('at_payer_name')->nullable();
            $table->string('at_payer_email')->nullable();
            $table->string('at_status')->nullable();
            $table->string('at_amount')->nullable();
            $table->string('at_paypal_fee')->nullable();
            $table->string('at_tagg_fee')->nullable();
            $table->string('at_final_amount')->nullable();
            $table->string('at_token_alloted')->nullable();
            $table->string('wt_payout_item_id')->nullable();
            $table->string('wt_sender_item_id')->nullable();
            $table->string('wt_payout_batch_id')->nullable();
            $table->string('wt_receiver')->nullable();
            $table->string('wt_status')->nullable();
            $table->string('wt_token_withdraw')->nullable();
            $table->string('wt_tagg_fee')->nullable();
            $table->string('wt_paypal_fee')->nullable();
            $table->string('wt_amount')->nullable();
            $table->longText('response_data')->nullable();
            $table->timestamps();
        });

        Schema::create('tagg_transactions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->bigInteger('wallet_id')->nullable();
            $table->bigInteger('paypal_transaction_id')->nullable();
            $table->string('transaction_for')->nullable();
            $table->string('add_amount')->nullable();
            $table->string('withdraw_amount')->nullable();
            $table->string('tagg_credit')->nullable();
            $table->string('tagg_fee')->nullable();
            $table->timestamps();
        });

        Schema::create('formats', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->timestamps();
        });

        Schema::create('modifiers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });

        Schema::create('platforms', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('icon_url')->nullable();
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });

        Schema::create('genres', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('icon_url')->nullable();
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });

        Schema::create('games_genres', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('game_id');
            $table->bigInteger('genres_id');
            $table->timestamps();
        });

        Schema::create('game_posts', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('game_id');
            $table->bigInteger('user_id');
            $table->longText('post_description')->nullable()->comment("Contact of user, like phone number, email, tagg user id etc according to the medium");
            $table->integer('status')->default(1);
            $table->integer('is_deleted')->default(0);
            $table->timestamps();
        });

        Schema::create('game_post_images', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('post_id');
            $table->text('image_url');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });
        Schema::create('game_post_videos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('post_id');
            $table->string('video_url');
            $table->integer('is_deleted')->default(0);
            $table->timestamps();
        });

        Schema::create('game_post_share', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('post_id');
            $table->bigInteger('user_id');
            $table->timestamps();
        });
        Schema::create('game_post_likes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('post_id');
            $table->bigInteger('user_id');
            $table->timestamps();
        });
        Schema::create('game_post_comments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('post_id');
            $table->bigInteger('user_id');
            $table->bigInteger('comment_id')->nullable();
            $table->text('comment')->nullable();
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });

        Schema::create('token_transfers', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('sender_id');
            $table->bigInteger('receiver_id');
            $table->string('amount')->nullable();
            $table->timestamps();
        });
        Schema::create('friend_requests', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('sender_id');
            $table->bigInteger('receiver_id');
            $table->enum('status', ['sent', 'accepted', 'declined'])->default('sent')->comment('Friend request status can be sent, accepted, declined, by default it will be sent');
            $table->timestamps();
        });

        Schema::create('groups', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('leader_id');
            $table->bigInteger('tournament_id');
            $table->string('name');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });

        Schema::create('group_requests', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('sender_id');
            $table->bigInteger('receiver_id');
            $table->enum('status', ['sent', 'accepted', 'declined'])->default('sent')->comment('Group request status can be sent, accepted, declined, by default it will be sent');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });
        Schema::create('group_members', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->bigInteger('group_id');
            $table->timestamps();
        });
        Schema::create('tag_friends', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->bigInteger('post_id');
            $table->bigInteger('tagger_id')->comment("The user who is tagging the other users.");
            $table->timestamps();
        });


        Schema::create('self_assessments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tournament_id');
            $table->bigInteger('game_id');
            $table->bigInteger('user_id');
            $table->enum('win_lose_status', ['win', 'lose'])->comment('win lose status selected by user');
            $table->integer('position')->nullable();
            $table->longText('description')->nullable();
            $table->boolean('verified')->default(0)->comment("Admin or affiliate mark the user assessment as verified");
            $table->timestamps();
        });

        Schema::create('self_assessment_images', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('self_assessment_id');
            $table->bigInteger('user_id');
            $table->text('image_url')->nullable();
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });

        Schema::create('report_requests', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment("Cheating, Hacking, Player etc.");
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });

        Schema::create('self_assessment_report_requests', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('self_assessment_id');
            $table->bigInteger('report_request_id');
            $table->string('player_id')->comment('The player who is cheating.');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });
        Schema::create('tournament_invites', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('sender_id');
            $table->enum('medium', ['facebook', 'twitter', 'tagg', 'sms', 'email', 'other'])->comment('medium of invite.');
            $table->string('contact')->comment("Contact of user, like phone number, email, tagg user id etc according to the medium");
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });

        Schema::create('contactus', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('phone')->nullable();
            $table->string('email');
            $table->string('comment');
            $table->timestamps();
        });
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('email');
            $table->timestamps();
        });

        Schema::create('prize_pools', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->string('first_price')->nullable();
            $table->string('second_price')->nullable();
            $table->string('third_price')->nullable();
            $table->string('fourth_price')->nullable();
            $table->integer('next_positions')->nullable();
            $table->string('next_position_price')->nullable();
            $table->timestamps();
        });

        Schema::create('tournament_skill_levels', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tournament_id');
            $table->bigInteger('skill_level_id');
            $table->timestamps();
        });

        Schema::create('user_interests', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->bigInteger('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
        Schema::dropIfExists('modifiers');
        Schema::dropIfExists('games');
        Schema::dropIfExists('notifications');
        Schema::dropIfExists('tournaments');
        Schema::dropIfExists('tournament_purchases');
        Schema::dropIfExists('tournament_funds');
        Schema::dropIfExists('winner_player_logs');
        Schema::dropIfExists('user_social_profiles');
        Schema::dropIfExists('player_earnings');
        Schema::dropIfExists('admin_stats');
        Schema::dropIfExists('settings');
        Schema::dropIfExists('wallets');
        Schema::dropIfExists('follow_unfollows');
        Schema::dropIfExists('games_platforms');
        Schema::dropIfExists('skill_levels');
        Schema::dropIfExists('game_platform_connections');
        Schema::dropIfExists('paypal_transactions');
        Schema::dropIfExists('tagg_transactions');
        Schema::dropIfExists('formats');
        Schema::dropIfExists('platforms');
        Schema::dropIfExists('genres');
        Schema::dropIfExists('games_genres');
        Schema::dropIfExists('game_posts');
        Schema::dropIfExists('game_post_images');
        Schema::dropIfExists('game_post_videos');
        Schema::dropIfExists('game_post_share');
        Schema::dropIfExists('game_post_likes');
        Schema::dropIfExists('game_post_comments');
        Schema::dropIfExists('token_transfers');
        Schema::dropIfExists('friend_requests');
        Schema::dropIfExists('groups');
        Schema::dropIfExists('group_requests');
        Schema::dropIfExists('group_members');
        Schema::dropIfExists('tag_friends');
        Schema::dropIfExists('self_assessments');
        Schema::dropIfExists('self_assessment_images');
        Schema::dropIfExists('report_requests');
        Schema::dropIfExists('self_assessment_report_requests');
        Schema::dropIfExists('tournament_invites');
        Schema::dropIfExists('contactus');
        Schema::dropIfExists('subscriptions');
        Schema::dropIfExists('prize_pools');
        Schema::dropIfExists('tournament_skill_levels');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsPaypalTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paypal_transactions', function (Blueprint $table) {
            $table->string('t_token_transfer_id')->nullable()->after('wt_amount');
            $table->string('t_receiver_id')->nullable()->after('t_token_transfer_id');
            $table->string('t_amount')->nullable()->after('t_receiver_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paypal_transactions', function (Blueprint $table) {
            $table->dropColumn('t_token_transfer_id');
            $table->dropColumn('t_receiver_id');
            $table->dropColumn('t_amount');
        });
    }
}

<?php

function pr($data){
    echo "<pre>";
    print_r($data);
    die();
}

function prr($data){
    echo "<pre>";
    print_r($data);
}

function sendSuccessResponse($alertType, $message) {
    $notification = array(
        'message'   => $message,
        'alert-type'=> $alertType
    );
    return $notification;
}
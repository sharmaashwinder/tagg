<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FriendAcceptence extends Model
{
    use HasFactory;

    protected $fillable = [
        'requester_id',
        'sender_id',
        'accepted',

    ];
}

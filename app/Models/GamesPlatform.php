<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GamesPlatform extends Model
{
    use HasFactory;

    protected $fillable = [
        'game_id',
        'platform_id'
    ];

    public function PlatformName()
    {
        return $this->belongsTo(Platform::class, 'platform_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrizePool extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',    
        'first_price',      
        'second_price',
        'third_price',
        'next_positions	',
        'next_position_price'
       
    ];
    
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GamesGenre extends Model
{
    use HasFactory;

    protected $fillable = [
        'game_id',
        'genres_id'
    ];

    public function GenreName()
    {
        return $this->belongsTo(Genre::class, 'genres_id',);
    }
}

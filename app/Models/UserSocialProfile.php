<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSocialProfile extends Model
{
    use HasFactory;

    protected $fillable = [

        'user_id',
        'profile_show',
        'facebook_url',
        'insta_url',
        'twitter_url',
        'linkedin_url',
       'discord_url',

    ];

}

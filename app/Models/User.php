<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Contracts\Auth\CanResetPassword;
use  App\Traits\friendable;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use HasFactory, Notifiable,HasRoles,friendable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'gamer_name',
        'email',
        'password',
        'profile_photo_path',
        'provider',
        'provider_id',
        'date_of_birth',
        'country'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function comments()
    {
        return $this->hasMany(GameConnection::class);
    }

   public function slug_creater(){
         $store_string=Str::random(10);
         if ($this->slugExists($store_string)) {
            return slug_creater();
        }

        // otherwise, it's valid and can be used
        return $store_string;
   }
   function slugExists($number) {
    // query the database and return a boolean
    // for instance, it might look like this in Laravel
    return User::where('slug',$number)->exists();
}

public function getRouteKeyName()
{
    return 'slug';
}
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    use HasFactory;

    protected $fillable = [
    'name',
    'status',
    'skill_level',
    'modifier_id',
    'entry_fee',
    'start_time',
    'end_time',
    'game_id',
    'user_id',
    'start_date',
    'end_date',
    'player_numbers',
    'description',
    'prize_pool_id',
    'formate_id',
    'console_tournament_id',
    'console_tournament_password',
    'age_limit',
    'rules',
    'host_name',
    'speciality'
    ];


}

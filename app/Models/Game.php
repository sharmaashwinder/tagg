<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use HasFactory;

    protected $fillable = [
        'game_name',
        'description',
        'total_tournaments',
        'expiration_date',
        'image'
    ];

    public function GamesPlatform()
    {
        return $this->hasMany(GamesPlatform::class);
    }

    public function GamesGenre()
    {
        return $this->hasMany(GamesGenre::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaggTransaction extends Model {
    use HasFactory;

    public function TxnUser() {
        return $this->belongsTo(User::class, 'user_id');//->select(['id', 'email']);
    }
    public function Transaction() {
        return $this->belongsTo(PaypalTransaction::class, 'paypal_transaction_id');//->select(['id', 'email']);
    }
}

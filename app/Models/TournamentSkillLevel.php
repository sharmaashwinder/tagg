<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TournamentSkillLevel extends Model
{
    use HasFactory;

    protected $fillable = [
        'tournament_id',
        'skill_level_id'
    ];


}
<?php
namespace App\Traits;
use Auth;
use DB;
use Exception;
use App\Models\User;
use App\Models\GamePlatformConnection;

trait connectGameTrait{

    public function connectGamePlatform($userId, $data){
        $gameConnection = 0;
        if($data['provider_name'] == 'xbox'){
            $gameConnection = new GamePlatformConnection;
            $gameConnection->user_id = $userId;
            $gameConnection->provider_id = $data['profile']['profileUsers']['0']['id'];
            $gameConnection->provider_name = 'xbox';
            $gameConnection->username = $data['profile']['profileUsers']['0']['settings']['0']['value'];
            $gameConnection->response_data = json_encode($data);
            $gameConnection->save();
        }elseif($data['provider_name'] == 'epic'){
            $gameConnection = new GamePlatformConnection;
            $gameConnection->user_id = $userId;
            $gameConnection->provider_id = $data['profile']['sub'];
            $gameConnection->provider_name = 'epic';
            $gameConnection->username = $data['profile']['preferred_username'];
            $gameConnection->response_data = json_encode($data);
            $gameConnection->save();
        }elseif($data['provider_name'] == 'twitch'){
            $gameConnection = new GamePlatformConnection;
            $gameConnection->user_id = $userId;
            $gameConnection->provider_id = $data['profile']['id'];
            $gameConnection->provider_name = 'twitch';
            $gameConnection->username = $data['profile']['nickname'];
            $gameConnection->response_data = json_encode($data);
            $gameConnection->save();
        }
        return $gameConnection;
    }

    public function updateConnectGamePlatform($userId, $data){
        $gameConnection =0;
        if($data['provider_name'] == 'xbox'){
            $gameConnection = GamePlatformConnection::where('provider_id', $data['profile']['profileUsers']['0']['id'])->first();
            $gameConnection->user_id = $userId;
            $gameConnection->provider_id = $data['profile']['profileUsers']['0']['id'];
            $gameConnection->provider_name = 'xbox';
            $gameConnection->username = $data['profile']['profileUsers']['0']['settings']['0']['value'];
            $gameConnection->response_data = json_encode($data);
            $gameConnection->save();
        }else if($data['provider_name'] == 'epic'){
            $gameConnection = GamePlatformConnection::where('provider_id', $data['profile']['sub'])->first();
            $gameConnection->user_id = $userId;
            $gameConnection->provider_id = $data['profile']['sub'];
            $gameConnection->provider_name = 'epic';
            $gameConnection->username = $data['profile']['preferred_username'];
            $gameConnection->response_data = json_encode($data);
            $gameConnection->save();
        }else if($data['provider_name'] == 'twitch'){
            $gameConnection = GamePlatformConnection::where('provider_id', $data['profile']['id'])->first();
            $gameConnection->user_id = $userId;
            $gameConnection->provider_id = $data['profile']['id'];
            $gameConnection->provider_name = 'twitch';
            $gameConnection->username = $data['profile']['nickname'];
            $gameConnection->response_data = json_encode($data);
            $gameConnection->save();
        }
        return $gameConnection;
    }
}

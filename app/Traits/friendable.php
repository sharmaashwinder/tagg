<?php
namespace App\Traits;
use Auth;
use DB;
use App\Models\FriendAcceptence;
use App\Models\FollowUnfollow;
use Exception;
use App\Models\User;
use App\Notifications\Followrequest;


trait friendable{

  public function isfriend($requesterid=null,$recieverid=null)
  {
      $friend=FriendAcceptence::where('requester_id',$requesterid)->where('sender_id',$recieverid)->where('accepted',1)->get();

      if(!$friend->isEmpty()){
          return true;
      }
    return false;
  }

  public function unfriend($requesterid=null,$recieverid=null)
  {
      $friend=FriendAcceptence::where('requester_id',$requesterid)->where('sender_id',$recieverid)->where('accepted',1)->get();
    //   dd();
      $record=FriendAcceptence::find($friend[0]->id);
      $record->delete();
      return true;
  }

  public function isfollow($requesterid=null,$recieverid=null){
      $check=FollowUnfollow::where('following_id',$requesterid)->where('follower_id',$recieverid)->first();
    //   dd($check)
      if($check!=null)
      return true;
        return false;
  }
  public function follow($requesterid=null,$recieverid=null){
    $check=FollowUnfollow::create([
        'following_id'=>$requesterid,
        'follower_id'=>$recieverid
    ]);
    // dd($check);
    if($check!=null)
    return true;
      return false;
}

public function unfollow($requesterid=null,$recieverid=null){
    $check=FollowUnfollow::where('following_id',$requesterid)->where('follower_id',$recieverid);
    $check=$check->delete();
    // dd($check);
    if($check!=null)
    return true;
      return false;
}
}

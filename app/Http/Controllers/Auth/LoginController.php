<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\MailController;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    protected function redirectTo(){
        if( Auth()->user()->hasRole('admin')){
            return route('admin.dashboard');
        }

    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(){
        return view('auth.login');
    }

    public function login(Request $request){

        $input = $request->all();
        $validate=  $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required'
        ]);
        $user = User::where('email' , $input['email'])->first();
        // if(!empty($user->email_verified_at)){
            if (empty($user)) {
                return  back()->withErrors([__("messages.wrongEmailPass")])->withInput();
            }


             if($user->status == 1){
                return  back()->withErrors([__("your account is Suspended please contact to admin")]);
             }else{
                if( auth()->attempt(array('email'=>$input['email'], 'password'=>$input['password'])) ){
                    if( auth()->user()->hasRole('admin') ){
                        auth()->logout();
                        return back()->withErrors([__("messages.wrongEmailPass")])->withInput();
                    }
                    else if( auth()->user()->hasRole('gamer') || !auth()->user()->hasRole('gamer') )
                    {
                        if(!empty($user->email_verified_at))
                        {
                            return redirect()->intended('home')->with(sendSuccessResponse('success', __("messages.loginSuccessful")));
                        }
                        else
                        {
                            auth()->logout();
                            MailController::sendsignUpmail($user->name, $user->email, $user->verification_code);
                            return redirect()->route('verify_mail')->with(['data'=>$user->email, 'success'=>__("messages.notVerifyEmail")]);
                         }

                    }
                } else{
                    return  back()->withErrors([__("messages.wrongEmailPass")])->withInput();
                }

             }

    }
public function display_loginpage(){

    return view('auth.admin-login');
}
    public function admin_login(Request $request){

        $input = $request->all();
        $validate =  $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required'
        ]);

         $user=User::where('email',$request->email)->first();
         if($user){
            $is_admin=DB::table('model_has_roles')->where('model_id',$user->id)->select('role_id')->first();
            $roleid=DB::table('roles')->where('name','admin')->select('id')->first();
            if($is_admin->role_id==$roleid->id){

                if( auth()->attempt(array('email'=>$input['email'], 'password'=>$input['password']))){
                    return redirect()->route('admin.dashboard');
                }
                else{
                    return back()->withErrors([__("messages.wrongEmailPass")])->withInput();
                }
            }
            else{
                return back()->withErrors([__("messages.wrongEmailPass")])->withInput();
            }
         }
         else{
            return  back()->withErrors([__("messages.wrongEmailPass")])->withInput();
         }




    }
}

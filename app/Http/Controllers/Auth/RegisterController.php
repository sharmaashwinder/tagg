<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\SocialController;
use App\Traits\connectGameTrait;
use App\Http\Controllers\MailController;
use App\Jobs\SendWelcomeEmail;
use Illuminate\Http\Request;
use App\Models\UserSocialProfile;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers, connectGameTrait;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:2', 'confirmed'],
        ]);
    }

//     public function messages()
// {
//     return [
//         'title.required' => 'A title is required',
//         'body.required' => 'A message is required',
//     ];
// }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    // protected function create(array $data)
    // {
    //     $user= User::create([
    //         'name' => $data['name'],
    //         'email' => $data['email'],
    //         'password' => Hash::make($data['password']),
    //     ]);
    //     $user->assignRole('user');
    //     $details = [
    //         'title' => 'Mail from Tagg us',
    //         'body' => 'Tagg us Subscription',
    //         'email'=>$data['email'],
    //         'name' => $data['name'],
    //     ];
    //     $datas = $data['email'];
    //     if(!empty($data['gamingPlatform'])){
    //         $this->connectGamePlatform($user->id, json_decode($data['gamingPlatform'], true));
    //     }
    //     \Mail::to($datas)->send(new \App\Mail\RegisterMail($details));

    //     // $notification = array(
    //     //     'message' => 'Signup Successfully!',
    //     //     'alert-type' => 'success'
    //     // );
    //     // return redirect()->route('home')->with($notification);
    //     return $user;
    // }
      public  function register(Request $req){
        $req->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:2', 'confirmed'],
        ]);
      $user=new User();
      $user->status = 0;
      $user->name=$req['name'];
      $user->gamer_name = $req['gamer_name'];
      $user->email=$req['email'];
      $user->password=Hash::make($req['password']);
      $user->verification_code=sha1(time());
      $user->slug=$user->slug_creater();
      $user->save();
      $user->assignRole('gamer');
      $profile=new UserSocialProfile;
      $profile->user_id=$user->id;
      $profile->save();

                    if($user!=null){
                        if(!empty($req['gamingPlatform'])){
                            $this->connectGamePlatform($user->id, json_decode($req['gamingPlatform'], true));
                        }
                        MailController::sendsignUpmail($user->name,$user->email,$user->verification_code);
                        return redirect()->route('verify_mail')->with(['data'=>$req['email'],'success'=>'Mail has been successfully sent. Please check email for verification']);


                    }
                    return redirect()->back()->with('message','something went wrong');
                    // else show errro message
   }

   public function verifyuser(Request $req){
          $verification_code=$req['code'];
          $user=User::where(['verification_code' => $verification_code])->first();
          if($user!=null){

              $user->email_verified_at = date('Y-m-d H:m:s');
              $user->save();
              $details = [
                        'title' => 'Mail from Tagg us',
                        'body' => 'Tagg us Registration',
                        'email'=>$user['email'],
                        'name' => $user['name'],
                    ];
                    dispatch(new SendWelcomeEmail($user,$details));
              return redirect()->route('login')->with('success',' Your account is verified. Please login.');
          }
          return redirect()->back()->with('message','the link has expired');
   }
   public function resendverifyuser(Request $req){

    $user=User::where(['email' => $req['email']])->first();

    $user->verification_code=sha1(time());
    $user->save();
    if($user!=null){
    MailController::sendsignUpmail($user->name,$user->email,$user->verification_code);
    return redirect()->route('verify_mail')->with(['data'=>$user->email,'success'=>'Mail has been successfully sent. Please check email for verification link']);
    }


   }
}

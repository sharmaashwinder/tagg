<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;
use Session;

class SettingController extends Controller
{
    public function admin_setting(Request $request) {
        if ($request->isMethod('post')) {
            $postData = $request->all();
            unset($postData['_token']);
            $setting = new Setting;
            foreach ($postData as $key => $value) {
                $setting = Setting::where('key_name', $key)->first();
                $setting->key_value = $value;
                $setting->save();
            }
        }
        $settings = Setting::all();
        return view('dashboards.admins.setting', compact('settings'));
    }

  
            public function theme_change(Request $request)
            {
                if ($request->ajax())  
                { 

                   $mode = $request->mode;

                   if($mode == 'change_theme'){
                  
                    Session::put('theme_mode', 'light');
                    Session::put('botton_mode', 'change_theme');
                    $mode =  Session::get('theme_mode');
                    $bottonmode = Session::get('botton_mode');
                    $response = array('status' => 'sucess', 'theme_mode' => $mode ,'botton_mode' =>$bottonmode);                
                     return response()->json($response, 200);     
                   }else if ($mode == '') {
                  
                    Session::put('theme_mode', 'dark');
                    Session::put('botton_mode', '');
                    $mode = Session::get('theme_mode');
                    $bottonmode = Session::get('botton_mode');
                  
                    $response = array('status' => 'sucess', 'theme_mode' => $mode ,'botton_mode' =>$bottonmode);
                  
                    return response()->json($response, 200);
                } else {
                   
                    $response = array('status' => 'error');
                    return response()->json($response, 200);
                }
                }  
            }



}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Redirect;
use DataTables;
use App\Models\PrizePool;
use Illuminate\Support\Str;
use App\Models\Tournament;



class PrizePollController extends Controller
{
    public function prizeindex(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::table('prize_pools');

            return DataTables::of($data)
                ->filterColumn('first_price', function ($query, $keyword) use ($request) {
                    $query->orWhere('prize_pools.first_price', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('prize_pools.second_price', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('prize_pools.third_price', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('prize_pools.next_position_price', 'LIKE', '%' . $keyword . '%');
                })

                ->addIndexColumn()

                ->addColumn('first_price', function ($data) {
                    if($data->first_price){
                     $first_price = '$'.$data->first_price;
                    }else
                    {
                     $first_price = "N/A";  
                    }
                    return $first_price;
 
                 })

                ->addColumn('next_position_price', function ($data) {
                    $next_positions = "$data->next_positions";
                    $next_position_price = "$data->next_position_price";
                    $alldata = "$next_positions*$$next_position_price";
                    return $alldata;
                })

                ->addColumn('second_price', function ($data) {
                   if($data->second_price){
                    $second_price = '$'.$data->second_price;
                   }else
                   {
                    $second_price = "N/A";  
                   }
                   return $second_price;

                })

              

                ->addColumn('third_price', function ($data) {
                    if($data->third_price){
                     $third_price = '$'.$data->third_price;
                    }else
                    {
                     $third_price = "N/A";  
                    }
                    return $third_price;
 
                 })


                ->addColumn('action', function ($data) {
                    $actions =
                        '<a class="btn btn-sm btn-success mr-2" href="' . route('edit.prize', $data->id) . '">
                <i class="fas fa-pencil-alt"></i>
                </a>';

                    $actions =
                        $actions =    $actions . '<a class="btn btn-sm btn-danger" onclick="return myFunction1();" href="' . route('delete.prize', $data->id) . '"><i class="fa fa-trash"></i></a>';
                    return $actions;
                })
                ->rawColumns(['action','c'])
                ->make(true);
        }


        return view('dashboards.admins.prize');
    }

    public function create_prize(Request $request)
    { 
        return view('dashboards.admins.create_prize');
    }


    public function store(Request $request)
    {  
      
        $user = auth()->user();
        $request->validate([

            'first_price'           => 'required',
        ]);
        
        $prize = new PrizePool($request->input());
        $prize->user_id  = $user->id;
        $prize->first_price  = $request->first_price;
        $prize->second_price  = $request->second_price;
        $prize->third_price  = $request->third_price;
        $prize->next_positions  = $request->next_positions;
        $prize->next_position_price  = $request->next_position_price;

        
       if(Str::contains($request->url, ['create-tournament'])){  
        $prize->save();
        $notification = array(
            'message' => 'Prize Added Successfully!',
            'alert-type' => 'success'
        );
        return redirect()->route('create.tournament')->with($notification);
       }else{
        $prize->save();
        $notification = array(
            'message' => 'Prize Added Successfully!',
            'alert-type' => 'success'
        );
        return redirect()->route('prize.index')->with($notification);
       }
        
    }

    public function prizeedit($id)
    {
        $data = PrizePool::where('id', $id)
            ->first();


        return view('dashboards.admins.prize_edit', compact('data'));
    }

    public function updateprize($id, Request $request)
    {
        
        $prize = PrizePool::findOrFail($id);
        $prize->first_price  = $request->first_price;
        $prize->second_price  = $request->second_price;
        $prize->third_price  = $request->third_price;
        $prize->next_positions  = $request->next_positions;
        $prize->next_position_price  = $request->next_position_price;

        if(Str::contains($request->url, ['tournament-edit'])){  
            $prize->save();
            $notification = array(
                'message' => 'Prize Added Successfully!',
                'alert-type' => 'success'
            );
            return redirect($request->url)->with($notification);
           }else{
            $prize->save();
            $notification = array(
                'message' => 'Prize Updated Successfully!',
                'alert-type' => 'success'
            );
            return redirect()->route('prize.index')->with($notification);
           }
            
           
      
    
        
    }

    public function destroy($id)
    {
        $data = PrizePool::where('id', $id)->delete();
        $notification = array(
            'message' => 'Prize deleted successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Game;
use App\Models\Genre;
use App\Models\Platform;
use App\Models\GamesGenre;
use App\Models\GamesPlatform;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Redirect;
use DataTables;
use Illuminate\Support\Facades\Storage;


class GameController extends Controller
{
  public function create_game(Request $request)
  {

    if ($request->ajax()) {
        $data = Game::where('is_deleted', 0)->with("GamesPlatform.PlatformName", "GamesGenre.GenreName");
  
      return DataTables::of($data)
        ->addIndexColumn()

        ->addColumn('action', function ($data) {
          $actions =
            '<a class="btn btn-sm btn-success mr-2" href="' . route('edit.games', $data->id) . '">
                    <i class="fas fa-pencil-alt"></i>
                    </a>';

          $actions =
            $actions . '<a class="btn btn-sm btn-success mr-2" href="' . route('view.games', $data->id) . '">
                    <i class="fa fa-eye"></i>
                    </a>';

          $actions =    $actions . '<a class="btn btn-sm btn-danger" onclick="return myFunction1();" href="' . route('destroy.games', $data->id) . '"><i class="fa fa-trash"></i></a>';
          return $actions;
        })
        ->rawColumns(['action'])
        ->make(true);
    }



    return view('dashboards.admins.game');
  }

  public function add_game()
  {
    $Genre = Genre::orderBy('name', 'asc')->get();
   
    $Platform = Platform::orderBy('name', 'asc')->get();

    return view('dashboards.admins.create_game', compact('Genre', 'Platform'));
  }


  public function store(Request $request)
  {


    $request->validate([
      'game_name'           => 'required',
      'platform_id'           => 'required',
      'genres_id'           => 'required',

      'image' => 'required'

    ]);

    $game_img = $request->file('image')->store('images', 'public');

    $game = new Game;
    $game->game_name  = $request->game_name;
    $game->description  = $request->description;
    $game->image        = $game_img;

    $game->save();
    $GameId = Game::where('id', $game->id)->first();


    $game = new GamesPlatform;
    $game->game_id        = $GameId->id;
    $game->platform_id        = $request->platform_id;
    $game->save();

    $game = new GamesGenre;
    $game->game_id        = $GameId->id;
    $game->genres_id        = $request->genres_id;
    $game->save();


    $notification = array(
      'message' => 'Game Added Successfully!',
      'alert-type' => 'success'
    );
    return redirect()->route('create.game')->with($notification);
  }


  public function gameview($id)
  {
    // $data = Game::where('id', $id)
    //   ->first();

      $data =  Game::where('id', $id)->where('is_deleted', 0)->with("GamesPlatform.PlatformName", "GamesGenre.GenreName")->first();
      pr($data);
    return view('dashboards.admins.game_view', compact('data'));
  }


  public function gameedit($id)
  {
    $data = Game::where('id', $id)
      ->first();

    $Genres = Genre::orderBy('name', 'asc')->get();
    $Platform = Genre::orderBy('name', 'asc')->get();



    return view('dashboards.admins.edit_game', compact('data', 'Genres', 'Platform'));
  }

  public function updategame($id, Request $request)
  {

    $patch = $request->file('image')->store('images', 'public');

    $game = Game::findOrFail($id);
    $game->game_name  = $request->game_name;
    $game->description  = $request->description;
    $game->image        = $patch;
    $game->save();
    $GameId = Game::where('id', $game->id)->first();


    $GamesPlatform = GamesPlatform::findOrFail($id);
    $GamesPlatform->game_id        = $GameId->id;
    $GamesPlatform->platform_id        = $request->platform_id;
    $GamesPlatform->save();

    $GamesGenre = GamesGenre::findOrFail($id);
    $GamesGenre->game_id        = $GameId->id;
    $GamesGenre->genres_id        = $request->genres_id;
    $GamesGenre->save();


    $notification = array(
      'message' => 'Game Updated Successfully!',
      'alert-type' => 'success'
    );
    return redirect()->route('create.game')->with($notification);
  }



  public function destroy($id)
  {
    $game = Game::findOrFail($id);
    $game->is_deleted = 1;
    $game->save();

    $GamesPlatform = GamesPlatform::findOrFail($id);
    $GamesPlatform->is_deleted = 1;
    $GamesPlatform->save();

    $GamesGenre = GamesGenre::findOrFail($id);
    $GamesGenre->is_deleted = 1;
    $GamesGenre->save();




    $notification = array(
      'message' => 'Game Deleted Successfully!',
      'alert-type' => 'success'
    );
    return redirect()->back()->with($notification);
  }
}

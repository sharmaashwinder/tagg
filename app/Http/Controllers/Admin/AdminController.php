<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\Game;
use App\Models\Tournament;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use DataTables;
use App\Mail\ActiveMail;
use App\Mail\DeactiveMail;
use Illuminate\Support\Facades\Mail;


class AdminController extends Controller
{
    function index()
    {
        $all_games = Game::all()->count();
        $all_tournaments = Tournament::all()->count();
        $all_players = User::where('name', '!=', 'admin')->count();

        return view('dashboards.admins.index', compact('all_games', 'all_tournaments', 'all_players'));
    }

    function profile()
    {
        return view('dashboards.admins.profile');
    }
    /*  */
    function settings()
    {

        return view('dashboards.admins.settings');
    }

    function player(Request $request, Role $role)
    {

        $user = Auth::user()->id;


        if ($request->ajax()) {
            $data = User::where('id', '!=', $user);


            return DataTables::of($data)
                // ->filterColumn('name', function($query, $keyword) use ($request) {
                //     $query->orWhere('users.name', 'LIKE', '%' . $keyword . '%')
                //     ->orWhere('users.gamer_name', 'LIKE', '%' . $keyword . '%')
                //     ->orWhere('users.email', 'LIKE', '%' . $keyword . '%')

                //    ;
                // })
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    $actions =
                        '<a class="btn btn-sm btn-success mr-2" href="' . route('player.view', $data->id) . '">
                    <i class="fa fa-eye"></i>
                    </a>';

                    $actions = $actions . '<a class="btn btn-sm btn-success mr-2" href="' . route('player.edit', $data->id) . '">
                    <i class="far fa-edit"></i>
                    </a>';
                    if ($data->status == '0') {
                        $actions =    $actions . '<a class="btn btn-sm btn-success"  href="' . route('player.destroy', $data->id) . '">Reactivate</a>';
                    } else {
                        $actions =    $actions . '<a class="btn btn-sm btn-success"  href="' . route('player.destroy', $data->id) . '">Suspended</a>';
                    }

                    return $actions;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        $permissions = DB::table('permissions')->whereIn('name', [
            'Games', 'Tournament', 'LeaderManagement', 'Ordermanagement', 'ProductManagement', 'GiveAwaymanagement'
        ])->get()->toArray();
        $admin = DB::table('role_has_permissions')->where('role_id', 1)->select('permission_id')->get()->toArray();
        $adminn = array();
        $special = array();
        // foreach ($admin as $key => $value) {
        //     array_push($adminn,$value->permission_id);
        // }
        $special_user = DB::table('role_has_permissions')->where('role_id', 2)->get()->toArray();
        foreach ($special_user as $key => $value) {
            array_push($special, $value->permission_id);
        }


        $data = array($permissions, $adminn, $special);



        return view('dashboards.admins.players', compact('data'));
    }

    function affiliates(Request $request)
    {

        $user = Auth::user()->id;


        if ($request->ajax()) {

            $data = User::where('id', '!=', $user)->leftjoin('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')->where('model_has_roles.role_id', '=', '2')
                // ->leftJoin('earnings', 'users.id', '=', 'earnings.user_id')
            ;
            return DataTables::of($data)
                ->filterColumn('name', function ($query, $keyword) use ($request) {
                    $query->orWhere('users.name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('users.gamer_name', 'LIKE', '%' . $keyword . '%')
                        // ->orWhere('earnings.rank', 'LIKE', '%' . $keyword . '%')
                        // ->orWhere('earnings.balance', 'LIKE', '%' . $keyword . '%')
                    ;
                })
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    $actions =
                        '<a class="btn btn-sm btn-success" href="' . route('player.view', $data->id) . '">
                    <i class="fa fa-eye"></i>
                    </a>';

                    $actions =    $actions . '<a class="btn btn-sm btn-danger" onclick="return myFunction();" href="' . route('player.destroy', $data->id) . '"><i class="fa fa-trash"></i></a>';
                    return $actions;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }


    public function playerview($id)
    {
        $data = User::where('id', $id)
            ->first();


        return view('dashboards.admins.player-view', compact('data'));
    }
    function playeredit($id, Request $req)
    {
        $data = User::where('id', $id)
            ->first();
        $user = User::find($id);
        // dd($user);
        $data2 = $user->getRoleNames()->toArray();
        //  dd($data2);
        $permissions = DB::table('permissions')->whereIn('name', [
            'Games', 'Tournament', 'LeaderManagement', 'Ordermanagement', 'ProductManagement', 'GiveAwaymanagement'
        ])->get()->toArray();
        $role=Role::where('name','!=','admin')->get();
        // $role_check=$user->role();
        // dd($role);
        $data = array($data, $permissions, $id, $data2,$role);




        return view('dashboards.admins.editplayer-view', compact('data'));
    }

    function playereditrolesave(Request $req){
// dd($req);
$user_id = $req->id;
$data = $req->toArray();
$roles_get = array();
$user = User::find($user_id);

foreach ($data as $key => $value) {
    if ($key != '_token' && $key != 'id') {
        $name = Role::where('id', $key)->select('name')->first();
        array_push($roles_get, $name->name);
    }
}
$user->syncRoles($roles_get);
// dd($roles_get);
$notification = array(
    'message' => 'Player Role edited successfully',
    'alert-type' => 'success'
);
return redirect()->back()->with($notification);
    }


    function playereditsave(Request $req)
    {
        $per = Permission::all()->toArray();
        $permission = [
            'Games', 'Tournament', 'LeaderManagement', 'Ordermanagement', 'ProductManagement', 'GiveAwaymanagement'
        ];
        $user_id = $req->id;
        // dd($user_id);
        $data = $req->toArray();
        $user = User::find($user_id);
        // dd($user);
        $permission_get = array();
        foreach ($data as $key => $value) {
            if ($key != '_token' && $key != 'id') {
                $name = Permission::where('id', $key)->select('name')->first();
                array_push($permission_get, $name->name);
            }
        }
        // dd($permission_get);
        $user->syncPermissions($permission_get);
        $notification = array(
            'message' => 'Player permissions edited successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }



    public function updateuser($id, Request $request)
    {
        $user = User::findOrFail($id);

        $user->name = $request->name;
        // $user->email = $request->email;
        $user->gamer_name = $request->gamer_name;
        $user->save();
        $notification = array(
            'message' => 'User Updated successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }



    public function destroy($id, Request $request)
    {

        $data = User::findOrFail($id);
        if ($data->status == '0') {
            $data->status = 1;
        } else if ($data->status == '1') {
            $data->status = 0;
        }
        $data->save();

        $datas = $data->email;
        if ($data->status == '0') {
            $details = [
                'title' => 'your account is activate',
                'body' => 'Tagg us'
            ];

            Mail::to($datas)->send(new \App\Mail\ActiveMail($details));
        } else if ($data->status == '1') {
            $details = [
                'title' => 'your account is deactive please contact to admin',
                'body' => 'Tagg us'
            ];
            Mail::to($datas)->send(new \App\Mail\DeactiveMail($details));
        }
        $notification = array(
            'message' => 'Player deleted successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }




    function permission(Role $role)
    {
        $permissions = DB::table('permissions')->whereIn('name', [
            'Games', 'Tournament', 'LeaderManagement', 'Ordermanagement', 'ProductManagement', 'GiveAwaymanagement'
        ])->get()->toArray();
        $admin = DB::table('role_has_permissions')->where('role_id', 1)->select('permission_id')->get()->toArray();
        $adminn = array();
        $special = array();
        foreach ($admin as $key => $value) {
            array_push($adminn, $value->permission_id);
        }
        $special_user = DB::table('role_has_permissions')->where('role_id', 2)->get()->toArray();
        foreach ($special_user as $key => $value) {
            array_push($special, $value->permission_id);
        }


        $data = array($permissions, $adminn, $special);

        return view('dashboards.admins.permissions', ['data' => $data]);
    }
    function permission_save(Request $req, Role $role)
    {

        $req = $req->toArray();
        $permissions = DB::table('permissions')->whereIn('name', [
            'Games', 'Tournament', 'LeaderManagement', 'Ordermanagement', 'ProductManagement', 'GiveAwaymanagement'
        ])->get()->toArray();

        array_shift($req);
        $special_permission_accept = array();

        foreach ($req as $key => $value) {
            $a = explode("_", $key);

            if (strpos($a[0], 'special') !== false) {

                array_push($special_permission_accept, $a[1]);
            }
        }
        $role = Role::findByName('admin');
        $spec = Role::findByName('affiliate');
        foreach ($permissions as $key => $value) {


            if (in_array($value->id, $special_permission_accept)) {
                $spec->givePermissionTo($value->name);
            } else {
                $spec->revokePermissionTo($value->name);
            }
        }

        return Redirect::back()->with('message', 'Permissions Changed Successfully');
    }
    function updateInfo(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . Auth::user()->id,
            'favoritecolor' => 'required',
        ]);

        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'error' => $validator->errors()->toArray()]);
        } else {
            $query = User::find(Auth::user()->id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'favoriteColor' => $request->favoritecolor,
            ]);

            if (!$query) {
                return response()->json(['status' => 0, 'msg' => 'Something went wrong.']);
            } else {
                return response()->json(['status' => 1, 'msg' => 'Your profile info has been update successfuly.']);
            }
        }
    }

    function updatePicture(Request $request)
    {
        $path = 'users/images/';
        $file = $request->file('admin_image');
        $new_name = 'UIMG_' . date('Ymd') . uniqid() . '.jpg';

        //Upload new image
        $upload = $file->move(public_path($path), $new_name);

        if (!$upload) {
            return response()->json(['status' => 0, 'msg' => 'Something went wrong, upload new picture failed.']);
        } else {
            //Get Old picture
            $oldPicture = User::find(Auth::user()->id)->getAttributes()['picture'];

            if ($oldPicture != '') {
                if (\File::exists(public_path($path . $oldPicture))) {
                    \File::delete(public_path($path . $oldPicture));
                }
            }

            //Update DB
            $update = User::find(Auth::user()->id)->update(['picture' => $new_name]);

            if (!$upload) {
                return response()->json(['status' => 0, 'msg' => 'Something went wrong, updating picture in db failed.']);
            } else {
                return response()->json(['status' => 1, 'msg' => 'Your profile picture has been updated successfully']);
            }
        }
    }


    function changePassword(Request $request)
    {
        //Validate form
        $validator = \Validator::make($request->all(), [
            'oldpassword' => [
                'required', function ($attribute, $value, $fail) {
                    if (!\Hash::check($value, Auth::user()->password)) {
                        return $fail(__('The current password is incorrect'));
                    }
                },
                'min:8',
                'max:30'
            ],
            'newpassword' => 'required|min:8|max:30',
            'cnewpassword' => 'required|same:newpassword'
        ], [
            'oldpassword.required' => 'Enter your current password',
            'oldpassword.min' => 'Old password must have atleast 8 characters',
            'oldpassword.max' => 'Old password must not be greater than 30 characters',
            'newpassword.required' => 'Enter new password',
            'newpassword.min' => 'New password must have atleast 8 characters',
            'newpassword.max' => 'New password must not be greater than 30 characters',
            'cnewpassword.required' => 'ReEnter your new password',
            'cnewpassword.same' => 'New password and Confirm new password must match'
        ]);

        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'error' => $validator->errors()->toArray()]);
        } else {

            $update = User::find(Auth::user()->id)->update(['password' => \Hash::make($request->newpassword)]);

            if (!$update) {
                return response()->json(['status' => 0, 'msg' => 'Something went wrong, Failed to update password in db']);
            } else {
                return response()->json(['status' => 1, 'msg' => 'Your password has been changed successfully']);
            }
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\TaggTransaction;
use App\Models\Genre;
use App\Models\Platform;
use App\Models\GamesGenre;
use App\Models\GamesPlatform;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Redirect;
use DataTables;
use Illuminate\Support\Facades\Storage;


class TaggFeeController extends Controller {
  public function index(Request $request) {
    $data = TaggTransaction::with("TxnUser", "Transaction")->paginate();
    return view('dashboards.admins.transactionfee', compact('data'));
  }

  public function add_game() {
    $Genre = Genre::orderBy('name', 'asc')->get();
    $Platform = Platform::orderBy('name', 'asc')->get();
    return view('dashboards.admins.create_game', compact('Genre', 'Platform'));
  }



}

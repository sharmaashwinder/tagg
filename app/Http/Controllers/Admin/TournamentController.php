<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Game;
use App\Models\Tournament;
use Auth;
use Illuminate\Support\Facades\DB;
use DataTables;
use App\Models\PrizePool;
use App\Models\SkillLevel;
use App\Models\Format;
use App\Models\TournamentSkillLevel;


class TournamentController extends Controller
{
    public function tournament(Request $request)
    {
        
        if ($request->ajax()) {
            $data = Tournament::leftJoin('games', 'tournaments.game_id', '=', 'games.id')
                ->leftJoin('formats', 'tournaments.format_id', '=', 'formats.id')
                ->leftJoin('prize_pools', 'tournaments.prize_pool_id', '=', 'prize_pools.id')
                ->select('games.game_name',  'formats.name', 'prize_pools.*','tournaments.*');

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('prize_pool_id', function ($data) {


                    if(empty($data->next_position_price)){
                        
                       $first = "$data->first_price";
                       if($data->second_price){
                        $second = "$data->second_price";
                       }else{
                        $second = "N/A";
                       }
                       if($data->third_price){
                        $third = "$data->third_price";
                       }else{
                        $third = "N/A"; 
                       }
                       
                       $totalsum = "$first+$second+$third";
                    }else{
                        $first = "$data->first_price";

                        if($data->second_price){
                        $second = "$data->second_price";
                       }else{
                        $second = "N/A";
                       }
                       if($data->third_price){
                        $third = "$data->third_price";
                       }else{
                        $third = "N/A"; 
                       }
                        $nextprice = "$data->next_position_price*$data->next_positions";
                        $totalsum = "$first+$second+$third+$nextprice";
                    }
                    return $totalsum;
                })


                ->addColumn('entry_fee', function ($data) {
                    $entry_fee = '$'.$data->entry_fee;
                    return $entry_fee;
                })



                ->addColumn('action', function ($data) {
                    $actions =
                     '<a class="btn btn-sm btn-success mr-2" href="' . route('edit.prize', $data->prize_pool_id) . '">
                    <i class="fas fa-pencil-alt">Prize Edit</i>
                    </a>';


                    $actions =
                    $actions .'<a class="btn btn-sm btn-success mr-2" href="' . route('tournament.games', $data->id) . '">
                    <i class="fas fa-pencil-alt"></i>
                    </a>';

                    $actions =
                        $actions . '<a class="btn btn-sm btn-success" href="' . route('view.tournament', $data->id) . '">
                    <i class="fa fa-eye"></i>
                    </a>';

                    return $actions;
                })
                ->rawColumns(['action', 'totalsum'])
                ->make(true);
        }



        return view('dashboards.admins.tournament');
    }

    public function create_tournament(Request $request)
    {
        $game = Game::where('is_deleted', '==', '0')->get();
        $prize = PrizePool::get();
        $skill = SkillLevel::orderBy('name', 'asc')->get();
        $format = Format::get();



        return view('dashboards.admins.create_tournament', compact('game', 'prize', 'skill', 'format'));
    }



    public function store(Request $request)
    {
        
        $user = auth()->user();
        $request->validate([
            'entry_fee'           => 'required|numeric', 
            'skill_level_id'           => 'required',       
        ]);
        $tournament = new Tournament($request->input());
        $tournament->user_id  = $user->id;
        $tournament->name  = $request->name;
        $tournament->status        = $request->status;
        $tournament->format_id        = $request->format_id;
        $tournament->entry_fee        = $request->entry_fee;
        $tournament->prize_pool_id  = $request->prize_pool_id;
        $tournament->start_time        = $request->start_time;
        $tournament->end_time        = $request->end_time;
        $tournament->game_id        = $request->game_id;
        $tournament->descriptions        = $request->descriptions;
        $tournament->player_numbers        = $request->player_numbers;
        $tournament->consol_tournament_password  = 1;
        $tournament->start_date        = date('Y-m-d H:i:s', strtotime($request->start_date));
        $tournament->end_date        = date('Y-m-d H:i:s', strtotime($request->end_date));
        $tournament->host_name        = $request->host_name;
        $tournament->speciality        = $request->speciality;
        $tournament->save();

        $TournamentId = Tournament::where('id', $tournament->id)->first();

        
         foreach($request->skill_level_id as $key) {

            $TournamentSkillLevel = new TournamentSkillLevel($request->input());
            $TournamentSkillLevel->tournament_id        = $TournamentId->id;
            $TournamentSkillLevel->skill_level_id  =  $key;
            $TournamentSkillLevel->save();
         }
      
        

        $notification = array(
            'message' => 'Tournaments Add Successfully!',
            'alert-type' => 'success'
        );
        return redirect()->route('tournament')->with($notification);
    }


    public function view_tournament($id)
    {
        $data = Tournament::where('id', $id)
            ->first();

            $skilllevel = TournamentSkillLevel::where('tournament_skill_levels.tournament_id' , $data->id)
            ->leftJoin('skill_levels', 'tournament_skill_levels.skill_level_id', '=', 'skill_levels.id')
            ->select('tournament_skill_levels.*','skill_levels.name as names')
           ->get();
          
        //    $skillname = array();
        //     foreach($skilllevel as $skilllevels){
        //      array_push($skillname, $skilllevels->names);
        //       }

         
        return view('dashboards.admins.tournament_view', compact('data','skilllevel'));
    }


    public function tournamentedit($id)
    {

          $data = Tournament::where('tournaments.id', $id)
           ->leftJoin('tournament_skill_levels', 'tournaments.id', '=', 'tournament_skill_levels.tournament_id')
          ->select('tournament_skill_levels.*','tournaments.*')
          ->first();
          $skilllevel = TournamentSkillLevel::where('tournament_skill_levels.tournament_id' , $data->id)
           ->leftJoin('skill_levels', 'tournament_skill_levels.skill_level_id', '=', 'skill_levels.id')
           ->select('tournament_skill_levels.*','skill_levels.name as names')
          ->get();
          $skillname = array();
         foreach($skilllevel as $skilllevels){
         array_push($skillname, $skilllevels->names);
         }

        $game = Game::where('is_deleted', '==', '0')->get();
        $prize =  PrizePool::get();
        $skill = SkillLevel::orderBy('name', 'asc')->get();
        $formate = Format::get();

        return view('dashboards.admins.edit_tournaments', compact('data', 'prize', 'skill', 'formate', 'game','skillname'));
    }

    public function UpdateTournament($id, Request $request)
    {
     
        $request->validate([
            'end_date'           => 'required|date|after:start_date'
        ]);
        $tournament = Tournament::findOrFail($id);
        $tournament->name  = $request->name;
        $tournament->status        = $request->status;
        $tournament->consol_tournament_password  = 1;
        $tournament->format_id        = $request->format_id;
        $tournament->entry_fee        = $request->entry_fee;
        $tournament->prize_pool_id  = $request->prize_pool_id;
        $tournament->start_time        = $request->start_time;
        $tournament->end_time        = $request->end_time;
        $tournament->game_id        = $request->game_id;
        $tournament->descriptions        = $request->descriptions;
        $tournament->player_numbers        = $request->player_numbers;
        $tournament->start_date        = date('Y-m-d', strtotime($request->start_date));
        $tournament->end_date        = date('Y-m-d', strtotime($request->end_date));
        $tournament->host_name        = $request->host_name;
        $tournament->speciality        = $request->speciality;
        $tournament->save();


        $TournamentId = Tournament::where('id', $tournament->id)->first();
        foreach($request->skill_level_id as $key) {

            $TournamentSkillLevel = TournamentSkillLevel::findOrFail($id);
            $TournamentSkillLevel->tournament_id        = $TournamentId->id;
            $TournamentSkillLevel->skill_level_id  =  $key;
            $TournamentSkillLevel->save();
         }

        $notification = array(
            'message' => 'Tournament Update Successfully!',
            'alert-type' => 'success'
        );
        return redirect()->route('tournament')->with($notification);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subscription;
use App\Models\Contactus;
use App\Models\User;

class StaticController extends Controller {
     public function profile() {
          return view('dashboards.users.profile');
     }
     public function about() {
          return view('about');
     }

     public function ui(){
         return view('ui');
     }

     public function contactus() {
          return view('contact-us');
     }

     public function faq() {
          return view('faq');
     }

     public function privacypolicy() {
          return view('privacy');
     }
     public function article() {
          return view('article');
     }
     public function becomeaffiliate() {
          return view('become-affiliate');
     }
     public function aboutrating() {
          return view('about-rating');
     }
     public function pressrelease() {
          return view('press-release');
     }
     public function careers() {
          return view('careers');
     }
     public function developers() {
          return view('developers');
     }
     public function sitemap() {
          return view('site-map');
     }

     public function store(Request $request) {
          $datas = $request->email;
          Subscription::create($request->all());
          $details = [
               'title' => 'Thanks for Subscribe - TAGG',
               'body' => 'Tagg us Subscription'
          ];

          \Mail::to($datas)->send(new \App\Mail\SubscribeEmail($details));

          $notification = array(
               'message' => 'Page Subscribe Successfully!',
               'alert-type' => 'success'
          );
          return redirect()->back()->with($notification);
     }
     public function contactussave(Request $request) {
          // User::with('roles')->get()

          $users = User::whereHas("roles", function ($q) {
               $q->where("name", "admin");
          })->get();


          // dd($users[0]->email);
          $sendto = $request->email;
          $details = [
               'type' => 'admin',
               'name' => $request->name,
               'phone' => $request->phone,
               'email' => $request->email,
               'comment' => $request->comment,
          ];
          foreach ($users as $recipient) {
               \Mail::to($recipient->email)->send(new \App\Mail\Contactus($details));
          }
          Contactus::create($request->all());
          $details['type'] = 'user';
          \Mail::to($sendto)->send(new \App\Mail\Contactus($details));



          $notification = array(
               'message' => 'Query Submitted Successfully!',
               'alert-type' => 'success'
          );

          return redirect()->back()->with($notification);
     }
}

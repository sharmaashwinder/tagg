<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use Exception;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Session;
use MrPropre\OAuth2\Client\Provider\EpicGames;
use League\OAuth2\Client\OptionProvider\HttpBasicAuthOptionProvider;
use App\Services\XboxService;
use App\Models\GamePlatformConnection;
use App\Traits\connectGameTrait;
use App\Models\UserSocialProfile;
use App\Http\Controllers\MailController;

class SocialController extends Controller {
    use connectGameTrait;
    protected $providers = [
        'facebook', 'google', 'twitter', 'epic', 'apple', 'xbox', 'twitch'
    ];
    protected $xboxService;
    public function __construct(){
        $this->xboxService = new XboxService([
            'client_id'     => config('services.xbox.appId'),
            'client_secret' => config('services.xbox.appSecret'),
            'redirect_uri'  => config('services.xbox.redirectUri'),
            'scope'         => config('services.xbox.scopes'),
            'state'         => $state = random_int(1, 200000)
        ]);
    }

    public function redirectToProvider($driver) {
        if (!$this->isProviderAllowed($driver)) {
            return $this->sendFailedResponse("{$driver} is not currently supported");
        }
        try {
            if ($driver == 'epic') {
                return $this->epicGames();
            }else if($driver == 'xbox'){
                return $this->xboxSignIn();
            } else if ($driver == 'apple') {
                return Socialite::driver($driver)->redirect();
            } else {
                return Socialite::driver($driver)->redirect();
            }
        } catch (Exception $e) {
            return $this->sendFailedResponse($e->getMessage());
        }
    }

    public function handleProviderCallback(Request $request, $driver) {
        try {
            if ($driver == 'epic') {
                return  $this->epicGames();
            }else if($driver == 'xbox'){
                return $this->xboxCallback($request);
            } else if ($driver == 'apple') {
                $user = Socialite::driver($driver)->user();
            } else if($driver == 'twitch'){
                $user = Socialite::driver($driver)->user();
                $userData = array('id' => $user->id, 'nickname' => $user->nickname);
                return $this->userGameConnection('twitch', $user->id, $userData);
            } else {
                $user = Socialite::driver($driver)->user();
            }
        } catch (Exception $e) {
            return $this->sendFailedResponse($e->getMessage());
        }
        // check for email in returned user
        return empty($user->email)
            ? $this->sendFailedResponse("No email id returned from {$driver} provider.")
            : $this->loginOrCreateAccount($user, $driver);
    }

    protected function loginOrCreateAccount($providerUser, $driver) {
        // check for already has account
        $user = User::where('email', $providerUser->getEmail())->first();
        // if user already found
        if ($user) {
            // update the avatar and provider that might have changed
            $user->update([
                'provider' => $driver,
                'provider_id' => $providerUser->id,
            ]);
        } else {
            // create a new user
            $user = User::create([
                'name' => $providerUser->getName(),
                'email' => $providerUser->getEmail(),
                'email_verified_at' => date('Y-m-d H:m:s'),
                'provider' => $driver,
                'provider_id' => $providerUser->getId(),
                'password' => ''
            ]);
            $details = [
                'title' => 'Mail from Tagg us',
                'body' => 'Tagg us Registration',
                'email'=>$user['email'],
                'name' => $user['name'],
            ];
    \Mail::to($user->email)->send(new \App\Mail\RegisterMail($details));
    $user->assignRole('gamer');
    $profile=new UserSocialProfile;
    $profile->user_id=$user->id;
    $profile->save();
        }
        // login the user
        Auth::login($user);
               $notification = array(
            'message' => 'Logged in Successfully with '.$driver,
            'alert-type' => 'success'
        );
        return  redirect()->intended('home')->with($notification);
    }

    protected function sendFailedResponse($msg = null) {
        return redirect()->route('login')
            ->withErrors(['msg' => $msg ?: 'Unable to login, try with another provider to login.']);
    }

    private function isProviderAllowed($driver) {
        return in_array($driver, $this->providers) && config()->has("services.{$driver}");
    }

    public function epicGames() {
        $config = config('services.epic');
        $provider = new EpicGames(
            [
                'clientId'          => $config['client_id'],
                'clientSecret'      => $config['client_secret'],
                'redirectUri'       => $config['redirect'],
            ],
            [
                'optionProvider' => new HttpBasicAuthOptionProvider()
            ]
        );

        if (!isset($_GET['code'])) {    // If we don't have an authorization code then get one
            $authUrl = $provider->getAuthorizationUrl(); // to show only epic login form instead of all identity provides like xbox, psn, nintendo
            $authUrl = str_replace("https://www.epicgames.com/id/authorize", "https://www.epicgames.com/id/login/epic", $authUrl);
            session(['oauthState' => $provider->getState()]);
            return redirect()->away($authUrl);
            // Check given state against previously stored one to mitigate CSRF attack
        } elseif (empty($_GET['state']) || ($_GET['state'] != session('oauthState'))) {
            unset($_SESSION['oauth2state']);
            exit('Invalid state');
        } else {        // Try to get an access token (using the authorization code grant)
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);
            // Optional: Now you have a token you can look up a users profile data
            try {       // We got an access token, let's now get the user's details
                $user = $provider->getResourceOwner($token);
                $user = $user->toArray();
                return $this->userGameConnection('epic', $user['sub'], $user);
                // Use these details to create a new profile
            } catch (\Exception $e) {
                // Failed to get user details
                exit('Something went wrong: ' . $e->getMessage());
            }
        }
    }

    public function xboxSignIn() {
        $authUrl = $this->xboxService->getBaseAuthorizationUrl();
        // Save client state so we can validate in callback
        session(['oauthState' => $this->xboxService->getState()]);
        // Redirect to AAD signin page
        return redirect()->away($authUrl);
      }

      public function xboxCallback(Request $request) {
        // Validate state
        $expectedState = session('oauthState');
        $request->session()->forget('oauthState');
        $providedState = $request->query('state');
        $xboxService = new XboxService([
            'client_id'     => config('azure.appId'),
            'redirect_uri'  => config('azure.redirectUri'),
            'scope'         => config('azure.scopes'),
            'state'         => $providedState
        ]);
        if (!isset($expectedState)) {
          // If there is no expected state in the session,
          // do nothing and redirect to the home page.
            return redirect('/');
        }
        if (!isset($providedState) || $expectedState != $providedState) {
          return redirect('/')
            ->with('error', 'Invalid auth state')
            ->with('errorDetail', 'The provided auth state did not match the expected value');
        }
        // Authorization code should be in the "code" query param
        $authCode = $request->query('code');
        if (isset($authCode)) {
            $msaToken = $this->xboxService->GetAccessToken(['code' => $authCode] );
            if(!$msaToken){
                return redirect('/')->with('error', 'msa Token not received');
            }
            $xasuToken = $this->xboxService->getXasuToken($msaToken);
            if(!$xasuToken){
                return redirect('/')->with('error', 'xasu Token not received');
            }
            $xstsToken = $this->xboxService->getXstsToken($xasuToken);
            if(!$xstsToken){
                return redirect('/')->with('error', 'xsts Token not received');
            }
            $profile = $this->xboxService->getLoggedUserProfile($xstsToken);
            // sign in user or register and connect with xbox connection
            return $this->userGameConnection('xbox', $profile['profileUsers']['0']['id'], $profile);
        }
        return redirect('/')
          ->with('error', $request->query('error'))
          ->with('errorDetail', $request->query('error_description'));
    }

    function userGameConnection($platform, $gamerId, $profile){
        $gamingPlatform = [
            'provider_name' => $platform,
            'profile'       => $profile
        ];
        $gameConnection = GamePlatformConnection::where(["provider_name"=>$platform, "provider_id"=>$gamerId])->first();
        if (Auth::user() && @$gameConnection->user_id != Auth::id()){
            if(!empty($gameConnection)){        // create gaming platform connection with user
                return redirect('connections')->with(sendSuccessResponse('warning', ucfirst($platform).'! '.__("messages.platformConnectedWithAnother")));
            }else{
                $gameConnect = $this->connectGamePlatform(Auth::id(), $gamingPlatform);
            }
            return redirect('connections')->with(sendSuccessResponse('success', ucfirst($platform).'! '.__("messages.platformConnectedSuccess")));
        }elseif(empty($gameConnection)){        // signup user and create game platform connection
            session(['gamingPlatform' => $gamingPlatform]);
            return redirect('register')->with(sendSuccessResponse('info', ucfirst($platform).'! '.__("messages.platformNotConnectedAccount")));
        }else{
            $gameConnect = $this->updateConnectGamePlatform($gameConnection->user_id, $gamingPlatform);
            $user = User::where('id', $gameConnection->user_id)->first();
            if(empty($user->email_verified_at)){
                MailController::sendsignUpmail($user->name, $user->email, $user->verification_code);
                return redirect()->route('verify_mail')->with(['data'=>$user->email, 'success'=>__("messages.notVerifyEmail")]);
            }
            Auth::loginUsingId($gameConnection->user_id);
            return redirect()->intended('home')->with(sendSuccessResponse('success', ucfirst($platform).'! '.__("messages.loginSuccessful")));
        }
    }
}

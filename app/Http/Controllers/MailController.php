<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerifyEmail;
class MailController extends Controller
{
    //
    public static function sendsignUpmail($name,$email,$verification_code){
        $data=[
           'name'=>$name,
           'verification_code'=>$verification_code
        ];
        Mail::to($email)->send(new VerifyEmail($data));
        // \Mail::to($datas)->send(new \App\Mail\VerifyEmail($details));
    }
}

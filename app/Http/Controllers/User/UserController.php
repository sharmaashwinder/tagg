<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;
use App\Models\TournamentPurchase;

use App\Models\FriendAcceptence;
use App\Models\FollowUnfollow;
use Illuminate\Support\Facades\Crypt;
use App\Notifications\Followrequest;
use DataTables;
use Hashids\Hashids;
use App\Models\UserSocialProfile;
use App\Rules\Uppercase;
Use Share;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller {
    public function share(){
        $shareButtons = \Share::page(
            'https://www.itsolutionstuff.com',
            'Your share text comes here',
        )
        ->facebook()
        ->twitter()
        ->linkedin()
        ->telegram()
        ->whatsapp()
        ->reddit();

        $posts = User::get();

        return view('socialshare', compact('shareButtons', 'posts'));
    }


    public function profile() {
        $name = auth()->user()->name;
        $details = User::where('id', auth()->user()->id)->first();
        $gamerinfo = DB::table('game_platform_connections')->where('user_id', auth()->user()->id)->get();
        if ($gamerinfo == null) {
            $gamerinfo = 0;
        } else {
            $gamerinfo = $gamerinfo;
        }

        $profile = UserSocialProfile::where('user_id', auth()->user()->id)->first();
        if ($profile) {
            $details = array($details, $profile, $gamerinfo);
            return view('dashboards.users.user_profile', compact("details"));
        } else {
            $profile = new UserSocialProfile;
            $profile->user_id = auth()->user()->id;
            $profile->save();
            $details = array($details, $profile, $gamerinfo);
            return view('dashboards.users.user_profile', compact("details"));
        }
    }
    public function editprofile() {
        return view('dashboards.users.edituser_profile');
    }

    public function editprofilesave(Request $req) {

        if ($req->profile_photo_path != null) {
            $patch = $req->file('profile_photo_path')->store('images', 'public');
        } else {
            $patch = null;
        }

        $user = User::find(auth()->user()->id);

        $attributes = $req->validate([

            'country' => '|regex:/^[a-zA-Z]+$/u|max:50|nullable',
            'date_of_birth' => 'nullable|date',
            'gamer_name' => [
                'max:50',
                'nullable',

                'unique:users,gamer_name,' . auth()->user()->id
            ]


        ]);
        $user->gamer_name = $req->gamer_name;
        $user->country = $req->country;
        $user->name = $req->name;
        $user->bio = $req->bio;
        $user->date_of_birth = $req->date_of_birth;
        if ($patch != null)
            $user->profile_photo_path = $patch;
        $user->gender = $req->gender;
        $user->save();
        return back()->with('success', 'Profile edited successfully');
    }
    public function imageUploadPost(Request $request) {
        if ($request->profile_image != null) {
            $patch = $request->file('profile_image')->store('images', 'public');
        } else {
            return back()
                ->with('success', 'You have successfully upload image.');
        }
        $user = new User();

        $user = User::where('id', auth()->user()->id)->update(array('profile_photo_path' =>   $patch));

        return back()
            ->with('success', 'You have successfully uploaded image.');
    }
    public function user_tournaments(Request $request) {

        if ($request->ajax()) {

            $data = TournamentPurchase::join('tournaments', 'tournament_purchases.tournament_id', '=', 'tournaments.id')
                ->where('tournament_purchases.user_id', '=', auth()->user()->id)
                ->where('tournaments.start_time', '<', now())
                ->where('tournaments.endtime', '>', now())
                ->where('tournaments.status', '=', 'active')
                ->select('tournaments.id', 'tour_name', 'start_time', 'skill_level', 'entry_fee', 'formate')
                ->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('start_time', function ($data) {

                    return date('Y-m-d', strtotime($data->start_time));
                })
                ->make(true);
        }
        return view('dashboards.users.myTournaments');
    }

    public function upcoming_tournaments(Request $request) {

        if ($request->ajax()) {

            $data = TournamentPurchase::join('tournaments', 'tournament_purchases.tournament_id', '=', 'tournaments.id')
                ->where('tournament_purchases.user_id', '=', auth()->user()->id)
                ->where('tournaments.start_time', '>', now())
                ->where('tournaments.endtime', '>', now())
                ->where('tournaments.status', '=', 'active')
                ->select('tournaments.id', 'tour_name', 'start_time', 'skill_level', 'entry_fee', 'formate')
                ->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('start_time', function ($data) {

                    return date('Y-m-d', strtotime($data->start_time));
                })
                ->make(true);
        }
    }
    public function completed_tournaments(Request $request) {


        if ($request->ajax()) {

            $data = TournamentPurchase::join('tournaments', 'tournament_purchases.tournament_id', '=', 'tournaments.id')
                ->where('tournament_purchases.user_id', '=', auth()->user()->id)
                ->where('tournaments.start_time', '<', now())
                ->where('tournaments.endtime', '<', now())
                ->where('tournaments.status', '=', 'active')
                ->select('tournaments.id', 'tour_name', 'start_time', 'skill_level', 'entry_fee', 'formate')
                ->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('start_time', function ($data) {

                    return date('Y-m-d', strtotime($data->start_time));
                })
                ->make(true);
        }
    }
    function get_socialpermissions() {
        $user = auth()->user()->id;

        $urls = UserSocialProfile::where('user_id', auth()->user()->id)->get();

        if ($urls)
            return view('dashboards.users.social_permissions', ['data' => $urls]);
        else
            return view('dashboards.users.social_permissions', ['data' => null]);
    }

    function social_permission(Request $req) {
       $req->validate([
           'facebook_url'=>['filled', new Uppercase],
           'twitter_url'=>'required_with:facebook_url'
       ]);
        $find_permission = UserSocialProfile::where('user_id', auth()->user()->id)->first();
        if ($find_permission == null) {

            $per_save = new UserSocialProfile();
            if ($req->profile_show) {
                $per_save->profile_show = 1;
            } else {
                $per_save->profile_show = 0;
            }

            $per_save->user_id = auth()->user()->id;
            $per_save->facebook_url = $req->facebook_url;
            $per_save->twitter_url = $req->twitter_url;
            $per_save->insta_url = $req->insta_url;
            $per_save->linkedin_url = $req->linkedin_url;

            $per_save->save();
            return redirect()->route('users.profile')->with('success', __('messages.profilesuccess'));
        } else {
            if ($req->profile_show) {
                $profiletoggle = 1;
            } else {
                $profiletoggle = 0;
            }
            $record = UserSocialProfile::where('user_id', auth()->user()->id)->first();
            $record->facebook_url = $req->facebook_url;
            $record->twitter_url = $req->twitter_url;
            $record->insta_url = $req->insta_url;
            $record->linkedin_url = $req->linkedin_url;
            $record->discord_url = $req->discord_url;
            $record->profile_show = $profiletoggle;
            $record->twitch_url = $req->twitch_url;
            $record->save();
            return redirect()->route('users.profile')->with('success', __('messages.profilesuccess'));
        }
        return redirect()->back()->with('changes saved');
    }



    function guest_listing() {
        $hashids = new Hashids();
        $data = User::where('users.id', '!=', 1)->where('users.id', '!=', auth()->user()->id)
            ->rightjoin('user_social_profiles', 'user_social_profiles.user_id', '=', 'users.id')
            ->select(
                'users.*',
                'user_social_profiles.facebook_url',
                'user_social_profiles.twitter_url',
                'user_social_profiles.insta_url',
                'user_social_profiles.linkedin_url'
            )->get();
// dd($data);
        return view('guest.showusers', ['data' => $data, 'hashid' => $hashids]);
    }
    public function user_search(Request $request) {
        $gamename = $request->game_name;
        $data = User::where('name', 'LIKE', "%{$gamename}%")->get();
        $hashids = new Hashids();
        return view('guest.showusers', ['data' => $data, 'hashid' => $hashids]);
    }
    function gamer_profile(Request $req, User $id) {
        $setting = Setting::all()->pluck('key_value', 'key_name');
        // $hashids = new Hashids();
        // $user = $hashids->decode($user);
// dd($id);
        $data = User::where('users.id', $id->id)
            ->join('user_social_profiles', 'user_social_profiles.user_id', '=', 'users.id')
            ->select('user_social_profiles.*', 'users.*')->get();
        $follow = auth()->user()->isfollow(auth()->user()->id, $id->id);

        $gamerinfo = DB::table('game_platform_connections')->where('user_id', $id->id)->get();
        if ($gamerinfo == null) {
            $gamerinfo = 0;
        }
        // dd($gamerinfo);

        $data = [$data, $follow, $gamerinfo];
        // dd($data);
        // dd($data);
        return view('guest.gamerprofile', compact('data', 'setting'));
    }
    function follow_request(Request $req) {
        $reciever = User::find($req->recieverid);
        $friend =  $reciever->follow(auth()->user()->id, $req->recieverid);
        if ($friend) {
            $requesteduser = User::find($req->recieverid)->notify((new Followrequest(auth()->user()->name)));
        }
        return back()->with('success', 'followed succesfully');
    }
    function unfollow_request(Request $req) {
        $friend = auth()->user()->unfollow(auth()->user()->id, $req->recieverid);
        return back()->with('success', 'Unfollowed succesfully');
    }
}

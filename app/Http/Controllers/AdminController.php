<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\Game;
use App\Models\Tournament;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use DataTables;
use Session;

class AdminController extends Controller
{
    function index()
    {

        $gametotal = Game::all()->count();
        $tournamenttotal = Tournament::all()->count();
        $playertotal = User::where('name','!=','admin')->count();
        
      

        return view('dashboards.admins.index',compact('gametotal','tournamenttotal','playertotal'));
       }

       function profile(){
           return view('dashboards.admins.profile');
       }
       function settings(){
           return view('dashboards.admins.settings');
       }

       function player(Request $request,Role $role){
        $userlogin = Auth::user()->id;
        if ($request->ajax()) {
            $data = DB::table('users')
            ->leftJoin('earnings', 'users.id', '=', 'earnings.user_id')
            ->where('id','!=',$userlogin);
            return DataTables::of($data)
            ->filterColumn('name', function($query, $keyword) use ($request) {
                $query->orWhere('users.name', 'LIKE', '%' . $keyword . '%')
                ->orWhere('users.gamer_name', 'LIKE', '%' . $keyword . '%')
                ->orWhere('earnings.rank', 'LIKE', '%' . $keyword . '%')
                ->orWhere('earnings.balance', 'LIKE', '%' . $keyword . '%')


               ;
            })
                ->addIndexColumn()
          ->addColumn('name', function($data){
            return $data->name;
                })

           ->addColumn('gamer_name', function($data){
              return
              $data->gamer_name;
                     })
          ->addColumn('rank', function($data){
                 return $data->rank;

                     })
           ->addColumn('balance', function($data){
             return $data->balance;
                      })

                ->addColumn('action', function($data){
                    // '<a href="javascript:void(0)" class="edit btn btn-danger btn-sm" data-action="'. route('product.destroy',$data->id) .'" onclick="deleteConfirmation('.$data->id.')">Delete</a>';
                    // $btn = '<a href="javascript:void(0)" class="edit btn btn-info btn-sm">View</a>';
                    // $btn = $btn.'<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">Edit</a>';
                    $btn =
                    '<a class="btn btn-success" href="'. route('player.view',$data->id) .'">
                    <i class="fa fa-eye"></i>
                    </a>';

                  $btn =    $btn.'<a class="btn btn-danger" onclick="return myFunction();" href="'. route('player.destroy',$data->id) .'"><i class="fa fa-trash"></i></a>';
                  return $btn;
             })
                ->rawColumns(['action'])
                ->make(true);
        }
        $permissions = DB::table('permissions')->whereIn('name',['Games','Tournament','LeaderManagement','Ordermanagement'
        ,'ProductManagement','GiveAwaymanagement'])->get()->toArray();
        $admin = DB::table('role_has_permissions')->where('role_id',1)->select('permission_id')->get()->toArray();
        $adminn=array();
        $special=array();
        foreach ($admin as $key => $value) {
            array_push($adminn,$value->permission_id);
        }
        $special_user=DB::table('role_has_permissions')->where('role_id',2)->get()->toArray();
        foreach ($special_user as $key => $value) {
            array_push($special,$value->permission_id);
        }


    $data=array($permissions,$adminn,$special);


        return view('dashboards.admins.players',['data'=>$data]);
    }


     public function playerview($id)
     {
        $data = DB::table('users')
        ->leftJoin('earnings', 'users.id', '=', 'earnings.user_id')
        ->where('id', $id)
        ->first();
        return view('dashboards.admins.player-view',compact('data'));
      }



      public function destroy($id)
      {
          $data =DB::table('users')
          ->leftJoin('earnings','users.id', '=','earnings.user_id')
         ->where('users.id', $id);
          DB::table('earnings')->where('user_id', $id)->delete();
          $data->delete();
          $notification = array(
            'message' => 'User deleted successfully',
            'alert-type' => 'success'
        ); 
        return redirect()->back()->with($notification);
    }

      


    function permission(Role $role){
        $permissions = DB::table('permissions')->whereIn('name',['Games','Tournament','LeaderManagement','Ordermanagement'
        ,'ProductManagement','GiveAwaymanagement'])->get()->toArray();
        $admin = DB::table('role_has_permissions')->where('role_id',1)->select('permission_id')->get()->toArray();
        $adminn=array();
        $special=array();
        foreach ($admin as $key => $value) {
            array_push($adminn,$value->permission_id);
        }
        $special_user=DB::table('role_has_permissions')->where('role_id',2)->get()->toArray();
        foreach ($special_user as $key => $value) {
            array_push($special,$value->permission_id);
        }


    $data=array($permissions,$adminn,$special);

        return view('dashboards.admins.permissions',['data'=>$data]);
    }
    function permission_save(Request $req,Role $role){

        $req=$req->toArray();
        $permissions = DB::table('permissions')->whereIn('name',['Games','Tournament','LeaderManagement','Ordermanagement'
        ,'ProductManagement','GiveAwaymanagement'])->get()->toArray();

        array_shift($req);
   $admin_permission_accept=array();
   $special_permission_accept=array();

        foreach ($req as $key => $value) {
              $a= explode("_",$key);
            if(strpos($a[0],'admin')!==false){
                array_push($admin_permission_accept,$a[1]);

            }
            elseif(strpos($a[0],'special')!==false){
                // echo "special".$a[1];
                array_push($special_permission_accept,$a[1]);
            }

         }
         $role = Role::findByName('admin');
         $spec= Role::findByName('special_user');
         foreach ($permissions as $key => $value) {

            if(in_array($value->id,$admin_permission_accept))
            {
                $role->givePermissionTo($value->name);
            }
            else{
                $role->revokePermissionTo($value->name);
            }
            if(in_array($value->id,$special_permission_accept)){
                $spec->givePermissionTo($value->name);
            }
            else{
                $spec->revokePermissionTo($value->name);
            }
         }

      return Redirect::back()->with('message', 'Permissions Changed Successfully');


    }
       function updateInfo(Request $request){

               $validator = \Validator::make($request->all(),[
                   'name'=>'required',
                   'email'=> 'required|email|unique:users,email,'.Auth::user()->id,
                   'favoritecolor'=>'required',
               ]);

               if(!$validator->passes()){
                   return response()->json(['status'=>0,'error'=>$validator->errors()->toArray()]);
               }else{
                    $query = User::find(Auth::user()->id)->update([
                         'name'=>$request->name,
                         'email'=>$request->email,
                         'favoriteColor'=>$request->favoritecolor,
                    ]);

                    if(!$query){
                        return response()->json(['status'=>0,'msg'=>'Something went wrong.']);
                    }else{
                        return response()->json(['status'=>1,'msg'=>'Your profile info has been update successfuly.']);
                    }
               }
       }

       function updatePicture(Request $request){
           $path = 'users/images/';
           $file = $request->file('admin_image');
           $new_name = 'UIMG_'.date('Ymd').uniqid().'.jpg';

           //Upload new image
           $upload = $file->move(public_path($path), $new_name);

           if( !$upload ){
               return response()->json(['status'=>0,'msg'=>'Something went wrong, upload new picture failed.']);
           }else{
               //Get Old picture
               $oldPicture = User::find(Auth::user()->id)->getAttributes()['picture'];

               if( $oldPicture != '' ){
                   if( \File::exists(public_path($path.$oldPicture))){
                       \File::delete(public_path($path.$oldPicture));
                   }
               }

               //Update DB
               $update = User::find(Auth::user()->id)->update(['picture'=>$new_name]);

               if( !$upload ){
                   return response()->json(['status'=>0,'msg'=>'Something went wrong, updating picture in db failed.']);
               }else{
                   return response()->json(['status'=>1,'msg'=>'Your profile picture has been updated successfully']);
               }
           }
       }


       function changePassword(Request $request){
           //Validate form
           $validator = \Validator::make($request->all(),[
               'oldpassword'=>[
                   'required', function($attribute, $value, $fail){
                       if( !\Hash::check($value, Auth::user()->password) ){
                           return $fail(__('The current password is incorrect'));
                       }
                   },
                   'min:8',
                   'max:30'
                ],
                'newpassword'=>'required|min:8|max:30',
                'cnewpassword'=>'required|same:newpassword'
            ],[
                'oldpassword.required'=>'Enter your current password',
                'oldpassword.min'=>'Old password must have atleast 8 characters',
                'oldpassword.max'=>'Old password must not be greater than 30 characters',
                'newpassword.required'=>'Enter new password',
                'newpassword.min'=>'New password must have atleast 8 characters',
                'newpassword.max'=>'New password must not be greater than 30 characters',
                'cnewpassword.required'=>'ReEnter your new password',
                'cnewpassword.same'=>'New password and Confirm new password must match'
            ]);

           if( !$validator->passes() ){
               return response()->json(['status'=>0,'error'=>$validator->errors()->toArray()]);
           }else{

            $update = User::find(Auth::user()->id)->update(['password'=>\Hash::make($request->newpassword)]);

            if( !$update ){
                return response()->json(['status'=>0,'msg'=>'Something went wrong, Failed to update password in db']);
            }else{
                return response()->json(['status'=>1,'msg'=>'Your password has been changed successfully']);
            }
           }
       }

}

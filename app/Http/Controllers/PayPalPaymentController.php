<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Exception;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use DateTime;
use Hashids\Hashids;
use App\Models\User;
use App\Models\PaypalTransaction;
use App\Models\TaggTransaction;
use App\Models\Wallet;
use App\Models\Setting;
use Illuminate\Support\Facades\Mail;
use App\Mail\TokenTransactionEmail;
use Illuminate\Support\Facades\Auth;
use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use PayPal\Api\Payout;
use PayPal\Api\PayoutSenderBatchHeader;
use PayPal\Api\PayoutItem;
use PayPal\Api\Currency;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

class PayPalPaymentController extends Controller
{
    public $gateway;
    protected $clientId, $clientSecret;
    public function __construct() {
        $this->middleware('auth');
        $paypal_conf    =   \Config::get('paypal');
        $this->clientId       =   $paypal_conf['client_id'];
        $this->clientSecret   =   $paypal_conf['secret'];
    }

    public function wallet(Request $request){
        $wallet = Wallet::where('user_id', Auth::id())->first();
        $setting = Setting::all()->pluck('key_value', 'key_name');
        $paypalTransaction = PaypalTransaction::where('user_id', Auth::id())->orderBy('created_at', 'desc')->paginate();
        $walletAmount = $wallet->balance ?? '0.00';
        return view('payment.wallet')->with(compact('walletAmount', 'setting', 'paypalTransaction'));
    }

    public function createOrder(Request $request) {
        $amount         =   (int)$request->amount;
        $clientId       =   $this->clientId;
        $clientSecret   =   $this->clientSecret;
        $environment    =   new SandboxEnvironment($clientId, $clientSecret);
        $client         =   new PayPalHttpClient($environment);
        $request        =   new OrdersCreateRequest();
        $request->prefer('return=representation');
        $request->body = [
                            "intent" => "CAPTURE",
                            "purchase_units" => [[
                                "reference_id" => uniqid().microtime('true'),
                                "amount" => [
                                    "value"        => "$amount",
                                    "currency_code"=> "USD"
                                    //put other details here as well related to your order
                                ]
                            ]],
                            "application_context" => [
                                "shipping_preference" => 'NO_SHIPPING',
                                "cancel_url" => route('paypalReturn'),
                                "return_url" => route('paypalCancel')
                            ]
                        ];
        try {
            // Call API with your client and get a response for your call
            $response = $client->execute($request);
            // If call returns body in response, you can get the deserialized version from the result attribute of the response
            echo json_encode($response); die;
        }catch (HttpException $ex) {
            echo $ex->statusCode;
        }
    }

    public function capturePayment($orderId){
        $setting = Setting::all()->pluck('key_value', 'key_name');
        $clientId = $this->clientId;
        $clientSecret = $this->clientSecret;
        $environment = new SandboxEnvironment($clientId, $clientSecret);
        $client = new PayPalHttpClient($environment);
        // Here, OrdersCaptureRequest() creates a POST request to /v2/checkout/orders
        // $response->result->id gives the orderId of the order created above
        $request = new OrdersCaptureRequest($orderId);
        $request->prefer('return=representation');
        try {
            // Call API with your client and get a response for your call
            $response = $client->execute($request);
            $amount   =   0;
            $finalAmount = 0;
            $tokenAlloted = 0;
            if($response->result->purchase_units[0]->payments->captures[0]->status == 'COMPLETED'){
                $wallet = Wallet::where('user_id', Auth::id())->first();
                if(empty($wallet)) { $wallet = new Wallet; }
                $amount             =   $response->result->purchase_units[0]->payments->captures[0]->seller_receivable_breakdown->net_amount->value;
                $taggCredit         =   number_format((float)(($amount / 100) * $setting['tagg_add_token_fee']), 2, '.', '');
                $finalAmount        =   $amount - $taggCredit;
                $tokenAlloted       =   (int)round($finalAmount * $setting['token_value']);
                $wallet->user_id    =   Auth::id();
                $wallet->balance    =   $wallet->balance + $tokenAlloted;
                $wallet->save();
                $response->tokenAlloted =   $tokenAlloted;
                $mail['status']     =   "COMPLETED";
                $mail['name']       =   Auth::user()->name;
                $mail['message']    =   __("messages.successfullyPurchasedToken");
                $mail['token']      =   $tokenAlloted;
                $mail['amount']     =   $response->result->purchase_units[0]->payments->captures[0]->amount->value;
                $mail['template']   =   "token-purchase-email";
                Mail::to(Auth::user()->email)->send(new TokenTransactionEmail($mail));
            }
            $transaction = new PaypalTransaction;
            $transaction->user_id           =   Auth::id();
            $transaction->wallet_id         =   $wallet->id ?? null;
            $transaction->transaction_for   =   "add_token";
            $transaction->tg_token_rate     =   $setting['token_value'];
            $transaction->transaction_id    =   $response->result->purchase_units[0]->payments->captures[0]->id ?? null;
            $transaction->at_order_id       =   $response->result->id ?? null;
            $transaction->at_payer_name     =   $response->result->payer->name->given_name.' '.$response->result->payer->name->surname ?? null;
            $transaction->at_payer_email    =   $response->result->payer->email_address ?? null;
            $transaction->at_status         =   $response->result->purchase_units[0]->payments->captures[0]->status ?? null;
            $transaction->at_amount         =   $response->result->purchase_units[0]->payments->captures[0]->amount->value ?? null;
            $transaction->at_paypal_fee     =   $response->result->purchase_units[0]->payments->captures[0]->seller_receivable_breakdown->paypal_fee->value ?? null;
            $transaction->at_tagg_fee       =   $setting['tagg_add_token_fee'];
            $transaction->at_final_amount   =   $finalAmount;
            $transaction->at_token_alloted  =   $tokenAlloted;
            $transaction->balance           =   $wallet->balance;
            $transaction->response_data     =   json_encode($response);
            $transaction->save();
            if($response->result->purchase_units[0]->payments->captures[0]->status == 'COMPLETED'){
                $taggTransaction    =   new TaggTransaction;
                $taggTransaction->user_id               =   Auth::id();
                $taggTransaction->wallet_id             =   $wallet->id ?? null;
                $taggTransaction->paypal_transaction_id =   $transaction->id;
                $taggTransaction->transaction_for       =   "add_token";
                $taggTransaction->add_amount            =   $finalAmount;
                $taggTransaction->tagg_credit           =   $taggCredit;
                $taggTransaction->tagg_fee              =   $setting['tagg_add_token_fee'];
                $taggTransaction->save();
            }
            echo json_encode($response); die;
        }catch (HttpException $ex) {
            echo $ex->statusCode; die;
        }
    }

    public function paypalReturn(Request $request){
        pr($request->all());
    }

    public function paypalCancel(Request $request){
        pr($request->all());
    }

    public function withdrawFunds(Request $request){
        $mail['name']       =   Auth::user()->name;
        $mail['template']   =   "token-withdraw-email";
        $withdrawToken = $request->withdrawToken;
        $setting = Setting::all()->pluck('key_value', 'key_name');
        $balance = Wallet::where('user_id', Auth::id())->pluck('balance')->first();
        if($balance < $request->withdrawToken || $request->withdrawToken < $setting['min_token_withdraw'] || $request->withdrawToken > $setting['max_token_withdraw']){
            $return['withdrawStatus'] = 'info';
            $return['withdrawMessage'] = __("messages.insufficientBalance");
        }else{
            $senderItemId   =   uniqid().microtime('true');
            $amount         =   number_format((float)($request->withdrawToken / $setting['token_value']), 2, '.', '');
            $taggCredit     =   number_format((float)(($amount / 100) * $setting['tagg_withdraw_token_fee']), 2, '.', '');
            $withdrawAmount =   number_format((float)($amount - $taggCredit), 2, '.', '');
            $email          =   $request->withdrawEmail;
            $apiContext     =   new ApiContext(
                new OAuthTokenCredential(
                    $this->clientId,
                    $this->clientSecret
                )
            );
            $payouts = new Payout;
            $senderBatchHeader  = new PayoutSenderBatchHeader;
            $senderBatchHeader->setSenderBatchId(uniqid().microtime(true))->setEmailSubject(__("messages.withdrawFunds"));
            $senderItem  = new PayoutItem;
            $senderItem->setRecipientType('Email')
                ->setNote(__("messages.withdrawFunds"))
                ->setReceiver($email)
                ->setSenderItemId($senderItemId)
                ->setAmount(new Currency('{
                            "value":'.$withdrawAmount.',
                            "currency":"USD"
                        }
                    ')
                );
            $payouts->setSenderBatchHeader($senderBatchHeader)->addItem($senderItem);
            $request = clone $payouts;
            try {
                $payoutBatch        =   $payouts->create(null, $apiContext);
                $payoutBatchId      =   $payoutBatch->getBatchHeader()->getPayoutBatchId();
                $payoutBatchStatus  =   Payout::get($payoutBatchId, $apiContext);
                $payoutItems        =   $payoutBatchStatus->getItems();
                $payoutItem         =   $payoutItems[0];
                $payoutItemId       =   $payoutItem->getPayoutItemId();
                $response           =   PayoutItem::get($payoutItemId, $apiContext);
                $response           =   $response->toArray();
            } catch (Exception $ex) {
                echo "catch"; pr($ex->getMessage());
                exit(1);
            }
            if($response['transaction_status'] == "SUCCESS" || $response['transaction_status'] == "UNCLAIMED"){
                $wallet = Wallet::where('user_id', Auth::id())->first();
                $wallet->balance = (int)round($wallet->balance - $withdrawToken);
                $wallet->save();
            }
            $transaction = new PaypalTransaction;
            $transaction->user_id               =   Auth::id();
            $transaction->wallet_id             =   $wallet->id ?? null;
            $transaction->transaction_for       =   "withdraw_token";
            $transaction->tg_token_rate         =   $setting['token_value'];
            $transaction->transaction_id        =   $response['transaction_id'] ?? null;
            $transaction->wt_payout_item_id     =   $response['payout_item_id'] ?? null;
            $transaction->wt_sender_item_id     =   $response['payout_item']['sender_item_id'] ?? null;
            $transaction->wt_payout_batch_id    =   $response['payout_batch_id'] ?? null;
            $transaction->wt_receiver           =   $response['payout_item']['receiver'] ?? null;
            $transaction->wt_status             =   $response['transaction_status'] ?? null;
            $transaction->wt_token_withdraw     =   $withdrawToken;
            $transaction->wt_tagg_fee           =   $setting['tagg_withdraw_token_fee'];
            $transaction->wt_paypal_fee         =   $response['payout_item_fee']['value'] ?? null;
            $transaction->wt_amount             =   $withdrawAmount;
            $transaction->balance               =   $wallet->balance ?? null;
            $transaction->response_data         =   json_encode($response);
            $transaction->save();
            if($response['transaction_status'] == "SUCCESS" || $response['transaction_status'] == "UNCLAIMED"){
                $taggTransaction    =   new TaggTransaction;
                $taggTransaction->user_id               =   Auth::id();
                $taggTransaction->wallet_id             =   $wallet->id ?? null;
                $taggTransaction->paypal_transaction_id =   $transaction->id;
                $taggTransaction->transaction_for       =   "withdraw_token";
                $taggTransaction->withdraw_amount       =   $withdrawAmount;
                $taggTransaction->tagg_credit           =   $taggCredit;
                $taggTransaction->tagg_fee              =   $setting['tagg_withdraw_token_fee'];
                $taggTransaction->save();
                $mail['token']      =   $withdrawToken;
                $mail['amount']     =   $withdrawAmount;
            }
            if($response['transaction_status'] == "SUCCESS"){
                $mail['status']     =   "COMPLETED";
                $mail['message']    =   __("messages.withdrawSuccessfully");
                $return['withdrawStatus']     =   'SUCCESS';
                $return['withdrawMessage']    =   $request->withdrawToken." ".__("messages.withdrawSuccessfully");
            }elseif($response['transaction_status'] == "UNCLAIMED"){
                $mail['status']     =   "UNCLAIMED";
                $mail['message']    =   $response['errors']['message'];
                $return['withdrawStatus'] = 'UNCLAIMED';
                $return['withdrawMessage'] = $request->withdrawToken." ".__("messages.withdrawUnclaimed")." ".$response['errors']['message'];
            }else{
                $mail['status']     =   "ERROR";
                $mail['message']    =   __("messages.withdrawIssue");
                $return['withdrawStatus'] = 'ERROR';
                $return['withdrawMessage'] = __("messages.withdrawIssue");
            }
        }
        Mail::to(Auth::user()->email)->send(new TokenTransactionEmail($mail));
        return redirect('wallet')->with($return);
    }

    public function transactionInfo(Request $request){
        $setting = Setting::all()->pluck('key_value', 'key_name');
        $transaction = PaypalTransaction::where('id', $request->id)->first();
        if($transaction->transaction_for == 'add_token'){
            $date = new DateTime($transaction->created_at);
            $return['transactionFor']   =   $transaction->transaction_for;
            $return['transactionId']    =   $transaction->transaction_id;
            $return['transactionDate']  =   $date->format('d M, Y, h:i A');
            $return['payerName']        =   ucfirst($transaction->at_payer_name);
            $return['payerEmail']       =   $transaction->at_payer_email;
            $return['amount']           =   $transaction->at_amount;
            $return['paypalFee']        =   $transaction->at_paypal_fee;
            $return['taggFee']          =   number_format((float)((($transaction->at_amount - $transaction->at_paypal_fee) / 100) * $transaction->at_tagg_fee), 2, '.', '');
            $return['finalAmount']      =   $transaction->at_final_amount;
            $return['tokenAdded']       =   $transaction->at_token_alloted;
            $return['tgTokenBalance']   =   $transaction->balance;
        }else if($transaction->transaction_for == 'withdraw_token'){
            $date = new DateTime($transaction->created_at);
            $amount         =   number_format((float)($transaction->wt_token_withdraw / $transaction->tg_token_rate), 2, '.', '');
            $taggCredit     =   number_format((float)(($amount / 100) * $transaction->wt_tagg_fee), 2, '.', '');
            $withdrawAmount =   number_format((float)($amount - $taggCredit), 2, '.', '');
            $return['transactionFor']   =   $transaction->transaction_for;
            $return['transactionId']    =   $transaction->transaction_id;
            $return['transactionDate']  =   $date->format('d M, Y, h:i A');
            $return['receiver']         =   $transaction->wt_receiver;
            $return['withdrawToken']    =   $transaction->wt_token_withdraw;
            $return['amount']           =   $amount;
            $return['taggFee']          =   $taggCredit;
            $return['withdrawAmount']   =   $transaction->wt_amount;
            $return['tgTokenBalance']   =   $transaction->balance;
        }else if($transaction->transaction_for == 'transfer_token'){
            $hashId = new Hashids();
            $date = new DateTime($transaction->created_at);
            $user = User::where('id', $transaction->t_receiver_id)->first();
            $return['transactionFor']   =   $transaction->transaction_for;
            $return['transactionId']    =   $transaction->id;
            $return['transactionDate']  =   $date->format('d M, Y, h:i A');
            $return['receiverName']     =   $user->name;
            $return['userLink']         =   route('gamer.view').'/'.$hashId->encode($user->id);
            $return['transferToken']    =   $transaction->t_amount;
        }
        return $return;
    }
}

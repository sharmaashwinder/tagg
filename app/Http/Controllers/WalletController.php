<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Wallet;
use App\Models\User;
use App\Models\Setting;
use App\Models\PaypalTransaction;
use Auth;
use App\Models\TokenTransfer;
use Illuminate\Support\Facades\Mail;
use App\Mail\TokenTransactionEmail;

class WalletController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function add_amount(Request $request) {
        $user = auth()->user();
        $wallet = new Wallet;
        $deposit_amount = $request->balance;
        $wallet->user_id = $user->id;
        $wallet->balance = $wallet->balance + $deposit_amount;
        $wallet->save();
    }

    public function remove_amount(Request $request)
{
    $user = Wallet::findOrFail($request->user_id);
    $service_fee = $request->balance;
    $user->balance = $user->balance - $service_fee;
    $user->save();
}

    public function donateToken($reciverId){
        $donateTo   =   User::where('id', $reciverId)->first();
        if(!empty($donateTo) && Auth::id() != $reciverId){
            $wallet     =   Wallet::where('user_id', Auth::id())->first();
            $donateMinMaxAmount    =   Setting::select('min_token_donate', 'max_token_donate')->first();
            return view('payment.donate-token')->with(compact('donateTo', 'donateMinMaxAmount', 'wallet'));
        }
        return redirect('home');
    }

    public function transferToken(Request $request){
        $setting = Setting::all()->pluck('key_value', 'key_name');
        $user   =   User::where('id', $request->id)->first();
        $senderWallet     =   Wallet::where('user_id', Auth::id())->first();
        if(@$senderWallet['balance'] > $request->amount){
            // addition of token into receiver Wallet
            $receiverWallet = Wallet::where('user_id', $request->id)->first();
            if(empty($receiverWallet)) { $receiverWallet = new Wallet; }
            $receiverWallet->user_id = $request->id;
            $receiverWallet->balance = $receiverWallet->balance + $request->amount;
            $receiverWallet->save();
            // subtraction on token in sender Wallet
            $senderWallet->balance  =   $senderWallet->balance - $request->amount;
            $senderWallet->save();
            $tokenTransfer = new TokenTransfer;
            $tokenTransfer->sender_id    =   $senderWallet->user_id;
            $tokenTransfer->receiver_id  =   $receiverWallet->user_id;
            $tokenTransfer->amount       =   $request->amount;
            $tokenTransfer->save();
            $transaction    =   new PaypalTransaction;
            $transaction->user_id               =   Auth::id();
            $transaction->transaction_for       =   "transfer_token";
            $transaction->t_token_transfer_id   =   $tokenTransfer->id;
            $transaction->t_receiver_id         =   $receiverWallet->user_id;
            $transaction->t_amount              =   $request->amount;
            $transaction->balance               =   $senderWallet->balance;
            $transaction->save();
            $senderMail['message']    =   __("messages.tokenTransferSuccess");
            $senderMail['name']       =   Auth::user()->name;
            $senderMail['token']      =   $request->amount;
            $senderMail['template']   =   "token-transfer-sender-email";
            $receiverMail['message']  =   __("messages.tokenReceivedSuccess");
            $receiverMail['name']     =   $user->name;
            $receiverMail['token']    =   $request->amount;
            $receiverMail['template'] =   "token-transfer-receiver-email";
            Mail::to(Auth::user()->email)->send(new TokenTransactionEmail($senderMail));
            Mail::to($user->email)->send(new TokenTransactionEmail($receiverMail));
            $return['withdrawStatus']     =   'SUCCESS';
            $return['withdrawMessage']    =   $request->amount." ".__("messages.tokenTransferSuccess");
            return back()->with($return);
        }
        $return['withdrawStatus'] = 'FAILED';
        $return['withdrawMessage'] = __("messages.tokenTransferFailed");
        return back()->with($return);
    }
}

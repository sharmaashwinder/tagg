<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Game;
use App\Models\Tournament;
use Hashids\Hashids;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{

     public function gameListing(Request $request)
     {
          $gamename = $request->game_name;
          $all_games = Game::where('is_deleted', '==', '0')
               ->where('game_name', 'LIKE', "%{$gamename}%")
               ->get();


          $TournamentCount = Tournament::join('games', 'tournaments.game_id', '=', 'games.id')
               ->select('tournaments.*', 'games.id')
               ->get();

          $hashids = new Hashids();
          $all_games = array($all_games, $hashids);

          return view('game.game_listing', compact('all_games','TournamentCount'));
     }


     public function gamepage(Request $req, $id)
     {
          $hashids = new Hashids();
          $id = $hashids->decode($id);
          $game_detail = Game::find($id)->first();
          $data = array($game_detail);
          $Tournament =  Tournament::orderBy('id', 'DESC')->take(5)->get();
          $tournamentCount = Tournament::where('game_id' ,$game_detail->id)->count();
         

          return view('game.game_details', compact('data','Tournament','tournamentCount'));
     }
     public function gameDetails()
     {

          return view('game.game_details');
     }
}

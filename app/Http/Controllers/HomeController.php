<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;
use App\Models\GamePlatformConnection;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('welcome');
    }

    public function gameConnections(){
        $gameConnections = GamePlatformConnection::where('user_id', Auth::id())->get()->keyBy('provider_name')->toArray();
        return view('gameConnection.gameConnection')->with(compact('gameConnections'));
    }

    public function disconnectPlatform($platform){
        GamePlatformConnection::where(['user_id' => Auth::id(), 'provider_name'=> $platform])->delete();
        $notification = array(
            'message' => ucfirst($platform).' '.__("messages.gamePlatformDisconnect"),
            'alert-type' => 'info'
        );
        return redirect()->back()->with($notification);
    }
}

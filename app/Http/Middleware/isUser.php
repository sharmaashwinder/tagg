<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class isUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if( Auth::check() && Auth::user()->hasRole('admin')){
             auth()->logout();
             return redirect()->route('login');
            // return $next($request);
        }

        if( Auth::check()){
            return $next($request);
        }else{
            return redirect()->route('login');
        }
        // return $next($request);
    }
}

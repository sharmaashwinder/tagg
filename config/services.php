<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain'    => env('MAILGUN_DOMAIN'),
        'secret'    => env('MAILGUN_SECRET'),
        'endpoint'  => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key'           => env('AWS_ACCESS_KEY_ID'),
        'secret'        => env('AWS_SECRET_ACCESS_KEY'),
        'region'        => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'google' => [
        'client_id'     => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect'      => env('GOOGLE_REDIRECT')
    ],

    'facebook' => [
        'client_id'     => env('FACEBOOK_CLIENT_ID'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
        'redirect'      => env('FACEBOOK_REDIRECT'),
    ],

    'twitter' => [
        'client_id'     => env('TWITTER_CLIENT_ID'),
        'client_secret' => env('TWITTER_CLIENT_SECRET'),
        'redirect'      => env('TWITTER_REDIRECT'),
    ],

    'apple' => [
        'client_id'     => env('APPLE_CLIENT_ID'),
        'client_secret' => env('APPLE_CLIENT_SECRET'),
        'redirect'      => env('APPLE_REDIRECT'),
    ],

    "epic" => [
        "client_id"     => env('EPIC_CLIENT_ID'),
        "client_secret" => env('EPIC_CLIENT_SECRET'),
        "redirect"      => env('EPIC_REDIRECT')
    ],
    "xbox" => [
        "appId"             => env('OAUTH_APP_ID', ''),
        "appSecret"         => env('OAUTH_APP_SECRET', ''),
        "redirectUri"       => env('OAUTH_REDIRECT_URI', ''),
        "scopes"            => env('OAUTH_SCOPES', ''),
        "authority"         => env('OAUTH_AUTHORITY', 'https://login.microsoftonline.com/common'),
        "authorizeEndpoint" => env('OAUTH_AUTHORIZE_ENDPOINT', '/oauth2/v2.0/authorize'),
        "tokenEndpoint"     => env('OAUTH_TOKEN_ENDPOINT', '/oauth2/v2.0/token'),
    ],

    'twitch' => [
        'client_id'     => env('TWITCH_CLIENT_ID'),
        'client_secret' => env('TWITCH_CLIENT_SECRET'),
        'redirect'      => env('TWITCH_REDIRECT_URI')
      ],
];

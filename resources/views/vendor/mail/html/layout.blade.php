<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Tagg</title>
  </head>
{{-- <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="color-scheme" content="light"> --}}
{{-- <meta name="supported-color-schemes" content="light"> --}}
<style>
@media only screen and (max-width: 600px) {
.inner-body {
width: 100% !important;
}

.footer {
width: 100% !important;
}
}

@media only screen and (max-width: 500px) {
.button {
width: 100% !important;
}
}
</style>
</head>
<body>
       <div style="background: #ececf7; padding: 30px 20px">
                        <div
                    style="
                    font-family: 'PT Sans', sans-serif;
                    max-width: 640px;
                    margin: 0 auto;
                    font-size: 14px;
                    "
                >
                    <div style="padding:15px;background: #1f1f3b; text-align: center">
                    <img
                        style="max-width: 200px; height: auto"
                        src="https://tagg.zapbuild.in/frontend/assets/img/tagg-logo.png"
                        alt="Tagg"
                        width="194"
                        height="82"
                    />
                    </div>
                    <div
                    style="
                        background: #fff;
                        box-shadow: 0px 5px 17px rgba(211, 211, 250, 0.4);
                        padding: 25px 30px;
                    "
                    >
          <div style="padding: 0px; color: #1b1b1b">



            <table style="background:#fff;" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                <tr>
                <td align="center">
                <table class="content" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                {{-- {{ $header ?? '' }} --}}

                <!-- Email Body -->
                <tr>
                <td style="border:none;" width="100%" cellpadding="0" cellspacing="0">
                <table style="margin:0px; border:none;" align="center" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                <!-- Body content -->
                <tr>
                <td style="padding:0px;">
                {{ Illuminate\Mail\Markdown::parse($slot) }}

                {{ $subcopy ?? '' }}
                </td>
                </tr>
                </table>
                </td>
                </tr>

                {{-- {{ $footer ?? '' }} --}}
                </table>
                </td>
                </tr>
                </table>



          </div>
        </div>
        <div
          style="
            background: #1f1f3b;
            padding: 10px 30px;
            font-weight: 600;
            font-size: 14px;
            display: inline-block;
            vertical-align: top;
            width: 100%;
            box-sizing: border-box;
          "
        >
          <p
            style="
              display: inline-block;
              vertical-align: top;
              margin: 9px 0 0;
              color: #fff;
              font-size: 15px;
              font-weight: normal;
            "
          >
            TAGG
          </p>
          <ul style="padding: 0; margin: 0; float: right">
            <li style="display: inline-block; padding: 5px 8px;">
              <a
                href="https://www.facebook.com/TAGGgaming"
                title="Facebook"
                style="
                  display: inline-block;
                  height: 26px;
                  width: 14px;
                  background: url(https://tagg.zapbuild.in/frontend/assets/img/sprite.png)
                    no-repeat;
                  background-position: -2px -2px;
                "
                target="_blank"
              ></a>
            </li>
            <li style="display: inline-block; padding: 5px 8px; ">
              <a
                href="https://twitter.com"
                title="Twitter"
                style="
                  display: inline-block;
                  height: 22px;
                  width: 26px;
                  background: url(https://tagg.zapbuild.in/frontend/assets/img/sprite.png)
                    no-repeat;
                  background-position: -27px -2px;
                "
                target="_blank"
              ></a>
            </li>
            <li style="display: inline-block; padding: 5px 8px;">
              <a
                href="https://www.instagram.com/"
                title="Instagram"
                style="
                  display: inline-block;
                  height: 26px;
                  width: 26px;
                  background: url(https://tagg.zapbuild.in/frontend/assets/img/sprite.png)
                    no-repeat;
                  background-position: -62px -3px;
                "
                target="_blank"
              ></a>
            </li>
            <li style="display: inline-block; vertical-align: top; padding: 5px 8px;">
            <a href="https://www.youtube.com/channel/UCOVeQPXBLvGVeTk-rVnnidA?view_as=subscriber" title="Youtube" style="
                  display: inline-block;
                  vertical-align: top;
                  height: 23px;
                  width: 26px;
                  background: url(https://tagg.zapbuild.in/frontend/assets/img/sprite.png)
                    no-repeat;
                  background-position: -98px 0px;
                " target="_blank"></a>
          </li>
          <li style="display: inline-block; vertical-align: top; padding: 5px 8px;">
            <a href="javascript:void(0);" title="Discord" style="
                  display: inline-block;
                  vertical-align: top;
                  height: 23px;
                  width: 25px;
                  background: url(https://tagg.zapbuild.in/frontend/assets/img/sprite.png)
                    no-repeat;
                  background-position: -134px -2px;
                " target="_blank"></a>
          </li>
          <li style="display: inline-block; vertical-align: top; padding: 5px 8px;">
            <a href="https://www.twitch.tv/tagg_gaming" title="Twitch" style="
                  display: inline-block;
                  vertical-align: top;
                  height: 26px;
                  width: 25px;
                  background: url(https://tagg.zapbuild.in/frontend/assets/img/sprite.png)
                    no-repeat;
                  background-position: -169px -2px;
                " target="_blank"></a>
          </li>
          </ul>
        </div>
      </div>
    </div>


</body>
</html>

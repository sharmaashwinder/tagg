<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('dashboards.users.layout.head')

<body>
    <div class="outer-container">
        @include('dashboards.users.layout.header')
        <!-- header end -->
        <!-- banner start -->
        <div class="banner-outer banner-bg">
            <div class="inner-container">
                <div class="row align-items-center justify-content-between">
                    <div class="col-sm-6">
                        <img class="banner-logo" src="frontend/assets/img/banner-text.png" width="250" height="87" alt="Call of Duty" />
                        <div class="py-3 py-md-4 py-lg-5">
                            <p class="typo1 md ellipse3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s typesetting industry.</p>
                        </div>
                        <div class="custom-row">
                            <a class="border-btn2 custom-col" href="{{ route('register') }}" title="Join Now">Join Now</a>
                        </div>
                    </div>
                    <div class="col-sm-6 d-md-block d-none">
                        <img src="frontend/assets/img/game-img.png" class="img-fluid" width="639" height="639" alt="Banner Game image" />
                    </div>
                </div>
            </div>
        </div>
        <!-- banner end -->
        <!-- brand logos -->
        <div class="logos-row py-md-4 py-3">
            <div class="inner-container">
                <ul class="d-flex align-items-center justify-content-between logos-list">
                    <li><img class="img-fluid" src="frontend/assets/img/fa-games.png" width="111" height="73" alt="FA Games Logo" /></li>
                    <li><img class="img-fluid" src="frontend/assets/img/activeVision-games.png" width="223" height="58" alt="activeVision Games Logo" /></li>
                    <li><img class="img-fluid" src="frontend/assets/img/epic-games.png" width="86" height="100" alt="Epic Games Logo" /></li>
                    <li><img class="img-fluid" src="frontend/assets/img/ghost-games.png" width="120" height="95" alt="Epic Games Logo" /></li>
                    <li><img class="img-fluid" src="frontend/assets/img/atari-games.png" width="85" height="113" alt="Epic Games Logo" /></li>
                </ul>
            </div>
        </div>
        <!-- brand logos end -->
        <!-- how it works start -->
        <div class="common-space bg-color1">
            <div class="inner-container">
                <div class="text-center">
                    <h1 class="heading-1">How it <span class="theme-color">works</span></h1>
                    <p class="typo2 px-lg-5 py-lg-4 py-2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                </div>
                <div id="carouselExampleControls" class="carousel slide mt-3 custom-slider" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row align-items-center">
                                <div class="col-md-3"><img class="img-fluid" src="frontend/assets/img/Img-1.png" width="300" height="519" alt="Epic Games Logo" /></div>
                                <div class="col-md-3 px-md-0">
                                    <h2 class="heading-2">Lorem <span>Ipsum</span></h2>
                                    <p class="typo2 py-4">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters</p>
                                    <a class="arrow-btn" href="javascript:void(0);" title="Discover More">Discover More</a>
                                </div>
                                <div class="col-md-6 pl-0"><img class="img-fluid" src="frontend/assets/img/img-2.png" width="661" height="561" alt="Epic Games Logo" /></div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row align-items-center">
                                <div class="col-md-3"><img class="img-fluid" src="frontend/assets/img/Img-1.png" width="300" height="519" alt="Epic Games Logo" /></div>
                                <div class="col-md-3 px-md-0">
                                    <h2 class="heading-2">Lorem <span>Ipsum</span></h2>
                                    <p class="typo2 py-4">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters</p>
                                    <a class="arrow-btn" href="javascript:void(0);" title="Discover More">Discover More</a>
                                </div>
                                <div class="col-md-6 pl-0"><img class="img-fluid" src="frontend/assets/img/img-2.png" width="661" height="561" alt="Epic Games Logo" /></div>
                            </div>
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                        <span class="prev-icon" aria-hidden="true"></span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                        <span class="next-icon" aria-hidden="true"></span>
                    </button>
                </div>
            </div>
        </div>
        <!-- how it works end -->
        <!-- how it works new start
        <div class="common-space bg-color1">
            <div class="inner-container">
                <div class="text-center">
                    <h1 class="heading-1">How it <span class="theme-color">works</span></h1>
                    <p class="typo2 px-lg-5 py-lg-4 py-2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                </div>
                <div class="custom-row d-flex overflow-hidden mt-3">
                    <div class="d-flex align-items-center">
                        <input type="radio" name="size" id="small" value="small" checked="checked" />
                        <label class="figure-tab" for="small"><img class="img-fluid" src="frontend/assets/img/Img-1.jpg" width="236" height="454" alt="Epic Games Logo" /></label>
                        <div class="figure-content ps-3">
                            <h2 class="heading-2">Lorem <span>Ipsum</span></h2>
                            <p class="typo2 py-4">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            <a class="arrow-btn" href="javascript:void(0);" title="Discover More">Discover More</a>
                        </div>
                    </div>
                    <div class="d-flex align-items-center">
                        <input type="radio" name="size" id="medium" value="medium" />
                        <label class="figure-tab" for="medium"><img class="img-fluid" src="frontend/assets/img/Img-2.jpg" width="236" height="454" alt="Epic Games Logo" /></label>
                        <div class="figure-content ps-3">
                            <h2 class="heading-2">Lorem <span>Ipsum</span></h2>
                            <p class="typo2 py-4">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            <a class="arrow-btn" href="javascript:void(0);" title="Discover More">Discover More</a>
                        </div>
                    </div>
                    <div class="d-flex align-items-center">
                        <input type="radio" name="size" id="large" value="large" />
                        <label class="figure-tab" for="large"><img class="img-fluid" src="frontend/assets/img/Img-1.jpg" width="236" height="454" alt="Epic Games Logo" /></label>
                        <div class="figure-content ps-3">
                            <h2 class="heading-2">Lorem <span>Ipsum</span></h2>
                            <p class="typo2 py-4">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            <a class="arrow-btn" href="javascript:void(0);" title="Discover More">Discover More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        how it works new end -->
        <!-- about start -->
        <div class="common-space bg-color2">
            <div class="inner-container">
                <div class="text-center">
                    <h2 class="heading-1">about <span class="theme-color">Tagg</span></h2>
                    <p class="typo2 px-2 px-lg-5 pt-lg-4 pt-2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                </div>
                <div class="row align-items-center mt-lg-5 mt-md-4 mt-3">
                    <figure class="col-lg-6 col-sm-4 cpr-3">
                        <div class="border-shadow">
                            <img class="img-fluid" src="frontend/assets/img/about-tagg.jpg" width="597" height="575" alt="Tagg About Photo" />
                        </div>
                    </figure>
                    <div class="col-lg-6 col-sm-8 cpl-3 pt-sm-0 pt-3">
                        <h2 class="heading-2">Lorem <span>Ipsum</span></h2>
                        <p class="typo2 pt-2 pt-md-4">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters</p>
                        <p class="typo2 py-3">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters</p>
                        <div class="custom-row pt-2 pt-md-4">
                            <a class="border-btn2 custom-col" href="javascript:void(0);" title="About Tagg">About Tagg</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- about end -->
        <!-- Features start -->
        <div class="common-space bg-image2">
            <div class="inner-container">
                <div class="text-center">
                    <h2 class="heading-1">Tagg <span class="theme-color">Features</span></h2>
                    <p class="typo2 px-2 px-lg-5 pt-lg-4 pt-2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                </div>
                <div class="row mt-lg-5 mt-2">
                    <div class="col-sm-4 text-center">
                        <figure><img class="img-fluid" src="frontend/assets/img/G1.png" width="315" height="301" alt="Feature Game Photo" /></figure>
                        <h3 class="heading-2">Feature <span>1</span></h3>
                        <p class="typo2 px-0 px-lg-5 pt-lg-4 pt-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                    </div>
                    <div class="col-sm-4 text-center">
                        <figure><img class="img-fluid" src="frontend/assets/img/G2.png" width="315" height="301" alt="Feature Game Photo" /></figure>
                        <h3 class="heading-2">Feature <span>2</span></h3>
                        <p class="typo2 px-0 px-lg-5 pt-lg-4 pt-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                    </div>
                    <div class="col-sm-4 text-center">
                        <figure><img class="img-fluid" src="frontend/assets/img/G3.png" width="315" height="301" alt="Feature Game Photo" /></figure>
                        <h3 class="heading-2">Feature <span>3</span></h3>
                        <p class="typo2 px-0 px-lg-5 pt-lg-4 pt-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                    </div>
                </div>
                <div class="custom-row pt-lg-5 pt-4 text-center">
                    <a class="border-btn2 custom-col" href="javascript:void(0);" title="Browse Tournaments">Browse Tournaments</a>
                </div>
            </div>
        </div>
        <!-- Features start -->
        <!-- popular start -->
        @include('dashboards.users.layout.popular')
        <!-- Subscribe start -->
        @include('dashboards.users.layout.subscribe')
        <!-- footer start -->
        @include('dashboards.users.layout.footer')
    </div>
    <link rel="stylesheet" href="https://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">

    <script src="https://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
    <script>
        @if(Session::has('message'))
        var type = "{{ Session::get('alert-type', 'info') }}";
        switch (type) {
            case 'info':
                toastr.info("{{ Session::get('message') }}");
                break;

            case 'warning':
                toastr.warning("{{ Session::get('message') }}");
                break;

            case 'success':
                toastr.success("{{ Session::get('message') }}");
                break;

            case 'error':
                toastr.error("{{ Session::get('message') }}");
                break;
        }
        @endif
    </script>
</body>

</html>

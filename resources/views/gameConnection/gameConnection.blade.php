<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('dashboards.users.layout.head')

<body>
    <div class="outer-container">
        @include('dashboards.users.layout.header')
        <!-- header end -->
        <!-- banner start -->
        <div class="banner-outer banner-bg bg-color4 color2">
            <div class="inner-container my-md-5 my-3">
                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-5 justify-content-center">
                    @include("layouts.flash")
                    <div class="col my-3 justify-content-center">
                        <div class="tab-card bg-color3 text-center p-0 d-flex flex-column justify-content-between min-card">
                            <div class="left-logo black d-flex align-items-center justify-content-center w-100 img-minH"><img src="frontend/assets/img/epic-white.png" width="43" height="46" alt="Epic Games" /></div>
                            <div class="p-3">
                                @if(!empty($gameConnections['epic']))
                                <div class="heading-4 py-md-5 py-3">{{ @$gameConnections['epic']['username'] }}</div>
                                @endif
                                <div class="custom-row py-md-2 py-1">
                                    @if(empty($gameConnections['epic']))
                                    <a href="{{ url('auth/epic') }}" class="custom-col border-btn text-center w-100">Connect</a>
                                    @else
                                    <a href="{{ url('disconnect-platform/epic') }}" class="custom-col border-btn-ylow w-100 text-center">Disconnect</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col my-3 justify-content-center">
                        <div class="tab-card bg-color3 text-center p-0 d-flex flex-column justify-content-between min-card">
                            <div class="left-logo s-blue d-flex align-items-center justify-content-center w-100 img-minH"><img class="img-fluid-xy" src="frontend/assets/img/steam.png" alt="Steam" width="46" height="46"></div>
                            <div class="p-3">
                                @if(!empty($gameConnections['steam']))
                                <div class="heading-4 py-md-5 py-3">{{ @$gameConnections['steam']['username'] }}</div>
                                @endif
                                <div class="custom-row py-md-2 py-1">
                                    @if(empty($gameConnections['steam']))
                                    <a href="#" class="custom-col border-btn text-center w-100">Connect</a>
                                    @else
                                    <a href="#" class="custom-col border-btn-ylow w-100 text-center">Disconnect</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="col my-3 justify-content-center">
                        <div class="tab-card bg-color3 text-center p-0 d-flex flex-column justify-content-between min-card">
                            <div class="left-logo white d-flex align-items-center justify-content-center w-100 img-minH"><img class="img-fluid-xy" src="frontend/assets/img/active-vision.png" alt="Activision" width="79" height="21"></div>
                            <div class="p-3">
                                @if(!empty($gameConnections['activision']))
                                <div class="heading-4 py-md-5 py-3">{{ @$gameConnections['activision']['username'] }}</div>
                                @endif
                                <div class="custom-row py-md-2 py-1">
                                    @if(empty($gameConnections['activision']))
                                    <a href="{{ url('auth/activision') }}" class="custom-col border-btn text-center w-100">Connect</a>
                                    @else
                                    <a href="{{ url('disconnect-platform/activision') }}" class="custom-col border-btn-ylow w-100 text-center">Disconnect</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <div class="col my-3 justify-content-center">
                        <div class="tab-card bg-color3 text-center p-0 d-flex flex-column justify-content-between min-card">
                            <div class="left-logo t-purple d-flex align-items-center justify-content-center w-100 img-minH"><img class="img-fluid-xy" src="frontend/assets/img/twitch.png" alt="Activision" width="79" height="21"></div>
                            <div class="p-3">
                                @if(!empty($gameConnections['twitch']))
                                <div class="heading-4 py-md-5 py-3">{{ @$gameConnections['twitch']['username'] }}</div>
                                @endif
                                <div class="custom-row py-md-2 py-1">
                                    @if(empty($gameConnections['twitch']))
                                    <a href="{{ url('auth/twitch') }}" class="custom-col border-btn text-center w-100">Connect</a>
                                    @else
                                    <a href="{{ url('disconnect-platform/twitch') }}" class="custom-col border-btn-ylow w-100 text-center">Disconnect</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col my-3 justify-content-center">
                        <div class="tab-card bg-color3 text-center p-0 d-flex flex-column justify-content-between min-card">
                            <div class="left-logo l-blue d-flex align-items-center justify-content-center w-100 img-minH"><img class="img-fluid-xy" src="frontend/assets/img/playstation-logo.png" alt="Playstation Network" width="51" height="41"></div>
                            <div class="p-3">
                                @if(!empty($gameConnections['psn']))
                                <div class="heading-4 py-md-5 py-3">{{ @$gameConnections['psn']['username'] }}</div>
                                @endif
                                <div class="custom-row py-md-2 py-1">
                                    @if(empty($gameConnections['psn']))
                                    <a href="#" class="custom-col border-btn text-center w-100">Connect</a>
                                    @else
                                    <a href="#" class="custom-col border-btn-ylow text-center w-100">Disconnect</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col my-3 justify-content-center">
                        <div class="tab-card bg-color3 text-center p-0 d-flex flex-column justify-content-between min-card">
                            <div class="left-logo green d-flex align-items-center justify-content-center w-100 img-minH"><img class="img-fluid-xy" src="frontend/assets/img/xbox.png" alt="Xbox live" width="42" height="42"></div>
                            <div class="p-3">
                                @if(!empty($gameConnections['xbox']))
                                <div class="heading-4 py-md-5 py-3">{{ @$gameConnections['xbox']['username'] }}</div>
                                @endif
                                <div class="custom-row py-md-2 py-1">
                                    @if(empty($gameConnections['xbox']))
                                    <a href="auth/xbox" class="custom-col border-btn text-center w-100">Connect</a>
                                    @else
                                    <a href="disconnect-platform/xbox" class="custom-col w-100 border-btn-ylow text-center">Disconnect</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- popular start -->
        @include('dashboards.users.layout.popular')
        <!-- Subscribe start -->
        @include('dashboards.users.layout.subscribe')
        <!-- footer start -->
        @include('dashboards.users.layout.footer')
    </div>

</body>

</html>
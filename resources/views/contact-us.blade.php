<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('dashboards.users.layout.head')

<body>
    <div class="outer-container">
        @include('dashboards.users.layout.header')
        <!-- header end -->
        <!-- banner start -->
        <!-- banner start -->
        <div class="banner-outer banner-faq">
            <div class="inner-container-sm">
                <div class="text-center py-lg-5 py-md-3 py-0">
                    <h1 class="heading-1 py-4 m-0">Get In Touch</h1>
                </div>
            </div>
        </div>
        <!-- banner end -->
        <div class="common-space bg-full bg-color4 color3">
            <div class="inner-container">
                <div class="color-box">
                    <div class="text-center">
                        <h1 class="heading-1 md pb-3"><span class="theme-color md">Contact Us</span></h1>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-5">
                            <h2 class="heading-3 mb-3">Questions? Send us a Message!</h2>
                            <p class="typo2">Feel free to get in touch with us and our team will respond back as soon as possible</p>
                            <div class="pt-md-5 pt-3">
                                <h2 class="heading-3 mb-3">Open Practices</h2>
                                <p class="typo4">Monday to Saturday: – <span class="typo2">9:00 am to 7:00 pm</span></p>
                                <p class="typo4">Sunday : – <span class="typo2">10:00 am to 5:00 pm</span> </p>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <form class="" method="POST" id="registerForm" action="{{ route('contactuss') }}">

                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-row my-2">
                                            <input class="form-control input-transparent" placeholder="Name*" id="name" type="text" name="name" value="{{ old('name') }}" required autocomplete="name" />

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="custom-row my-2">
                                            <input class="form-control input-transparent" placeholder="Phone Number*" id="phone" type="text" name="phone" value="{{ old('phone') }}" required />
                                        </div>
                                    </div>
                                </div>

                                <div class="custom-row my-2">
                                    <input class="form-control input-transparent" placeholder="Email*" id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" />
                                </div>
                                <div class="custom-row my-2">
                                    <textarea class="form-control input-transparent textarea-h" rows="4" cols="50" name="comment" placeholder="Enter your message*"></textarea>
                                </div>

                                <div class="custom-row mt-md-3 mt-2">
                                    <button type="submit" class="btn-yellow-lg text-center custom-row" title="Send Your Message">Send Your Message</button>
                                </div>

                            </form>
                        </div>
                    </div>

                    <script>
                        $(document).ready(function() {
                            $("#registerForm").validate({
                                rules: {
                                    name: {
                                        required: true,
                                        maxlength: 25
                                    },
                                    phone: 'phone',
                                    email: {
                                        required: true,
                                        email: true
                                    },
                                    name: {
                                        required: true,

                                    },
                                    comment: {
                                        required: true
                                    }


                                },
                                messages: {
                                    phone: {
                                        required: 'Phone is required'
                                    },
                                    email: {
                                        required: 'Email is required'
                                    },
                                    name: {
                                        required: 'Gamer Name is required'
                                    },
                                    comment: {
                                        required: 'Comments is required'
                                    }

                                }
                            });
                        });
                        $.validator.methods.email = function(value, element) {
                            // return this.optional(element) || /[a-z]+@[a-z]+\.[a-z]+/.test(value);
                            return this.optional(element) || /^[a-z0-9][-a-z0-9.!#$%&'*+-=?^_`{|}~\/]+@([-a-z0-9]+\.)+[a-z]{2,5}$/.test(value);
                        }

                        // $.validator.methods.phone = function( value, element ) {
                        //   return this.optional( element ) ||  /^\d{3}-\d{3}-\d{4}$/.test(value);
                        // }, "Please enter a valid phone number");


                        $.validator.addMethod('phone', function(value, element) {
                            return this.optional(element) || /^(\+\d{1,3}[- ]?)?\d{10}$/.test(value);
                        }, "Please enter a valid phone number");
                    </script>
                    <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">

                    <script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
                    <script>
                        @if(Session::has('message'))
                        var type = "{{ Session::get('alert-type', 'info') }}";
                        switch (type) {
                            case 'info':
                                toastr.info("{{ Session::get('message') }}");
                                break;

                            case 'warning':
                                toastr.warning("{{ Session::get('message') }}");
                                break;

                            case 'success':
                                toastr.success("{{ Session::get('message') }}");
                                break;

                            case 'error':
                                toastr.error("{{ Session::get('message') }}");
                                break;
                        }
                        @endif
                    </script>

                </div>
            </div>
            <div class="col-md-6">
                sandeep
            </div>
        </div>
    </div>
    </div>
    </div>
    <!-- popular start -->
    @include('dashboards.users.layout.popular')
    <!-- Subscribe start -->
    @include('dashboards.users.layout.subscribe')
    <!-- footer start -->
    @include('dashboards.users.layout.footer')
    </div>

</body>

</html>

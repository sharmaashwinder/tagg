<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('dashboards.users.layout.head')

<body>
    <div class="outer-container">
        @include('dashboards.users.layout.header')
        <div class="banner-outer banner-profile">
            <div class="inner-container">
                <div class="custom-row c-my-5">
                    <div class="row align-items-center">
                        <div class="col-lg-3 col-md-4">

                                <div class="custom-row">

                                    @if($data[0][0]->profile_photo_path)
                                    <img src="{{ asset('storage/' . $data[0][0]->profile_photo_path) }}" alt="Admin" class="img-fluid" width="270" height="215">
                                    @else
                                    <img id="preview_img" src="{{asset('frontend/assets/img/user-default.jpg')}}" class="img-fluid" width="270" height="215" />
                                    @endif
                                </div>
                                <div class="input-group mt-3">
                                    <div class="auto-row">
                                        <button title="Share Profile" class="btn-yellow-border lg custom-row text-center text-uppercase px-4 text-nowrap"><em class="icon-share custom-col me-2"></em> Share Profile</button>
                                    </div>
                                </div>


                        </div>
                        <div class="col-lg-9 col-md-8 pt-4 pt-md-0">
                            <div class="row">
                                <div class="col-md-6">
                                    @if($data[0][0]->name)
                                    <h1 class="heading-1 md">{{$data[0][0]->name}}</h1>
                                    @endif
                                    <ul class="list-tags d-flex">
                                        <li>Special</li>
                                        <li>Pro</li>
                                        <li class="d-flex align-items-center"><em class="icon-verified me-2"></em> Verified</li>

                                    </ul>

                                    <ul class="list-with-icons mt-2 d-flex flex-column">
                                        @if ( ($data[2])->count() )
                      @foreach ($data[2] as $item)
                      @if(strcasecmp($item->provider_name, 'twitch')==0 )
                      <li class="typo7 mb-2 me-2"><span class="text-white">Twitch - </span>{{ $item->username }}</li>
                      @elseif (strcasecmp($item->provider_name, 'xbox')==0)
                      <li class="typo7 mb-2 me-2"><span class="text-white">Xbox - </span>{{ $item->username }}</li>
                      @elseif (strcasecmp($item->provider_name, 'epic')==0)
                      <li class="typo7 mb-2 me-2"><span class="text-white">Epic- </span>{{ $item->username }}</li>
                      @elseif (strcasecmp($item->provider_name, 'steam')==0)
                      <li class="typo7 mb-2 me-2"><span class="text-white">Steam- </span>{{ $item->username }}</li>
                      @elseif (strcasecmp($item->provider_name, 'psn')==0)
                      <li class="typo7 mb-2 me-2"><span class="text-white">PSN- </span>{{ $item->username }}</li>
                      @endif
                      @endforeach
                    </ul>
                  </div>
                  <ul class="list-with-icons my-1 d-flex flex-column ">
                      @else
                      <li class="d-flex align-items-center py-1"><em class="icon-game me-3"></em></li>
                      @endif

                                        <li class="d-flex align-items-center py-2"><em class="icon-inbox me-3"></em>
                                            <div>{{ $data[0][0]->email}}</div>
                                        </li>
                                    </ul>
                                </div>
                                @if($data[0][0]->profile_show)
                                <div class="col-md-6">
                                    <div class="bg-color2 p-2 d-flex">
                                        <div class="auto-row me-2">
                                            <div class="row g-2">
                                                <div class="col text-center">
                                                    <div class="bg-color3 box-xs d-flex flex-column align-items-center justify-content-center">
                                                        <div class="heading-3 mb-2">258</div>
                                                        <div class="typo-xs">Win Loss<br> Record</div>
                                                    </div>
                                                </div>
                                                <div class="col text-center">
                                                    <div class="bg-color3 box-xs d-flex flex-column align-items-center justify-content-center">
                                                        <div class="heading-3 mb-2"><span class="tg">tg</span>365</div>
                                                        <div class="typo-xs">Earnings</div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="bg-color3 box-xs d-flex flex-column align-items-center justify-content-center">
                                                        @if($data[0][0]->twitch_url)
                                                        <a href="{{$data[0][0]->twitch_url}}" target="_blank" title="Twitch"> <em class="social-icon twitch"></em>
                                                        </a>
                                                        @else
                                                        <a href="javascript:void(0);" title="Facebook" >
                                                            <em class="social-icon twitch"></em>
                                                        </a>
                                                        @endif

                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="bg-color3 box-xs d-flex flex-column align-items-center justify-content-center">
                                                        @if($data[0][0]->discord_url)
                                                        <a href="{{$data[0][0]->discord_url}}" target="_blank" title="Twitch"> <em class="social-icon soc"></em>
                                                        </a>
                                                        @else
                                                        <a href="javascript:void(0);" title="Facebook" >
                                                            <em class="social-icon soc"></em>
                                                        </a>
                                                        @endif

                                                        {{-- <a href="javascript:void(0);" title="Discord" class="social-icon soc">

                                                        </a> --}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bg-color3 mt-2">
                                                <div class="row g-0">

                                                    <div class="col">
                                                        <div class="bg-color3 box-xs d-flex flex-column align-items-center justify-content-center">
                                                            @if($data[0][0]->facebook_url)
                                                            <a href="{{$data[0][0]->facebook_url}}" target="_blank" class="social-icon fb" title="Facebook"> <em class="social-icon fb"></em>
                                                            </a>
                                                            @else
                                                            <a href="javascript:void(0);" title="Facebook" class="social-icon fb"><em class="social-icon fb"></em></a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="bg-color3 box-xs d-flex flex-column align-items-center justify-content-center">
                                                            @if($data[0][0]->twitter_url)
                                                            <a href="{{$data[0][0]->twitter_url}}" target="_blank" title="Tweeter" class=""><em class="social-icon twitter"></em></a>
                                                            @else
                                                            <a href="javascript:void(0);" title="Tweeter"  class="social-icon twitter"><em class="social-icon twitter"></em></a>
                                                            @endif

                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="bg-color3 box-xs d-flex flex-column align-items-center justify-content-center">
                                                            @if($data[0][0]->insta_url)
                                                            <a href="{{$data[0][0]->insta_url}}" title="Instagram" target="_blank" class="">
                                                                <em class="social-icon insta"></em>
                                                            </a>
                                                            @else
                                                            <a href="javascript:void(0);" title="Instagram" class="">
                                                                <em class="social-icon insta"></em>
                                                            </a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="bg-color3 box-xs d-flex flex-column align-items-center justify-content-center">
                                                            @if($data[0][0]->linkedin_url)
                                                            <a href="{{$data[0][0]->linkedin_url}}" title="Linkedin"   target="_blank" class="social-icon ldin"></a>
                                                                {{-- <em class="social-icon insta"></em> --}}
                                                            </a>
                                                            @else
                                                            <a href="javascript:void(0);" title="Linkedin" class="social-icon ldin">
                                                            {{-- <a href="javascript:void(0);" title="Instagram" class=""> --}}
                                                                {{-- <em class="social-icon insta"></em> --}}
                                                            </a>
                                                            @endif

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-end">
                                            {{-- <div class="switch">
                                                <input type="checkbox" checked>
                                                <span class="slider round"></span>
                                            </div> --}}
                                        </div>

                                    </div>
                                </div>
                                @endif

                                <div class="col-12 ">
                                    <div class="row">
                                        <div class="col-md-6 pt-md-4 pt-3">
                                            <a href="javascript:void(0);" title="Send Tokens" class="btn-yellow-border lg custom-row text-center text-uppercase px-4 text-nowrap" data-bs-toggle="modal"
                                            data-bs-target="#sendToken">Send Tokens</a>
                                        </div>
                                        <div class="col-md-6 pt-md-4 pt-3">


                                            @if(!$data[1])
                                            <form class="custom-row" action={{route('followrequest') }} method="post">
                                                @csrf
                                                <input name="recieverid" hidden value={{$data[0][0]->id}}>
                                                <button type="submit" class="btn-yellow-border md custom-row text-center text-uppercase px-4 text-nowrap">Follow</button>
                                            </form>
                                            @else
                                            <form class="custom-row" action={{route('unfollowrequest') }} method="post">
                                                @csrf
                                                <input name="recieverid" hidden value={{$data[0][0]->id}}>
                                                <button  type="submit" title="Follow" class="btn-yellow-fill md custom-row text-uppercase text-center">UnFollow</a>
                                                {{-- <button type="submit" class="btn-yellow-border md custom-row text-center text-uppercase px-4 text-nowrap">UnFollow</button> --}}
                                            </form>
                                            @endif




                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- tabs start  -->
        <div class="common-space-sm bg-color3">
            <div class="inner-container-sm">
                <ul class="nav nav-tabs border-0 justify-content-center" id="myTab" role="tablist">
                    <li class="nav-item mx-2 my-1" role="presentation">
                        <button class="border-btn-ylow active " id="home-tab" data-bs-toggle="tab" data-bs-target="#all" type="button" role="tab" aria-controls="home" aria-selected="true">Profile</button>
                    </li>
                    <li class="nav-item mx-2 my-1" role="presentation">
                        <button class="border-btn-ylow" id="profile-tab" data-bs-toggle="tab" data-bs-target="#catagory1" type="button" role="tab" aria-controls="profile" aria-selected="false">Friends</button>
                    </li>
                    <li class="nav-item mx-2 my-1" role="presentation">
                        <button class="border-btn-ylow" id="contact-tab" data-bs-toggle="tab" data-bs-target="#catagory2" type="button" role="tab" aria-controls="contact" aria-selected="false">Trophies</button>
                    </li>
                    <li class="nav-item mx-2 my-1" role="presentation">
                        <button class="border-btn-ylow" id="contact-tab" data-bs-toggle="tab" data-bs-target="#catagory3" type="button" role="tab" aria-controls="contact" aria-selected="false">Tournaments</button>
                    </li>
                </ul>
            </div>
        </div>
        <!-- tab content start  -->
        <div class="tab-content common-space-md bg-full" id="myTabContent">
            <div class="inner-container tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="home-tab">
                <h2 class="title-1 mb-3">About</h2>
                <p class="typo3 md pb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmaod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                <h2 class="title-1 mb-3">History</h2>
                <div class="row">
                  <div class="col-md-4 py-2">
                    <div class="bg-color3 p-4 box-height d-flex justify-content-between align-items-center">
                      <div>
                        <div class="heading-2 lg">50%</div>
                        <p class="typo7 pt-4">Win Rate</p>
                      </div>
                      <figure><img src="{{asset('frontend/assets/img/win-img.png')}}" alt="Win Icon" class="img-fluid" width="67" height="67" /></figure>
                    </div>
                  </div>
                  <div class="col-md-4 py-2">
                    <div class="bg-color3 p-4 box-height d-flex justify-content-between align-items-center">
                      <div>
                        <div class="heading-2 lg">42</div>
                        <p class="typo7 pt-4">Tournament Played</p>
                      </div>
                      <figure><img src="{{asset('frontend/assets/img/tournament-img.png')}}" alt="Tournament Icon" class="img-fluid" width="68" height="68" /></figure>
                    </div>
                  </div>
                  <div class="col-md-4 py-2">
                    <div class="bg-color3 p-4 box-height d-flex justify-content-between align-items-center">

                      <div>
                        <div class="heading-2 lg">42</div>
                        <p class="typo7 pt-4">Total Winnings</p>
                      </div>
                      <div>
                        <div class="text-end pb-2">
                          <select class="border-select">
                            <option>Per Week</option>
                            <option>Per Month</option>
                          </select>
                        </div>
                        <figure class="text-end"><img src="{{asset('frontend/assets/img/winnings-img.png')}}" alt="Winnings Icon" class="img-fluid" width="73" height="68" /></figure>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 py-2">
                    <div class="bg-color3 p-4 box-height d-flex justify-content-between align-items-center">
                      <div>
                        <div class="heading-2 lg">60%</div>
                        <p class="typo7 pt-4">Recent Result</p>
                      </div>
                      <figure><img src="{{asset('frontend/assets/img/result-img.png')}}" alt="Result Icon" class="img-fluid" width="64" height="60" /></figure>
                    </div>
                  </div>
                  <div class="col-md-4 py-2">
                    <div class="bg-color3 p-4 box-height d-flex justify-content-between align-items-center">
                      <div>
                        <div class="heading-2 lg">15</div>
                        <p class="typo7 pt-4">Upcoming Tournament</p>
                      </div>
                      <figure><img src="{{asset('frontend/assets/img/tournament-img.png')}}" alt="Tournament Icon" class="img-fluid" width="68" height="68" /></figure>
                    </div>
                  </div>
                  <div class="col-md-4 py-2">
                    <div class="bg-color3 p-4 box-height d-flex justify-content-between align-items-center">
                      <div class="custom-row">
                        <div class="d-flex justify-content-between">
                          <div class="heading-2 sm">Term & Conditions</div>
                          <div class="switch">
                            <input type="checkbox" checked="">
                            <span class="slider round"></span>
                          </div>
                        </div>
                        <p class="typo7 pt-4">User Term & Conditions</p>
                        <p class="typo7 pt-2">Hoster Term & Conditions</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="custom-row pt-md-4 pt-3">
                  <div class="d-flex justify-content-between align-items-center">
                    <h2 class="title-1 mb-0">My Games</h2>
                    <a href="javascript:void(0);" title="View All" class="anchor-ylw">View All</a>
                  </div>
                  <div class="row">
                    <div class="col-md-5 py-3">
                      <div class="bg-color3 p-2">
                        <div class="d-flex align-items-center">
                          <figure>
                            <img src="{{asset('frontend/assets/img/game-img.jpg')}}" alt="Tournament Icon" class="img-fluid" width="239" height="162" />
                          </figure>
                          <ul class="check-list-sm typo4 md ps-3">
                            <li><em class="check-mark"></em>Top Rated</li>
                            <li><em class="check-mark"></em>Rank= 164</li>
                            <li><em class="check-mark"></em>Skill = Pro</li>
                          </ul>
                        </div>
                      </div>
                      <h3 class="heading-8 pt-3 mb-0">Zombie: White Walkers</h3>
                    </div>
                    <div class="col-md-7 py-3">
                      <div class="row">
                        <div class="col-md-4">
                          <div class="bg-color3 p-2">
                            <figure>
                              <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                            </figure>
                          </div>
                          <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                        </div>
                        <div class="col-md-4">
                          <div class="bg-color3 p-2">
                            <figure>
                              <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                            </figure>
                          </div>
                          <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                        </div>
                        <div class="col-md-4">
                          <div class="bg-color3 p-2">
                            <figure>
                              <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                            </figure>
                          </div>
                          <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="custom-row pt-md-4 pt-3">
                  <div class="d-flex justify-content-between align-items-center">
                    <h2 class="title-1 mb-0">Highlights</h2>
                  </div>
                  <div class="slider-outer">
                    <div id="carousel" class="owl-carousel arrow-top pt-3">
                      <div class="item">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                      </div>
                      <div class="item">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                      </div>
                      <div class="item">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                      </div>
                      <div class="item">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                      </div>
                      <div class="item">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="custom-row pt-md-4 pt-3">
                  <div class="d-flex justify-content-between align-items-center">
                    <h2 class="title-1 mb-0">Interest and Hobbies</h2>
                  </div>
                  <div class="slider-outer">
                    <div id="interest" class="owl-carousel arrow-top pt-3">
                      <div class="item">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                      </div>
                      <div class="item">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                      </div>
                      <div class="item">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                      </div>
                      <div class="item">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                      </div>
                      <div class="item">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="custom-row pt-md-4 pt-3">
                  <div class="d-flex justify-content-between align-items-center">
                    <h2 class="title-1 mb-0">News Feeds</h2>
                    <a href="javascript:void(0);" title="View All" class="anchor-ylw">View All</a>
                  </div>
                  <div class="d-flex justify-content-between align-items-center pt-md-4 pt-3 border-b1 pb-3 mt-2 position-relative">
                    <div>
                      <h2 class="heading-8 mb-0">James Smith shared a video</h2>
                      <p class="typo6 pt-2">3 min ago</p>
                    </div>
                    <div>
                      <a href="javascript:void(0);" title="Play Video" class="typo4 md d-flex align-items-center"><em class="video-icon me-2"></em>Play Video</a>
                    </div>
                    <em class="sb10"></em>
                  </div>
                  <div class="row mt-3">
                    <div class="col-md-3 py-3">
                      <div class="bg-color3">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                      </div>
                      <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                    </div>
                    <div class="col-md-3 py-3">
                      <div class="bg-color3">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                      </div>
                      <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                    </div>
                    <div class="col-md-3 py-3">
                      <div class="bg-color3">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                      </div>
                      <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                    </div>
                    <div class="col-md-3 py-3">
                      <div class="bg-color3">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                      </div>
                      <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                    </div>
                  </div>
                </div>
            </div>
            <div class="inner-container tab-pane fade" id="catagory1" role="tabpanel" aria-labelledby="profile-tab">
                <h2 class="title-1 mb-0">My Friends</h2>
                <div class="table-responsive mt-3">
                    <table class="table custom-table">
                    <thead>
                        <tr>
                        <th scope="col">Player Name</th>
                        <th scope="col">Gamer Name</th>
                        <th scope="col">ID</th>
                        <th scope="col">Rank</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td>
                            <div class="d-flex align-items-center">
                            <div class="rounded-img-xs me-md-3 me-2">
                                <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                            </div>
                            Barry Allen
                            </div>
                        </td>
                        <td>Gamer Name</td>
                        <td></td>
                        <td>
                            <ul class="d-flex">
                            <li class="mx-1"><em class="icon-rank"></em></li>
                            <li class="mx-1"><em class="icon-rank"></em></li>
                            <li class="mx-1"><em class="icon-rank"></em></li>
                            <li class="mx-1"><em class="icon-rank"></em></li>
                            </ul>
                        </td>
                        <td>
                            <div class="d-flex align-items-center">
                            <a href="javascript:void(0);" title="Chat" class="icon-chat mx-1">Chat</a>
                            <a href="javascript:void(0);" title="Gift" class="icon-gift mx-1">Gift</a>
                            </div>
                        </td>
                        </tr>
                        <tr>
                        <td>
                            <div class="d-flex align-items-center">
                            <div class="rounded-img-xs me-md-3 me-2">
                                <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                            </div>
                            Barry Allen
                            </div>
                        </td>
                        <td>Gamer Name</td>
                        <td>PSN ID : 25487</td>
                        <td>
                            <ul class="d-flex">
                            <li class="mx-1"><em class="icon-rank"></em></li>
                            <li class="mx-1"><em class="icon-rank"></em></li>
                            <li class="mx-1"><em class="icon-rank"></em></li>
                            <li class="mx-1"><em class="icon-rank"></em></li>
                            </ul>
                        </td>
                        <td>
                            <div class="d-flex align-items-center">
                            <a href="javascript:void(0);" title="Chat" class="icon-chat mx-1">Chat</a>
                            <a href="javascript:void(0);" title="Gift" class="icon-gift mx-1">Gift</a>
                            </div>
                        </td>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>
            <div class="inner-container tab-pane fade" id="catagory2" role="tabpanel" aria-labelledby="contact-tab">
                <div class="typo3 md text-center">No Data</div>
            </div>
            <div class="inner-container tab-pane fade" id="catagory3" role="tabpanel" aria-labelledby="contact-tab">
                <div class="typo3 md text-center">No Data</div>
            </div>
        </div>
{{-- send token modal --}}
        <div class="modal fade" id="sendToken" tabindex="-1" aria-labelledby="withdrawLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content bg-color2 modal-box">
                    <span class="trianle-tl"></span>
                    <span class="trianle-tr"></span>
                    <span class="trianle-bl"></span>
                    <span class="trianle-br"></span>
                    <div class="modal-header cpx-2">
                        <h5 class="heading-3 md mb-0 py-2">Send Tokens</h5>
                        <button type="button" class="custom-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body cpx-2 py-4">
                        <div class="custom-row" id="smart-button-container">
                            <form action="{{ route('transfer-token') }}" method="post">
                                @csrf
                            <div class="custom-row my-3">
                                <input type="hidden" name="id" value="{{ $data[0][0]->id }}">
                                <input class="form-control input-transparent" name="amount"
                                    min="{{ $setting['min_token_transfer'] }}" max="{{ $setting['max_token_transfer'] }}"
                                    type="number" id="amount" value="" placeholder="Add TG Token" required>
                            </div>
                            <div class="custom-row my-3">
                                <input class="btn-yellow-fill md custom-row text-uppercase text-center" type="submit"
                                    value="Send">
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Transection Status Modal -->
        <div class="modal fade" id="transectionStatusModal" tabindex="-1" aria-labelledby="withdrawLabel"
        aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content bg-color2 modal-box">
                    <span class="trianle-tl"></span>
                    <span class="trianle-tr"></span>
                    <span class="trianle-bl"></span>
                    <span class="trianle-br"></span>
                    <button type="button" class="custom-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body cpx-2 py-4">
                        <div class="modal-body cpx-2 py-md-5 py-4">
                            <div class="custom-row text-center py-md-4 py-3">
                                <em class="check-mark-border"></em>
                                <div class="heading-6 md my-3" id="message_status"></div>
                                <p class="typo3 md" id="message"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if (Session::has('withdrawStatus'))
        <script type="text/javascript">
            window.onload = function() {
                $('#message_status').html("{{ Session::get('withdrawStatus') }}");
                $('#message').html("{{ Session::get('withdrawMessage') }}");
                $('#transectionStatusModal').modal('show');
            };
        </script>
    @endif
        @include('dashboards.users.layout.footer')
        @include('dashboards.users.layout.script')
        <link rel="stylesheet" href="https://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
        @include('toaster')
        <script>
    $(document).ready(function() {
      $("#carousel").owlCarousel({
        autoplay: true,
        lazyLoad: true,
        loop: false,
        margin: 20,
        nav: true,
        responsiveClass: true,
        autoHeight: true,
        autoplayTimeout: 7000,
        smartSpeed: 800,
        responsive: {
          0: {
            items: 1
          },

          767: {
            items: 2
          },

          1024: {
            items: 3
          },

          1366: {
            items: 4
          }
        }
      });
      $("#interest").owlCarousel({
        autoplay: true,
        lazyLoad: true,
        loop: false,
        margin: 20,
        nav: true,
        responsiveClass: true,
        autoHeight: true,
        autoplayTimeout: 7000,
        smartSpeed: 800,
        responsive: {
          0: {
            items: 1
          },

          767: {
            items: 2
          },

          1024: {
            items: 3
          },

          1366: {
            items: 4
          }
        }
      });
    });
  </script>
    </div>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>Tagg</title>
</head>

<body>
  <div style="background: #ececf7; padding: 30px 20px">
    <div style="font-family: 'PT Sans', sans-serif; max-width: 640px; margin: 0 auto; font-size: 14px;">
        <div style="padding: 15px; background: #1f1f3b; text-align: center;">
            <img style="max-width: 200px; height: auto;" src="{{asset('frontend/assets/img/tagg-logo.png')}}" alt="Tagg" width="194" height="82" />
        </div>
      <div style="background: #fff; box-shadow: 0px 5px 17px rgba(211, 211, 250, 0.4); padding: 25px 30px;">
        <div style="padding: 0px; color: #1b1b1b">
          <h1 style="margin: 10px 0 30px; color: #0f0f0f; font-size: 16px;"> Hi  {{ $detail['name'] }},</h1> 
          <p style="font-size: 14px; margin: 10px 0 5px; line-height: 1.3; font-weight: normal;">Welcome to TAGG!</p>
          <p style="font-size: 14px; margin: 5px 0 30px; line-height: 1.3; font-weight: normal;">
          <span style="color: #00dfff; font-size: 14px;">{{ $detail['email'] }}</span> is registered successfully.
          </p>
          <p style="font-size: 14px;margin: 10px 0 30px;line-height: 1.3;font-weight: normal;">We are thrilled that 
              you have joined us! Congratulations.  
          </p>
          <p style="font-size: 14px;">
            <b>Cheers,</b><br>
            <b>TAGG Team</b>
          </p>
        </div>
      </div>
      <div style="background: #1f1f3b;padding: 10px 30px;font-weight: 600;font-size: 14px;
            display: inline-block;vertical-align: top;width: 100%;box-sizing: border-box;">
        <p style="display: inline-block;vertical-align: top;margin: 9px 0 0;color: #fff;font-size: 15px;font-weight: normal;">TAGG</p>
        <ul style="padding: 0; margin: 0; float: right">
            <li style="display: inline-block; vertical-align: top; padding: 5px 8px;">
              <a href="https://www.facebook.com/TAGGgaming" title="Facebook" style="display: inline-block;vertical-align: top;height: 26px;width: 14px;background: url({{asset('frontend/assets/img/sprite.png')}}) no-repeat; background-position: -2px -2px;" target="_blank"></a>
            </li>
            <li style="display: inline-block; vertical-align: top; padding: 5px 8px; ">
              <a href="https://twitter.com" title="Twitter" style="display: inline-block;vertical-align: top;height: 25px;width: 26px;background: url({{asset('frontend/assets/img/sprite.png')}}) no-repeat; background-position: -27px 0px;" target="_blank"></a>
            </li>
            <li style="display: inline-block; vertical-align: top; padding: 5px 8px;">
              <a href="https://www.instagram.com/" title="Instagram" style="display: inline-block;vertical-align: top;height: 26px;width: 26px;background: url({{asset('frontend/assets/img/sprite.png')}}) no-repeat;background-position: -62px -3px;" target="_blank"></a>
            </li>
            <li style="display: inline-block; vertical-align: top; padding: 5px 8px;">
              <a href="https://www.youtube.com/channel/UCOVeQPXBLvGVeTk-rVnnidA?view_as=subscriber" title="Youtube" style="display: inline-block; vertical-align: top; height: 23px;width: 26px; background: url({{asset('frontend/assets/img/sprite.png')}}) no-repeat; background-position: -98px 0px;" target="_blank"></a>
            </li>
            <li style="display: inline-block; vertical-align: top; padding: 5px 8px;">
              <a href="javascript:void(0);" title="Discord" style=" display: inline-block;vertical-align: top;height: 23px;width: 25px;background: url({{asset('frontend/assets/img/sprite.png')}}) no-repeat;background-position: -134px -2px;" target="_blank"></a>
            </li>
            <li style="display: inline-block; vertical-align: top; padding: 5px 8px;">
              <a href="https://www.twitch.tv/tagg_gaming" title="Twitch" style="display: inline-block;vertical-align: top;height: 26px;width: 25px;background: url({{asset('frontend/assets/img/sprite.png')}}) no-repeat; background-position: -169px -2px;" target="_blank"></a>
            </li>
          </ul>
      </div>
    </div>
  </div>
</body>

</html>
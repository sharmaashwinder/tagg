<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('dashboards.users.layout.head')
<body>
    <div class="outer-container">
        @include('dashboards.users.layout.header')
        <!-- header end -->
        <!-- banner start -->
        <div class="banner-outer bg-full">
            <div class="inner-container mt-5">
                <div class="row">
                    <div class="col-md-8 mx-auto">
                        <h2 class="heading-2 ">Donate <span>Token</span></h2>
                        <div class="pay_form">
                            <div class="col-md-12">
                                <div id="smart-button-container">
                                    <h2 class="heading-2 heading-2-sm mb-3">TG Token in your Wallet<span>
                                        TG: {{ $wallet->balance }}</span></h2>
                                    <h2 class="heading-2 heading-2-sm mb-3">Add Donate Amount to send<span>
                                            {{ $donateTo->name }}</span></h2>
                                    <h2 class="heading-2 heading-2-sm mb-3">Donate TG Token in between  <span>TG: {{ $donateMinMaxAmount['min_token_donate'] }} to TG: {{ $donateMinMaxAmount['max_token_donate'] }}</span></h2>
                                    <div style="text-align: center"><label for="amount"> </label>
                                        <form action="{{ route('transfer-token') }}" method="post">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $donateTo->id }}">
                                            <input type="hidden" name="name" value="{{ $donateTo->name }}">
                                            <input class="form-control" name="amount" min="{{ $donateMinMaxAmount['min_token_donate'] }}" max="{{ $donateMinMaxAmount['max_token_donate'] }}"
                                                type="number" id="amount" value="" placeholder="Add Amount 10" required>
                                                @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <input class="form-control" type="submit" value="Send">
                                    </div>
                                </div><br><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- popular start -->
        @include('dashboards.users.layout.popular')
        <!-- footer start -->
        @include('dashboards.users.layout.footer')
    </div>
</body>

</html>

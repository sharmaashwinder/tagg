<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<!-- <link rel="stylesheet" href="{{ asset('frontend/assets/css/paypalCard.css') }}"> -->
@include('dashboards.users.layout.head')

<body>
    <div class="outer-container">
        @include('dashboards.users.layout.header')
        <!-- header end -->
        <!-- banner start -->
        <div class="banner-outer banner-bg bg-color4 color2">
            <div class="form-container">
                <div class="personal-information">
                    <form method="post" action="{{ route('add-fund') }}">
                        @csrf
                        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-5 justify-content-center">
                            @include("layouts.flash")
                            <h1 class="heading-1">Add Fund into Your wallet</h1>
                            @foreach ($fundAmounts as $val)
                            <div class="form-check">
                                <input type="radio" class="btn-check" id="fundAmount{{ $val->key }}" value="{{ $val->key }}" name="fund_amount_key" autocomplete="off" required>
                                <label class="btn btn-primary" for="fundAmount{{ $val->key }}">Add - {{ $val->text }}</label>
                            </div>
                            @endforeach

                            <h1>Payment Information</h1>
                        </div> end of personal-information -->
                        <!-- <input id="column-left" type="text" name="first_name" placeholder="First Name" required="required" />
                        <input id="column-right" type="text" name="last_name" placeholder="Surname" required="required" />
                        <input type="text" id="cr_no" name="number" placeholder="Card Number" value="3" placeholder="0000 0000 0000 0000" minlength="12" maxlength="19" required="required" />
                        <input id="exp" type="text" name="expiry" placeholder="MM/YY" minlength="5" maxlength="5" required="required" />
                        <input id="column-right" type="text" name="cvc" placeholder="&#9679;&#9679;&#9679;" required="required" minlength="3" maxlength="3" />
                        <div class="card-wrapper"></div>
                        <input id="input-field" type="text" name="streetaddress" required="required" autocomplete="on" maxlength="45" placeholder="Streed Address" />
                        <input id="column-left" type="text" name="city" required="required" autocomplete="on" maxlength="20" placeholder="City" />
                        <input id="column-right" type="text" name="zipcode" required="required" autocomplete="on" pattern="[0-9]*" maxlength="5" placeholder="ZIP code" />
                        <input id="input-field" type="email" name="email" required="required" autocomplete="on" maxlength="40" placeholder="Email" />
                        <input id="input-button" name="submit" type="submit" value="Pay Now" />
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- popular start -->
    @include('dashboards.users.layout.popular')
    <!-- footer start -->
    @include('dashboards.users.layout.footer')
    </div>
</body>

</html>


<script src="{{ asset('frontend/assets/js/paypalCard.js') }}"></script>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<link rel="stylesheet" href="{{ asset('frontend/assets/css/paypalCard.css') }}">
@include('dashboards.users.layout.head')

<body>
    <div class="outer-container">
        @include('dashboards.users.layout.header')
        <!-- header end -->
        <div class="banner-outer banner-wallet">
            <div class="inner-container-sm">
                <div class="text-center py-lg-5 py-md-3 py-0">
                    <h1 class="heading-1 m-0">Wallet</h1>
                </div>
            </div>
        </div>
        <div class="common-space-md bg-color5">
            <div class="inner-container">
                <h2 class="title-1 mb-3">Available Balance</h2>
                <div class="bg-color3 p-4">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <div class="typo6">Total TG Token</div>
                            <div class="heading-2 d-flex align-items-center pt-2"><span
                                    class="tg light teal me-2">tg</span><span id="tgToken">{{ $walletAmount }}</span>
                            </div>
                        </div>
                        <div class="typo6">$1 Dollar = {{ $setting['token_value'] }} TG Tokens</div>
                        <div>
                            <a href="javascript:void(0);" title="Add Funds"
                                class="btn-yellow-border fill-yellow md px-4 text-uppercase me-3" data-bs-toggle="modal"
                                data-bs-target="#addFund">Add Funds</a>
                            <a href="javascript:void(0);" title="Withdraw"
                                class="btn-yellow-border md px-4 me-3 text-uppercase" data-bs-toggle="modal"
                                data-bs-target="#withdraw">Withdraw</a>
                        </div>
                    </div>
                </div>
                <div class="custom-row mt-md-5 mt-4">
                    <h3 class="title-1 mb-3">Transaction History</h3>
                    <div class="table-responsive">
                        <table class="table custom-table custom-tableHover p-md">
                            <thead>
                                <tr>
                                    <th scope="col">Transaction ID</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Tokens</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Amount</th>
                                    <th scope="col">Balance</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($paypalTransaction->count())
                                    @foreach ($paypalTransaction as $val)
                                        @php
                                            $date = new DateTime($val['created_at']);
                                            $dateTime = $date->format('d M, Y, h:i A');
                                            if ($val['transaction_for'] == 'add_token') {
                                                $token = $val['at_token_alloted'];
                                                $status = 'Purchase Tokens';
                                                $amount = '$'.$val['at_amount'];
                                                $transactionId = $val['transaction_id'];
                                            } else if($val['transaction_for'] == 'withdraw_token'){
                                                $token = $val['wt_token_withdraw'];
                                                $status = 'Withdraw Tokens';
                                                $amount = '$'.$val['wt_amount'];
                                                $transactionId = $val['transaction_id'];
                                            }else{
                                                $token = $val['t_amount'];
                                                $status = 'Transfer Tokens';
                                                $amount =   '-';
                                                $transactionId = $val['id'];
                                            }
                                        @endphp
                                        <tr onclick="transaction({{ $val['id'] }});">
                                            <td>{{ $transactionId }}</td>
                                            <td>{{ $dateTime }}</td>
                                            <td>
                                            <div class="d-flex align-items-center">
                                                <span class="tg me-2">tg</span> {{ $token }}
                                            </div>
                                            </td>
                                            <td>{{ $status }}</td>
                                            <td>{{ $amount }}</td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                <span class="tg me-2">tg</span> {{ $val['balance'] }}
                                                </div></td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan=7 scope="col">
                                            <div class="typo3 md text-center">No record found</div>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="page-items d-flex justify-content-end pt-3">
                        {{ @$paypalTransaction->onEachSide(1)->links() }}
                    </div>

                    <!-- <div class="d-flex justify-content-end pt-3">
                      <nav aria-label="Page navigation example">
                        <ul class="pagination">
                          <li class="page-item border-pre disabled">
                            <a class="page-link" href="javascript:void(0);"><span class="page-prev"></span></a>
                          </li>
                          <li class="page-item">
                            <a class="page-link" href="javascript:void(0);">1</a>
                          </li>
                          <li class="page-item">
                            <a class="page-link" href="javascript:void(0);">2</a>
                          </li>
                          <li class="page-item">
                            <a class="page-link" href="javascript:void(0);">3</a>
                          </li>
                          <li class="page-item border-next">
                            <a class="page-link" href="javascript:void(0);"><span class="page-next"></span></a>
                          </li>
                        </ul>
                      </nav>
                    </div> -->
                </div>
            </div>
        </div>
        <!-- popular start -->

        <!-- footer start -->
        @include('dashboards.users.layout.footer')
    </div>
    {{-- Add Funds Modal --}}
    <div class="modal fade" id="addFund" tabindex="-1" aria-labelledby="withdrawLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content bg-color2 modal-box">
                <span class="trianle-tl"></span>
                <span class="trianle-tr"></span>
                <span class="trianle-bl"></span>
                <span class="trianle-br"></span>
                <div class="modal-header cpx-2">
                    <h5 class="heading-3 md mb-0 py-2">Add Funds</h5>
                    <button type="button" class="custom-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body cpx-2 py-4">
                    <div class="custom-row" id="smart-button-container">
                        <div class="custom-row my-3">
                            <input class="form-control input-transparent" name="amountInput"
                                min="{{ $setting['min_token_add'] }}" max="{{ $setting['max_token_add'] }}"
                                type="number" id="amount" value="" placeholder="Add Amount 100" required>
                        </div>
                        <p id="priceLabelError" style="visibility: hidden; color:red; text-align: center;">
                            Please enter Amount between {{ $setting['min_token_add'] }} to
                            {{ $setting['max_token_add'] }}</p>
                        <div class="custom-row my-3">
                            <div style="text-align: center; margin-top: 0.625rem;" id="paypal-button-container"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Withdraw Funds Modal --}}
    <div class="modal fade" id="withdraw" tabindex="-1" aria-labelledby="withdrawLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content bg-color2 modal-box">
                <span class="trianle-tl"></span>
                <span class="trianle-tr"></span>
                <span class="trianle-bl"></span>
                <span class="trianle-br"></span>
                <div class="modal-header cpx-2">
                    <h5 class="heading-3 md mb-0 py-2">Enter Paypal details</h5>
                    <button type="button" class="custom-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body cpx-2 py-4">
                    <div class="custom-row">
                        <form action="{{ route('withdraw') }}" method="post">
                            @csrf
                            <div class="custom-row my-3">
                                <input class="form-control input-transparent" name="withdrawToken"
                                    min="{{ $setting['min_token_withdraw'] }}"
                                    max="{{ $setting['max_token_withdraw'] }}" type="number" id="withdrawToken"
                                    value="" placeholder="Add TG Tokens 5000" required>
                            </div>
                            <div class="custom-row my-3">
                                <input class="form-control input-transparent" name="withdrawEmail" type="email"
                                    id="withdrawEmail" value="" placeholder="Email" required>
                            </div>
                            <div class="custom-row my-3">
                                <input class="btn-yellow-fill md custom-row text-uppercase text-center" type="submit"
                                    value="withdraw">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Transection Status Modal -->
    <div class="modal fade" id="transectionStatusModal" tabindex="-1" aria-labelledby="withdrawLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content bg-color2 modal-box">
                <span class="trianle-tl"></span>
                <span class="trianle-tr"></span>
                <span class="trianle-bl"></span>
                <span class="trianle-br"></span>
                <button type="button" class="custom-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body cpx-2 py-4">
                    <div class="modal-body cpx-2 py-md-5 py-4">
                        <div class="custom-row text-center py-md-4 py-3">
                            <em class="check-mark-border"></em>
                            <div class="heading-6 md my-3" id="message_status"></div>
                            <p class="typo3 md" id="message"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Transection Status Modal -->
    <div class="modal fade" id="transactionInfoModal" tabindex="-1" aria-labelledby="withdrawLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content bg-color2 modal-box">
                <span class="trianle-tl"></span>
                <span class="trianle-tr"></span>
                <span class="trianle-bl"></span>
                <span class="trianle-br"></span>
                <button type="button" class="custom-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body py-4">
                    <div class="modal-body">
                        <div class="custom-row py-md-4 py-3">
                            <div id="infoMessage">
                            </div>
                            <div id="infoPurchase">
                                <h3 class="heading-3 md mb-3 py-2 text-center">Purchase Tokens</h3>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Transaction ID :</div>
                                    <div class="col-md-7"><span id="pTransactionId"></span></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Transaction Date :</div>
                                    <div class="col-md-7"><span id="pTransactionDate"></span></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Payer Name :</div>
                                    <div class="col-md-7"><span id="pPayerName"></span></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Payer Email :</div>
                                    <div class="col-md-7"><span id="pPayerEmail"></span></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Amount :</div>
                                    <div class="col-md-7"><span id="pAmount"></span></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Paypal Fee :</div>
                                    <div class="col-md-7"><span id="pPaypalFee"></span></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Tagg Fee :</div>
                                    <div class="col-md-7"><span id="pTaggFee"></span></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Final Amount :</div>
                                    <div class="col-md-7"><span id="pFinalAmount"></span></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Tokens Added :</div>
                                    <div class="col-md-7"><span id="pTokenAdded"></span></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">TG Token Balance :</div>
                                    <div class="col-md-7"><span id="pTgTokenBalance"></span></div>
                                </div>
                            </div>
                            <div id="infoWithdraw">
                                <h3 class="heading-3 md mb-3 py-2 text-center">Withdraw Tokens</h3>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Transaction ID :</div>
                                    <div class="col-md-7"><span id="wTransactionId"></span></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Transaction Date :</div>
                                    <div class="col-md-7"><span id="wTransactionDate"></span></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Reciver :</div>
                                    <div class="col-md-7"><span id="wReceiver"></span></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Withdraw Tokens :</div>
                                    <div class="col-md-7"><span id="wWithdrawToken"></span></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Amount :</div>
                                    <div class="col-md-7"><span id="wAmount"></span></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Tagg Fee :</div>
                                    <div class="col-md-7"><span id="wTaggFee"></span></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Withdraw Amount :</div>
                                    <div class="col-md-7"><span id="wWithdrawAmount"></span></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">TG Token Balance :</div>
                                    <div class="col-md-7"><span id="wTgTokenBalance"></span></div>
                                </div>
                            </div>
                            <div id="infoTransfer">
                                <h3 class="heading-3 md mb-3 py-2 text-center">Transfer Tokens</h3>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Transaction ID :</div>
                                    <div class="col-md-7"><span id="tTransactionId"></span></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Transaction Date :</div>
                                    <div class="col-md-7"><span id="tTransactionDate"></span></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Send To :</div>
                                    <div class="col-md-7"><a id="tReceiverProfile" href=""><span id="tReceiverName"></span><a></div>
                                </div>
                                <div class="row typo3 md">
                                    <div class="col-md-5 text-end">Transfer Tokens :</div>
                                    <div class="col-md-7"><span id="tTransferToken"></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>

</html>
@section('script')

    <script type="text/javascript">
        function transaction(id) {
            $("#infoPurchase").hide();
            $("#infoWithdraw").hide();
            $("#infoTransfer").hide();
            $('#infoMessage').html('<div class="spinner-border text-primary"></div>');
            $('#transactionInfoModal').modal('show');
            $.ajax({
                url: "{{ route('transaction-info') }}",
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id
                },
                dataType: 'JSON',
                success: function(data) {
                    if(data.transactionFor == 'add_token'){
                        $('#pTransactionId').html(data.transactionId);
                        $('#pTransactionDate').html(data.transactionDate);
                        $('#pPayerName').html(data.payerName);
                        $('#pPayerEmail').html(data.payerEmail);
                        $('#pAmount').html(data.amount);
                        $('#pPaypalFee').html(data.paypalFee);
                        $('#pTaggFee').html(data.taggFee);
                        $('#pFinalAmount').html(data.finalAmount);
                        $('#pTokenAdded').html(data.tokenAdded);
                        $('#pTgTokenBalance').html(data.tgTokenBalance);
                        $('#infoMessage').html('');
                        $("#infoPurchase").show();
                        $("#infoWithdraw").hide();
                        $("#infoTransfer").hide();
                    }else if(data.transactionFor == 'withdraw_token'){
                        $('#wTransactionId').html(data.transactionId);
                        $('#wTransactionDate').html(data.transactionDate);
                        $('#wReceiver').html(data.receiver);
                        $('#wWithdrawToken').html(data.withdrawToken);
                        $('#wAmount').html(data.amount);
                        $('#wTaggFee').html(data.taggFee);
                        $('#wWithdrawAmount').html(data.withdrawAmount);
                        $('#wTgTokenBalance').html(data.tgTokenBalance);
                        $('#infoMessage').html('');
                        $("#infoPurchase").hide();
                        $("#infoWithdraw").show();
                        $("#infoTransfer").hide();
                    }else if(data.transactionFor == 'transfer_token'){
                        $('#tTransactionId').html(data.transactionId);
                        $('#tTransactionDate').html(data.transactionDate);
                        $('#tReceiverName').html(data.receiverName);
                        $('#tReceiverProfile').attr("href", data.userLink);
                        $('#tTransferToken').html(data.transferToken);
                        $('#infoMessage').html('');
                        $("#infoPurchase").hide();
                        $("#infoWithdraw").hide();
                        $("#infoTransfer").show();
                    }
                }
            });
        }
        @if (Session::has('withdrawStatus'))
            window.onload = function() {
            $('#message_status').html("{{ Session::get('withdrawStatus') }}");
            $('#message').html("{{ Session::get('withdrawMessage') }}");
            $('#transectionStatusModal').modal('show');
            };
        @endif
    </script>

    <script
        src="https://www.paypal.com/sdk/js?client-id=ASj4pT6f9C7edMZvCbclwIyyvAF6mgTozRthl6FxgyGHgcU2VkyLR04KQ-2ICJBlqmr3iy_XB9wyqOts&enable-funding=venmo&currency=USD"
        data-sdk-integration-source="button-factory"></script>
    <script>
        function initPayPalButton() {
            var maxAddAmount = "{{ $setting['max_token_add'] }}";
            var minAddAmount = "{{ $setting['min_token_add'] }}";
            var amountValue = '0';
            var amount = document.querySelector('#smart-button-container #amount');
            var priceError = document.querySelector('#smart-button-container #priceLabelError');
            var amountArr = [amount];
            $("#amount").keyup(function() {
                amountValue = $('#amount').val();
            });

            function validate(event) {
                if (parseInt(event.value) <= parseInt(maxAddAmount) && parseInt(event.value) >= parseInt(minAddAmount) &&
                    parseInt(event.value) % 1 === 0) {
                    return true;
                } else {
                    return false;
                }
            }

            paypal.Buttons({
                style: {
                    color: 'blue',
                    shape: 'rect',
                    label: 'pay',
                    layout: 'horizontal',
                    tagline: false
                },

                onInit: function(data, actions) {
                    actions.disable();
                    amountArr.forEach(function(item) {
                        item.addEventListener('keyup', function(event) {
                            var result = amountArr.every(validate);
                            if (result) {
                                actions.enable();
                            } else {
                                actions.disable();
                            }
                        });
                    });
                },
                onClick: function() {
                    if (parseInt(amount.value) <= parseInt(maxAddAmount) && parseInt(amount.value) >= parseInt(
                            minAddAmount) && parseInt(amount.value) % 1 === 0) {
                        priceError.style.visibility = "hidden";
                    } else {
                        priceError.style.visibility = "visible";
                    }
                },

                // Call your server to set up the transaction
                createOrder: function(data, actions) { //alert(amountValue);
                    var _token = "{{ csrf_token() }}";
                    return fetch('{{ route('createOrder') }}', {
                        method: 'post',
                        headers: {
                            'X-CSRF-TOKEN': _token,
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            amount: amountValue,
                        })
                    }).then(function(res) {
                        return res.json();
                    }).then(function(orderData) {
                        return orderData.result.id;
                    });
                },
                // Call your server to finalize the transaction
                onApprove: function(data, actions) {
                    var _token = "{{ csrf_token() }}";
                    return fetch('/captureOrder/' + data.orderID + '/capture/', {
                        method: 'post',
                        headers: {
                            'X-CSRF-TOKEN': _token,
                            'Content-Type': 'application/json',
                        },
                    }).then(function(res) {
                        return res.json();
                    }).then(function(orderData) {
                        // Three cases to handle:
                        //   (1) Recoverable INSTRUMENT_DECLINED -> call actions.restart()
                        //   (2) Other non-recoverable errors -> Show a failure message
                        //   (3) Successful transaction -> Show a success / thank you message
                        // Your server defines the structure of 'orderData', which may differ
                        var errorDetail = Array.isArray(orderData.details) && orderData.details[
                            0];
                        if (errorDetail && errorDetail.issue === 'INSTRUMENT_DECLINED') {
                            // Recoverable state, see: "Handle Funding Failures"
                            return actions.restart();
                        }
                        if (errorDetail) {
                            var msg = 'Sorry, your transaction could not be processed.';
                            if (errorDetail.description) msg += '\n\n' + errorDetail
                                .description;
                            if (orderData.debug_id) msg += ' (' + orderData.debug_id + ')';
                            // Show a failure message
                            return alert(msg);
                        }
                        // Show a success message to the buyer

                        if (orderData.result.status == 'COMPLETED') {
                            var tgToken = parseFloat($('#tgToken').html());
                            tgToken = (tgToken) + parseFloat(orderData.tokenAlloted);
                            $('#tgToken').html(tgToken);
                            $('#message_status').html("COMPLETED");
                            $('#message').html(orderData.tokenAlloted +
                                " TG Token added successfully in your wallet");
                        } else {
                            $('#message_status').html("ISSUE");
                            $('#message').html("There are some issue with the payment");
                        }
                        $('#addFund').modal('hide');
                        $('#transectionStatusModal').modal('show');
                        $('#amount').val('')
                    });
                }
            }).render('#paypal-button-container');
        }
        initPayPalButton();
    </script>
    @include('layouts.flash')

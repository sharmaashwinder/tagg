<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('dashboards.users.layout.head')

<body>
    <div class="outer-container">
        @include('dashboards.users.layout.header')
        <!-- header end -->
        <!-- banner start -->
        <div class="banner-outer banner-about">
            <div class="inner-container">
                <div class="text-center py-lg-5 py-md-4 py-0 my-lg-5 my-md-4 my-0">
                    <h1 class="heading-1 pt-lg-4 pt-md-2 pt-0 pb-2">About TAGG</h1>
                    <h2 class="heading-2 pb-1 pb-lg-3"><span>Subheading Will be here</span></h2>
                    <p class="typo1">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s typesetting industry.</p>
                    <div class="custom-row pt-lg-4 pt-3">
                        <a class="border-btn2 custom-col" href="{{ route('contact-us') }}" title="Contact Us">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- banner end -->

        <!-- about our company start   -->
        <div class="common-space bg-full bg-full-2">
            <div class="inner-container">
                <div class="row align-items-center justify-content-between">
                    <div class="col-md-6">
                        <div>
                            <img src="frontend/assets/img/gaming.jpg" class="w-100" alt="man playing game image" width="559" height="auto">
                        </div>
                    </div>
                    <div class="col-md-6 mt-md-0 mt-3">
                        <div class="px-lg-2">
                            <div>
                                <h2 class="heading-1">About Our <span class="theme-color">Company</span></h2>
                            </div>
                            <p class="typo2 md pt-2">We’re TAGG Gaming, and video games are our passion. Competition is at the heart of what we do. We are here to provide gamers like you a platform to showcase your skill and get rewarded doing what you love, playing video games! Our vision is to provide tournaments where people of all ages can come and play. Whether you are a seasoned vet, or this is your first rodeo. Enjoy casual competition in the free tournaments, or squad up and take on the best while competing for a cash prize.</p>
                            <p class="typo2 md py-2">Are you a streamer and want to grow your views? Play in our tournaments to be featured on our live streams! Your highlights might even make it on the front page! Your journey to the top begins right here, and we’re here to support you every step of the way.</p>
                            <div class="pt-2">
                                <a class="arrow-btn" href="javascript:void(0);" title="Discover More">Continue Reading</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="common-space remote-sec">
            <div class="inner-container-xs text-center">
                <div class="text-center">
                    <h2 class="heading-2 heading-2-white">Lorem ipsum is <span class="theme-color-2">simply dummy</span> <br> text of the printing</h2>
                </div>
                <p class="typo2 md pt-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi veritatis reiciendis perferendis, excepturi dolorum earum facere tempora architecto tenetur voluptate voluptatibus. Totam minima inventore voluptates voluptas eum beatae.</p>
                <div class="custom-row mt-5">
                    <a class="border-btn2 custom-col" href="javascript:void(0);" title="Join Tagg">join tagg</a>
                </div>
            </div>
        </div>
        <!-- about our company end  -->

        <!-- after join us start  -->
        <div class="common-space bg-color2">
            <div class="inner-container">
                <div class="row align-items-center justify-content-between pb-5">
                    <div class="col-md-6 py-md-0 py-3">
                        <h2 class="heading-2">Our <span>Mission</span></h2>
                        <p class="typo2 md pt-2">To create positive competitive based gaming environment for professional / casual gamers alike. A community that is safe and dedicated to the individual interest of all gamers no matter the skill level.</p>
                        <p class="typo2 md py-2">A community for everyone to find participation, or even just watch gamers play against one another, support their favorites and have real possible opportunities to connect with there favorite players.</p>
                        <div class="pt-2">
                            <a class="arrow-btn" href="javascript:void(0);" title="Discover More">Continue Reading</a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <img src="frontend/assets/img/working-people.jpg" alt="blue joystick image" width="559" height="auto" class="w-100">
                    </div>
                </div>
                <div class="row align-items-center justify-content-between py-lg-5">
                    <div class="col-md-6">
                        <img src="frontend/assets/img/gaming.jpg" alt="black joystick image" width="559" height="auto" class="w-100">
                    </div>
                    <div class="col-md-6 py-md-0 py-3">
                        <h2 class="heading-2 ">Lorem ipsum <span>simply</span></h2>
                        <p class="typo2 md pt-2">Gaming is the new way people come together to hang out. People are spending more time on
                            electronic devices. People are spending more time connecting with people through the
                            internet. TAGG is going to serve as a connection hub, a bridge one might say between gamer’s
                            and social media.</p>
                        <p class="typo2 md py-2">TAGG is also going to there very own walking, talking, breathing and sleeping
                            representation of these gamers. As these gamers of all ages come together and win the
                            tournaments, they are going to help build their brand, their image, and their platform being
                            built as TAGG plans to endorse the champions.</p>
                        <div class="pt-2">
                            <a class="arrow-btn" href="javascript:void(0);" title="Discover More">Continue Reading</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- after join us ends  -->

        <!-- technology we use section start  -->
        <div class="common-space bg-color5 tech-container">
            <div class="inner-container">
                <div class="row">
                    <div class="col-md-6">
                        <div>
                            <h2 class="heading-1">Technology We <span class="theme-color">Use</span></h2>
                        </div>
                        <p class="typo2 md pt-2">Security is a top priority. TAGG will have always have an opportunity to stay on the leading edge
                            of technology. Here we can utilize fingerprint authentication, google authentication and
                            password protection.</p>
                        <p class="typo2 md py-2">An area that will be a constant focus on monitoring the block chain
                            technology that is being used in the crypto industry. We will maintain constant focus on the
                            development in this tech as this tech continues to develop TAGG will make
                            Protecting people’s information where it is not to be sold or distributed will be a goal of TAGG.</p>
                    </div>
                </div>
            </div>
            <img class="vr-player" src="frontend/assets/img/vr-player.png" alt="vr player image" width="895" height="auto">
        </div>
        <!-- technology we use section ends  -->

        <!-- ceo and founder section start  -->
        <div class="common-space bg-image2 d-none">
            <div class="inner-container">
                <div class="text-center">
                    <h2 class="heading-1">CEO & <span class="theme-color">Founders</span></h2>
                    <div class="custom-row pt-lg-4 pt-md-3">
                        <ul class="d-flex align-items-center justify-content-center picture-top-list">
                            <li class="rotated-img-col d-flex align-items-center justify-content-center">
                                <div class="rotated-img-inner d-flex align-items-center justify-content-center">
                                    <img class="auto-img" src="{{asset('frontend/assets/img/ceo.jpg')}}" width="539" height="539" alt="Tagg Us Logo" />
                                    <div class="pic-detail">
                                        <h4 class="heading-6 text-nowrap">Ronny Saints</h4>
                                        <p class="heading-4 md text-nowrap pt-1">CEO & Founder</p>
                                    </div>
                                </div>
                            </li>
                            <li class="rotated-img-col d-flex align-items-center justify-content-center">
                                <div class="rotated-img-inner d-flex align-items-center justify-content-center">
                                    <img class="auto-img" src="{{asset('frontend/assets/img/founder.jpg')}}" width="539" height="539" alt="Tagg Us Logo" />
                                    <div class="pic-detail">
                                        <h4 class="heading-6 text-nowrap">Tagg</h4>
                                        <p class="heading-4 md text-nowrap pt-1">CEO</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="py-4">
                        <h2 class="heading-1">Team <span class="theme-color">Tagg Us</span></h2>
                    </div>
                    <ul class="d-flex align-items-center justify-content-center picture-list-sm">
                        <li class="rotated-img-col d-flex align-items-center justify-content-center">
                            <div class="rotated-img-inner d-flex align-items-center justify-content-center">
                                <img class="auto-img" src="{{asset('frontend/assets/img/employee1.jpg')}}" width="306" height="306" alt="Employee image" />
                                <div class="pic-detail">
                                    <h4 class="heading-6 text-nowrap">Tagg</h4>
                                    <p class="heading-4 md text-nowrap pt-1">Dev Lead</p>
                                </div>
                            </div>
                        </li>
                        <li class="rotated-img-col d-flex align-items-center justify-content-center">
                            <div class="rotated-img-inner d-flex align-items-center justify-content-center">
                                <img class="auto-img" src="{{asset('frontend/assets/img/employee2.jpg')}}" width="306" height="306" alt="Employee image" />
                                <div class="pic-detail">
                                    <h4 class="heading-6 text-nowrap">Tagg</h4>
                                    <p class="heading-4 md text-nowrap pt-1">UI Lead</p>
                                </div>
                            </div>
                        </li>
                        <li class="rotated-img-col d-flex align-items-center justify-content-center">
                            <div class="rotated-img-inner d-flex align-items-center justify-content-center">
                                <img class="auto-img" src="{{asset('frontend/assets/img/employee1.jpg')}}" width="306" height="306" alt="Employee image" />
                                <div class="pic-detail">
                                    <h4 class="heading-6 text-nowrap">Tagg</h4>
                                    <p class="heading-4 md text-nowrap pt-1">Graphic Lead</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <ul class="d-flex align-items-center justify-content-center picture-list-sm">
                        <li class="rotated-img-col d-flex align-items-center justify-content-center">
                            <div class="rotated-img-inner d-flex align-items-center justify-content-center">
                                <img class="auto-img" src="{{asset('frontend/assets/img/employee2.jpg')}}" width="306" height="306" alt="Employee image" />
                                <div class="pic-detail">
                                    <h4 class="heading-6 text-nowrap">Tagg</h4>
                                    <p class="heading-4 md text-nowrap pt-1">Graphic Lead</p>
                                </div>
                            </div>
                        </li>
                        <li class="rotated-img-col d-flex align-items-center justify-content-center">
                            <div class="rotated-img-inner d-flex align-items-center justify-content-center">
                                <img class="auto-img" src="{{asset('frontend/assets/img/employee1.jpg')}}" width="306" height="306" alt="Employee image" />
                                <div class="pic-detail">
                                    <h4 class="heading-6 text-nowrap">Tagg</h4>
                                    <p class="heading-4 md text-nowrap pt-1">Graphic Lead</p>
                                </div>
                            </div>
                            <div class="pic-detail">
                                <h4 class="heading-6 text-nowrap">Tagg</h4>
                                <p class="heading-4 md text-nowrap pt-1">Graphic Lead</p>
                            </div>
                        </li>
                        <li class="rotated-img-col d-flex align-items-center justify-content-center">
                            <div class="rotated-img-inner d-flex align-items-center justify-content-center">
                                <img class="auto-img" src="{{asset('frontend/assets/img/employee2.jpg')}}" width="306" height="306" alt="Employee image" />
                                <div class="pic-detail">
                                    <h4 class="heading-6 text-nowrap">Tagg</h4>
                                    <p class="heading-4 md text-nowrap pt-1">Graphic Lead</p>
                                </div>
                            </div>
                        </li>
                        <li class="rotated-img-col d-flex align-items-center justify-content-center">
                            <div class="rotated-img-inner d-flex align-items-center justify-content-center">
                                <img class="auto-img" src="{{asset('frontend/assets/img/employee1.jpg')}}" width="306" height="306" alt="Employee image" />
                                <div class="pic-detail">
                                    <h4 class="heading-6 text-nowrap">Tagg</h4>
                                    <p class="heading-4 md text-nowrap pt-1">Graphic Lead</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <ul class="d-flex align-items-center justify-content-center picture-list-sm">
                        <li class="rotated-img-col d-flex align-items-center justify-content-center">
                            <div class="rotated-img-inner d-flex align-items-center justify-content-center">
                                <img class="auto-img" src="{{asset('frontend/assets/img/employee2.jpg')}}" width="306" height="306" alt="Employee image" />
                                <div class="pic-detail">
                                    <h4 class="heading-6 text-nowrap">Tagg</h4>
                                    <p class="heading-4 md text-nowrap pt-1">Graphic Lead</p>
                                </div>
                            </div>
                        </li>
                        <li class="rotated-img-col d-flex align-items-center justify-content-center">
                            <div class="rotated-img-inner d-flex align-items-center justify-content-center">
                                <img class="auto-img" src="{{asset('frontend/assets/img/employee1.jpg')}}" width="306" height="306" alt="Employee image" />
                                <div class="pic-detail">
                                    <h4 class="heading-6 text-nowrap">Tagg</h4>
                                    <p class="heading-4 md text-nowrap pt-1">Graphic Lead</p>
                                </div>
                            </div>
                        </li>
                        <li class="rotated-img-col d-flex align-items-center justify-content-center">
                            <div class="rotated-img-inner d-flex align-items-center justify-content-center">
                                <img class="auto-img" src="{{asset('frontend/assets/img/employee2.jpg')}}" width="306" height="306" alt="Employee image" />
                                <div class="pic-detail">
                                    <h4 class="heading-6 text-nowrap">Tagg</h4>
                                    <p class="heading-4 md text-nowrap pt-1">Graphic Lead</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- ceo and founder section end  -->

        <!-- popular start -->
        @include('dashboards.users.layout.popular')
        <!-- Subscribe start -->
        @include('dashboards.users.layout.subscribe')
        <!-- footer start -->
        @include('dashboards.users.layout.footer')
    </div>

</body>

</html>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('dashboards.users.layout.head')

<body>
    <div class="outer-container">
    @include('dashboards.users.layout.header')
        <!-- header end -->
        <!-- banner start -->
        <div class="banner-outer banner-faq">
            <div class="inner-container-sm">
                <div class="text-center py-lg-5 py-md-4 py-0 my-lg-5 my-md-4 my-0">
                    <h1 class="heading-1 pt-lg-4 pt-md-2 pt-0 pb-3">Frequently asked questions</h1>
                    <p class="typo1 md ellipse3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s typesetting industry.</p>
                </div>
            </div>
        </div>
        <!-- banner end -->
        <!-- Tabs section -->
        <div class="common-space-md bg-color3">
            <div class="inner-container-sm">
                <ul class="nav nav-tabs custom-tabs justify-content-center" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <div class="nav-btn d-flex flex-column align-items-center justify-content-center active" id="home-tab" data-bs-toggle="tab" data-bs-target="#tournament" type="button" role="tab" aria-controls="home" aria-selected="true">
                            <div class="d-flex align-items-center tab-icon">
                                <img class="img-fluid" src="frontend/assets/img/tournament.png" width="50" height="50" alt="Tournament icon" />
                            </div>
                            <h2 class="heading-4 mt-2">Tournaments</h2>
                        </div>
                    </li>
                    <li class="nav-item" role="presentation">
                        <div class="nav-btn d-flex flex-column align-items-center justify-content-center" id="profile-tab" data-bs-toggle="tab" data-bs-target="#challenges" type="button" role="tab" aria-controls="profile" aria-selected="false">
                            <div class="d-flex align-items-center tab-icon">
                                <img class="img-fluid" src="frontend/assets/img/challenge.png" width="54" height="40" alt="Challenge icon" />
                            </div>
                            <h2 class="heading-4 mt-2">Challenges</h2>
                        </div>
                    </li>
                    <li class="nav-item" role="presentation">
                        <div class="nav-btn d-flex flex-column align-items-center justify-content-center" id="contact-tab" data-bs-toggle="tab" data-bs-target="#membership" type="button" role="tab" aria-controls="contact" aria-selected="false">
                            <div class="d-flex align-items-center tab-icon">
                                <img class="img-fluid" src="frontend/assets/img/member.png" width="54" height="54" alt="Tournament icon" />
                            </div>
                            <h2 class="heading-4 mt-2">Membership</h2>
                        </div>
                    </li>
                    <li class="nav-item" role="presentation">
                        <div class="nav-btn d-flex flex-column align-items-center justify-content-center" id="contact-tab" data-bs-toggle="tab" data-bs-target="#giveaway" type="button" role="tab" aria-controls="contact" aria-selected="false">
                            <div class="d-flex align-items-center tab-icon">
                                <img class="img-fluid" src="frontend/assets/img/unboxing.png" width="46" height="50" alt="Give Away icon" />
                            </div>
                            <h2 class="heading-4 mt-2">Give Away</h2>
                        </div>
                    </li>
                    <li class="nav-item" role="presentation">
                        <div class="nav-btn d-flex flex-column align-items-center justify-content-center" id="contact-tab" data-bs-toggle="tab" data-bs-target="#donate" type="button" role="tab" aria-controls="contact" aria-selected="false">
                            <div class="d-flex align-items-center tab-icon">
                                <img class="img-fluid" src="frontend/assets/img/donation.png" width="50" height="52" alt="Donate icon" />
                            </div>
                            <h2 class="heading-4 mt-2">Donate</h2>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- tab section end -->
        <!-- tabs data started -->
        <div class="tab-content common-space bg-full" id="myTabContent">

            <div class="inner-container-sm tab-pane fade show active" id="tournament" role="tabpanel" aria-labelledby="home-tab">
                <h2 class="heading-1 md"><span class="theme-color md">tournaments</span></h2>
                <!-- accordions started -->
                <div class="accordion custom-accordion my-lg-4 my-3" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Question will be bere
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body typo2">
                                It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Question will be bere
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div class="accordion-body typo2">
                                It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Question will be bere
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                            <div class="accordion-body typo2">
                                It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables.
                            </div>
                        </div>
                    </div>

                </div>
                <!-- accordions end -->
            </div>
            <div class="inner-container-sm tab-pane fade" id="challenges" role="tabpanel" aria-labelledby="profile-tab">
                <h2 class="heading-1 md"><span class="theme-color md">Challenges</span></h2>
                <div class="accordion custom-accordion my-lg-4 my-3" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Question will be bere
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body typo2">
                                It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Question will be bere
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div class="accordion-body typo2">
                                It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Question will be bere
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                            <div class="accordion-body typo2">
                                It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables.
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="inner-container-sm tab-pane fade" id="membership" role="tabpanel" aria-labelledby="contact-tab">
                <h2 class="heading-1 md"><span class="theme-color md">Membership</span></h2>
                <div class="accordion custom-accordion my-lg-4 my-3" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Question will be bere
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body typo2">
                                It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Question will be bere
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div class="accordion-body typo2">
                                It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Question will be bere
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                            <div class="accordion-body typo2">
                                It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables.
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="inner-container-sm tab-pane fade" id="giveaway" role="tabpanel" aria-labelledby="contact-tab">
                <h2 class="heading-1 md"><span class="theme-color md">Give Away</span></h2>
                <div class="accordion custom-accordion my-lg-4 my-3" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Question will be bere
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body typo2">
                                It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Question will be bere
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div class="accordion-body typo2">
                                It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Question will be bere
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                            <div class="accordion-body typo2">
                                It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables.
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="inner-container-sm tab-pane fade" id="donate" role="tabpanel" aria-labelledby="contact-tab">
                <h2 class="heading-1 md"><span class="theme-color md">Donate</span></h2>
                <div class="accordion custom-accordion my-lg-4 my-3" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Question will be bere
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body typo2">
                                It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Question will be bere
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div class="accordion-body typo2">
                                It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Question will be bere
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                            <div class="accordion-body typo2">
                                It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables.
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Tabs section end -->
        <!-- popular start -->
        @include('dashboards.users.layout.popular')
        <!-- Subscribe start -->
        @include('dashboards.users.layout.subscribe')
        <!-- footer start -->
        @include('dashboards.users.layout.footer')
    </div>

</body>

</html>
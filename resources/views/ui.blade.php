<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('dashboards.users.layout.head')

<body>
    <div class="outer-container">
        <header class="header py-2 py-lg-2 header-top border-full">
            <div class="inner-container">
                <div class="navbar navbar-expand-lg py-1 justify-content-between">
                    <div class="">
                        <a href="/" title="Tagg">
                            <img class="top-logo" src="{{asset('frontend/assets/img/tagg-logo.png')}}" width="194" height="82" alt="Tagg Logo" />
                        </a>
                    </div>
                    <div class="d-flex">
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon d-flex align-items-center"><em></em></span>
                        </button>
                        <nav class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav me-auto nav-left">
                                <li class="nav-item ">
                                    <a class="nav-link homeClsss {{ (request()->is('/*')) ? 'active' : '' }}  " id="home" aria-current="page" href="{{ url('/') }}" title="Home">Home</a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link aboutCls {{ (request()->is('about*')) ? 'active' : '' }}" id="about" aria-current="page" href="{{ route('about') }}" title="About TAGG">About TAGG</a>
                                </li>
                            </ul>
                        </nav>
                        <nav class="collapse navbar-collapse">
                            <ul class="navbar-nav me-auto nav-right">
                                <li class="nav-item">
                                    <div class="border-white d-flex align-items-center justify-content-center">
                                        <span class="bg-rounded d-flex align-items-center justify-content-center me-2">TG</span>254
                                    </div>
                                </li>
                                <li class="nav-item dropdown no-arrow">
                                    <a class="dropdown-toggle d-flex align-items-center justify-content-center" href="javascript:void(0);" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        <em class="notification-icon"></em>
                                        <span class="badge-top d-flex align-items-center justify-content-center">5<span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-end dropdown-theme dropdown-left" aria-labelledby="navbarDropdown">
                                        <li class="typo8">Today</li>
                                        <li class="d-flex typo8 py-2">
                                            <div class="rounded-img-xs me-md-2 me-2">
                                                <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                                            </div>
                                            <div>
                                                <p class="py-3"><strong class="text-white">Barry Allen</strong> Starting Following You.</p>
                                                <div>
                                                    <a href="javascript:void(0);" title="Accept" class="btn-yellow-fill sm text-uppercase text-center custom-col me-2">Accept</a>
                                                    <a href="javascript:void(0);" title="Decline" class="btn-yellow-border sm text-uppercase text-center custom-col">Decline</a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="d-flex typo8 py-2">
                                            <div class="rounded-img-xs me-md-2 me-2">
                                                <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                                            </div>
                                            <div>
                                                <p class="py-3"><strong class="text-white">Barry Allen</strong> Starting Following You.</p>
                                                <div>
                                                    <a href="javascript:void(0);" title="Accept" class="btn-yellow-fill sm text-uppercase text-center custom-col me-2">Accept</a>
                                                    <a href="javascript:void(0);" title="Decline" class="btn-yellow-border sm text-uppercase text-center custom-col">Decline</a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="typo8 pt-3">Yesterday</li>
                                        <li class="d-flex typo8 py-2">
                                            <div class="rounded-img-xs me-md-2 me-2">
                                                <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                                            </div>
                                            <div>
                                                <p class="py-3"><strong class="text-white">Barry Allen</strong> Starting Following You.</p>
                                                <div>
                                                    <a href="javascript:void(0);" title="Accept" class="btn-yellow-fill sm text-uppercase text-center custom-col me-2">Accept</a>
                                                    <a href="javascript:void(0);" title="Decline" class="btn-yellow-border sm text-uppercase text-center custom-col">Decline</a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!-- header end -->
        <!-- banner start -->
        <div class="banner-outer banner-bg">
            <div class="inner-container">
               <div class="row align-items-center justify-content-between">
                  <div class="col-sm-3">
                     <div class="py-3 py-md-4 py-lg-5">
                        <figure class="full-width-image text-center">  <img src="http://localhost:8000/frontend/assets/img/game-img2.jpg" alt="Tournament Icon" class="img-fluid" width="199" height="187">
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0 text-center">Reg. Ends In<span class="yellow-text"> 42:45 Min</span></h3>
                        <div class="input-group mt-3">
                           <div class="auto-row">
                              <input type="file" name="profile_image" id="profile_image" onchange="loadPreview(this);" class="form-control element-hidden">
                              <button onclick="uploadpic()" title="Add Cover Photo" id="upload" class="share-btn md custom-row text-center text-uppercase px-4 text-nowrap"><em class="share-yellow me-2"></em>SHARE</button>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-9 d-md-block d-none">
                     <div class="py-3 py-md-4 py-lg-5">
                        <h2 class="heading-1 md">Snipper Tournament</h2>
                        <p class="typo3 md pb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmaod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <div class=" pt-md-4 pt-3">
                           <a href="javascript:void(0);" title="Add Friends" class="btn-yellow-fill md custom-row text-uppercase text-center">PAY ENTRY FEES</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- banner end -->
 <!--tabber start -->
 <div class=" bg-color3  pt-5">
            <div class="custom-row">
               <ul class="nav nav-tabs border-0 align-items-center justify-content-center id="tournamentsTab" role="tablist">
                  <li class="nav-item me-3 my-1" role="presentation">
                     <button class="btn-yellow-border px-4 text-uppercase active " id="active-tab" data-bs-toggle="tab" data-bs-target="#active" type="button" role="tab" aria-controls="home" aria-selected="true">Info</button>
                  </li>
                  <li class="nav-item me-3 my-1" role="presentation">
                     <button class="btn-yellow-border px-5 text-uppercase" id="upcoming-tab" data-bs-toggle="tab" data-bs-target="#upcoming" type="button" role="tab" aria-controls="profile" aria-selected="false">Rules</button>
                  </li>
               </ul>
               <div class="tab-content bg-color3 mt-5" id="myTabContent4">
                  <div class="custom-row tab-pane fade show active" id="active" role="tabpanel" aria-labelledby="active-tab">
                     <!-- Prize pool start -->
                     <div class="common-space-md bg-color2">
                        <div class="inner-container">
                           <h2 class="heading-1 md">Prize Pool</h2>
                           <div class="row">
                              <div class="col-md-3 py-2 text-center">
                                 <div class="bg-color3 p-5 ">
                                    <figure>
                                       <img src="http://localhost:8000/frontend/assets/img/gold.png" alt="Gold" class="img-fluid" width="51" height="67">
                                    </figure>
                                    <h3 class="heading-8 pt-3 mb-0">1st</h3>
                                    <div class="heading-2 lg">$500</div>
                                    <p class="typo7 pt-2">+5 crdits Back</p>
                                 </div>
                              </div>
                              <div class="col-md-3 py-2 text-center">
                                 <div class="bg-color3 p-5 ">
                                    <figure>
                                       <img src="http://localhost:8000/frontend/assets/img/gold.png" alt="Gold" class="img-fluid" width="51" height="67">
                                    </figure>
                                    <h3 class="heading-8 pt-3 mb-0">1st</h3>
                                    <div class="heading-2 lg">$500</div>
                                    <p class="typo7 pt-2">+5 crdits Back</p>
                                 </div>
                              </div>
                              <div class="col-md-3 py-2 text-center">
                                 <div class="bg-color3 p-5 ">
                                    <figure>
                                       <img src="http://localhost:8000/frontend/assets/img/gold.png" alt="Gold" class="img-fluid" width="51" height="67">
                                    </figure>
                                    <h3 class="heading-8 pt-3 mb-0">1st</h3>
                                    <div class="heading-2 lg">$500</div>
                                    <p class="typo7 pt-2">+5 crdits Back</p>
                                 </div>
                              </div>
                              <div class="col-md-3 py-2 text-center">
                                 <div class="bg-color3 p-5 ">
                                    <figure>
                                       <img src="http://localhost:8000/frontend/assets/img/gold.png" alt="Gold" class="img-fluid" width="51" height="67">
                                    </figure>
                                    <h3 class="heading-8 pt-3 mb-0">1st</h3>
                                    <div class="heading-2 lg">$500</div>
                                    <p class="typo7 pt-2">+5 crdits Back</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="inner-container  mt-5">
                           <h2 class="heading-1 md ">Game Associated</h2>
                           <div class="row">
                              <div class="col-md-12 py-2">
                                 <div class="bg-color3 p-4 d-flex justify-content-between align-items-center">
                                    <div  class="col-md-3">
                                       <figure class="full-width-image text-center">  <img src="http://localhost:8000/frontend/assets/img/game-img2.jpg" alt="Tournament Icon" class="img-fluid" width="199" height="187">
                                       </figure>
                                    </div>
                                    <div  class="col-md-9 ps-4">
                                       <h2 class="heading-1 md my-4">Red Dead Redemption</h2>
                                       <ul class="game-listing d-flex align-items-center justify-content-start">
                     <li>
                      <a href="#"> <img class="social-icon  cup-icon"> Thriller</a>
                
                     </li>
                     <li>
                     <a href="#"> <img class="social-icon  cup-icon"> PSN</a>
             
                     </li>
                     <li>
                     <a href="#"> <img class="social-icon  cup-icon"> 87</a>
                
                     </li>
                     <li>
                     <a href="#"> <img class="social-icon  cup-icon"> 125 Active Tournament</a>
                  
                     </li>
                 
                 </ul>



                                       <p class="typo3 md my-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmaod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="inner-container  mt-5">
                           <h2 class="heading-1 md">Advertisment</h2>
                           <div class="row">
                              <div class="col-md-12 py-2">
                                
                                  
                                 <figure>
                            <img src="http://localhost:8000/frontend/assets/img/game-img.jpg" alt="Tournament Icon" class="img-fluid" width="239" height="162">
                          </figure>
                                 
                              </div>
                           </div>
                        </div>
                        <div class="inner-container  mt-5">
                           <h2 class="heading-1 md">Prize Claim</h2>
                           <div class="row">
                              <div class="col-md-12 py-2">
                                 <div class="bg-color3 p-4 d-flex justify-content-between align-items-center">
                                 
                                 <p class="typo3 md pb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmaod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmaod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="inner-container  mt-5">
                           <h2 class="heading-1 md">Legal Disclaimer</h2>
                           <div class="row">
                              <div class="col-md-12 py-2">
                                 <div class="bg-color3 p-4 d-flex justify-content-between align-items-center">
                                 
                                 <p class="typo3 md pb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmaod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmaod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmaod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmaod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmaod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmaod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                 </p>
                                    
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="custom-row tab-pane fade" id="upcoming" role="tabpanel" aria-labelledby="upcoming-tab">
                     <div class="table-responsive">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Game Associated end -->


 <!-- footer start -->
 @include('dashboards.users.layout.footer')

    </div>
</body>

</html>



   <link rel="stylesheet" href="https://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">

<script src="https://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
  <script>
    @if(Session::has('success'))
    toastr.success("{{ Session::get('success') }}");
    @endif
    @if(Session::has('message'))
    toastr.success("{{ Session::get('message') }}");
    @endif
    @if(Session::has('error'))
    toastr.success("{{ Session::get('error') }}");
    @endif

    @if(Session::has('warning'))
    toastr.success("{{ Session::get('warning') }}");
    @endif
  </script>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('dashboards.users.layout.head')

<body>
    <div class="outer-container">
        @include('dashboards.users.layout.header')
        <!-- header end -->
        <!-- banner start -->
        <div class="banner-outer bg-color2">
            <div class="inner-container">
                <div class="pt-lg-5 pt-md-3 pt-0 mt-lg-4 mt-md-3 mt-0">
                    <div class="row align-items-center">
                        <div class="col-sm-6 pb-sm-0 pb-3">
                            <img class="img-fluid" src="frontend/assets/img/working-people.jpg" alt="working people" width="559" height="415">
                        </div>
                        <div class="col-sm-6">
                            <div class="px-lg-2">
                                <h2 class="heading-1 md mb-0">Article Heading will be here</h2>
                                <div class="border-btm dashed py-3 py-md-3 d-flex">
                                    <div class="d-flex align-items-center me-3">
                                        <span class="icon-round user me-2"></span>Smith
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <span class="icon-round date me-2"></span>june 8, 2021
                                    </div>
                                </div>
                                <p class="typo3 md py-lg-4 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat ex cum nobis, quidem earum optio aspernatur possimus veritatis aliquid eos, at officia laborum deserunt error ipsam fugit ad voluptatibus! Quia.at officia laborum deserunt error ipsam fugit ad voluptatibus! Quia.</p>
                                <div class="custom-row">
                                    <a class="border-btn2 border-md custom-col" href="" title="Continue Reading">Continue Reading</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- banner end -->
        <!-- tabs start  -->
        <div class="common-space-sm bg-color3">
            <div class="inner-container-sm">
                <ul class="nav nav-tabs border-0 justify-content-center" id="myTab" role="tablist">
                    <li class="nav-item mx-2 my-1" role="presentation">
                        <button class="border-btn-ylow active " id="home-tab" data-bs-toggle="tab" data-bs-target="#all" type="button" role="tab" aria-controls="home" aria-selected="true">All</button>
                    </li>
                    <li class="nav-item mx-2 my-1" role="presentation">
                        <button class="border-btn-ylow" id="profile-tab" data-bs-toggle="tab" data-bs-target="#catagory1" type="button" role="tab" aria-controls="profile" aria-selected="false">catagory</button>
                    </li>
                    <li class="nav-item mx-2 my-1" role="presentation">
                        <button class="border-btn-ylow" id="contact-tab" data-bs-toggle="tab" data-bs-target="#catagory2" type="button" role="tab" aria-controls="contact" aria-selected="false">Catagory</button>
                    </li>
                    <li class="nav-item mx-2 my-1" role="presentation">
                        <button class="border-btn-ylow" id="contact-tab" data-bs-toggle="tab" data-bs-target="#catagory3" type="button" role="tab" aria-controls="contact" aria-selected="false">Catagory</button>
                    </li>
                    <li class="nav-item mx-2 my-1" role="presentation">
                        <button class="border-btn-ylow" id="contact-tab" data-bs-toggle="tab" data-bs-target="#catagory4" type="button" role="tab" aria-controls="contact" aria-selected="false">Catagory</button>
                    </li>
                </ul>
            </div>
        </div>
        <!-- tab content start  -->
        <div class="tab-content common-space bg-full" id="myTabContent">
            <div class="inner-container-sm px-sm-5 px-4 tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="home-tab">
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-sm-6 my-2">
                        <div class="custom-row bg-color3 text-center">
                            <img class="img-fluid" src="frontend/assets/img/article1.jpg" alt="article image" width="309" height="207">
                        </div>
                        <div class="tab-card bg-color3">
                            <p class="typo3 md ellipse2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</p>
                            <div class="border-btm dashed py-3 py-md-3 d-flex">
                                <div class="d-flex align-items-center me-3">
                                    <span class="icon-round user me-2"></span>Smith
                                </div>
                                <div class="d-flex align-items-center">
                                    <span class="icon-round date me-2"></span>june 8, 2021
                                </div>
                            </div>
                            <p class="typo3 mt-3 text-small ellipse2">Lorem ipsum dolor, sit amet dolor ipsum adipisicing elit.</p>
                            <div class="d-flex align-items-center justify-content-between mt-4">
                                <a class="anchor-ylw" href="javascript:void(0)" title="Read More">Read More</a>
                                <div class="d-flex align-items-center border-btm border-0">
                                    <span class="icon-share me-2"></span>Share
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 my-2">
                        <div class="custom-row bg-color3 text-center">
                            <img class="img-fluid" src="frontend/assets/img/article2.jpg" alt="article image" width="309" height="207">
                        </div>
                        <div class="tab-card bg-color3">
                            <p class="typo3 md ellipse2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</p>
                            <div class="border-btm dashed py-3 py-md-3 d-flex">
                                <div class="d-flex align-items-center me-3">
                                    <span class="icon-round user me-2"></span>Smith
                                </div>
                                <div class="d-flex align-items-center">
                                    <span class="icon-round date me-2"></span>june 8, 2021
                                </div>
                            </div>
                            <p class="typo3 mt-3 text-small ellipse2">Lorem ipsum dolor, sit amet dolor ipsum adipisicing elit.</p>
                            <div class="d-flex align-items-center justify-content-between mt-4">
                                <a class="anchor-ylw" href="javascript:void(0)" title="Read More">Read More</a>
                                <div class="d-flex align-items-center border-btm border-0">
                                    <span class="icon-share me-2"></span>Share
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 my-2">
                        <div class="custom-row bg-color3 text-center">
                            <img class="img-fluid" src="frontend/assets/img/article3.jpg" alt="article image" width="309" height="207">
                        </div>
                        <div class="tab-card bg-color3">
                            <p class="typo3 md ellipse2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</p>
                            <div class="border-btm dashed py-3 py-md-3 d-flex">
                                <div class="d-flex align-items-center me-3">
                                    <span class="icon-round user me-2"></span>Smith
                                </div>
                                <div class="d-flex align-items-center">
                                    <span class="icon-round date me-2"></span>june 8, 2021
                                </div>
                            </div>
                            <p class="typo3 mt-3 text-small ellipse2">Lorem ipsum dolor, sit amet dolor ipsum adipisicing elit.</p>
                            <div class="d-flex align-items-center justify-content-between mt-4">
                                <a class="anchor-ylw" href="javascript:void(0)" title="Read More">Read More</a>
                                <div class="d-flex align-items-center border-btm border-0">
                                    <span class="icon-share me-2"></span>Share
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="inner-container-sm px-sm-5 px-4 tab-pane fade" id="catagory1" role="tabpanel" aria-labelledby="profile-tab">
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-sm-6 my-2">
                        <div class="custom-row bg-color3 text-center">
                            <img class="img-fluid" src="frontend/assets/img/article1.jpg" alt="article image" width="309" height="207">
                        </div>
                        <div class="tab-card bg-color3">
                            <p class="typo3 md ellipse2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</p>
                            <div class="border-btm dashed py-3 py-md-3 d-flex">
                                <div class="d-flex align-items-center me-3">
                                    <span class="icon-round user me-2"></span>Smith
                                </div>
                                <div class="d-flex align-items-center">
                                    <span class="icon-round date me-2"></span>june 8, 2021
                                </div>
                            </div>
                            <p class="typo3 mt-3 text-small ellipse2">Lorem ipsum dolor, sit amet dolor ipsum adipisicing elit.</p>
                            <div class="d-flex align-items-center justify-content-between mt-4">
                                <a class="anchor-ylw" href="javascript:void(0)" title="Read More">Read More</a>
                                <div class="d-flex align-items-center border-btm border-0">
                                    <span class="icon-share me-2"></span>Share
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="inner-container-sm px-sm-5 px-4 tab-pane fade" id="catagory2" role="tabpanel" aria-labelledby="contact-tab">
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-sm-6 my-2">
                        <div class="custom-row bg-color3 text-center">
                            <img class="img-fluid" src="frontend/assets/img/article2.jpg" alt="article image" width="309" height="207">
                        </div>
                        <div class="tab-card bg-color3">
                            <p class="typo3 md ellipse2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</p>
                            <div class="border-btm dashed py-3 py-md-3 d-flex">
                                <div class="d-flex align-items-center me-3">
                                    <span class="icon-round user me-2"></span>Smith
                                </div>
                                <div class="d-flex align-items-center">
                                    <span class="icon-round date me-2"></span>june 8, 2021
                                </div>
                            </div>
                            <p class="typo3 mt-3 text-small ellipse2">Lorem ipsum dolor, sit amet dolor ipsum adipisicing elit.</p>
                            <div class="d-flex align-items-center justify-content-between mt-4">
                                <a class="anchor-ylw" href="javascript:void(0)" title="Read More">Read More</a>
                                <div class="d-flex align-items-center border-btm border-0">
                                    <span class="icon-share me-2"></span>Share
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="inner-container-sm px-sm-5 px-4 tab-pane fade" id="catagory3" role="tabpanel" aria-labelledby="contact-tab">
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-sm-6 my-2">
                        <div class="custom-row bg-color3 text-center">
                            <img class="img-fluid" src="frontend/assets/img/article1.jpg" alt="article image" width="309" height="207">
                        </div>
                        <div class="tab-card bg-color3">
                            <p class="typo3 md ellipse2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</p>
                            <div class="border-btm dashed py-3 py-md-3 d-flex">
                                <div class="d-flex align-items-center me-3">
                                    <span class="icon-round user me-2"></span>Smith
                                </div>
                                <div class="d-flex align-items-center">
                                    <span class="icon-round date me-2"></span>june 8, 2021
                                </div>
                            </div>
                            <p class="typo3 mt-3 text-small ellipse2">Lorem ipsum dolor, sit amet dolor ipsum adipisicing elit.</p>
                            <div class="d-flex align-items-center justify-content-between mt-4">
                                <a class="anchor-ylw" href="javascript:void(0)" title="Read More">Read More</a>
                                <div class="d-flex align-items-center border-btm border-0">
                                    <span class="icon-share me-2"></span>Share
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="inner-container-sm px-sm-5 px-4 tab-pane fade" id="catagory4" role="tabpanel" aria-labelledby="contact-tab">
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-sm-6 my-2">
                        <div class="custom-row bg-color3 text-center">
                            <img class="img-fluid" src="frontend/assets/img/article3.jpg" alt="article image" width="309" height="207">
                        </div>
                        <div class="tab-card bg-color3">
                            <p class="typo3 md ellipse2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</p>
                            <div class="border-btm dashed py-3 py-md-3 d-flex">
                                <div class="d-flex align-items-center me-3">
                                    <span class="icon-round user me-2"></span>Smith
                                </div>
                                <div class="d-flex align-items-center">
                                    <span class="icon-round date me-2"></span>june 8, 2021
                                </div>
                            </div>
                            <p class="typo3 mt-3 text-small ellipse2">Lorem ipsum dolor, sit amet dolor ipsum adipisicing elit.</p>
                            <div class="d-flex align-items-center justify-content-between mt-4">
                                <a class="anchor-ylw" href="javascript:void(0)" title="Read More">Read More</a>
                                <div class="d-flex align-items-center border-btm border-0">
                                    <span class="icon-share me-2"></span>Share
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- article start  -->
        <div class="common-space bg-color3 remote-sec">
            <div class="inner-container">
                <div class="row align-items-center">
                    <div class="col-sm-6 pb-sm-0 pb-3">
                        <img class="img-fluid" src="frontend/assets/img/gaming.jpg" alt="Game image" width="559" height="415">
                    </div>
                    <div class="col-sm-6">
                        <div class="px-lg-2">
                            <h2 class="heading-1 md mb-0">Article Heading will be here</h2>
                            <div class="border-btm dashed py-3 py-md-3 d-flex">
                                <div class="d-flex align-items-center me-3">
                                    <span class="icon-round user me-2"></span>Smith
                                </div>
                                <div class="d-flex align-items-center">
                                    <span class="icon-round date me-2"></span>june 8, 2021
                                </div>
                            </div>
                            <p class="typo3 md py-lg-4 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat ex cum nobis, quidem earum optio aspernatur possimus veritatis aliquid eos, at officia laborum deserunt error ipsam fugit ad voluptatibus! Quia.at officia laborum deserunt error ipsam fugit ad voluptatibus! Quia.</p>
                            <div class="custom-row">
                                <a class="border-btn2 border-md custom-col" href="" title="Continue Reading">Continue Reading</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- article list start  -->
        <div class="common-space bg-color2">
            <div class="inner-container-sm px-sm-5 px-4">
                <div class="row justify-content-center">
                    <div class="col-sm-6">
                        <div class="custom-row bg-color3 text-center">
                            <img class="img-fluid" src="frontend/assets/img/article4.jpg" alt="card image" width="496" height="389">
                        </div>
                        <div class="tab-card bg-color3">
                            <p class="typo3 md ellipse2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</p>
                            <div class="border-btm dashed py-3 py-md-3 d-flex">
                                <div class="d-flex align-items-center me-3">
                                    <span class="icon-round user me-2"></span>Smith
                                </div>
                                <div class="d-flex align-items-center">
                                    <span class="icon-round date me-2"></span>june 8, 2021
                                </div>
                            </div>
                            <p class="typo3 mt-3 text-small ellipse2">Lorem ipsum dolor, sit amet dolor ipsum adipisicing elit.</p>
                            <div class="d-flex align-items-center justify-content-between mt-4">
                                <a class="anchor-ylw" href="javascript:void(0)" title="Read More">Read More</a>
                                <div class="d-flex align-items-center border-btm border-0">
                                    <span class="icon-share me-2"></span>Share
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="custom-row bg-color3 text-center">
                            <img class="img-fluid" src="frontend/assets/img/article4.jpg" alt="card image" width="496" height="389">
                        </div>
                        <div class="tab-card bg-color3">
                            <p class="typo3 md ellipse2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</p>
                            <div class="border-btm dashed py-3 py-md-3 d-flex">
                                <div class="d-flex align-items-center me-3">
                                    <span class="icon-round user me-2"></span>Smith
                                </div>
                                <div class="d-flex align-items-center">
                                    <span class="icon-round date me-2"></span>june 8, 2021
                                </div>
                            </div>
                            <p class="typo3 mt-3 text-small ellipse2">Lorem ipsum dolor, sit amet dolor ipsum adipisicing elit.</p>
                            <div class="d-flex align-items-center justify-content-between mt-4">
                                <a class="anchor-ylw" href="javascript:void(0)" title="Read More">Read More</a>
                                <div class="d-flex align-items-center border-btm border-0">
                                    <span class="icon-share me-2"></span>Share
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- related article start  -->
        <div class="common-space bg-full">
            <div class="inner-container-sm px-sm-5 px-4">
                <div class="text-center">
                    <h2 class="heading-1">Related <span class="theme-color">Articles</span></h2>
                </div>
                <div class="row justify-content-center pt-lg-4 pt-2">
                    <div class="col-lg-4 col-sm-6 my-2">
                        <div class="custom-row bg-color3 text-center">
                            <img class="img-fluid" src="frontend/assets/img/article1.jpg" alt="article image" width="309" height="207">
                        </div>
                        <div class="tab-card bg-color3">
                            <p class="typo3 md ellipse2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</p>
                            <div class="border-btm dashed py-3 py-md-3 d-flex">
                                <div class="d-flex align-items-center me-3">
                                    <span class="icon-round user me-2"></span>Smith
                                </div>
                                <div class="d-flex align-items-center">
                                    <span class="icon-round date me-2"></span>june 8, 2021
                                </div>
                            </div>
                            <p class="typo3 mt-3 text-small ellipse2">Lorem ipsum dolor, sit amet dolor ipsum adipisicing elit.</p>
                            <div class="d-flex align-items-center justify-content-between mt-4">
                                <a class="anchor-ylw" href="javascript:void(0)" title="Read More">Read More</a>
                                <div class="d-flex align-items-center border-btm border-0">
                                    <span class="icon-share me-2"></span>Share
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 my-2">
                        <div class="custom-row bg-color3 text-center">
                            <img class="img-fluid" src="frontend/assets/img/article2.jpg" alt="article image" width="309" height="207">
                        </div>
                        <div class="tab-card bg-color3">
                            <p class="typo3 md ellipse2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</p>
                            <div class="border-btm dashed py-3 py-md-3 d-flex">
                                <div class="d-flex align-items-center me-3">
                                    <span class="icon-round user me-2"></span>Smith
                                </div>
                                <div class="d-flex align-items-center">
                                    <span class="icon-round date me-2"></span>june 8, 2021
                                </div>
                            </div>
                            <p class="typo3 mt-3 text-small ellipse2">Lorem ipsum dolor, sit amet dolor ipsum adipisicing elit.</p>
                            <div class="d-flex align-items-center justify-content-between mt-4">
                                <a class="anchor-ylw" href="javascript:void(0)" title="Read More">Read More</a>
                                <div class="d-flex align-items-center border-btm border-0">
                                    <span class="icon-share me-2"></span>Share
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 my-2">
                        <div class="custom-row bg-color3 text-center">
                            <img class="img-fluid" src="frontend/assets/img/article3.jpg" alt="article image" width="309" height="207">
                        </div>
                        <div class="tab-card bg-color3">
                            <p class="typo3 md ellipse2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</p>
                            <div class="border-btm dashed py-3 py-md-3 d-flex">
                                <div class="d-flex align-items-center me-3">
                                    <span class="icon-round user me-2"></span>Smith
                                </div>
                                <div class="d-flex align-items-center">
                                    <span class="icon-round date me-2"></span>june 8, 2021
                                </div>
                            </div>
                            <p class="typo3 mt-3 text-small ellipse2">Lorem ipsum dolor, sit amet dolor ipsum adipisicing elit.</p>
                            <div class="d-flex align-items-center justify-content-between mt-4">
                                <a class="anchor-ylw" href="javascript:void(0)" title="Read More">Read More</a>
                                <div class="d-flex align-items-center border-btm border-0">
                                    <span class="icon-share me-2"></span>Share
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- popular start -->
        @include('dashboards.users.layout.popular')
        <!-- Subscribe start -->
        @include('dashboards.users.layout.subscribe')
        <!-- footer start -->
        @include('dashboards.users.layout.footer')
    </div>

</body>

</html>
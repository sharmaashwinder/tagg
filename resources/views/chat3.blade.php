<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chat3</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <style>
        .user-wrapper,  .message-wrapper{
            border: 1px solid #dddddd;
            overflow-y: auto
        }
        .user-wrapper{
            height: 600px;
        }
        .user{
            cursor: pointer;
            padding: 5px 0;
            position:relative;
        }
        .user:hover{
            background: #eeeeee
        }
       .user:last-child{
           margin-bottom: 0px
       }
       .pending{
           position: absolute;
           left: 13px;
           top: 9px;
           background: #b600ff;
           margin: 0;
           border-radius: 50%;
           width: 18px;
           height: 18px;
           line-height: 18px;
           padding-left: 5px;
           color: #ffffff;
           font-size: 12px
       }
        </style>
</head>


<body  style="margin-top: 10px">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <div class="user-wrapper">
                <ul class="users">
                    <li class="user">
                         <span class="pending">1</span>
                         <div class="media">

                            <div class="media-left">
                            <img src="https://via.placeholder.com/100" alt="" class="media-object">
                            </div>

                        <div class="media-body">
                            <p class="name"> ashwinder sharma</p>
                            <p class="email">shraashwinder@gmail.com</p>
                        </div>
                        </div>
                    </li>
                    <li class="user">
                        <span class="pending">1</span>
                        <div class="media">

                           <div class="media-left">
                           <img src="https://via.placeholder.com/100" alt="" class="media-object">
                           </div>

                       <div class="media-body">
                           <p class="name"> ashwinder sharma</p>
                           <p class="email">shraashwinder@gmail.com</p>
                       </div>
                       </div>
                   </li>
                   <li class="user">
                    <span class="pending">1</span>
                    <div class="media">

                       <div class="media-left">
                       <img src="https://via.placeholder.com/100" alt="" class="media-object">
                       </div>

                   <div class="media-body">
                       <p class="name"> ashwinder sharma</p>
                       <p class="email">shraashwinder@gmail.com</p>
                   </div>
                   </div>
               </li>
               <li class="user">
                <span class="pending">1</span>
                <div class="media">

                   <div class="media-left">
                   <img src="https://via.placeholder.com/100" alt="" class="media-object">
                   </div>

               <div class="media-body">
                   <p class="name"> ashwinder sharma</p>
                   <p class="email">shraashwinder@gmail.com</p>
               </div>
               </div>
           </li>
           <li class="user">
            <span class="pending">1</span>
            <div class="media">

               <div class="media-left">
               <img src="https://via.placeholder.com/100" alt="" class="media-object">
               </div>

           <div class="media-body">
               <p class="name"> ashwinder sharma</p>
               <p class="email">shraashwinder@gmail.com</p>
           </div>
           </div>
       </li>
                    <li class="user">
                        <span class="pending">1</span>
                        <div class="media">
                       <div class="media-left">
                           <img src="https://via.placeholder.com/100" alt="" class="media-object">
                       </div>
                       <div class="media-body">
                           <p class="name"> ashwinder sharma</p>
                           <p class="email">shraashwinder@gmail.com</p>
                       </div>
                       </div>
                   </li>
                   <li class="user">
                    <span class="pending">1</span>
                    <div class="media">
                   <div class="media-left">
                       <img src="https://via.placeholder.com/100" alt="" class="media-object">
                   </div>
                   <div class="media-body">
                       <p class="name"> ashwinder sharma</p>
                       <p class="email">shraashwinder@gmail.com</p>
                   </div>
                   </div>
               </li>
                </ul>
            </div>
        </div>
        <div class="col-md-8" id="messages">
            <div class="message-wrapper">
                <ul class="messages">
                    <li class="message clearfix">
                        <div class="sent">
                            <p>Loremispsum</p>
                            <p>1.Sep 2019</p>
                        </div>
                    </li>
                    <li class="message clearfix">
                        <div class="recieved">
                            <p>Loremispsum</p>
                            <p>1.Sep 2019</p>
                        </div>
                    </li>
                    <li class="message clearfix">
                        <div class="sent">
                            <p>Loremispsum</p>
                            <p>1.Sep 2019</p>
                        </div>
                    </li>
                    <li class="message clearfix">
                        <div class="recieved">
                            <p>Loremispsum</p>
                            <p>1.Sep 2019</p>
                        </div>
                    </li>
                    <li class="message clearfix">
                        <div class="sent">
                            <p>Loremispsum</p>
                            <p>1.Sep 2019</p>
                        </div>
                    </li>
                    <li class="message clearfix">
                        <div class="recieved">
                            <p>Loremispsum</p>
                            <p>1.Sep 2019</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="input-text">
            <input type="text" name="message"  class="submit">
        </div>

    </div>
</div>
</body>
</html>

<!DOCTYPE html>
<html lang="en">

@include('dashboards.admins.layouts.admin-head')


  <div class="wrapper">
  @include('dashboards.admins.layouts.admin-sidebar')
    <div class="main-panel">
      <!-- Navbar -->
      @include('dashboards.admins.layouts.admin-header')
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header mb-5">
              
                <h3 class="card-title head-font">User Details</h3>
              </div>
              <div class="card-body">

                <table class="table">
                    <tbody>
                      <tr>
                        <th>Name</th>
                        <td class="text-center">{{$data->name}}</td>

                      </tr>
                      <tr>
                        <th>Email</th>
                        <td class="text-center">{{$data->email}}</td>

                      </tr>
                      <tr>
                        <th>Gamer Name</th>
                        <td class="text-center">{{$data->gamer_name}}</td>
                      </tr>
                      <!-- <tr>
                        <th>Rank</th>
                        <td class="text-center">{{$data->rank}}</td>
                      </tr> -->
                      <!-- <tr>
                        <th>Earning</th>
                        <td class="text-center">{{$data->balance}}</td>
                      </tr> -->

                    </tbody>
                  </table>
               
     
              </div>
            </div>
          </div>
        </div>
      </div>
      @include('dashboards.admins.layouts.admin-footer')
    </div>
  </div>
  @include('dashboards.admins.layouts.theme')
  <!--   Core JS Files   -->
  @include('dashboards.admins.layouts.admin-script')
</body>

</html>
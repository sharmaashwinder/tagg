<!DOCTYPE html>
<html lang="en">
  

@include('dashboards.admins.layouts.admin-head')


  <div class="wrapper">
  @include('dashboards.admins.layouts.admin-sidebar')
    <div class="main-panel">
      <!-- Navbar -->
      @include('dashboards.admins.layouts.admin-header')
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-8 mx-auto">
            <div class="card">
              <div class="card-header">
                <h5 class="title head-font">Edit Tournament</h5>
              </div>
              <div class="card-body">
              <form method="POST" action="{{route('tournament.update',$data->id)}}" enctype="multipart/form-data" id="edit_tournament">
                  @csrf

                  @foreach ($errors->all() as $error)
                  <p class="text-danger">{{ $error }}</p>
                  @endforeach

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Tournament Name</label>
                        <input id="password" type="text" class="form-control" name="name" value="{{$data->name}}">
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Host Name</label>
                        <input type="text" class="form-control" name="host_name" value="{{$data->host_name}}">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Speciality</label>
                        <input type="text" class="form-control" name="speciality" value="{{$data->speciality}}">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Game Name</label>
                        <select id="cars" name="game_id" class="form-control" value="{{$data->game_id}}">
                          @if(!empty($game))
                          @foreach($game as $games)
                          <option value="{{$games->id}}">{{$games->game_name}}</option>
                          @endforeach
                          @endif
                        </select>
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="start_time">Start Date</label>
                        <input type="date" class="form-control" name="start_date" id="start_time" value="{{ date('Y-m-d', strtotime($data->start_date)) }}" min="<?php echo date('Y-m-d') ?>">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="datepicker1">End Date</label>
                        <input type="date" class="form-control" id="datepicker1" name="end_date" value="{{ date('Y-m-d', strtotime($data->end_date)) }}" min="<?php echo date('Y-m-d') ?>">
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Start Time</label>
                        <input type="time" class="form-control" name="start_time" value="{{ date('H:i:s', strtotime($data->start_time)) }}">
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>End Time</label>
                        <input type="time" class="form-control" name="end_time" value="{{ date('H:i:s', strtotime($data->end_time)) }}">
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>No. of Players</label>
                        <input type="text" class="form-control" name="player_numbers" value="{{$data->player_numbers}}">
                      </div>
                    </div>
                  </div>

                   <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Skill Level</label>
                        <select id="cars" name="skill_level_id[]" class="form-control" value="{{$data->skill_level_id}}" multiple="">
                        @if(!empty($skill))
                       @foreach($skill as $skills)
                       @if(in_array($skills->name , $skillname))
                        <option value="{{$skills->id}}" selected>{{$skills->name}}</option>
                        @else
                        <option value="{{$skills->id}}">{{$skills->name}}</option>
                       @endif
                        @endforeach
                        @endif
                      </select>
                      </div>
                    </div>
                  </div> 

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Format</label>
                        <select id="cars" name="format_id" class="form-control" value="{{$data->format_id}}">
                        @if(!empty($formate))
                          @foreach($formate as $formates)
                          <option value="{{$formates->id}}">{{$formates->name}}</option>
                          @endforeach
                          @endif
                        </select>
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Entry Fees</label>
                        <input id="new_password" type="text" class="form-control" name="entry_fee" value="{{$data->entry_fee}}">

                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Prize Pool</label>
                        <select id="prize" name="prize_pool_id" class="form-control" value="{{$data->prize_pool_id}}">
                   
                        @if(!empty($prize))
                        @foreach($prize as $prizes)
                       @if(empty($prizes->next_position_price))
                        <option value="{{$prizes->id}}">{{$prizes->id}}-${{$prizes->first_price+$prizes->second_price+$prizes->third_price}}</option>
                        @else
                        <option value="{{$prizes->id}}">{{$prizes->id}}-${{$prizes->first_price+$prizes->second_price+$prizes->third_price+$prizes->next_position_price}}</option>
                          @endif
                        @endforeach
                        @endif 
                      </select>
                      </div>
                     
                    </div>
                    <div class="col-md-2 d-flex align-items-center">
                      <a class="btn btn-sm btn-success" id="edit_prize"><i class="fas fa-pencil-alt"></i>Edit</a>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Status</label>
                        <select id="cars" name="status" class="form-control" value="{{$data->status}}">
                        <option value="created">Created</option>
                          <option value="cancelled">Cancelled</option>
                          <option value="finished">Finished</option>
                          <option value="paid">Paid</option>
                      </select>
                      </div>
                    </div>
                  </div>


                     <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="exampleFormControlTextarea1">Comment</label>
                        <textarea class="form-control custom-textarea" id="exampleFormControlTextarea1" rows="3" name="descriptions">{{ $data->descriptions }}</textarea>  

                      </div>
                    </div>
                  </div>
                 

                  <a title="Back" class="btn btn-light" href="{{ route('tournament')}}">
                        Back
                      </a>

                <button type="submit" class="btn btn-fill btn-success">Save</button>
                </form>
              </div>
             
            </div>
          </div>
        
        </div>
      </div>
      
      @include('dashboards.admins.layouts.admin-footer')
    </div>
  </div>
  @include('dashboards.admins.layouts.theme')
  <!--   Core JS Files   -->
  @include('dashboards.admins.layouts.admin-script')


  <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
  
  <script type="text/javascript">
    $(document).ready(function() {
      $.validator.addMethod("end_date", function(value, element) {
        var start_date = $('#start_time').val();
        return Date.parse(start_date) <= Date.parse(value) || value == "";
      }, "* End date must be after start date");
    });
  </script>

<script>
    $("#edit_prize").click(function() {
      var prize = $('#prize').val();
     
      var tournamnet_id = '{{$data->id}}';
     
     
      var url = "{{ url('/') }}/admin/edit_prize/" + prize + '/'+tournamnet_id;
      window.location = url;
    });
  </script>
</script>

<script>
    $(document).ready(function() {
      $("#edit_tournament").validate({
        rules: {

          entry_fee: 'entry_fee',

          name: {
            required: true
          },
          player_numbers: {
            required: true
          },
          descriptions: {
            required: true
          },
         
        },
        messages:{
          name:{
              required:'Tournament Name is required'
            },  
            player_numbers:{
              required:'Player No. is required'
            }, 
            descriptions:{
              required:'Description is required'
            },
        }

      });
    });


    $.validator.addMethod('entry_fee',
      function(value) {
        return Number(value) > 0;
      }, 'Enter a positive Entry Fee.');
    
    
  
  </script>





</body>

</html>
<!DOCTYPE html>
<html lang="en">

@include('dashboards.admins.layouts.admin-head')


  <div class="wrapper">
  @include('dashboards.admins.layouts.admin-sidebar')
    <div class="main-panel">
      <!-- Navbar -->
      @include('dashboards.admins.layouts.admin-header')
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-8 mx-auto">
            <div class="card">
              <div class="card-header">
                <h5 class="title head-font">Create Game</h5>
              </div>
              <div class="card-body">
              <form method="POST" action="{{route('store.games')}}" id="form89" enctype="multipart/form-data">
                  @csrf
                  @foreach ($errors->all() as $error)
                  <p class="text-danger">{{ $error }}</p>
                  @endforeach
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Game Name</label>
                        <input type="text" class="form-control" placeholder="Game Name" name="game_name">
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Platform</label>
                        <select id="cars" name="platform_id" class="form-control">
                        @if(!empty($Platform))
                        @foreach($Platform as $Platforms)
                        <option value="{{$Platforms->id}}">{{$Platforms->name}}</option>
                        @endforeach
                        @endif
                      </select>
                      </div>
                    </div>
                  </div>





                  <!-- <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Platform</label>
                        <input type="text" class="form-control" placeholder="Platform" name="platform">
                      </div>
                    </div>
                  </div> -->

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Image</label>
                        <input type="file" class="form-control choose-file" placeholder="Image" name="image">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Genre</label>
                        <select id="cars" name="genres_id" class="form-control">
                        @if(!empty($Genre))
                        @foreach($Genre as $Genres)
                        <option value="{{$Genres->id}}">{{$Genres->name}}</option>
                        @endforeach
                        @endif
                      </select>
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="exampleFormControlTextarea1">Description</label>
                        <textarea class="form-control custom-textarea" id="exampleFormControlTextarea1" rows="3" name="description"></textarea>

                      </div>
                    </div>
                  </div>


                <button type="submit" class="btn btn-fill btn-success">Save</button>
                </form>
              </div>
             
            </div>
          </div>
        
        </div>
      </div>
      @include('dashboards.admins.layouts.admin-footer')
    </div>
  </div>
  @include('dashboards.admins.layouts.theme')
  <!--   Core JS Files   -->
  @include('dashboards.admins.layouts.admin-script')


  <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
  <script>
    $(document).ready(function() {
      $('#form89').validate({
        rules: {
          game_name: {
            required: true
          },
          platform: {
            required: true
          },
          genre: {
            required: true
          },
        },
        messages:{
          game_name:{
              required:'Game Name is required'
            },
            platform:{
              required:'Platform is required'
            },
            genre:{
                required:'Genre is required'
            },
           
        }
      });
    });
  </script>
</body>

</html>

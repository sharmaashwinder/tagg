
<!DOCTYPE html>
<html lang="en">

@include('dashboards.admins.layouts.admin-head')



  <div class="wrapper">
  @include('dashboards.admins.layouts.admin-sidebar')
    <div class="main-panel">
      <!-- Navbar -->
      @include('dashboards.admins.layouts.admin-header')
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header">
                <h4 class="card-title head-font">All Games</h4>
              </div>


              <div class="col-lg-12 col-12 text-right">
                <a href="{{route('create.games')}}" class="btn btn-success custom-primary">Add New Game</a>
              </div>


              <div class="card-body">
                <div class="table-responsive">
                  <table class="table tablesorter" id="yajra">
                    <thead class=" text-primary">
                      <tr>
                        <th>
                        Game
                        </th>
                         <th>
                        Platform
                        </th>
                         <!-- <th>
                        No. of Tournaments
                        </th> -->
                        <th>
                        Genre
                        </th>
                        <th>
                        Action
                        </th>
                      </tr>
                    </thead>

                  </table>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
      @include('dashboards.admins.layouts.admin-footer')
    </div>
  </div>
  @include('dashboards.admins.layouts.theme')
  <!--   Core JS Files   -->
  @include('dashboards.admins.layouts.admin-script')

  <script>

   $(document).ready(function() {
     $("#adminClick").click(function() {
       $('.showAdmin').addClass('show');
     });
     $(document).on('click', 'body', function(e) {
       if (!$(e.target).is('.show'))
         $('.show').removeClass('show');
     })
   });
 </script>

 <script>
   function myFunction1() {
     if (!confirm("Are you sure?,This record and it`s details will be permanantly deleted!"))
       event.preventDefault();
   }
 </script>


 <script type="text/javascript">
   $(document).ready(function() {
     var table = $('#yajra').DataTable({
       processing: true,
        serverSide: true,
       ajax: "{{ route('create.game') }}",

       columns: [{
           data: 'game_name',
           name: 'games.game_name'
         },

         {
           data: "games_platform[, ].platform_name.name",
           name: 'games.game_name'
         },

         {
           data: 'games_genre[, ].genre_name.name',
           name: 'games.game_name'
         },
         {
           data: 'action',
           name: 'action',
           orderable: false,
           searchable: false
         },
       ]
     });
   });
 </script>

</body>

</html>

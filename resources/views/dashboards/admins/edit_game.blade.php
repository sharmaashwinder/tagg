<!DOCTYPE html>
<html lang="en">

@include('dashboards.admins.layouts.admin-head')



  <div class="wrapper">
    @include('dashboards.admins.layouts.admin-sidebar')
    <div class="main-panel">
      <!-- Navbar -->
      @include('dashboards.admins.layouts.admin-header')
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-8 mx-auto">
            <div class="card">
              <div class="card-header">
                <h5 class="title head-font">Edit Game</h5>
              </div>
              <div class="card-body">
                <form method="POST" action="{{route('update.games', $data->id)}}" enctype="multipart/form-data">
                  @csrf

                  @foreach ($errors->all() as $error)
                  <p class="text-danger">{{ $error }}</p>
                  @endforeach
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Game Name</label>
                        <input type="text" class="form-control" placeholder="Game Name" name="game_name" value="{{$data->game_name}}">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Platform</label>
                        <select id="cars" name="platform_id" class="form-control">
                        @if(!empty($Platform))
                        @foreach($Platform as $Platforms)
                        <option value="{{$Platforms->id}}">{{$Platforms->name}}</option>
                        @endforeach
                        @endif
                      </select>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Image</label>
                        <img src="{{ asset('storage/' . $data->image) }}" class="img-fluid">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Change Image</label>
                        <input type="file" class="form-control choose-file" placeholder="Image" name="image" value="{!! $data->image !!}">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Genre</label>
                        <select  name="genres_id" class="form-control">
                        @if(!empty($Genres))
                        @foreach($Genres as $Genre)
                        <option value="{{$Genre->id}}">{{$Genre->name}}</option>
                        @endforeach
                        @endif
                        </select>
                      </div>
                    </div>
                  </div>


                <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="exampleFormControlTextarea1">Description</label>
                        <textarea class="form-control custom-textarea" id="exampleFormControlTextarea1" rows="3" name="description">{{ $data->description }}</textarea>  

                      </div>
                    </div>
                  </div>   
             


                  <button type="submit" class="btn btn-fill btn-success">Save</button>
                </form>
              </div>

            </div>
          </div>

        </div>
      </div>
      @include('dashboards.admins.layouts.admin-footer')
    </div>
  </div>
  @include('dashboards.admins.layouts.theme')
  <!--   Core JS Files   -->
  @include('dashboards.admins.layouts.admin-script')


</body>

</html>
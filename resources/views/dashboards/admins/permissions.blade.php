<!DOCTYPE html>
<html>
@include('dashboards.admins.layouts.head')

<body>
  <!-- Sidenav -->
  @include('dashboards.admins.layouts.sidebar')
  <!-- Main content -->
  <div class="main-content d-flex justify-content-between flex-column full-h" id="panel">
    <div class="custom-row">
      <!-- Topnav -->
      @include('dashboards.admins.layouts.header')
      <!-- Header -->
      <!-- Header -->
      <div class="container">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">Permissions</th>
              <th scope="col">admin</th>
              <th scope="col">affliates</th>
            </tr>
          </thead>
          <tbody>
            <form action="{{route('admin.permission_save')}}" method="post">
              {{ csrf_field() }}
              @foreach ($data[0] as $item)
              <tr>
                <td scope="row">{{$item->name}}</td>
                @if (in_array( $item->id,$data[1]))
                <td> <input type="checkbox" name="admin_{{$item->id}}" checked id="admin_{{$item->id}}" /></td>
                @else
                <td> <input type="checkbox" name="admin_{{$item->id}}" id="admin_{{$item->id}}" /></td>
                @endif

                @if (in_array( $item->id,$data[2]))
                <td> <input type="checkbox" name="special_{{$item->id}}" checked id="special_{{$item->id}}" /></td>
                @else
                <td> <input type="checkbox" name="special_{{$item->id}}" id="admin_{{$item->id}}" /></td>
                @endif
              </tr>
              @endforeach



          </tbody>
        </table>
        <input class="admin-btn" type="submit" name="submit" value="Submit" />

        </form>
      </div>
      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif
    </div>
    @include('dashboards.admins.layouts.footer')

  </div>
  {{-- @if (session('success'))
      <div class="alert alert-success">
     added successfully

        </div>
@endif --}}


</body>

</html>
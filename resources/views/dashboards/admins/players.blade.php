<!DOCTYPE html>
<html lang="en">

@include('dashboards.admins.layouts.admin-head')

  <div class="wrapper">
    @include('dashboards.admins.layouts.admin-sidebar')
    <div class="main-panel">
      <!-- Navbar -->
      @include('dashboards.admins.layouts.admin-header')
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-12">

            <div class="container-fluid mb-3">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#home">Player</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#menu1">Affiliates</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#menu2">Permissions</a>
                </li>

              </ul>
            </div>

            <!-- tab data start  -->
            <div class="container-fluid">
              <div class="tab-content">
                <!-- first tab data  -->
                <div class="row tab-pane active" id="home">
                  <div class="col">
                    <div class="card ">
                      <div class="card-header">
                        <h4 class="card-title head-font">Player Management</h4>
                      </div>
                      <div class="card-body">
                        <div class="table-responsive">
                          <table class="table tablesorter" id="table5">
                            <thead class=" text-primary">
                              <tr>
                                <th>
                                  Player Name
                                </th>
                                <th>
                                  Email
                                </th>
                                <th>
                                  Gamer Name
                                </th>
                               
                                <th>
                                  Action
                                </th>
                              </tr>
                            </thead>

                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- second tab data  -->
                <div class="row tab-pane" id="menu1">
                  <div class="col">
                    <div class="card ">
                      <div class="card-header">
                        <h4 class="card-title head-font">Affiliates</h4>
                      </div>
                      <div class="card-body">
                        <div class="table-responsive">
                          <table class="table tablesorter w-100" id="table2">
                            <thead class=" text-primary">
                              <tr>
                                <th>
                                  Player Name
                                </th>
                                <th>
                                  Gamer Name
                                </th>
                                <!-- <th>
                                  Rank
                                </th>
                                <th>
                                  Earning
                                </th> -->
                                <th>
                                  Action
                                </th>
                              </tr>
                            </thead>

                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <!-- third tab data   -->
                <div class="row tab-pane" id="menu2">
                  <div class="col">
                    <div class="card ">
                      <div class="card-header">
                        <h4 class="card-title head-font">Permissions</h4>
                      </div>
                      <div class="card-body">
                        <div class="table-responsive">
                          <table class="table tablesorter " id="">
                            <thead class=" text-primary">
                              <tr>
                                <th scope="col">Permissions</th>

                                <th scope="col">affliates</th>
                              </tr>
                            </thead>
                            <tbody>
                              <form action="{{route('admin.permission_save')}}" method="post">
                                {{ csrf_field() }}
                                @foreach ($data[0] as $item)
                                <tr>
                                  <td scope="row">{{$item->name}}</td>
                                  {{-- @if (in_array( $item->id,$data[1]))
                          <td> <input type="checkbox" name="admin_{{$item->id}}" checked id="admin_{{$item->id}}" /></td>
                                  @else
                                  <td> <input type="checkbox" name="admin_{{$item->id}}" id="admin_{{$item->id}}" /></td>
                                  @endif --}}

                                  @if (in_array( $item->id,$data[2]))
                                  <td> <input type="checkbox" name="special_{{$item->id}}" checked id="special_{{$item->id}}" /></td>
                                  @else
                                  <td> <input type="checkbox" name="special_{{$item->id}}" id="admin_{{$item->id}}" /></td>
                                  @endif
                                </tr>
                                @endforeach



                            </tbody>
                          </table>
                          <div class="custom-row py-2 ">
                            <input class="btn btn-success custom-primary" type="submit" name="submit" value="Submit" />
                          </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- tab data end  -->




          </div>

        </div>
      </div>
      @include('dashboards.admins.layouts.admin-footer')
    </div>
  </div>


  @include('dashboards.admins.layouts.theme')
  
  <!--   Core JS Files   -->
  @include('dashboards.admins.layouts.admin-script')


  <script>
    $(document).ready(function() {
      $("#adminClick").click(function() {
        $('.showAdmin').addClass('show');
      });
      $(document).on('click', 'body', function(e) {
        if (!$(e.target).is('.show'))
          $('.show').removeClass('show');
      })
    });
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
      var table = $('#table5').DataTable({
        processing: true,
        serverSide: true,

        ajax: "{{ route('admin.player') }}",
        columns: [{
            data: 'name',
            name: 'users.name'

          },
          {
            data: 'email',
            name: 'users.email'

          },
          {
            data: 'gamer_name',
            name: 'users.gamer_name'

          },
         
          {
            data: 'action',
            name: 'action',
            orderable: false,
            searchable: false
          },
        ]
      });
    });
  </script>


  <script type="text/javascript">
    $(document).ready(function() {
      var table = $('#table2').DataTable({
        processing: true,
        serverSide: true,

        ajax: "{{ route('admin.affiliate') }}",
        columns: [{
            data: 'name',
            name: 'users.name'

          },

          {
            data: 'gamer_name',
            name: 'users.gamer_name'

          },
          // {
          //   data: 'rank',
          //   name: 'earnings.rank'

          // },
          // {
          //   data: 'balance',
          //   name: 'earnings.balance'

          // },
          {
            data: 'action',
            name: 'action',
            orderable: false,
            searchable: false
          },
        ]
      });
    });
  </script>




  <script>
    function myFunction() {
      if (!confirm("Are you sure you want to delete this?"))
        event.preventDefault();
    }
  </script>




</body>

</html>
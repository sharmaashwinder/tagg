<!DOCTYPE html>
<html lang="en">

@include('dashboards.admins.layouts.admin-head')


  <div class="wrapper">
    @include('dashboards.admins.layouts.admin-sidebar')

    <div class="main-panel">
      <!-- Navbar -->
      @include('dashboards.admins.layouts.admin-header')
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-8 mx-auto">
            <div class="card">
              <div class="card-header">
                <h3 class="title mb-2">Edit Profile</h3>
              </div>
              <div class="card-body">
                <form method="POST" action="{{route('player.update',$data[0]->id )}}" enctype="multipart/form-data">
                  @csrf

                  @foreach ($errors->all() as $error)
                  <p class="text-danger">{{ $error }}</p>
                  @endforeach

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" placeholder="Name" name="name" value="{{$data[0]->name}}">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Gamer Name</label>
                        <input type="text" class="form-control" placeholder="Gamer Name" name="gamer_name" value="{{$data[0]->gamer_name}}">
                      </div>
                    </div>
                  </div>





                  <button type="submit" class="btn btn-fill btn-success">Save</button>
                </form>
              </div>

              {{--  --}}


              <div class="" id="menu3">
                <div class="">
                  <div class="">
                    <!-- Card header -->
                    <div class="card-header border-0">
                      <h3 class="title mb-2">Assign Role</h3>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive mt-3">
                        <table class="table align-items-center table-flush dataTable no-footer">
                          <tbody>
                            <form action="{{route('playereditrole.save')}}" method="post">
                              {{ csrf_field() }}
                               <tr hidden>
                              <td><input type="input" name="id" value="{{$data[2]}}" hidden /></td>
                            </tr>
                              @foreach ($data[4] as $item)
                              <tr>

                                <td scope="row">{{$item->name}}</td>
                                @if (in_array( $item->name,$data[3]))
                                <td> <input type="checkbox" name="{{$item->id}}" checked id="admin_{{$item->id}}" /></td>
                                @else
                                <td> <input type="checkbox" name="{{$item->id}}" id="admin_{{$item->id}}" /></td>
                                @endif
                                {{-- <td> <input type="checkbox" name="admin_{{$item->id}}" id="admin_{{$item->id}}" /></td>



                                <td> <input type="checkbox" name="special_{{$item->id}}" checked id="special_{{$item->id}}" /></td>

                                <td> <input type="checkbox" name="special_{{$item->id}}" id="admin_{{$item->id}}" /></td> --}}
                              </tr>
                              @endforeach
                          </tbody>
                        </table>
                        <div class="custom-row py-2">
                          <input class="btn btn-success custom-primary" type="submit" value="Submit" />
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>


{{-- /////////////////////// --}}
              <div class="" id="menu2">
                <div class="">
                  <div class="">
                    <!-- Card header -->
                    <div class="card-header border-0">
                      <h3 class="title mb-2">Special Permissions</h3>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive mt-3">
                        <!-- @if(Session::has('message'))
                            <div class="alert alert-{{ Session::get('alert-type', 'danger') }}">
                                <ul>
                                    {{ Session::get('message') }}
                                </ul>
                            </div>
                            @endif
                            @if(Session::has('success'))
                            <div class="alert alert-{{ Session::get('alert-type', 'success') }}">
                                <ul>
                                    {{ Session::get('success') }}
                                </ul>
                            </div>
                            @endif -->
                        <table class="table align-items-center table-flush dataTable no-footer">
                          <!-- <thead class="thead-light">
                              <tr>
                                <th scope="col">Permissions</th>

                              </tr>
                            </thead> -->
                          <tbody>
                            <form action="{{route('playeredit.save')}}" method="post">
                              {{ csrf_field() }}
                               <tr hidden>
                              <td><input type="input" name="id" value="{{$data[2]}}" hidden /></td>
                            </tr>
                              @foreach ($data[1] as $item)
                              <tr>

                                <td scope="row">{{$item->name}}</td>
                                @if (in_array( $item->name,$data[3]))
                                <td> <input type="checkbox" name="{{$item->id}}" checked id="admin_{{$item->id}}" /></td>
                                @else
                                <td> <input type="checkbox" name="{{$item->id}}" id="admin_{{$item->id}}" /></td>
                                @endif
                                {{-- <td> <input type="checkbox" name="admin_{{$item->id}}" id="admin_{{$item->id}}" /></td>



                                <td> <input type="checkbox" name="special_{{$item->id}}" checked id="special_{{$item->id}}" /></td>

                                <td> <input type="checkbox" name="special_{{$item->id}}" id="admin_{{$item->id}}" /></td> --}}
                              </tr>
                              @endforeach
                          </tbody>
                        </table>
                        <div class="custom-row py-2">
                          <input class="btn btn-success custom-primary" type="submit" value="Submit" />
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>



            </div>





          </div>
        </div>

      </div>
    </div>
    @include('dashboards.admins.layouts.admin-footer')
  </div>
  </div>
  @include('dashboards.admins.layouts.theme')
  <!--   Core JS Files   -->
  @include('dashboards.admins.layouts.admin-script')

</body>

</html>

<!DOCTYPE html>
<html lang="en">
 

@include('dashboards.admins.layouts.admin-head')


  <div class="wrapper">
  @include('dashboards.admins.layouts.admin-sidebar')
    <div class="main-panel">
      <!-- Navbar -->
      @include('dashboards.admins.layouts.admin-header')
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-8 mx-auto">
            <div class="card">
              <div class="card-header">
                <h5 class="title head-font">Change Password</h5>
              </div>
              <div class="card-body">
              <form method="POST" action="{{ route('change.password') }}" id="form2">
                  @csrf

                  @foreach ($errors->all() as $error)
                  <p class="text-danger">{{ $error }}</p>
                  @endforeach
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Current Password</label>
                        <input id="password" type="password" placeholder="Current Password" class="form-control" name="current_password" autocomplete="current-password">
                      </div>
                    </div>
                  </div>



                   <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>New Password</label>
                        <input id="new_password" type="password" placeholder="New Password" class="form-control" name="new_password" autocomplete="current-password">
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Confirm New Password</label>
                        <input id="new_confirm_password" type="password" placeholder="Confirm New Password" class="form-control" name="new_confirm_password" autocomplete="current-password">
                      </div>
                    </div>
                  </div>


                <button type="submit" class="btn btn-fill btn-success">Update</button>
                </form>
              </div>
             
            </div>
          </div>
        
        </div>
      </div>
      @include('dashboards.admins.layouts.admin-footer')
    </div>
  </div>
  @include('dashboards.admins.layouts.theme')
  <!--   Core JS Files   -->
  @include('dashboards.admins.layouts.admin-script')


  <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
  <script>
    $(document).ready(function() {
      $('#form2').validate({
        rules: {
          current_password: {
            required: true
          },
          new_password: {
            required: true
          },
          new_confirm_password: {
            required: true,
            equalTo: "#new_password",

          },
        },
        messages:{
          current_password:{
              required:' Current Password is required'
            },
            new_password:{
              required:'New Password is required'
            },
            new_confirm_password:{
                required:'New Confirm Password is required',
                equalTo:'Password does not match'
            },

        }


      });
    });
  </script>
</body>

</html>
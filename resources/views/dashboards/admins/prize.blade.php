
<!DOCTYPE html>
<html lang="en">

@include('dashboards.admins.layouts.admin-head')

  <div class="wrapper">
  @include('dashboards.admins.layouts.admin-sidebar')
    <div class="main-panel">
      <!-- Navbar -->
      @include('dashboards.admins.layouts.admin-header')
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header">
                <h4 class="card-title head-font">All Prize Pools</h4>
              </div>


              <div class="col-lg-12 col-12 text-right">
                <a href="{{route('create.prize')}}" class="btn btn-success custom-primary">Add New Prize Pool</a>
              </div>


              <div class="card-body">
                <div class="table-responsive">
                  <table class="table tablesorter" id="pool">
                    <thead class=" text-primary">
                      <tr>
                        <th>
                        Position #1
                        </th>
                        <th>
                        Position #2
                        </th>
                        <th>
                        Position #3
                        </th>
                        <th>
                        Runner Ups
                        </th>
                        <th>
                        Action
                        </th>
                      </tr>
                    </thead>
                 
                  </table>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
      @include('dashboards.admins.layouts.admin-footer')
    </div>
  </div>
  @include('dashboards.admins.layouts.theme')
  <!--   Core JS Files   -->
  @include('dashboards.admins.layouts.admin-script')



  <script>
    function myFunction1() {
        if (!confirm("Are you sure you want to delete this?"))
        event.preventDefault();
    }
  </script>


  <script type="text/javascript">
    $(document).ready(function() {
      var table = $('#pool').DataTable({
        processing: true,
         serverSide: true,
        ajax: "{{ route('prize.index') }}",

        columns: [
          {
            data: 'first_price',
            name: 'prize_pools.first_price'
          },
          {
            data: 'second_price',
            name: 'prize_pools.second_price'
          },
          {
            data: 'third_price',
            name: 'prize_pools.third_price'
          },
          {
            data: 'next_position_price',
            name: 'prize_pools.next_position_price'
          },


          
          {
            data: 'action',
            name: 'action',
            orderable: false,
            searchable: false
          },
        ]
      });
    });
  </script>

</body>

</html>
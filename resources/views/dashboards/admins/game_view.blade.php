<!DOCTYPE html>
<html lang="en">

@include('dashboards.admins.layouts.admin-head')


  <div class="wrapper">
  @include('dashboards.admins.layouts.admin-sidebar')
    <div class="main-panel">
      <!-- Navbar -->
      @include('dashboards.admins.layouts.admin-header')
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header mb-5">
              
                <h3 class="card-title head-font">Game Details</h3>
              </div>
              <div class="card-body">

                <table class="table">
                    <tbody>
                      <tr>
                        <th>Game</th>
                        <td class="text-center">{{$data->game_name}}</td>

                      </tr>
                      <tr>
                        <th>Platform</th>
                        <td class="text-center">
                        {{$data->game_name}}
                       </td>

                      </tr>
                      <tr>
                        <th>Genre</th>
                        <td class="text-center">{{$data->game_name}}</td>
                      </tr>
                      <tr>
                        <th>No. of Tournaments</th>
                        <td class="text-center">{{$data->tournamnets_no}}</td>
                      </tr>
                    </tbody>
                  </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      @include('dashboards.admins.layouts.admin-footer')
    </div>
  </div>
  @include('dashboards.admins.layouts.theme')
  <!--   Core JS Files   -->
  @include('dashboards.admins.layouts.admin-script')
</body>

</html>
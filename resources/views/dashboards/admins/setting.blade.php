<!DOCTYPE html>
<html lang="en">
@include('dashboards.admins.layouts.admin-head')
<div class="wrapper">
    @include('dashboards.admins.layouts.admin-sidebar')
    <div class="main-panel">
        <!-- Navbar -->
        @include('dashboards.admins.layouts.admin-header')
        <!-- End Navbar -->
        <div class="content">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="title head-font">Settings</h5>
                        </div>
                        <div class="card-body">
                            @foreach ($errors->all() as $error)
                                <p class="text-danger">{{ $error }}</p>
                            @endforeach
                            <form action="{{ route('setting') }}" method="post">
                                @csrf
                                @foreach ($settings as $val)
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label>{{ $val->label }}</label>
                                                <input type="text" class="form-control"
                                                    placeholder="{{ $val->label }}" name="{{ $val->key_name }}"
                                                    value="{{ $val->key_value }}">
                                            </div>
                                        </div>
                                        <div class="col-md-2 d-flex align-items-center">
                                            <span type="button" data-toggle="tooltip" data-placement="right"
                                                title="{{ $val->description }}">
                                                <i class="fa fa-info-circle mt-2" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                    </div>
                                @endforeach

                                <button type="submit" class="btn btn-fill btn-success">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('dashboards.admins.layouts.admin-footer')
    </div>
</div>
@include('dashboards.admins.layouts.theme')
<!--   Core JS Files   -->
@include('dashboards.admins.layouts.admin-script')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script>
    $(document).ready(function() {
        $("#form6").validate({
            rules: {
                admin_fee: 'admin_fee',
                max_token_donate: 'max_token_donate',
                max_amount: 'max_amount',
                min_amount: 'min_amount',
            },
        });
    });
    $.validator.addMethod('admin_fee',
        function(value) {
            return Number(value) > 0;
        }, 'Enter a positive Admin Fee.');
    $.validator.addMethod('max_token_donate',
        function(value) {
            return Number(value) > 0;
        }, 'Enter a positive Max Donate.');
    $.validator.addMethod('max_amount',
        function(value) {
            return Number(value) > 0;
        }, 'Enter a positive Max Amount.');
    $.validator.addMethod('min_amount',
        function(value) {
            return Number(value) > 0;
        }, 'Enter a positive Min Amount.');
</script>
</body>

</html>

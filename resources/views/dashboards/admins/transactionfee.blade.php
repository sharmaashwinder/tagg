<!DOCTYPE html>
<html lang="en">

@include('dashboards.admins.layouts.admin-head')



<div class="wrapper">
  @include('dashboards.admins.layouts.admin-sidebar')
  <div class="main-panel">
    <!-- Navbar -->
    @include('dashboards.admins.layouts.admin-header')
    <!-- End Navbar -->
    <div class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card ">
            <div class="card-header">
              <h4 class="card-title head-font">Transaction fee</h4>
            </div>


            <div class="col-lg-12 col-12 text-right">
              <!-- <a href="{{route('create.games')}}" class="btn btn-success custom-primary">Add New Game</a> -->
            </div>


            <div class="card-body">
              <div class="table-responsive">

                <div class="row">
                  <div class="col-sm-12 col-md-6">
                    <div class="" id=""><label>Show <select name="per_page" class="custom-select custom-select-sm form-control form-control-sm">
                          <option value="10">10</option>
                          <option value="25">25</option>
                          <option value="50">50</option>
                          <option value="100">100</option>
                        </select> entries</label></div>
                  </div>
                  <div class="col-sm-12 col-md-6">
                    <div id="" class="dataTables_filter"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" name='search'></label></div>
                  </div>
                </div>

                <table class="table tablesorter" id="yajra">
                  <thead class=" text-primary">
                    <tr>
                      <th scope="col">Transaction ID</th>
                      <th scope="col">User name</th>
                      <th scope="col">Fee type</th>
                      <th scope="col">Fee (USD)</th>
                      <th scope="col">Date</th>
                      <th scope="col"></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($data as $val)
                    @php
                    if ($val->transaction_for == 'add_token') {
                    $txn_type = 'Purchase Tokens';
                    }
                    if ($val->transaction_for == 'withdraw_token') {
                    $txn_type = 'Withdraw Tokens';
                    }
                    @endphp
                    <tr role="row" class="odd">
                      <td class="">{{$val->Transaction->transaction_id}}</td>
                      <td class="">{{$val->TxnUser->name}}</td>
                      <td class="">{{ $txn_type }}</td>
                      <td class="">{{$val->tagg_credit}}</td>
                      <td class="">{{$val->created_at}}</td>

                    </tr>

                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    @include('dashboards.admins.layouts.admin-footer')
  </div>
</div>
@include('dashboards.admins.layouts.theme')
<!--   Core JS Files   -->
@include('dashboards.admins.layouts.admin-script')

<script>
  $(document).ready(function() {
    
  });
</script>

</body>

</html>
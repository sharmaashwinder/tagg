<!DOCTYPE html>
<html lang="en">
@include('dashboards.admins.layouts.admin-head')

  <div class="wrapper">
    @include('dashboards.admins.layouts.admin-sidebar')
    <div class="main-panel">
      <!-- Navbar -->
      @include('dashboards.admins.layouts.admin-header')
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-8 mx-auto">
            <div class="card">
              <div class="card-header">
                <h5 class="title head-font">Create Tournament</h5>
              </div>
              <div class="card-body">
                <form method="POST" action="{{route('store.tournament')}}" id="form1" enctype="multipart/form-data">
                  @csrf
                  @foreach ($errors->all() as $error)
                  <p class="text-danger">{{ $error }}</p>
                  @endforeach

                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <label>Tournament Name</label>
                        <input type="text" class="form-control" placeholder="Tournment Name" name="name" value="<?php echo (isset($_SESSION['name']) ? $_SESSION['name'] : "");?>" id="name">
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <label>Host Name</label>
                        <input type="text" class="form-control" name="host_name">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <label>Speciality</label>
                        <input type="text" class="form-control" name="speciality">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <label>Game Name</label>
                        <select id="cars" name="game_id" class="form-control">
                          @if(!empty($game))
                          @foreach($game as $games)
                          <option value="{{$games->id}}">{{$games->game_name}}</option>
                          @endforeach
                          @endif
                        </select>
                      </div>
                    </div>
                  </div>

                  

                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <label for="start_time">Start Date</label>
                        <input type="date" class="form-control" name="start_date" id="start_date" min="<?php echo date('Y-m-d') ?>" value="<?php echo date('Y-m-d') ?>">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <label for="end_date">End Date</label>
                        <input type="date" class="form-control" id="end_date" name="end_date" min="<?php echo date('Y-m-d') ?>">
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <label>Start Time</label>
                        <input type="time" class="form-control" name="start_time" id="start_time"> 
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <label>End Time</label>
                        <input type="time" class="form-control" name="end_time" id="end_time">
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <label>No. of Players</label>
                        <input type="text" class="form-control" name="player_numbers">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <label>Skill Level</label>
                        <select id="cars" name="skill_level_id[]" class="form-control" multiple="">
                          @if(!empty($skill))
                          @foreach($skill as $skills)
                          <option value="{{$skills->id}}">{{$skills->name}}</option>
                          @endforeach
                          @endif
                        </select>
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <label>Format</label>
                        <select id="cars" name="format_id" class="form-control">
                        @if(!empty($format))
                          @foreach($format as $formates)
                          <option value="{{$formates->id}}">{{$formates->name}}</option>
                          @endforeach
                          @endif
                        </select>
                      </div>
                    </div>
                  </div>



                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <label>Entry Fees</label>
                        <input type="text" class="form-control" name="entry_fee">

                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <label>Prize Pool</label>
                        <select id="prize" name="prize_pool_id" class="form-control">
                          @if(!empty($prize))
                          @foreach($prize as $prizes)
                          @if(empty($prizes->next_position_price))
                          <option value="{{$prizes->id}}">{{$prizes->id}}-${{$prizes->first_price+$prizes->second_price+$prizes->third_price}}</option>
                          @else
                          <option value="{{$prizes->id}}">{{$prizes->id}}-${{$prizes->first_price+$prizes->second_price+$prizes->third_price+$prizes->next_position_price*$prizes->next_positions}}</option>
                          @endif
                          @endforeach
                          @endif
                        </select>
                      </div>
                    </div>
                    <div class="col-md-2 d-flex align-items-center">
                      <a class="btn btn-sm btn-success" href="{{route('create.prize')}}"  id="tournament">Add Prize</a>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <label>Status</label>
                        <select id="cars" name="status" class="form-control">
                          <option value="created">Created</option>
                          <option value="cancelled">Cancelled</option>
                          <option value="finished">Finished</option>
                          <option value="paid">Paid</option>
                        </select>
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                        <label for="exampleFormControlTextarea1">Description</label>
                        <textarea class="form-control custom-textarea" id="exampleFormControlTextarea1" rows="3" name="descriptions"></textarea>

                      </div>
                    </div>
                  </div>


                  <a title="Back" class="btn btn-light" href="{{ route('tournament')}}">
                    Back
                  </a>

                  <button type="submit" class="btn btn-fill btn-success">Save</button>
                </form>
                
              </div>

            </div>
          </div>

        </div>
      </div>
      @include('dashboards.admins.layouts.admin-footer')
    </div>
  </div>
  @include('dashboards.admins.layouts.theme')
  <!--   Core JS Files   -->
  @include('dashboards.admins.layouts.admin-script')


  <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
  <script>
    $(document).ready(function() {
      $('#form1').validate({
        rules: {
          name: {
            required: true
          },
          start_time: {
            required: true
          },

          end_date: 'end_date',

          skill_level_id: {
            required: true
          },
          formate: {
            required: true
          },
          entry_fee: {
            required: true
          },
          prize_pool: {
            required: true
          },
          status: {
            required: true
          },
         
          user_no: {
            required: true
          },
          start_date: {
            required: true
          },
          // endtime: {
          //   required: true
          // },
          end_time: {
            required: 'End Time is required',
            end_time_check: true
          },

          descriptions: {
            required: true
          },
          host_name: {
            required: true
          },
          speciality: {
            required: true
          },

        },
        messages: {
          name: {
            required: 'Tournament Name is required'
          },

          host_name: {
            required: 'Host Name is required'
          },

          speciality: {
            required: 'Speciality is required'
          },


          end_time: {
            required: 'Endtime is required'
          },
          skill_level_id: {
            required: 'Skill Level is required'
          },
          entry_fee: {
            required: 'Entry Fee is required'
          },
         
          user_no: {
            required: 'User No. is required'
          },
          start_date: {
            required: 'Start Date is required'
          },

          end_date: {
            required: 'End Date is required'
          },
          start_time: {
            required: 'Start Time is required'
          },

         
          descriptions: {
            required: 'Description is required'
          },

        }
      });
    });
  </script>


  <script type="text/javascript">
    $(document).ready(function() {
      $.validator.addMethod("end_date", function(value, element) {
        var start_date = $('#start_date').val();
        return Date.parse(start_date) <= Date.parse(value) || value == "";
      }, "* End date must be after start date");
    });
  </script>

<script type="text/javascript">
    $(document).ready(function() {
      $.validator.addMethod("end_time_check", function(value, element) {
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        if(start_date <= end_date){
          var start_time = $('#start_time').val();
          var end_time = $('#end_time').val();
          if(end_time <= start_time){
            return  value == ""; 
          }else{
            return  value;
          }
        }
      }, "* End Time must be after start Time");
    });
  </script>



  <script>
    $("#edit_prize").click(function() {
      var prize = $('#prize').val();
      var url = "{{ url('/') }}/admin/edit_prize/" + prize;
      window.location = url;
    });
  </script>




</body>

</html>
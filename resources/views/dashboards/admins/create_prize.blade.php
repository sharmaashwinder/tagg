<!DOCTYPE html>
<html lang="en">

@include('dashboards.admins.layouts.admin-head')

  <div class="wrapper">
  @include('dashboards.admins.layouts.admin-sidebar')
    <div class="main-panel">
      <!-- Navbar -->
      @include('dashboards.admins.layouts.admin-header')
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-8 mx-auto">
            <div class="card">
              <div class="card-header">
                <h5 class="title head-font">Create Prize Pool</h5>
              </div>
              <div class="card-body">
              <form method="POST" action="{{route('store.prize')}}" id="form">
                  @csrf
                  @foreach ($errors->all() as $error)
                  <p class="text-danger">{{ $error }}</p>
                  @endforeach

                  <div id="wrapper1">
                    <div class="row" id="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleFormControlSelect1">Position</label>
                          <select class="form-control" id="exampleFormControlSelect1" name="first_postion">
                            <option>Choose Position</option>
                            <option value="first_postion">1st Position</option>
                            <option value="second_position">2nd position</option>
                            <option value="third_postion">3rd position</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="row align-items-center">
                          <div class="col-md-10">
                            <div class="form-group">
                              <label>Price/TG</label>
                              <input type="text" class="form-control" name="first_price">
                            </div>
                          </div>
                          <div class="col-md-2" id="add_btn1">
                            <div class="mt-2">
                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>



                  <div id="wrapper2">
                    <div class="row" id="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleFormControlSelect1">Position</label>
                          <select class="form-control" id="exampleFormControlSelect1" name="second_position">
                            <option>Choose Position</option>
                            <option value="first_postion">1st Position</option>
                            <option value="second_position">2nd position</option>
                            <option value="third_postion">3rd position</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="row align-items-center">
                          <div class="col-md-10">
                            <div class="form-group">
                              <label>Price/TG</label>
                              <input type="text" class="form-control" name="second_price">
                            </div>
                          </div>
                          <div class="col-md-2" id="add_btn2">
                            <div class="mt-2">
                              <i class="fa fa-minus-circle minus_btn_red" aria-hidden="true"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="wrapper3">
                    <div class="row" id="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleFormControlSelect1">Position</label>
                          <select class="form-control" id="exampleFormControlSelect1" name="third_postion">
                            <option>Choose Position</option>
                            <option value="first_postion">1st Position</option>
                            <option value="second_position">2nd position</option>
                            <option value="third_postion">3rd position</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="row align-items-center">
                          <div class="col-md-10">
                            <div class="form-group">
                              <label>Price/TG</label>
                              <input type="text" class="form-control" name="third_price">
                            </div>
                          </div>
                          <div class="col-md-2" id="add_btn3">
                            <div class="mt-2">
                              <i class="fa fa-minus-circle minus_btn_red" aria-hidden="true"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- work here  -->
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Runner up</label>
                        <input type="text" class="form-control" name="next_positions">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="row">
                        <div class="col-md-10">
                          <div class="form-group">
                            <label>Price/TG</label>
                            <input type="text" class="form-control" name="next_position_price">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" class="form-control" name="url" value=" {{ URL::previous() }}">



                    @if(app('router')->getRoutes()->match(app('request')
                  ->create(URL::previous()))->getName() == 'create.tournament')
                 
                  <a title="Back" class="btn btn-light" href="{{ route('create.tournament')}}">
                        Back
                      </a>
                      @else
                      <a title="Back" class="btn btn-light" href="{{ route('prize.index')}}">
                        Back
                      </a>
                      @endif
                  <button type="submit" class="btn btn-fill btn-success">Save</button>
                
                 
                 
                </form>
              </div>
             
            </div>
          </div>
          
        </div>
      </div>
      @include('dashboards.admins.layouts.admin-footer')
    </div>
  </div>
  @include('dashboards.admins.layouts.theme')
  <!--   Core JS Files   -->
  @include('dashboards.admins.layouts.admin-script')

  <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

<script>
var a = 0;
$("#add_btn1").click(function() {
if(a == 0){
  $("#wrapper2").show()
}
 else{
  $("#wrapper3").show()
 }
if(a == 0){
  a++;
}

});
</script>


  <script>
    $(document).ready(function() {
      
      $('#add_btn3').click(function() {
        $("#wrapper3").hide()
        if(a != 0)
        a--;
      });

      $('#add_btn2').click(function() {
        $("#wrapper2").hide()
        if(a != 0)
        a--;
      });

    })
  </script>
  <script>
    $("#wrapper2").hide();
    $("#wrapper3").hide();
  </script>
</body>

</html>
<div class="wrapper">
    <div class="sidebar sidebar-theme">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red"
    -->
      <div class="sidebar-wrapper">
        <div class="logo">
         
          <a class="navbar-brand admin-dash-logo" href="javascript:void(0)">
          <img src="{{asset('frontend/assets/img/tagg-logo.png')}}" class="navbar-brand-img" alt="Brand logo" title="brand logo" width="200" height="auto">
        </a>


        </div>
        <ul class="nav">
          <li class="{{ Request::routeIs('admin.dashboard') ? 'active' : '' }}">
            <a  href="{{ route('admin.dashboard')}}">
              <i class="tim-icons icon-chart-pie-36"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="{{ Request::routeIs('admin.player') ? 'active' : '' }}">
            <a  href="{{ route('admin.player')}}">
              <i class="tim-icons icon-bullet-list-67"></i>
              <p>Player Management</p>
            </a>
          </li>
          <li class="{{ Request::routeIs('create.game') ? 'active' : '' }}">
            <a href="{{ route('create.game')}}">
              <i class="tim-icons icon-puzzle-10"></i>
              <p>Games</p>
            </a>
          </li>
          <li class="{{ Request::routeIs('prize.index') ? 'active' : '' }}">
            <a href="{{ route('prize.index')}}">
              <i class="tim-icons icon-trophy"></i>
              <p>Prize Pool</p>
            </a>
          </li>
          <li class="{{ Request::routeIs('tournament') ? 'active' : '' }}">
            <a href="{{ route('tournament')}}">
              <i class="tim-icons icon-chart-bar-32"></i>
              <p>Tournaments</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    
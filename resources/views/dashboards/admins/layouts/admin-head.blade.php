<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('/admin-assets/img/apple-icon.png')}}">
  <link rel="icon" type="image/png" href="{{asset('/admin-assets/img/favicon.ico')}}">
  <title>
    Tagg Us-Admin
  </title>
 
  <!--     Fonts and icons     -->
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
  <!-- Nucleo Icons -->
  <link href="{{asset('/admin-assets/css/nucleo-icons.css')}}" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="{{asset('/admin-assets/css/black-dashboard.css?v=1.0.0')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{asset('/admin-assets/demo/demo.css')}}" rel="stylesheet" />

  <link href="{{asset('/admin-assets/css/admin-new.css')}}" rel="stylesheet">

</head>

      @php
        if (Session::has('theme_mode')) {
          
            $theme_mode = Session::get('theme_mode');
            if ($theme_mode == "light") {
                $theme_selected = "white-content";
            } else {
                $theme_selected = " ";
            }
        } else {
           
            $theme_selected = " ";
        }

        @endphp 

        <body class="{{$theme_selected}}">
        


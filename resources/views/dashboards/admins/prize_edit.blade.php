<!DOCTYPE html>
<html lang="en">

@include('dashboards.admins.layouts.admin-head')


  <div class="wrapper">
  @include('dashboards.admins.layouts.admin-sidebar')
    <div class="main-panel">
      <!-- Navbar -->
      @include('dashboards.admins.layouts.admin-header')
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-8 mx-auto">
            <div class="card">
              <div class="card-header">
                <h5 class="title head-font">Edit Prize Pool</h5>
              </div>
              <div class="card-body">
              <form method="POST" action="{{route('update.prize',$data->id)}}" id="form">
                  @csrf
                  @foreach ($errors->all() as $error)
                  <p class="text-danger">{{ $error }}</p>
                  @endforeach

                  <div id="wrapper1">
                    <div class="row" id="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleFormControlSelect1">Position</label>
                         <select class="form-control" id="exampleFormControlSelect1" name="first_postion" value="{{$data->first_postion}}">
                            <option value="first_postion" @if ($data->first_postion)
                                selected="selected"@endif>1st Position</option> 
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="row align-items-center">
                          <div class="col-md-10">
                            <div class="form-group">
                              <label>Price/TG</label>
                              <input type="text" class="form-control" name="first_price" value="{{$data->first_price}}">
                            </div>
                          </div>
                          <div class="col-md-2" id="add_btn1">
                            <div class="mt-2">
                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>


               @if($data->second_price)
                  <div id="wrapper2">
                    <div class="row" id="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleFormControlSelect1">Position</label>
                          <select class="form-control" id="exampleFormControlSelect1" name="second_position" value="{{$data->second_position}}">
                            <option value="second_position" @if ($data->second_position)
                                selected="selected"@endif>2nd position</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="row align-items-center">
                          <div class="col-md-10">
                            <div class="form-group">
                              <label>Price/TG</label>
                              <input type="text" class="form-control"  name="second_price" value="{{$data->second_price}}">
                            </div>
                          </div>
                          <div class="col-md-2" id="add_btn2">
                            <div class="mt-2">
                              <i class="fa fa-minus-circle minus_btn_red" aria-hidden="true"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endif


                  @if($data->third_price)
                  <div id="wrapper3">
                    <div class="row" id="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleFormControlSelect1">Position</label>
                           <select class="form-control" id="exampleFormControlSelect1" name="third_postion" value="{{$data->third_postion}}">
                            <option value="third_postion" @if ($data->third_postion)
                                selected="selected"@endif>3rd position</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="row align-items-center">
                          <div class="col-md-10">
                            <div class="form-group">
                              <label>Price/TG</label>
                              <input type="text" class="form-control" name="third_price" value="{{$data->third_price}}">
                            </div>
                          </div>
                          <div class="col-md-2" id="add_btn3">
                            <div class="mt-2">
                              <i class="fa fa-minus-circle minus_btn_red" aria-hidden="true"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endif
                  <!-- work here  -->
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Runner up</label>
                        <input type="text" class="form-control" name="next_positions" value="{{$data->next_positions}}">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="row">
                        <div class="col-md-10">
                          <div class="form-group">
                            <label>Price/TG</label>
                            <input type="text" class="form-control" name="next_position_price" value="{{$data->next_position_price}}">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" class="form-control" name="url" value=" {{ URL::previous() }}">
                  
        
                 @if(app('router')->getRoutes()->match(app('request')
                  ->create(URL::previous()))->getName() == 'tournament.games')
                  @php
                  $a = request()->tournamnet_id;
                  @endphp

                  <a title="Back" class="btn btn-light" href="{{route('tournament.games',$a)}}">
                        Back
                      </a>
                     @else 
                  <a title="Back" class="btn btn-light" href="{{ route('prize.index')}}">
                        Back
                      </a>
                    @endif



                  <button type="submit" class="btn btn-fill btn-success">Update</button>


                </form>
              </div>
             
            </div>
          </div>
          
        </div>
      </div>
      @include('dashboards.admins.layouts.admin-footer')
    </div>
  </div>
  @include('dashboards.admins.layouts.theme')
  <!--   Core JS Files   -->
  @include('dashboards.admins.layouts.admin-script')




</body>

</html>
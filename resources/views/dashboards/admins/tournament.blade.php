
<!DOCTYPE html>
<html lang="en">

@include('dashboards.admins.layouts.admin-head')

  <div class="wrapper">
  @include('dashboards.admins.layouts.admin-sidebar')
    <div class="main-panel">
      <!-- Navbar -->
      @include('dashboards.admins.layouts.admin-header')
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header">
                <h4 class="card-title head-font">All Tournaments</h4>
              </div>


              <div class="col-lg-12 col-12 text-right">
                <a href="{{route('create.tournament')}}" class="btn btn-success custom-primary">Add New Tournaments</a>
              </div>


              <div class="card-body">
                <div class="table-responsive">
                  <table class="table tablesorter" id="yajra1">
                    <thead class=" text-primary">
                      <tr>
                        <th>
                        Tournament Name
                        </th>
                        <th>
                        Host Name
                        </th>
                        <th>
                        Speciality
                        </th>
                        <th>
                        Game Name
                        </th>
                        <th>
                        Start Time
                        </th>
                        <th>
                        End Time
                        </th>
                        <th>
                        Status
                        </th>

                        <!-- <th>
                        Skill Level
                        </th> -->
                        <th>
                        Format
                        </th>
                        <th>
                        Entry Fees
                        </th>
                        <th>
                        Prize Pool
                        </th>

                        <th>
                        Action
                        </th>
                      </tr>
                    </thead>
                 
                  </table>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
      @include('dashboards.admins.layouts.admin-footer')
    </div>
  </div>
  @include('dashboards.admins.layouts.theme')
  <!--   Core JS Files   -->
  @include('dashboards.admins.layouts.admin-script')



 <script>
   function myFunction1() {
     if (!confirm("Are you sure?,This record and it`s details will be permanantly deleted!"))
       event.preventDefault();
   }
 </script>


<script type="text/javascript">
    $(document).ready(function() {
      var table = $('#yajra1').DataTable({
        processing: true,
         serverSide: true,
        ajax: "{{ route('tournament') }}",
        columns: [{
            data: 'name',
            name: 'tournaments.name'
          },

          {
            data: 'host_name',
            name: 'tournaments.host_name'
          },

          {
            data: 'speciality',
            name: 'tournaments.speciality'
          },
          {
            data: 'game_name',
            name: 'games.game_name'
          },
          {
            data: 'start_time',
            name: 'tournaments.start_time'
          },
          {
            data: 'end_time',
            name: 'tournaments.end_time'
          },
          {
            data: 'status',
            name: 'tournaments.status'
          },
          // {
          //   data: 'skill_level',
          //   name: 'tournaments.skill_level'
          // },
          {
            data: 'name',
            name: 'formats.name'
          },
          {
            data: 'entry_fee',
            name: 'tournaments.entry_fee'
          },
          {
            data: 'prize_pool_id',
            name: 'tournaments.prize_pool_id'
          },
          {
            data: 'action',
            name: 'action',
            orderable: false,
            searchable: false
          },
        ]
      });
    });
  </script>

</body>

</html>
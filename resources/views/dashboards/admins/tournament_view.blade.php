<!DOCTYPE html>
<html lang="en">

@include('dashboards.admins.layouts.admin-head')

  <div class="wrapper">
  @include('dashboards.admins.layouts.admin-sidebar')
    <div class="main-panel">
      <!-- Navbar -->
      @include('dashboards.admins.layouts.admin-header')
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header mb-5">
              
                <h3 class="card-title head-font">Tournament Details</h3>
              </div>
              <div class="card-body">

                <table class="table">
                    <tbody>
                      <tr>
                        <th>Tournament Name</th>
                        <td class="text-center">{{$data->name}}</td>

                      </tr>
                      <tr>
                        <th>Start Time</th>
                        <td class="text-center">{{$data->start_time}}</td>

                      </tr>
                      <tr>
                        <th>End Time</th>
                        <td class="text-center">{{$data->end_time}}</td>
                      </tr>
                      <tr>
                        <th>Status</th>
                        <td class="text-center">{{$data->status}}</td>
                      </tr>

                      <tr>
                   




                        <th>Skill Level</th>
                        @foreach ($skilllevel as $skilllevels)
                       
                        <td class="text-center">{{$skilllevels->names}}</td>
                        
                        @endforeach
                      </tr>

                      <tr>
                        <th>Format</th>
                        <td class="text-center">{{$data->format_id}}</td>
                      </tr>

                      <tr>
                        <th>Entry Fees</th>
                        <td class="text-center">${{$data->entry_fee}}</td>
                      </tr>
                      <tr>
                        <th>Description</th>
                        <td class="text-center">{{$data->descriptions}}</td>
                      </tr>
                      <tr>
                        <th>Player No.</th>
                        <td class="text-center">{{$data->player_numbers}}</td>
                      </tr>

                      
                      
                      <!-- <tr>
                        <th>Prize Pool</th>
                        <td class="text-center">${{$data->prize_pool_id}}</td>
                      </tr> -->




                    </tbody>
                  </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      @include('dashboards.admins.layouts.admin-footer')
    </div>
  </div>
  @include('dashboards.admins.layouts.theme')
  <!--   Core JS Files   -->
  @include('dashboards.admins.layouts.admin-script')
</body>

</html>
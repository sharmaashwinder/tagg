<!DOCTYPE html>
<html>

@include('dashboards.users.layout.head')

<body>
    <div class="outer-container">
        @include('dashboards.users.layout.header')
        <!-- header end -->
        <!-- banner end -->
        <div class="banner-outer bg-color2">
            <div class="inner-container-sm">
                <div class="color-box c-my-5">
                    <div class="text-center">
                        <h1 class="heading-1 md pb-3"><span class="theme-color md">Edit Social Accounts</span></h1>
                    </div>

                    <form method="POST" action="{{ route('savesocial.permissions') }}" id="form2">
                        @csrf

                        @foreach ($errors->all() as $error)
                        <p class="text-danger">{{ $error }}</p>
                        @endforeach

                        <div class="row align-items-center my-4 justify-content-center">
                            <label for="facebook" class="col-md-3 typo4 pb-md-0 pb-2 text-md-end text-start">Facebook</label>
                            <div class="col-md-7">
                                @if ($data[0]->facebook_url)
                                {{-- <input class="form-control input-transparent" placeholder="Email*" id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" /> --}}
                                <input name="facebook_url" type="url" class="form-control  input-transparent" value="{{$data[0]->facebook_url}}">
                                @else
                                <input name="facebook_url" type="url" class="form-control  input-transparent">
                                @endif

                            </div>

                        </div>

                        <div class="row align-items-center my-4 justify-content-center">
                            <label for="facebook" class="col-md-3 typo4 pb-md-0 pb-2 text-md-end text-start">Twitter</label>
                            <div class="col-md-7">
                                @if ($data[0]->twitter_url)
                                <input name="twitter_url" type="url" class="form-control input-transparent" value="{{$data[0]->twitter_url}}">
                                @else
                                <input name="twitter_url" type="url" class="form-control input-transparent">
                                @endif
                            </div>

                        </div>
                        <div class="row align-items-center my-4 justify-content-center">
                            <label for="facebook" class="col-md-3 typo4 pb-md-0 pb-2 text-md-end text-start">Insta</label>
                            <div class="col-md-7">
                                @if ($data[0]->insta_url)
                                <input name="insta_url" type="url" class="form-control input-transparent " value="{{$data[0]->insta_url}}">
                                @else
                                <input name="insta_url" type="url" class="form-control input-transparent">
                                @endif
                            </div>

                        </div>


                        <div class="row align-items-center my-4 justify-content-center">
                            <label for="facebook" class="col-md-3 typo4 pb-md-0 pb-2 text-md-end text-start">LinkedIn</label>
                            <div class="col-md-7">
                                @if ($data[0]->linkedin_url)
                                <input name="linkedin_url" type="url" class="form-control input-transparent" value="{{$data[0]->linkedin_url}}">
                                @else
                                <input name="linkedin_url" type="url" class="form-control input-transparent">
                                @endif
                            </div>

                        </div>
                        <div class="row align-items-center my-4 justify-content-center">
                            <label for="facebook" class="col-md-3 typo4 pb-md-0 pb-2 text-md-end text-start">Discord</label>
                            <div class="col-md-7">
                                @if ($data[0]->discord_url)
                                <input name="discord_url" type="url" class="form-control input-transparent" value="{{$data[0]->discord_url}}">
                                @else
                                <input name="discord_url" type="url" class="form-control input-transparent">
                                @endif
                            </div>

                        </div>

                        <div class="row align-items-center my-4 justify-content-center">
                            <label for="facebook" class="col-md-3 typo4 pb-md-0 pb-2 text-md-end text-start">Twitch</label>
                            <div class="col-md-7">
                                @if ($data[0]->twitch_url)
                                <input name="twitch_url" type="url" class="form-control input-transparent" value="{{$data[0]->twitch_url}}">
                                @else
                                <input name="twitch_url" type="url" class="form-control input-transparent">
                                @endif
                            </div>

                        </div>


                        <div class="row align-items-center my-4 justify-content-center">
                            <label class="col-md-3 typo4 pb-md-0 pb-2 text-md-end text-start">Show Accounts</label>
                            <div class="col-md-7">
                                <div class="container">
                                    <label class="switch" for="checkbox">
                                        @if ($data[0]->profile_show)
                                        <input type="checkbox" id="checkbox" name="profile_show" checked />
                                        @else
                                        <input type="checkbox" id="checkbox" name="profile_show" />
                                        @endif
                                        <div class="slider round"></div>
                                    </label>
                                </div>

                            </div>
                        </div>



                        <div class="row align-items-center justify-content-center pt-md-3 pt-2">
                            <div class="col-md-3"></div>
                            <div class="col-md-7">
                                <button type="submit" class="btn-yellow-fill lg custom-row text-uppercase">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>

                    {{-- <script>
                        $(document).ready(function() {
                            $("#form2").validate({
                                rules: {
                                    facebook_url: {
                                        required: false,
                                        url: true
                                    },
                                    phone: 'phone',
                                    linkedin_url: {
                                        required: false,
                                        url: true
                                    },
                                    twitter_url: {
                                        required: false,
                                        url: true

                                    },
                                    insta_url: {
                                        required: true,
                                        url: true
                                    },
                                    linkedin_url: {
                                        required: false,
                                        url: true
                                    }



                                },
                                messages: {
                                    phone: {
                                        required: 'Phone is required'
                                    },
                                    email: {
                                        required: 'Email is required'
                                    },
                                    name: {
                                        required: 'Gamer Name is required'
                                    },
                                    comment: {
                                        required: 'Comments is required'
                                    }

                                }
                            });
                        });
                        $.validator.methods.email = function(value, element) {

                            return this.optional(element) || /^[a-z0-9][-a-z0-9.!#$%&'*+-=?^_`{|}~\/]+@([-a-z0-9]+\.)+[a-z]{2,5}$/.test(value);
                        }

                        $.validator.addMethod('phone', function(value, element) {
                            return this.optional(element) || /^(\+\d{1,3}[- ]?)?\d{10}$/.test(value);
                        }, "Please enter a valid phone number");
                    </script> --}}
                    <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">

                    <script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
                    <script>
                        @if(Session::has('message'))
                        var type = "{{ Session::get('alert-type', 'info') }}";
                        switch (type) {
                            case 'info':
                                toastr.info("{{ Session::get('message') }}");
                                break;

                            case 'warning':
                                toastr.warning("{{ Session::get('message') }}");
                                break;

                            case 'success':
                                toastr.success("{{ Session::get('message') }}");
                                break;

                            case 'error':
                                toastr.error("{{ Session::get('message') }}");
                                break;
                        }
                        @endif
                    </script>

                </div>
            </div>
            <div class="col-md-6">
                sandeep
            </div>
        </div>
    </div>
    </div>
    </div>
    <!-- popular start -->
    {{-- @include('dashboards.users.layout.popular') --}}
    <!-- Subscribe start -->
    {{-- @include('dashboards.users.layout.subscribe') --}}
    <!-- footer start -->
    @include('dashboards.users.layout.footer')
    </div>
    {{-- //////////////////////////////////////// --}}


</body>

</html>

<!DOCTYPE html>
<html>

@include('dashboards.users.layout.head')

<body>
  <div class="outer-container">
    @include('dashboards.users.layout.header')
    <!-- header end -->
    <div class="banner-outer bg-color2">
      <!-- Page content -->
      <div class="inner-container-sm">
        <div class="color-box c-my-5">
          <div class="text-center">
            <h1 class="heading-1 md pb-3"><span class="theme-color md">Change Password</span></h1>
          </div>
          <form method="POST" action="{{ route('password_change') }}" id="form2">
            @csrf

            @foreach ($errors->all() as $error)
            <p class="text-danger">{{ $error }}</p>
            @endforeach

            <div class="row align-items-center my-4 justify-content-center">
              <label for="password" class="col-md-4 typo4 pb-md-0 pb-2 text-md-end text-start">Current Password</label>

              <div class="col-md-6">
                <input id="password" type="password" class="form-control input-transparent" name="current_password" autocomplete="current-password">
              </div>
            </div>

            <div class="row align-items-center justify-content-center my-4">
              <label for="new_password" class="col-md-4 typo4 pb-md-0 pb-2 text-md-end text-start">New Password</label>

              <div class="col-md-6">
                <input id="new_password" type="password" class="form-control input-transparent" name="new_password" autocomplete="current-password">
              </div>
            </div>

            <div class="row align-items-center justify-content-center my-4">
              <label for="new_confirm_password" class="col-md-4 typo4 pb-md-0 pb-2 text-md-end text-start">Confirm New Password</label>

              <div class="col-md-6">
                <input id="new_confirm_password" type="password" class="form-control input-transparent" name="new_confirm_password" autocomplete="current-password">
              </div>
            </div>

            <div class="row align-items-center justify-content-center pt-md-3 pt-2">
              <div class="col-md-4"></div>
              <div class="col-md-6">
                <button type="submit" class="btn-yellow-fill lg custom-row text-uppercase">
                  Update Password
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- Footer -->
    @include('dashboards.users.layout.footer')
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  @include('dashboards.users.layout.script')
  <link rel="stylesheet" href="https://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">

  <script src="https://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
  <script>
    @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch (type) {
      case 'info':
        toastr.info("{{ Session::get('message') }}");
        break;

      case 'warning':
        toastr.warning("{{ Session::get('message') }}");
        break;

      case 'success':
        toastr.success("{{ Session::get('message') }}");
        break;

      case 'error':
        toastr.error("{{ Session::get('message') }}");
        break;
    }
    @endif
  </script>


  <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
  <script>
    $(document).ready(function() {
      $('#form2').validate({
        rules: {
          current_password: {
            required: true
          },
          new_password: {
            required: true
          },
          new_confirm_password: {
            required: true,
            equalTo: "#new_password",

          },
        },
        messages:{
          current_password:{
              required:' Current Password is required'
            },
            new_password:{
              required:'New Password is required'
            },
            new_confirm_password:{
                required:'New Confirm Password is required',
                equalTo:'Password does not match'
            },

        }


      });
    });
  </script>


<!-- <script type="text/javascript">
    $(document).ready(function() {
        $.validator.addMethod("current_password", function(value, element) {
          var password =   $('#password').val();
          console.log(password);
            return Date.parse(start_date) <= Date.parse(value) || value == "";
        }, "* End date must be after start date");

    });
</script> -->
</body>

</html>

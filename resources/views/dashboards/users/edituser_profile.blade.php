<!DOCTYPE html>
<html>

@include('dashboards.users.layout.head')

<body>
  <div class="outer-container">
    @include('dashboards.users.layout.header')
    <!-- header end -->
    <div class="banner-outer bg-color2">
      <div class="inner-container-sm">
        <div class="color-box c-my-5">
          <div class="text-center">
            <h1 class="heading-1 md pb-3"><span class="theme-color md">Edit Social Accounts</span></h1>
          </div>
          <form method="POST" action="{{ route('profilesave') }}" id="form2" enctype="multipart/form-data">
            @csrf

            @foreach ($errors->all() as $error)
            <p class="text-danger">{{ $error }}</p>
            @endforeach
            <div class="row align-items-center my-4 justify-content-center">
              <label for="facebook" class="col-md-3 typo4 pb-md-0 pb-2 text-md-end text-start">Gamer Name</label>
              <div class="col-md-7">
                @if(isset(Auth::user()->gamer_name))
                <input name="gamer_name" id="gamer_name" type="text" class="form-control input-transparent" value="{{Auth::user()->gamer_name}}" />
                @else
                <input name="gamer_name" id="gamer_name" type="text" class="form-control input-transparent" />
                @endif
              </div>

            </div>

            <div class="row align-items-center my-4 justify-content-center">
              <label for="Name" class="col-md-3 typo4 pb-md-0 pb-2 text-md-end text-start">Name</label>
              <div class="col-md-7">
                @if(isset(Auth::user()->name))
                <input name="name" id="name" type="text" class="form-control input-transparent" value="{{Auth::user()->name}}" style='text-transform:uppercase'>
                @else

                <input type="text" name="name" id="name" class="form-control input-transparent">
                @endif
              </div>

            </div>

            <div class="row align-items-center my-4 justify-content-center">
              <div class="col-md-3 typo4 pb-md-0 pb-2 d-flex align-items-center justify-content-start justify-content-md-end">
                <label for="facebook" class="">Date Of Birth </label>
                <div class="custom-col ms-2">
                  <div class="tooltip-col custom-col">&#161;</div>
                  <div class="tooltip-box">Please ensure that You are 14+</div>
                </div>
              </div>
              <div class="col-md-7">
                @if(isset(Auth::user()->date_of_birth))
                <input name="date_of_birth" id="date_of_birth" type="date" class="form-control input-transparent" value="{{Auth::user()->date_of_birth}}" />
                @else
                <input name="date_of_birth" id="date_of_birth" type="date" class="form-control input-transparent" />
                @endif
              </div>

            </div>


            <div class="row align-items-center my-4 justify-content-center">
              <label for="country" class="col-md-3 typo4 pb-md-0 pb-2 text-md-end text-start">Country</label>
              <div class="col-md-7">
                @if(isset(Auth::user()->country))
                <input name="country" id="country" type="text" class="form-control input-transparent" value="{{Auth::user()->country}}" style='text-transform:uppercase'>
                @else

                <input type="text" name="country" id="country" class="form-control input-transparent">
                @endif
              </div>

            </div>

            <div class="row align-items-center my-4 justify-content-center">
              <label for="gender" class="col-md-3 typo4 pb-md-0 pb-2 text-md-end text-start">Gender</label>
              <div class="col-md-7">
                <select class="form-control input-transparent" name="gender" id="gender">
                  @if(auth()->user()->gender=='Male')
                  <option value="Male" selected>Male</option>
                  <option value="Female">Female</option>
                  @else
                  <option value="Male">Male</option>
                  <option value="Female" selected>Female</option>
                  @endif


                </select>
                {{-- @if(isset(Auth::user()->gender))
                                    <input name = "country" id="gender" type = "text" class = "form-control input-transparent"  value = "{{Auth::user()->gender}}" style = 'text-transform:uppercase'>
                @else

                <input type="text" name="country" id="gender" class="form-control input-transparent">
                @endif --}}
              </div>

            </div>
            <div class="row align-items-center my-4 justify-content-center">
              <label for="bio" class="col-md-3 typo4 pb-md-0 pb-2 text-md-end text-start">About</label>
              <div class="col-md-7">
               
                <textarea name="bio" id="bio" class="form-control input-transparent"> {{Auth::user()->bio}} </textarea>
               
              </div>

            </div>


            {{-- <div class="row">
                                  <div class = "form-group col-md-4">
                                  <label for = "profile_image pb-md-0 pb-2">Profile Picture</label>
                                  @if(auth()->user()->profile_photo_path)
                                    <img id = "preview_img" src = {{ asset('/storage/'.auth()->user()->profile_photo_path) }} class = "" width = "100" height = "100"/>
            @else
            <img id="preview_img" src="http://w3adda.com/wp-content/uploads/2019/09/No_Image-128.png" class="" width="200" height="150" />
            @endif
        </div>
      </div> --}}



      {{-- <input  type = "file" name    = "profile_photo_path" id = "profile_photo_path" onchange = "loadPreview(this);"  accept = "image/x-png,image/gif,image/jpeg" class = "form-control"> --}}
      <div class="row align-items-center justify-content-center pt-md-3 pt-2">
        <div class="col-md-3"></div>
        <div class="col-md-7">
          <button type="submit" class="btn-yellow-fill lg custom-row text-uppercase">Submit</button>
        </div>
      </div>

      </form>





@include('toaster')

    </div>
  </div>
  <div class="col-md-6">
    sandeep
  </div>
  </div>
  </div>
  </div>
  </div>

  @include('dashboards.users.layout.footer')
  </div>

  {{-- --}}
  <script>
    $(function() {
      var dtToday = new Date();

      var month = dtToday.getMonth() + 1; // jan=0; feb=1 .......
      var day = dtToday.getDate();
      var year = dtToday.getFullYear() - 14;
      if (month < 10)
        month = '0' + month.toString();
      if (day < 10)
        day = '0' + day.toString();
      var minDate = year + '-' + month + '-' + day;
      var maxDate = year + '-' + month + '-' + day;
      $('#date_of_birth').attr('max', maxDate);
    });
  </script>

  <script>
    $(document).ready(function() {


      $('#form2').validate({
        rules: {
          date_of_birth: {
            required: true
          },
          gamer_name: {
            required: true,

          },
          country: {
            country: true
          },
          name: {
            required: false
          },
          profile_photo_path: {
            required: false,
          }
        },
        messages: {
          gamer_name: {
            required: ' Gamer Name is required'
          },
          name: {
            name: 'Name is required'
          }



        }


      });


    });
  </script>
  <script>
    $.validator.addMethod("country", function(value, element) {
      return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
    }, "enter valid country name");
  </script>

  {{-- <script>
    function loadPreview(input, id) {
      id = id || '#preview_img';
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $(id)
                      .attr('src', e.target.result)
                      .width(200)
                      .height(150);
          };

          reader.readAsDataURL(input.files[0]);
      }
   }
  </script> --}}

</body>

</html>


<!DOCTYPE html>
<html>
    <head>
@include('dashboards.users.layout.head')
{{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/> --}}
{{-- <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet"> --}}
{{-- <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet"> --}}
<style>
body {
    color: #797979;
    background: #f1f2f7;
    font-family: 'Open Sans', sans-serif;
    padding: 0px !important;
    margin: 0px !important;
    font-size: 13px;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-font-smoothing: antialiased;
}

.profile-nav, .profile-info{
    margin-top:30px;
}

.profile-nav .user-heading {
    background: #fbc02d;
    color: #fff;
    border-radius: 4px 4px 0 0;
    -webkit-border-radius: 4px 4px 0 0;
    padding: 30px;
    text-align: center;
}

.profile-nav .user-heading.round a  {
    border-radius: 50%;
    -webkit-border-radius: 50%;
    border: 10px solid rgba(255,255,255,0.3);
    display: inline-block;
}

.profile-nav .user-heading a img {
    width: 112px;
    height: 112px;
    border-radius: 50%;
    -webkit-border-radius: 50%;
}

.profile-nav .user-heading h1 {
    font-size: 22px;
    font-weight: 300;
    margin-bottom: 5px;
}

.profile-nav .user-heading p {
    font-size: 12px;
}

.profile-nav ul {
    margin-top: 1px;
}

.profile-nav ul > li {
    border-bottom: 1px solid #ebeae6;
    margin-top: 0;
    line-height: 30px;
}

.profile-nav ul > li:last-child {
    border-bottom: none;
}

.profile-nav ul > li > a {
    border-radius: 0;
    -webkit-border-radius: 0;
    color: #89817f;
    border-left: 5px solid #fff;
}

.profile-nav ul > li > a:hover, .profile-nav ul > li > a:focus, .profile-nav ul li.active  a {
    background: #f8f7f5 !important;
    border-left: 5px solid #fbc02d;
    color: #89817f !important;
}

.profile-nav ul > li:last-child > a:last-child {
    border-radius: 0 0 4px 4px;
    -webkit-border-radius: 0 0 4px 4px;
}

.profile-nav ul > li > a > i{
    font-size: 16px;
    padding-right: 10px;
    color: #bcb3aa;
}

.r-activity {
    margin: 6px 0 0;
    font-size: 12px;
}


.p-text-area, .p-text-area:focus {
    border: none;
    font-weight: 300;
    box-shadow: none;
    color: #c3c3c3;
    font-size: 16px;
}

.profile-info .panel-footer {
    background-color:#f8f7f5 ;
    border-top: 1px solid #e7ebee;
}

.profile-info .panel-footer ul li a {
    color: #7a7a7a;
}

.bio-graph-heading {
    background: #fbc02d;
    color: #fff;
    text-align: center;
    font-style: italic;
    padding: 40px 110px;
    border-radius: 4px 4px 0 0;
    -webkit-border-radius: 4px 4px 0 0;
    font-size: 16px;
    font-weight: 300;
}

.bio-graph-info {
    color: #89817e;
}

.bio-graph-info h1 {
    font-size: 22px;
    font-weight: 300;
    margin: 0 0 20px;
}

.bio-row {
    width: 50%;
    float: left;
    margin-bottom: 10px;
    padding:0 15px;
}

.bio-row p span {
    width: 100px;
    display: inline-block;
}

.bio-chart, .bio-desk {
    float: left;
}

.bio-chart {
    width: 40%;
}

.bio-desk {
    width: 60%;
}

.bio-desk h4 {
    font-size: 15px;
    font-weight:400;
}

.bio-desk h4.terques {
    color: #4CC5CD;
}

.bio-desk h4.red {
    color: #e26b7f;
}

.bio-desk h4.green {
    color: #97be4b;
}

.bio-desk h4.purple {
    color: #caa3da;
}

.file-pos {
    margin: 6px 0 10px 0;
}

.profile-activity h5 {
    font-weight: 300;
    margin-top: 0;
    color: #c3c3c3;
}

.summary-head {
    background: #ee7272;
    color: #fff;
    text-align: center;
    border-bottom: 1px solid #ee7272;
}

.summary-head h4 {
    font-weight: 300;
    text-transform: uppercase;
    margin-bottom: 5px;
}

.summary-head p {
    color: rgba(255,255,255,0.6);
}
/*
ul.summary-list {
    display: inline-block;
    padding-left:0 ;
    width: 100%;
    margin-bottom: 0;
}

ul.summary-list > li {
    display: inline-block;
    width: 19.5%;
    text-align: center;
}
/* */
ul.summary-list > li > a > i {
    display:block;
    font-size: 18px;
    padding-bottom: 5px;
}

ul.summary-list > li > a {
    padding: 10px 0;
    display: inline-block;
    color: #818181;
}

ul.summary-list > li  {
    border-right: 1px solid #eaeaea;
}

ul.summary-list > li:last-child  {
    border-right: none;
} */

.activity {
    width: 100%;
    float: left;
    margin-bottom: 10px;
}

.activity.alt {
    width: 100%;
    float: right;
    margin-bottom: 10px;
}

.activity span {
    float: left;
}

.activity.alt span {
    float: right;
}
.activity span, .activity.alt span {
    width: 45px;
    height: 45px;
    line-height: 45px;
    border-radius: 50%;
    -webkit-border-radius: 50%;
    background: #eee;
    text-align: center;
    color: #fff;
    font-size: 16px;
}

.activity.terques span {
    background: #8dd7d6;
}

.activity.terques h4 {
    color: #8dd7d6;
}
.activity.purple span {
    background: #b984dc;
}

.activity.purple h4 {
    color: #b984dc;
}
.activity.blue span {
    background: #90b4e6;
}

.activity.blue h4 {
    color: #90b4e6;
}
.activity.green span {
    background: #aec785;
}

.activity.green h4 {
    color: #aec785;
}

.activity h4 {
    margin-top:0 ;
    font-size: 16px;
}

.activity p {
    margin-bottom: 0;
    font-size: 13px;
}

.activity .activity-desk i, .activity.alt .activity-desk i {
    float: left;
    font-size: 18px;
    margin-right: 10px;
    color: #bebebe;
}

.activity .activity-desk {
    margin-left: 70px;
    position: relative;
}

.activity.alt .activity-desk {
    margin-right: 70px;
    position: relative;
}

.activity.alt .activity-desk .panel {
    float: right;
    position: relative;
}

.activity-desk .panel {
    background: #F4F4F4 ;
    display: inline-block;
}


.activity .activity-desk .arrow {
    border-right: 8px solid #F4F4F4 !important;
}
.activity .activity-desk .arrow {
    border-bottom: 8px solid transparent;
    border-top: 8px solid transparent;
    display: block;
    height: 0;
    left: -7px;
    position: absolute;
    top: 13px;
    width: 0;
}

.activity-desk .arrow-alt {
    border-left: 8px solid #F4F4F4 !important;
}

.activity-desk .arrow-alt {
    border-bottom: 8px solid transparent;
    border-top: 8px solid transparent;
    display: block;
    height: 0;
    right: -7px;
    position: absolute;
    top: 13px;
    width: 0;
}

.activity-desk .album {
    display: inline-block;
    margin-top: 10px;
}

.activity-desk .album a{
    margin-right: 10px;
}

.activity-desk .album a:last-child{
    margin-right: 0px;
}
    </style>
    </head>
<body>

    <div class="main-content d-flex justify-content-between flex-column full-h" id="panel">
        <div class="custom-row">
          <!-- Topnav -->
          @include('dashboards.users.layout.header')
          <!-- Header -->
          <!-- Header -->
          <div class="header pb-6 d-flex align-items-center" style="min-height: 200px;  background-size: cover; background-position: center top;">
            <!-- Mask -->
            <span class="mask bg-gradient-default opacity-8"></span>
          </div>
          <!-- Page content -->
          <div class="container">
            <div class="row">

                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
                <div class="container bootstrap snippets bootdey">
                <div class="row">
                  <div class="profile-nav col-md-3">
                      <div class="panel">
                          <div class="user-heading round">
                              <a href="#">
                                  <img src="https://bootdey.com/img/Content/avatar/avatar3.png" alt="">
                              </a>
                              <h1></h1>
                              <p>deydey@theEmail.com</p>
                          </div>

                          <ul class="nav nav-pills nav-stacked">
                              <li class="active"><a href="#"> <i class="fa fa-user"></i> Profile</a></li>
                              <li><a href="#"> <i class="fa fa-edit"></i> Edit profile</a></li>
                              <li><a href="{{route('users.tournaments')}}"> <i class="fa fa-edit"></i> My tournaments</a></li>
                          </ul>
                      </div>
                  </div>
                  <div class="profile-info col-md-9">

                      <div class="panel">
                          <div class="bio-graph-heading">
                              Aliquam ac magna metus. Nam sed arcu non tellus fringilla fringilla ut vel ispum. Aliquam ac magna metus.
                          </div>
                          <div class="panel-body bio-graph-info">
                              <h1>Bio Graph</h1>
                              <div class="row">
                                  <div class="bio-row">
                                      <p><span>First Name </span></p>
                                  </div>
                                  <div class="bio-row">

                                      <p><span>Email </span></p>

                                  </div>

                              </div>
                          </div>
                      </div>

                  </div>
                </div>
            </div>
        </div>
    </div>

                  <div class="header  pb-3">
                    <div class="container-fluid">
                      <div class="header-body">
                        <div class="row align-items-center py-4">
                          <div class="col-lg-6 col-7">

                          </div>
                        </div>

                      </div>
                      <ul class="nav nav-tabs">
                        <li class="nav-item">
                          <a class="nav-link active" data-toggle="tab" href="#home">Active</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#menu1">Upcoming</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#menu2">Completed</a>
                        </li>
                      </ul>
                    </div>
                  </div>

                  <div class="container-fluid">
                    <div class="tab-content">
                      <div class="row tab-pane active" id="home">
                        <div class="col">
                          <div class="card">
                            <!-- Card header -->
                            <div class="card-header border-0">
                              <h3 class="mb-0">My Tournaments</h3>
                            </div>
                            <!-- Light table -->
                            <div class="table-responsive mt-3">
                              <table class="table align-items-center table-flush" id="table">
                                <thead class="thead-light">
                                  <tr>
                                    <th scope="col" >ID</th>
                                    <th scope="col" >Tournament Name</th>
                                    <th scope="col" class="sort" data-sort="budget">Start Time</th>
                                    <!-- <th scope="col" class="sort" data-sort="status">ID</th> -->
                                    <th scope="col"  class="sort">Skill level</th>
                                    <th scope="col"  class="sort">Format</th>
                                    <th scope="col"  class="sort">Entry Fees</th>
                                    {{-- <th scope="col"  class="sort">Prize Pool</th> --}}
                                  </tr>
                                </thead>
                              </table>
                            </div>
                            <!-- Card footer -->

                          </div>
                        </div>
                      </div>


                      <div class="row tab-pane" id="menu1">
                        <div class="col">
                          <div class="card">
                            <!-- Card header -->
                            <div class="card-header border-0">
                              <h3 class="mb-0">Affiliates</h3>
                            </div>
                            <!-- Light table -->
                            <div class="table-responsive mt-3">
                                <table class="table align-items-center datatable no-footer table-flush" id="table2">
                                    <thead class="thead-light">
                                      <tr>
                                        <th scope="col" >ID</th>
                                        <th scope="col" >Tournament Name</th>
                                        <th scope="col" class="sort" data-sort="budget">Start Time</th>
                                        <!-- <th scope="col" class="sort" data-sort="status">ID</th> -->
                                        <th scope="col"  class="sort">Skill level</th>
                                        <th scope="col"  class="sort">Format</th>
                                        <th scope="col"  class="sort">Entry Fees</th>
                                        {{-- <th scope="col"  class="sort">Prize Pool</th> --}}
                                      </tr>
                                    </thead>
                                  </table>
                            </div>
                          </div>
                        </div>
                      </div>


                      <div class="row tab-pane" id="menu2">
                        <div class="col">
                          <div class="card">
                            <!-- Card header -->
                            <div class="card-header border-0">
                              <h3 class="mb-0">Permissions</h3>
                            </div>
                            <div class="table-responsive mt-3">
                              <table class="table align-items-center table-flush dataTable no-footer" id="table3">
                                <thead class="thead-light">
                                  <tr>
                                    <th scope="col" >ID</th>
                                    <th scope="col" >Tournament Name</th>
                                    <th scope="col" class="sort" data-sort="budget">Start Time</th>
                                    <!-- <th scope="col" class="sort" data-sort="status">ID</th> -->
                                    <th scope="col"  class="sort">Skill level</th>
                                    <th scope="col"  class="sort">Format</th>
                                    <th scope="col"  class="sort">Entry Fees</th>
                                    {{-- <th scope="col"  class="sort">Prize Pool</th> --}}
                                  </tr>
                                </thead>
                                <tbody>
                                  {{-- <form action="{{route('admin.permission_save')}}" method="post">
                                    {{ csrf_field() }} --}}
                                    {{-- @foreach ($data[0] as $item)
                                    <tr>
                                      <td scope="row">{{$item->name}}</td>
                                      @if (in_array( $item->id,$data[1]))
                                      <td> <input type="checkbox" name="admin_{{$item->id}}" checked id="admin_{{$item->id}}" /></td>
                                      @else
                                      <td> <input type="checkbox" name="admin_{{$item->id}}" id="admin_{{$item->id}}" /></td>
                                      @endif

                                      @if (in_array( $item->id,$data[2]))
                                      <td> <input type="checkbox" name="special_{{$item->id}}" checked id="special_{{$item->id}}" /></td>
                                      @else
                                      <td> <input type="checkbox" name="special_{{$item->id}}" id="admin_{{$item->id}}" /></td>
                                      @endif
                                    </tr>
                                    @endforeach --}}



                                </tbody>
                              </table>
                              <div class="custom-row py-2 px-4">
                                <input class="btn btn-primary custom-primary" type="submit" name="submit" value="Submit" />
                              </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>


                </div>
                </div>

        @include('dashboards.users.layout.script')
        {{-- <link rel="stylesheet" href="https://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css"> --}}




<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>


<link rel="stylesheet" href="https://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">

<script src="https://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
<script type="text/javascript">
    $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax:  "{{ route('users.tournaments') }}",
        columns: [
             {data: 'id','visible': false},
            {data: 'tour_name'},
            {data: 'start_time'},
            {data: 'skill_level'},
            {data: 'formate'},
            {data: 'entry_fee'}


        ]
    });

    $('#table2').DataTable({
        processing: true,
        serverSide: true,
        ajax:  "{{ route('upcoming.tournaments') }}",
        columns: [
             {data: 'id','visible': false},
            {data: 'tour_name'},
            {data: 'start_time'},
            {data: 'skill_level'},
            {data: 'formate'},
            {data: 'entry_fee'}


        ]
    });
    $('#table3').DataTable({
        processing: true,
        serverSide: true,
        ajax:  "{{ route('completed.tournaments') }}",
        columns: [
             {data: 'id','visible': false},
            {data: 'tour_name'},
            {data: 'start_time'},
            {data: 'skill_level'},
            {data: 'formate'},
            {data: 'entry_fee'}


        ]
    });
  </script>

</body>
</html>

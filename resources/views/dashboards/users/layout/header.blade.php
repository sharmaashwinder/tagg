<header class="header py-2 py-lg-3 header-top">
    <div class="inner-container">
        <div class="navbar navbar-expand-lg py-md-2 py-1">
            <div class="logo-area">
                <a href="/" title="Tagg">
                    <img src="{{asset('frontend/assets/img/tagg-logo.png')}}" width="194" height="82" alt="Tagg Logo" />
                </a>
            </div>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon d-flex align-items-center"><em></em></span>
            </button>

            @if (Route::has('login'))
            <nav class="collapse navbar-collapse" id="navbarSupportedContent">
                @if (Route::currentRouteName() != 'adminlogin')
                <ul class="navbar-nav me-auto nav-left">
                    <li class="nav-item ">
                        <a class="nav-link homeClsss {{ (request()->is('/*')) ? 'active' : '' }}  " id="home" aria-current="page" href="{{ url('/') }}" title="Home">Home</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link aboutCls {{ (request()->is('about*')) ? 'active' : '' }}" id="about" aria-current="page" href="{{ route('about') }}" title="About TAGG">About TAGG</a>
                    </li>
                    <!-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0);" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Games
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item" href="javascript:void(0);" title="Game 1">Game 1</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="javascript:void(0);" title="Game 2">Game 2</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="javascript:void(0);" title="Game 3">Game 3</a></li>
                                </ul>
</li> -->
                    <!-- li class="nav-itemv  ">
                        <a class="nav-link faqCls {{ (request()->is('faq*')) ? 'active' : '' }}" id="faq" aria-current="page" href="{{ route('faq') }}" title="FAQ">FAQ</a>
                    </!-- -->
                    <li class="nav-item">
                        <a class="nav-link contactCls {{ (request()->is('game-listing*')) ? 'active' : '' }}" id="game" aria-current="page" href="{{ route('game-listing') }}" title="Games">Games</a>
                    </li>
                </ul>

                <div class="d-flex">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0 nav-right align-items-center">

                        <!-- <li class="nav-item">
                                        <a class="nav-link gameCls { (request()->is('game-listing*')) ? 'active' : '' }}" id="game" aria-current="page" href="{{ route('game-listing') }}" title="Games">Games</a>
                                    </li> -->

                        <li class="nav-item">
                            <a class="nav-link contactCls {{ (request()->is('contact-us*')) ? 'active' : '' }}" id="contact" aria-current="page" href="{{ route('contact-us') }}" title="Contact Us">Contact Us</a>
                        </li>

                        @if(auth()->user() && request()->is('profile*') )
                        <li class="nav-item">
                            <a class="nav-link contactCls " id="contact" aria-current="page"  title="Wallet">TG({{'0'}})</a>
                        </li>
                        @endif
                        @endif
                        @auth
{{--  --}}

<li class="nav-item dropdown no-arrow">




<a class="dropdown-toggle d-flex align-items-center justify-content-center" href="javascript:void(0);" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">

    <em class="notification-icon"></em>
      @if(auth()->user()->unreadnotifications->count())
      <span class="badge-top d-flex align-items-center justify-content-center">{{auth()->user()->unreadnotifications->count()}}<span>
          @endif
</a>




    {{-- <a class="dropdown-toggle user-name d-flex align-items-center justify-content-center" href="javascript:void(0);" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        {{ auth()->user()->name[0] }}
    </a> --}}
    <!-- <li class="nav-itemv  ">
        <a class="nav-link faqCls" id="faq" aria-current="page" href="{{ route('connections') }}" title="Game Platform">Game Platform</a>
    </li> -->

    <ul class="dropdown-menu dropdown-menu-end custom-dropdown dropdown-left" aria-labelledby="navbarDropdown">
        <!-- <li><a class="dropdown-item" href="javascript:void(0);" title="Profile">Profile</a></li> -->
        @foreach (auth()->user()->unreadnotifications as $notification)
               <li style="background-color:lightgray"><a class="dropdown-item" href="#" title="Change Password">{{$notification->data['data']}}</a></li>
        @endforeach
        @foreach (auth()->user()->readnotifications as $notification)
        <li><a class="dropdown-item" href="#" title="Change Password">{{$notification->data['data']}}</a></li>
 @endforeach
        {{-- <li><a class="dropdown-item" href="{{route('profile')}}" title="Change Password">Change Password</a></li> --}}
        {{-- <li><a class="dropdown-item" href="{{route('connections')}}" title="Connections">Connections</a></li>
        <li><a class="dropdown-item" href="{{route('users.profile')}}" title="Profile">Profile</a></li> --}}

    </ul>
</li>
{{--  --}}
                        <li class="nav-item dropdown no-arrow">


                            @if(auth()->user()->name)

            <a class="dropdown-toggle user-name d-flex align-items-center justify-content-center" href="javascript:void(0);" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                {{ auth()->user()->name[0] }}
            </a>

         @else
            <a class="dropdown-toggle user-name d-flex align-items-center justify-content-center" href="javascript:void(0);" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                {{ 'U' }}
            </a>

         @endif
                            {{-- <a class="dropdown-toggle user-name d-flex align-items-center justify-content-center" href="javascript:void(0);" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                {{ auth()->user()->name[0] }}
                            </a> --}}
                            <!-- <li class="nav-itemv  ">
                                <a class="nav-link faqCls" id="faq" aria-current="page" href="{{ route('connections') }}" title="Game Platform">Game Platform</a>
                            </li> -->
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                            <ul class="dropdown-menu dropdown-menu-end custom-dropdown dropdown-left" aria-labelledby="navbarDropdown">
                                <!-- <li><a class="dropdown-item" href="javascript:void(0);" title="Profile">Profile</a></li> -->
                                <li><a class="dropdown-item" href="{{route('users.profile')}}" title="Profile">Profile</a></li>
                                <li><a class="dropdown-item" href="{{route('connections')}}" title="Connections">Connections</a></li>
                                <li><a class="dropdown-item" href="{{route('wallet')}}" title="Wallet">Wallet</a></li>
                                <li><a class="dropdown-item" href="{{route('profile')}}" title="Change Password">Change Password</a></li>
                                <li><a class="dropdown-item" aria-current="page" href="{{ route('logout') }}" title="Logout" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">Logout</a></li>
                            </ul>
                        </li>
                        @else
                        @if (Route::currentRouteName() != 'adminlogin')
                        <li class="nav-item">
                            <a class="border-btn text-center" href="{{ route('login') }}" title="Sign In">Sign In</a>
                        </li>
                        @endif
                        @if (Route::has('register'))
                        <!-- <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">Register</a>
                        </li> -->
                        @endif
                        @endauth
                    </ul>
                </div>
            </nav>
            @endif
        </div>
    </div>
</header>
<script>
    // $(document).ready(function() {
    // $("#home").click(function () {
    // $('.homeCls').addClass('active');
    // $('.aboutCls').removeClass('active');
    // $('.faqCls').removeClass('active');
    // $('.articleCls').removeClass('active');
    // $('.contactCls').removeClass('active');
    // });


    // $("#about").click(function () {
    // $('.aboutCls').addClass('active');
    // $('.homeCls').removeClass('active');
    // $('.faqCls').removeClass('active');
    // $('.articleCls').removeClass('active');
    // $('.contactCls').removeClass('active');
    // });

    // $("#faq").click(function () {
    // $('.faqCls').addClass('active');
    // $('.homeCls').removeClass('active');
    // $('.aboutCls').removeClass('active');
    // $('.articleCls').removeClass('active');
    // $('.contactCls').removeClass('active');
    // });

    // $("#article").click(function () {
    // $('.articleCls').addClass('active');
    // $('.homeCls').removeClass('active');
    // $('.aboutCls').removeClass('active');
    // $('.faqCls').removeClass('active');
    // $('.contactCls').removeClass('active');
    // });

    // $("#contact").click(function () {
    // $('.contactCls').addClass('active');
    // $('.homeCls').removeClass('active');
    // $('.aboutCls').removeClass('active');
    // $('.faqCls').removeClass('active');
    // $('.articleCls').removeClass('active');
    // });
    // });
</script>

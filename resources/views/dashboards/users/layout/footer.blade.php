 <!-- footer start -->
 <div class="common-space bg-color3">
     <div class="inner-container">
         <div class="row">
             <div class="col-md-6 col-lg-3 order-1 mb-lg-0 mb-4">
                 <img class="img-fluid" src="{{asset('frontend/assets/img/tagg-logo.png')}}" width="145" height="61" alt="Tagg Logo" />
                 <p class="typo3 pt-lg-4 pt-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
             </div>
             <div class="col-sm-6 col-lg-3 order-lg-2 order-3 mb-lg-0 mb-4">
                 <h3 class="heading-3">Company</h3>
                 <ul class="links-listing">
                     <li>
                         <a href="{{ route('become-affiliate') }}" title="Become an Affiliate">Become an Affiliate</a>
                     </li>
                     <li>
                         <a href="{{ route('privacy') }}" title="Privacy Policy">Privacy Policy</a>
                     </li>
                     <li>
                         <a href="{{ route('about') }}" title="About Tagg">About Tagg</a>
                     </li>
                     <!-- li>
                         <a href="{{ route('article') }}" title="Article">Article</a>
                     </!-->
                     <li>
                         <a href="{{ route('faq') }}" title="FAQ">FAQ</a>
                     </li>
                 </ul>

             </div>
             <div class="col-sm-6 col-lg-3 order-4 order-lg-3">
                 <h3 class="heading-3">Useful Links</h3>
                 <ul class="links-listing">
                     <li>
                         <a href="{{ route('about-rating') }}" title="About ratings">About ratings</a>
                     </li>
                     <li>
                         <a href="{{ route('press-release') }}" title="Press release">Press release</a>
                     </li>
                     <li>
                         <a href="{{ route('careers') }}" title="Careers">Careers</a>
                     </li>
                     <li>
                         <a href="{{ route('developers') }}" title="Developers">Developers</a>
                     </li>
                     <li>
                         <a href="{{ route('site-map') }}" title="Site map">Site map</a>
                     </li>
                 </ul>

             </div>
             <div class="col-md-6 col-lg-3 order-lg-4 order-2 my-lg-0 mb-4">
                 <h3 class="heading-3">Follow Us</h3>
                 <ul class="social-listing d-flex align-items-center justify-content-between">
                     <li>
                         <a target="_blank" class="social-icon fb" href="https://www.facebook.com/TAGGgaming" title="Facebook">Facebook</a>
                     </li>
                     <li>
                         <a target="_blank" class="social-icon twitter" href="https://twitter.com/TAGGgaming" title="Twitter">Twitter</a>
                     </li>
                     <li>
                         <a target="_blank" class="social-icon insta" href="https://www.instagram.com/tagggaming/" title="Instagram">Instagram</a>
                     </li>
                     <li>
                         <a target="_blank" class="social-icon youtube" href="https://www.youtube.com/channel/UCOVeQPXBLvGVeTk-rVnnidA?view_as=subscriber" title="Youtube">Youtube</a>
                     </li>
                     <li>
                         <a target="_blank" class="social-icon soc" href="javascript:void(0);" title="Discord">Discord</a>
                     </li>
                     <li>
                         <a target="_blank" class="social-icon twitch" href="https://www.twitch.tv/tagg_gaming" title="Twitch">Twitch</a>
                     </li>
                 </ul>

             </div>
         </div>
     </div>
 </div>
 <div class="common-space-sm bg-color2">
     <div class="inner-container typo4 text-center">
         © <?php echo date("Y"); ?>
         TAGG. All Rights Reserved.
     </div>
 </div>
 <!-- footer end -->
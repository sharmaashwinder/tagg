<div class="common-space bg-image3">
    <div class="inner-container">
        <div class="text-center py-2">
            <h2 class="heading-1">TAGG, <span class="theme-color">you're it!</span></h2>
            <p class="typo1 px-2 px-lg-5 pt-lg-4 pt-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
        <div class="row justify-content-center pt-lg-4 pt-3">
            <div class=" col-md-7">
                <form action="{{route('subscription')}}" class="input-group order-group" id="subscribe" method="POST">
                    @csrf
                    <input type="text" class="form-control input-transparent" placeholder="Email" aria-label="Email" aria-describedby="basic-addon2" name="email" required>
                    <button type="submit" class="input-group-text button-yellow" title="Subscribe" id="basic-addon2">Subscribe</button>
                </form>
                <script>
                    $(document).ready(function() {
                        $("#subscribe").validate({
                            rules: {
                                email: {
                                     required: true,
                                    email: true,
                                    //  pattern:"/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/",
                                    // regex:'/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/',
                                },


                            },
                            messages: {
                                email: {
                                    required: 'Email is required',


                                },


                            }
                        });
                    });
                    $.validator.methods.email = function(value, element) {
                        // return this.optional(element) || /[a-z]+@[a-z]+\.[a-z]+/.test(value);
                        return this.optional(element) || /^[a-z0-9][-a-z0-9.!#$%&'*+-=?^_`{|}~\/]+@([-a-z0-9]+\.)+[a-z]{2,5}$/.test(value);
                    }
                </script>
            </div>
        </div>
    </div>
</div>

<div class="common-space bg-color2">
    <div class="inner-container">
        <div class="text-center">
            <h2 class="heading-1">Popular <span class="theme-color">games</span></h2>
            <p class="typo2 px-2 px-lg-5 pt-lg-4 pt-2">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
        </div>
        <ul class="d-flex align-items-center justify-content-between logos-list pt-md-5 pt-3">
            <li><img class="img-fluid" src="{{asset('frontend/assets/img/COD.png')}}" width="158" height="54" alt="COD Logo" /></li>
            <li><img class="img-fluid" src="{{asset('frontend/assets/img/UFC.png')}}" width="173" height="53" alt="UFC Logo" /></li>
            <li><img class="img-fluid" src="{{asset('frontend/assets/img/apex.png')}}" width="115" height="76" alt="Apex Logo" /></li>
            <li><img class="img-fluid" src="{{asset('frontend/assets/img/fifa.png')}}" width="211" height="55" alt="Fifa Logo" /></li>
            <li><img class="img-fluid" src="{{asset('frontend/assets/img/mass.png')}}" width="154" height="68" alt="Mass Logo" /></li>
        </ul>
    </div>
</div>
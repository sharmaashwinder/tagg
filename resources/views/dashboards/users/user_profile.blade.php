<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('dashboards.users.layout.head')

<body>
  <div class="outer-container">
    @include('dashboards.users.layout.header')
    <!-- header end -->
    <!-- banner start -->
    <div class="banner-outer banner-profile">
      <div class="inner-container">
        <div class="custom-row c-my-5">
          <div class="row align-items-center">
            <div class="col-lg-3 col-md-4">
              <form method="post" action="{{route('image.upload.post')}}" id="formpicture" enctype="multipart/form-data">
                @csrf
                <div class="custom-row">
                  @if(auth()->user()->profile_photo_path)
                  <img id="preview_img" src={{ asset('/storage/'.auth()->user()->profile_photo_path) }} class="img-fluid" width="270" height="215" />
                  @else
                  <img id="preview_img" src="{{asset('frontend/assets/img/user-default.jpg')}}" class="img-fluid" width="270" height="215" />
                  @endif
                </div>
                <div class="input-group mt-3">
                  <div class="auto-row me-3">
                    <input type="file" name="profile_image" id="profile_image" onchange="loadPreview(this);" class="form-control element-hidden">
                    <button onclick="uploadpic()" title="Add Cover Photo" id="upload" class="btn-yellow-border md custom-row text-center text-uppercase px-4 text-nowrap">Add Cover Photo</button>
                  </div>
                  <div class="">
                    <button type="button" title="Share" id="share" class="btn-yellow-border btn-icon"><em class="share-yellow"></em></button>
                  </div>
                </div>
              </form>
            </div>
            <div class="col-lg-9 col-md-8 pt-4 pt-md-0">
              <div class="row">
                <div class="col-md-6">
                  <h1 class="heading-1 md">{{$details[0]->name}}</h1>
                  <ul class="list-tags d-flex">
                    @role('affiliate')
                    <li class="d-flex align-items-center">Special</li>
                    @endrole
                    <li class="d-flex align-items-center">Pro</li>
                    <li class="d-flex align-items-center"><em class="icon-verified me-2"></em> Verified</li>
                  </ul>

                  <div class="d-flex pt-3">
                    <em class="icon-game me-3"></em>
                    <ul class="d-flex flex-wrap auto-row">
                      @if ( ($details[2])->count() )
                      @foreach ($details[2] as $item)
                      @if(strcasecmp($item->provider_name, 'twitch')==0 )
                      <li class="typo7 mb-2 me-2"><span class="text-white">Twitch - </span>{{ $item->username }}</li>
                      @elseif (strcasecmp($item->provider_name, 'xbox')==0)
                      <li class="typo7 mb-2 me-2"><span class="text-white">Xbox - </span>{{ $item->username }}</li>
                      @elseif (strcasecmp($item->provider_name, 'epic')==0)
                      <li class="typo7 mb-2 me-2"><span class="text-white">Epic- </span>{{ $item->username }}</li>
                      @elseif (strcasecmp($item->provider_name, 'steam')==0)
                      <li class="typo7 mb-2 me-2"><span class="text-white">Steam- </span>{{ $item->username }}</li>
                      @elseif (strcasecmp($item->provider_name, 'psn')==0)
                      <li class="typo7 mb-2 me-2"><span class="text-white">PSN- </span>{{ $item->username }}</li>
                      @endif
                      @endforeach

                      @endif
                    </ul>
                  </div>
                  <ul class="list-with-icons my-1 d-flex flex-column ">
                    <li class="d-flex align-items-center py-1"><em class="icon-inbox me-3"></em>
                      @if($details[0]->email)
                      <div>{{$details[0]->email}}</div>
                      @endif
                    </li>
                  </ul>

                </div>
                @php
                if($details[1]->twitch_url)
                {
                $twitch=$details[1]->twitch_url;
                $twitchtarget=' target="_blank"';
                }
                else
                {
                $twitch="javascript:void(0);";
                $twitchtarget='';
                }
                if($details[1]->twitter_url)
                {
                $twitter=$details[1]->twitter_url;
                $twittertarget=' target="_blank"';
                }
                else
                {
                $twitter="javascript:void(0);";
                $twittertarget='';
                }
                if($details[1]->insta_url)
                {
                $insta=$details[1]->insta_url;
                $instatarget='target="_blank"';
                }
                else
                {
                $insta="javascript:void(0);";
                $instatarget='';
                }
                if($details[1]->linkedin_url)
                {
                $linkedin=$details[1]->linkedin_url;
                $linkedintarget='target="_blank"';
                }else{
                $linkedin="javascript:void(0);";
                $linkedintarget='';
                }
                if($details[1]->discord_url)
                {
                $discord=$details[1]->discord_url;
                $discordtarget='target="_blank"';
                }else{
                $discord="#";
                $discordtarget='';
                }
                @endphp
                <div class="col-md-6">
                  <div class="bg-color2 p-2 d-flex">
                    <div class="auto-row me-2">
                      <div class="row g-2">
                        <div class="col text-center">
                          <div class="bg-color3 box-xs d-flex flex-column align-items-center justify-content-center">
                            <div class="heading-3 mb-2">258</div>
                            <div class="typo-xs">Win Loss<br> Record</div>
                          </div>
                        </div>
                        <div class="col text-center">
                          <div class="bg-color3 box-xs d-flex flex-column align-items-center justify-content-center">
                            <div class="heading-3 mb-2"><span class="tg">tg</span>365</div>
                            <div class="typo-xs">Earnings</div>
                          </div>
                        </div>
                        <div class="col">
                          <div class="bg-color3 box-xs d-flex flex-column align-items-center justify-content-center">
                            <a href="{{$twitch}}" {{ $twitchtarget}} title="Twitch">
                              <em class="social-icon twitch"></em>
                            </a>
                          </div>
                        </div>
                        <div class="col">
                          <div class="bg-color3 box-xs d-flex flex-column align-items-center justify-content-center">
                            <a href="{{$discord}}" {{ $discordtarget}} title="Discord" class="social-icon soc">
                              {{-- <em class="social-icon soc"></em> --}}
                            </a>
                          </div>
                        </div>
                      </div>
                      <div class="bg-color3 mt-2">
                        <div class="row g-0">

                          <div class="col">
                            <div class="bg-color3 box-xs d-flex flex-column align-items-center justify-content-center">
                              @if($details[1]->facebook_url)
                              <a href={{$details[1]->facebook_url}} target="_blank" class="social-icon fb" title="Facebook">
                              </a>
                              @else
                              <a href="javascript:void(0);" title="Facebook" class="social-icon fb"></a>
                              @endif
                            </div>
                          </div>
                          <div class="col">
                            <div class="bg-color3 box-xs d-flex flex-column align-items-center justify-content-center">
                              <a href="{{$twitter}}" {{ $twittertarget}} title="Tweeter" class="social-icon twitter"></a>

                            </div>
                          </div>
                          <div class="col">
                            <div class="bg-color3 box-xs d-flex flex-column align-items-center justify-content-center">
                              <a href="{{$insta}}" {{ $instatarget}} title="Instagram" class="social-icon insta"></a>
                            </div>
                          </div>
                          <div class="col">
                            <div class="bg-color3 box-xs d-flex flex-column align-items-center justify-content-center">
                              <a href="{{$linkedin}}" {{ $linkedintarget}} title="Linkedin" class="social-icon ldin"></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="text-end">
                      <div class="switch">

                        @if ($details[1]->profile_show)
                        <input type="checkbox" checked>
                        @else
                        <input type="checkbox">
                        @endif
                        <span class="slider round"></span>
                      </div>
                      <div class="pt-3">
                        <a href="{{route('social.permissions')}}" class="btn btn-sm btn-outline-info"> Edit social</a>
                      </div>
                    </div>

                  </div>
                </div>

                <div class="col-12 ">
                  <div class="row">
                    <div class="col-md-6 pt-md-4 pt-3">
                      {{-- <a href="javascript:void(0);" title="Request Tokens" class="btn-yellow-fill md custom-row text-uppercase text-center">Request Tokens</a> --}}
                    </div>
                    <div class="col-md-6 pt-md-4 pt-3">
                      <a href="javascript:void(0);" title="Add Friends" class="btn-yellow-fill md custom-row text-uppercase text-center">Add Friends</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- banner end -->
    <!-- Tabs section -->
    <div class="common-space-md bg-color2">
      <div class="inner-container">
        <div class="row">
          <div class="col-lg-3 col-md-4 col-12">
            <ul class="bg-color3 nav nav-tabs horizontal-tabs justify-content-center" id="myTab" role="tablist">
              <li class="nav-item" role="presentation">
                <div class="nav-btn active" id="home-tab" data-bs-toggle="tab" data-bs-target="#tournament" type="button" role="tab" aria-controls="home" aria-selected="true">
                  My Profile
                </div>
              </li>
              <li class="nav-item" role="presentation">
                <div class="nav-btn" id="profile-tab" data-bs-toggle="tab" data-bs-target="#challenges" type="button" role="tab" aria-controls="profile" aria-selected="false">
                  My Dashboard
                </div>
              </li>
              <li class="nav-item" role="presentation">
                <div class="nav-btn" id="contact-tab" data-bs-toggle="tab" data-bs-target="#membership" type="button" role="tab" aria-controls="contact" aria-selected="false">
                  My Friends
                </div>
              </li>
              <li class="nav-item" role="presentation">
                <div class="nav-btn" id="contact-tab" data-bs-toggle="tab" data-bs-target="#giveaway" type="button" role="tab" aria-controls="contact" aria-selected="false">
                  My Trophy Room
                </div>
              </li>
              <li class="nav-item" role="presentation">
                <div class="nav-btn" id="contact-tab" data-bs-toggle="tab" data-bs-target="#donate" type="button" role="tab" aria-controls="contact" aria-selected="false">
                  My Tournaments
                </div>
              </li>
              <li class="nav-item">
                <a href="{{ route('users.editprofile') }}" class="nav-btn custom-row">
                  Edit Profile
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('users.list') }}" class="nav-btn custom-row">
                  Users List
                </a>
              </li>
            </ul>
          </div>
          <div class="col-lg-9 col-md-8 col-12">
            <!-- tabs data started -->
            <div class="tab-content" id="myTabContent">
              <!-- my profile -->
              <div class="inner-container-sm tab-pane fade show active" id="tournament" role="tabpanel" aria-labelledby="home-tab">
                <h2 class="title-1 mb-3">About</h2>
                <p class="typo3 md pb-4">{{auth()->user()->bio}}</p>
                <h2 class="title-1 mb-3">History</h2>
                <div class="row">
                  <div class="col-md-6 py-2">
                    <div class="bg-color3 p-4 box-height d-flex justify-content-between align-items-center">
                      <div>
                        <div class="heading-2 lg">50%</div>
                        <p class="typo7 pt-4">Win Rate</p>
                      </div>
                      <figure><img src="{{asset('frontend/assets/img/win-img.png')}}" alt="Win Icon" class="img-fluid" width="67" height="67" /></figure>
                    </div>
                  </div>
                  <div class="col-md-6 py-2">
                    <div class="bg-color3 p-4 box-height d-flex justify-content-between align-items-center">
                      <div>
                        <div class="heading-2 lg">42</div>
                        <p class="typo7 pt-4">Tournament Played</p>
                      </div>
                      <figure><img src="{{asset('frontend/assets/img/tournament-img.png')}}" alt="Tournament Icon" class="img-fluid" width="68" height="68" /></figure>
                    </div>
                  </div>
                  <div class="col-md-6 py-2">
                    <div class="bg-color3 p-4 box-height d-flex justify-content-between align-items-center">

                      <div>
                        <div class="heading-2 lg">42</div>
                        <p class="typo7 pt-4">Total Winnings</p>
                      </div>
                      <div>
                        <div class="text-end pb-2">
                          <select class="border-select">
                            <option>Per Week</option>
                            <option>Per Month</option>
                          </select>
                        </div>
                        <figure class="text-end"><img src="{{asset('frontend/assets/img/winnings-img.png')}}" alt="Winnings Icon" class="img-fluid" width="73" height="68" /></figure>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 py-2">
                    <div class="bg-color3 p-4 box-height d-flex justify-content-between align-items-center">
                      <div>
                        <div class="heading-2 lg">60%</div>
                        <p class="typo7 pt-4">Recent Result</p>
                      </div>
                      <figure><img src="{{asset('frontend/assets/img/result-img.png')}}" alt="Result Icon" class="img-fluid" width="64" height="60" /></figure>
                    </div>
                  </div>
                  <div class="col-md-6 py-2">
                    <div class="bg-color3 p-4 box-height d-flex justify-content-between align-items-center">
                      <div>
                        <div class="heading-2 lg">15</div>
                        <p class="typo7 pt-4">Upcoming Tournament</p>
                      </div>
                      <figure><img src="{{asset('frontend/assets/img/tournament-img.png')}}" alt="Tournament Icon" class="img-fluid" width="68" height="68" /></figure>
                    </div>
                  </div>
                  <div class="col-md-6 py-2">
                    <div class="bg-color3 p-4 box-height d-flex justify-content-between align-items-center">
                      <div class="custom-row">
                        <div class="d-flex justify-content-between">
                          <div class="heading-2 sm">Term & Conditions</div>
                          <div class="switch">
                            <input type="checkbox" checked="">
                            <span class="slider round"></span>
                          </div>
                        </div>
                        <p class="typo7 pt-4">User Term & Conditions</p>
                        <p class="typo7 pt-2">Hoster Term & Conditions</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="custom-row pt-md-4 pt-3">
                  <div class="d-flex justify-content-between align-items-center">
                    <h2 class="title-1 mb-0">My Games</h2>
                    <a href="javascript:void(0);" title="View All" class="anchor-ylw">View All</a>
                  </div>
                  <div class="row">
                    <div class="col-md-6 py-3">
                      <div class="bg-color3 p-2">
                        <div class="d-flex align-items-center">
                          <figure>
                            <img src="{{asset('frontend/assets/img/game-img.jpg')}}" alt="Tournament Icon" class="img-fluid" width="239" height="162" />
                          </figure>
                          <ul class="check-list-sm typo4 md ps-3">
                            <li><em class="check-mark"></em>Top Rated</li>
                            <li><em class="check-mark"></em>Rank= 164</li>
                            <li><em class="check-mark"></em>Skill = Pro</li>
                          </ul>
                        </div>
                      </div>
                      <h3 class="heading-8 pt-3 mb-0">Zombie: White Walkers</h3>
                    </div>
                    <div class="col-md-6 py-3">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="bg-color3 p-2">
                            <figure>
                              <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                            </figure>
                          </div>
                          <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                        </div>
                        <div class="col-md-6">
                          <div class="bg-color3 p-2">
                            <figure>
                              <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                            </figure>
                          </div>
                          <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="custom-row pt-md-4 pt-3">
                  <div class="d-flex justify-content-between align-items-center">
                    <h2 class="title-1 mb-0">Highlights</h2>
                  </div>
                  <div class="slider-outer">
                    <div id="carousel" class="owl-carousel arrow-top pt-3">
                      <div class="item">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                      </div>
                      <div class="item">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                      </div>
                      <div class="item">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                      </div>
                      <div class="item">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="custom-row pt-md-4 pt-3">
                  <div class="d-flex justify-content-between align-items-center">
                    <h2 class="title-1 mb-0">Interest and Hobbies</h2>
                  </div>
                  <div class="slider-outer">
                    <div id="interest" class="owl-carousel arrow-top pt-3">
                      <div class="item">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                      </div>
                      <div class="item">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                      </div>
                      <div class="item">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                      </div>
                      <div class="item">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                        <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="custom-row pt-md-4 pt-3">
                  <div class="d-flex justify-content-between align-items-center">
                    <h2 class="title-1 mb-0">News Feeds</h2>
                    <a href="javascript:void(0);" title="View All" class="anchor-ylw">View All</a>
                  </div>
                  <div class="d-flex justify-content-between align-items-center pt-md-4 pt-3 border-b1 pb-3 mt-2 position-relative">
                    <div>
                      <h2 class="heading-8 mb-0">James Smith shared a video</h2>
                      <p class="typo6 pt-2">3 min ago</p>
                    </div>
                    <div>
                      <a href="javascript:void(0);" title="View All" class="typo4 md d-flex align-items-center"><em class="video-icon me-2"></em>View All</a>
                    </div>
                    <em class="sb10"></em>
                  </div>
                  <div class="row mt-3">
                    <div class="col-md-4 py-3">
                      <div class="bg-color3">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                      </div>
                      <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                    </div>
                    <div class="col-md-4 py-3">
                      <div class="bg-color3">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                      </div>
                      <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                    </div>
                    <div class="col-md-4 py-3">
                      <div class="bg-color3">
                        <figure>
                          <img src="{{asset('frontend/assets/img/game-img2.jpg')}}" alt="Tournament Icon" class="img-fluid" width="199" height="187" />
                        </figure>
                      </div>
                      <h3 class="heading-8 pt-3 mb-0">Sniper 2</h3>
                    </div>
                  </div>
                </div>
              </div>
              <!-- dashboard -->
              <div class="inner-container-sm tab-pane fade" id="challenges" role="tabpanel" aria-labelledby="profile-tab">
                <h2 class="title-1 mb-3">My Dashboard</h2>
                <div class="row">
                  <div class="col-md-6 py-2">
                    <div class="bg-color3 p-4 box-height d-flex justify-content-between align-items-center">
                      <div>
                        <div class="heading-2 lg">245</div>
                        <p class="typo7 pt-4">Created Tournaments</p>
                      </div>
                      <figure><img src="{{asset('frontend/assets/img/win-img.png')}}" alt="Win Icon" class="img-fluid" width="67" height="67" /></figure>
                    </div>
                  </div>
                  <div class="col-md-6 py-2">
                    <div class="bg-color3 p-4 box-height d-flex justify-content-between align-items-center">
                      <div>
                        <div class="heading-2 lg">Leaderboard</div>
                        <p class="typo7 pt-4">For Created Tournaments</p>
                      </div>
                      <figure><img src="{{asset('frontend/assets/img/tournament-img.png')}}" alt="Tournament Icon" class="img-fluid" width="68" height="68" /></figure>
                    </div>
                  </div>
                  <div class="col-md-6 py-2">
                    <div class="bg-color3 p-4 box-height d-flex justify-content-between align-items-center">

                      <div>
                        <div class="heading-2 lg">68</div>
                        <p class="typo7 pt-4">Play Outs</p>
                      </div>
                      <figure class="text-end"><img src="{{asset('frontend/assets/img/winnings-img.png')}}" alt="Winnings Icon" class="img-fluid" width="73" height="68" /></figure>
                    </div>
                  </div>
                  <div class="col-md-6 py-2">
                    <div class="bg-color3 p-4 box-height d-flex justify-content-between align-items-center">
                      <div>
                        <div class="heading-2 lg">$325</div>
                        <p class="typo7 pt-4">Earnings</p>
                      </div>
                      <figure><img src="{{asset('frontend/assets/img/result-img.png')}}" alt="Result Icon" class="img-fluid" width="64" height="60" /></figure>
                    </div>
                  </div>
                </div>
                <div class="custom-row pt-md-4 pt-3">
                  <ul class="nav nav-tabs border-0" id="playerTab" role="tablist">
                    <li class="nav-item mx-2 my-1" role="presentation">
                      <button class="border-btn-ylow blue active " id="player-tab" data-bs-toggle="tab" data-bs-target="#catagory1" type="button" role="tab" aria-controls="home" aria-selected="true">Players</button>
                    </li>
                    <li class="nav-item mx-2 my-1" role="presentation">
                      <button class="border-btn-ylow blue" id="tournament-tab" data-bs-toggle="tab" data-bs-target="#catagory2" type="button" role="tab" aria-controls="profile" aria-selected="false">Tournaments</button>
                    </li>
                  </ul>
                  <div class="tab-content bg-color3 p-4 mt-3" id="myTabContent3">
                    <div class="custom-row tab-pane fade show active" id="catagory1" role="tabpanel" aria-labelledby="player-tab">
                      <figure>
                        <img src="{{asset('frontend/assets/img/graph-img.jpg')}}" alt="Graph image" class="img-fluid" width="795" height="352" />
                      </figure>
                    </div>
                    <div class="custom-row tab-pane fade" id="catagory2" role="tabpanel" aria-labelledby="tournament-tab">
                      <figure>
                        <img src="{{asset('frontend/assets/img/graph-img.jpg')}}" alt="Graph image" class="img-fluid" width="795" height="352" />
                      </figure>
                    </div>
                  </div>
                </div>
                <div class="custom-row pt-md-4 pt-3 mt-2">
                  <h2 class="title-1 mb-3">Overwatch Earnings</h2>
                  <div class="bg-color3 p-4 mt-3">
                    <figure>
                      <img src="{{asset('frontend/assets/img/graph-img.jpg')}}" alt="Graph image" class="img-fluid" width="795" height="352" />
                    </figure>
                  </div>
                </div>
              </div>
              <!-- My Friends -->
              <div class="inner-container-sm tab-pane fade" id="membership" role="tabpanel" aria-labelledby="contact-tab">
                <h2 class="title-1 mb-0">My Friends</h2>
                <div class="d-flex justify-content-between py-4">
                  <div class="d-flex">
                    <ul class="nav nav-tabs border-0" id="tournamentsTab" role="tablist">
                      <li class="nav-item me-3 my-1" role="presentation">
                        <button class="btn-yellow-border md px-3 text-uppercase active " id="myFriends-tab" data-bs-toggle="tab" data-bs-target="#myFriends" type="button" role="tab" aria-controls="home" aria-selected="true">My Friends</button>
                      </li>
                      <li class="nav-item me-3 my-1" role="presentation">
                        <button class="btn-yellow-border md px-3 text-uppercase" id="myTeam-tab" data-bs-toggle="tab" data-bs-target="#myTeam" type="button" role="tab" aria-controls="profile" aria-selected="false">My Team</button>
                      </li>
                    </ul>
                  </div>
                  <div class="d-flex align-items-center">
                    <a href="javascript:void(0);" title="Friends Requests" class="btn-yellow-border md px-4 me-3 text-uppercase">Friends Requests</a>
                    <a href="javascript:void(0);" title="Add New Friend" class="btn-yellow-border fill-yellow md px-4 text-uppercase" data-bs-toggle="modal" data-bs-target="#exampleModal"><span class="add-text">+</span> Add New Friend</a>
                  </div>
                </div>
                <div class="tab-content bg-color3 p-4 mt-3" id="myTabContent4">
                  <div class="custom-row tab-pane fade show active" id="myFriends" role="tabpanel" aria-labelledby="myFriends-tab">
                    <div class="table-responsive">
                      <table class="table custom-table">
                        <thead>
                          <tr>
                            <th scope="col">Player Name</th>
                            <th scope="col">Gamer Name</th>
                            <th scope="col">ID</th>
                            <th scope="col">Rank</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              <div class="d-flex align-items-center">
                                <div class="rounded-img-xs me-md-3 me-2">
                                  <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                                </div>
                                Barry Allen
                              </div>
                            </td>
                            <td>Gamer Name</td>
                            <td></td>
                            <td>
                              <ul class="d-flex">
                                <li class="mx-1"><em class="icon-rank"></em></li>
                                <li class="mx-1"><em class="icon-rank"></em></li>
                                <li class="mx-1"><em class="icon-rank"></em></li>
                                <li class="mx-1"><em class="icon-rank"></em></li>
                              </ul>
                            </td>
                            <td>
                              <div class="d-flex align-items-center">
                                <a href="javascript:void(0);" title="Chat" class="icon-chat mx-1">Chat</a>
                                <a href="javascript:void(0);" title="Gift" class="icon-gift mx-1">Gift</a>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <div class="d-flex align-items-center">
                                <div class="rounded-img-xs me-md-3 me-2">
                                  <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                                </div>
                                Barry Allen
                              </div>
                            </td>
                            <td>Gamer Name</td>
                            <td>PSN ID : 25487</td>
                            <td>
                              <ul class="d-flex">
                                <li class="mx-1"><em class="icon-rank"></em></li>
                                <li class="mx-1"><em class="icon-rank"></em></li>
                                <li class="mx-1"><em class="icon-rank"></em></li>
                                <li class="mx-1"><em class="icon-rank"></em></li>
                              </ul>
                            </td>
                            <td>
                              <div class="d-flex align-items-center">
                                <a href="javascript:void(0);" title="Chat" class="icon-chat mx-1">Chat</a>
                                <a href="javascript:void(0);" title="Gift" class="icon-gift mx-1">Gift</a>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="d-flex justify-content-end pt-3">
                      <nav aria-label="Page navigation example">
                        <ul class="pagination">
                          <li class="page-item border-pre disabled">
                            <a class="page-link" href="javascript:void(0);"><span class="page-prev"></span></a>
                          </li>
                          <li class="page-item">
                            <a class="page-link" href="javascript:void(0);">1</a>
                          </li>
                          <li class="page-item">
                            <a class="page-link" href="javascript:void(0);">2</a>
                          </li>
                          <li class="page-item">
                            <a class="page-link" href="javascript:void(0);">3</a>
                          </li>
                          <li class="page-item border-next">
                            <a class="page-link" href="javascript:void(0);"><span class="page-next"></span></a>
                          </li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                  <div class="custom-row tab-pane fade" id="myTeam" role="tabpanel" aria-labelledby="myTeam-tab">
                    <div class="table-responsive">
                      <table class="table custom-table p-md">
                        <thead>
                          <tr>
                            <th scope="col">Player Name</th>
                            <th scope="col">Gamer Name</th>
                            <th scope="col">ID</th>
                            <th scope="col">Rank</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              <div class="d-flex align-items-center">
                                <div class="rounded-img-xs me-md-3 me-2">
                                  <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                                </div>
                                Barry Allen
                              </div>
                            </td>
                            <td>Gamer Name</td>
                            <td>PSN ID : 25487</td>
                            <td>
                              <ul class="d-flex">
                                <li class="mx-1"><em class="icon-rank"></em></li>
                                <li class="mx-1"><em class="icon-rank"></em></li>
                                <li class="mx-1"><em class="icon-rank"></em></li>
                                <li class="mx-1"><em class="icon-rank"></em></li>
                              </ul>
                            </td>
                            <td>
                              <div class="d-flex align-items-center">
                                <a href="javascript:void(0);" title="Leave Team" class="btn-yellow-border px-3 text-uppercase text-center">Leave Team</a>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="d-flex justify-content-end pt-3">
                      <nav aria-label="Page navigation example">
                        <ul class="pagination">
                          <li class="page-item border-pre disabled">
                            <a class="page-link" href="javascript:void(0);"><span class="page-prev"></span></a>
                          </li>
                          <li class="page-item">
                            <a class="page-link" href="javascript:void(0);">1</a>
                          </li>
                          <li class="page-item">
                            <a class="page-link" href="javascript:void(0);">2</a>
                          </li>
                          <li class="page-item">
                            <a class="page-link" href="javascript:void(0);">3</a>
                          </li>
                          <li class="page-item border-next">
                            <a class="page-link" href="javascript:void(0);"><span class="page-next"></span></a>
                          </li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </div>
              </div>
              <!-- My Trophy Room -->
              <div class="inner-container-sm tab-pane fade" id="giveaway" role="tabpanel" aria-labelledby="contact-tab">
                <h2 class="title-1 mb-3">My Trophy Room</h2>
                <div class="row">
                  <div class="col-md-4 py-2">
                    <div class="bg-color3 p-4 box-height2 d-flex justify-content-between align-items-center">
                      <div>
                        <div class="heading-2 lg">254</div>
                        <p class="typo7 pt-3">Gold Trophy</p>
                      </div>
                      <figure>
                        <img src="{{asset('frontend/assets/img/gold.png')}}" alt="Gold" class="img-fluid" width="51" height="67">
                      </figure>
                    </div>
                  </div>
                  <div class="col-md-4 py-2">
                    <div class="bg-color3 p-4 box-height2 d-flex justify-content-between align-items-center">
                      <div>
                        <div class="heading-2 lg">150</div>
                        <p class="typo7 pt-3">Silver Trophy</p>
                      </div>
                      <figure>
                        <img src="{{asset('frontend/assets/img/silver.png')}}" alt="Silver" class="img-fluid" width="50" height="67">
                      </figure>
                    </div>
                  </div>
                  <div class="col-md-4 py-2">
                    <div class="bg-color3 p-4 box-height2 d-flex justify-content-between align-items-center">
                      <div>
                        <div class="heading-2 lg">90</div>
                        <p class="typo7 pt-3">Bronze Trophy</p>
                      </div>
                      <figure>
                        <img src="{{asset('frontend/assets/img/bronze.png')}}" alt="Bronze" class="img-fluid" width="51" height="68">
                      </figure>
                    </div>
                  </div>
                </div>
                <div class="row g-3 mt-1">
                  <div class="col-md-6 py-2">
                    <form class="input-group order-group" id="" action="" method="get">
                      <span class="search-icon"></span>
                      <input type="text" class="form-control input-transparent icon-lp md" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2" name="game_name" required="">
                    </form>
                  </div>
                  <div class="col-md-3 py-2">
                    <select class="form-control input-transparent md">
                      <option>All Platform</option>
                      <option>All Platform</option>
                    </select>
                  </div>
                  <div class="col-md-3 py-2">
                    <select class="form-control input-transparent md">
                      <option>All Games</option>
                      <option>All Games</option>
                    </select>
                  </div>
                </div>
                <div class="custom-row mt-4">
                  <div class="d-flex justify-content-between align-items-center">
                    <ul class="nav nav-tabs border-0" id="trophyTab" role="tablist">
                      <li class="nav-item me-3 my-1" role="presentation">
                        <button class="btn-yellow-border md px-3 text-uppercase active " id="trophy-tab" data-bs-toggle="tab" data-bs-target="#catagory3" type="button" role="tab" aria-controls="home" aria-selected="true">All Winner</button>
                      </li>
                      <li class="nav-item me-3 my-1" role="presentation">
                        <button class="btn-yellow-border md px-3 text-uppercase" id="team-tab" data-bs-toggle="tab" data-bs-target="#catagory4" type="button" role="tab" aria-controls="profile" aria-selected="false">Game Specific</button>
                      </li>
                      <li class="nav-item me-3 my-1" role="presentation">
                        <button class="btn-yellow-border md px-3 text-uppercase" id="specific-tab" data-bs-toggle="tab" data-bs-target="#catagory5" type="button" role="tab" aria-controls="profile" aria-selected="false">Tournament Specific</button>
                      </li>
                    </ul>
                    <div class="d-flex">
                      <a href="javascript:void(0);" class="border-btn-ylow" title="Daily">Daily</a>
                      <a href="javascript:void(0);" class="border-btn-ylow" title="Weekly">Weekly</a>
                      <a href="javascript:void(0);" class="border-btn-ylow" title="Monthly">Monthly</a>
                    </div>
                  </div>
                  <div class="tab-content mt-3" id="myTabContent3">
                    <div class="custom-row tab-pane fade show active" id="catagory3" role="tabpanel" aria-labelledby="trophy-tab">
                      <div class="table-responsive">
                        <table class="table custom-table">
                          <thead>
                            <tr>
                              <th scope="col">Rank</th>
                              <th scope="col">Name</th>
                              <th scope="col">Platform Assoc.</th>
                              <th scope="col">Platform ID</th>
                              <th scope="col">Earnings</th>
                              <th scope="col">Tour. Played</th>
                              <th scope="col">M.P (Games)</th>
                              <th scope="col">Win Rate (%)</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>1</td>
                              <td>
                                <div class="d-flex align-items-center">
                                  <div class="rounded-img-xs me-md-3 me-2">
                                    <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                                  </div>
                                  Barry Allen
                                </div>
                              </td>
                              <td>Playstation</td>
                              <td>1542111</td>
                              <td>
                                <div class="d-flex align-items-center">
                                  <span class="tg">tg</span> 52466
                                </div>
                              </td>
                              <td>
                                36
                              </td>
                              <td>Fallout4</td>
                              <td>80</td>
                            </tr>
                            <tr>
                              <td>1</td>
                              <td>
                                <div class="d-flex align-items-center">
                                  <div class="rounded-img-xs me-md-3 me-2">
                                    <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                                  </div>
                                  Barry Allen
                                </div>
                              </td>
                              <td>Playstation</td>
                              <td>1542111</td>
                              <td>
                                <div class="d-flex align-items-center">
                                  <span class="tg">tg</span> 52466
                                </div>
                              </td>
                              <td>
                                36
                              </td>
                              <td>Fallout4</td>
                              <td>80</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="d-flex justify-content-end pt-3">
                        <nav aria-label="Page navigation example">
                          <ul class="pagination">
                            <li class="page-item border-pre disabled">
                              <a class="page-link" href="javascript:void(0);"><span class="page-prev"></span></a>
                            </li>
                            <li class="page-item">
                              <a class="page-link" href="javascript:void(0);">1</a>
                            </li>
                            <li class="page-item">
                              <a class="page-link" href="javascript:void(0);">2</a>
                            </li>
                            <li class="page-item">
                              <a class="page-link" href="javascript:void(0);">3</a>
                            </li>
                            <li class="page-item border-next">
                              <a class="page-link" href="javascript:void(0);"><span class="page-next"></span></a>
                            </li>
                          </ul>
                        </nav>
                      </div>
                    </div>
                    <div class="custom-row tab-pane fade" id="catagory4" role="tabpanel" aria-labelledby="team-tab">
                      <div class="table-responsive">
                        <table class="table custom-table">
                          <thead>
                            <tr>
                              <th scope="col">Rank</th>
                              <th scope="col">Name</th>
                              <th scope="col">Platform Assoc.</th>
                              <th scope="col">Platform ID</th>
                              <th scope="col">Earnings</th>
                              <th scope="col">Tour. Played</th>
                              <th scope="col">M.P (Games)</th>
                              <th scope="col">Win Rate (%)</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>1</td>
                              <td>
                                <div class="d-flex align-items-center">
                                  <div class="rounded-img-xs me-md-3 me-2">
                                    <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                                  </div>
                                  Barry Allen
                                </div>
                              </td>
                              <td>Playstation</td>
                              <td>1542111</td>
                              <td>
                                <div class="d-flex align-items-center">
                                  <span class="tg">tg</span> 52466
                                </div>
                              </td>
                              <td>
                                36
                              </td>
                              <td>Fallout4</td>
                              <td>80</td>
                            </tr>
                            <tr>
                              <td>1</td>
                              <td>
                                <div class="d-flex align-items-center">
                                  <div class="rounded-img-xs me-md-3 me-2">
                                    <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                                  </div>
                                  Barry Allen
                                </div>
                              </td>
                              <td>Playstation</td>
                              <td>1542111</td>
                              <td>
                                <div class="d-flex align-items-center">
                                  <span class="tg">tg</span> 52466
                                </div>
                              </td>
                              <td>
                                36
                              </td>
                              <td>Fallout4</td>
                              <td>80</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="d-flex justify-content-end pt-3">
                        <nav aria-label="Page navigation example">
                          <ul class="pagination">
                            <li class="page-item border-pre disabled">
                              <a class="page-link" href="javascript:void(0);"><span class="page-prev"></span></a>
                            </li>
                            <li class="page-item">
                              <a class="page-link" href="javascript:void(0);">1</a>
                            </li>
                            <li class="page-item">
                              <a class="page-link" href="javascript:void(0);">2</a>
                            </li>
                            <li class="page-item">
                              <a class="page-link" href="javascript:void(0);">3</a>
                            </li>
                            <li class="page-item border-next">
                              <a class="page-link" href="javascript:void(0);"><span class="page-next"></span></a>
                            </li>
                          </ul>
                        </nav>
                      </div>
                    </div>
                    <div class="custom-row tab-pane fade" id="catagory5" role="tabpanel" aria-labelledby="specific-tab">
                      <div class="table-responsive">
                        <table class="table custom-table">
                          <thead>
                            <tr>
                              <th scope="col">Rank</th>
                              <th scope="col">Name</th>
                              <th scope="col">Platform Assoc.</th>
                              <th scope="col">Platform ID</th>
                              <th scope="col">Earnings</th>
                              <th scope="col">Tour. Played</th>
                              <th scope="col">M.P (Games)</th>
                              <th scope="col">Win Rate (%)</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>1</td>
                              <td>
                                <div class="d-flex align-items-center">
                                  <div class="rounded-img-xs me-md-3 me-2">
                                    <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                                  </div>
                                  Barry Allen
                                </div>
                              </td>
                              <td>Playstation</td>
                              <td>1542111</td>
                              <td>
                                <div class="d-flex align-items-center">
                                  <span class="tg">tg</span> 52466
                                </div>
                              </td>
                              <td>
                                36
                              </td>
                              <td>Fallout4</td>
                              <td>80</td>
                            </tr>
                            <tr>
                              <td>1</td>
                              <td>
                                <div class="d-flex align-items-center">
                                  <div class="rounded-img-xs me-md-3 me-2">
                                    <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                                  </div>
                                  Barry Allen
                                </div>
                              </td>
                              <td>Playstation</td>
                              <td>1542111</td>
                              <td>
                                <div class="d-flex align-items-center">
                                  <span class="tg">tg</span> 52466
                                </div>
                              </td>
                              <td>
                                36
                              </td>
                              <td>Fallout4</td>
                              <td>80</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="d-flex justify-content-end pt-3">
                        <nav aria-label="Page navigation example">
                          <ul class="pagination">
                            <li class="page-item border-pre disabled">
                              <a class="page-link" href="javascript:void(0);"><span class="page-prev"></span></a>
                            </li>
                            <li class="page-item">
                              <a class="page-link" href="javascript:void(0);">1</a>
                            </li>
                            <li class="page-item">
                              <a class="page-link" href="javascript:void(0);">2</a>
                            </li>
                            <li class="page-item">
                              <a class="page-link" href="javascript:void(0);">3</a>
                            </li>
                            <li class="page-item border-next">
                              <a class="page-link" href="javascript:void(0);"><span class="page-next"></span></a>
                            </li>
                          </ul>
                        </nav>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- My Tournaments -->
              <div class="inner-container-sm tab-pane fade" id="donate" role="tabpanel" aria-labelledby="contact-tab">
                <h2 class="title-1 mb-3">My Tournaments</h2>
                <div class="row">
                  <div class="col-md-6 py-2">
                    <div class="bg-color3 p-4 box-height2 d-flex justify-content-between align-items-center">
                      <div>
                        <div class="heading-2 lg">03</div>
                        <p class="typo7 pt-3">Average Finish Position</p>
                      </div>
                      <figure>
                        <img src="{{asset('frontend/assets/img/win-img.png')}}" alt="Average Finish Position" class="img-fluid" width="67" height="67">
                      </figure>
                    </div>
                  </div>
                  <div class="col-md-6 py-2">
                    <div class="bg-color3 p-4 box-height2 d-flex justify-content-between align-items-center">
                      <div>
                        <div class="heading-2 lg d-flex align-items-center"><span class="tg light me-2">TG</span>90</div>
                        <p class="typo7 pt-3">Average Earning</p>
                      </div>
                      <figure>
                        <img src="{{asset('frontend/assets/img/earning.png')}}" alt="Average Earning" class="img-fluid" width="75" height="67">
                      </figure>
                    </div>
                  </div>
                </div>
                <div class="custom-row mt-md-4 mt-3">
                  <ul class="nav nav-tabs border-0" id="tournamentsTab" role="tablist">
                    <li class="nav-item me-3 my-1" role="presentation">
                      <button class="btn-yellow-border md px-3 text-uppercase active " id="active-tab" data-bs-toggle="tab" data-bs-target="#active" type="button" role="tab" aria-controls="home" aria-selected="true">Active</button>
                    </li>
                    <li class="nav-item me-3 my-1" role="presentation">
                      <button class="btn-yellow-border md px-3 text-uppercase" id="upcoming-tab" data-bs-toggle="tab" data-bs-target="#upcoming" type="button" role="tab" aria-controls="profile" aria-selected="false">Upcoming</button>
                    </li>
                    <li class="nav-item me-3 my-1" role="presentation">
                      <button class="btn-yellow-border md px-3 text-uppercase" id="completed-tab" data-bs-toggle="tab" data-bs-target="#completed" type="button" role="tab" aria-controls="profile" aria-selected="false">Completed</button>
                    </li>
                    <li class="nav-item me-3 my-1" role="presentation">
                      <button class="btn-yellow-border md px-3 text-uppercase" id="cenceled-tab" data-bs-toggle="tab" data-bs-target="#cenceled" type="button" role="tab" aria-controls="profile" aria-selected="false">Cenceled</button>
                    </li>
                  </ul>
                  <div class="tab-content bg-color3 p-4 mt-3" id="myTabContent4">
                    <div class="custom-row tab-pane fade show active" id="active" role="tabpanel" aria-labelledby="active-tab">
                      <div class="table-responsive">
                        <table class="table custom-table">
                          <thead>
                            <tr>
                              <th scope="col">Tournament Name</th>
                              <th scope="col">Start Time</th>
                              <th scope="col">Skill Level</th>
                              <th scope="col">Format</th>
                              <th scope="col">Entry Fees</th>
                              <th scope="col">Prize Pool</th>
                              <th scope="col">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                <div class="d-flex align-items-center">
                                  <div class="rounded-img-xs me-2">
                                    <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                                  </div>
                                  Sniper <span class="bg-blue-rounder ms-2">Tagg</span>
                                </div>
                              </td>
                              <td>10AM | 21 Apr, 2021</td>
                              <td>Pro</td>
                              <td>
                                3V3
                              </td>
                              <td>
                                $152
                              </td>
                              <td>
                                $3000
                              </td>
                              <td>
                                <div class="d-flex align-items-center">
                                  <a href="javascript:void(0);" title="Share" class="share-yellow">Chat</a>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="custom-row tab-pane fade" id="upcoming" role="tabpanel" aria-labelledby="upcoming-tab">
                      <div class="table-responsive">
                        <table class="table custom-table">
                          <thead>
                            <tr>
                              <th scope="col">Tournament Name</th>
                              <th scope="col">Start Time</th>
                              <th scope="col">Skill Level</th>
                              <th scope="col">Format</th>
                              <th scope="col">Entry Fees</th>
                              <th scope="col">Prize Pool</th>
                              <th scope="col">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                <div class="d-flex align-items-center">
                                  <div class="rounded-img-xs me-2">
                                    <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                                  </div>
                                  Sniper <span class="bg-blue-rounder ms-2">Tagg</span>
                                </div>
                              </td>
                              <td>10AM | 21 Apr, 2021</td>
                              <td>Pro</td>
                              <td>
                                3V3
                              </td>
                              <td>
                                $152
                              </td>
                              <td>
                                $3000
                              </td>
                              <td>
                                <div class="d-flex align-items-center">
                                  <a href="javascript:void(0);" title="Share" class="share-yellow">Chat</a>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="custom-row tab-pane fade" id="completed" role="tabpanel" aria-labelledby="completed-tab">
                      <div class="table-responsive">
                        <table class="table custom-table">
                          <thead>
                            <tr>
                              <th scope="col">Tournament Name</th>
                              <th scope="col">Start Time</th>
                              <th scope="col">Skill Level</th>
                              <th scope="col">Format</th>
                              <th scope="col">Entry Fees</th>
                              <th scope="col">Prize Pool</th>
                              <th scope="col">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                <div class="d-flex align-items-center">
                                  <div class="rounded-img-xs me-2">
                                    <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                                  </div>
                                  Sniper <span class="bg-blue-rounder ms-2">Tagg</span>
                                </div>
                              </td>
                              <td>10AM | 21 Apr, 2021</td>
                              <td>Pro</td>
                              <td>
                                3V3
                              </td>
                              <td>
                                $152
                              </td>
                              <td>
                                $3000
                              </td>
                              <td>
                                <div class="d-flex align-items-center">
                                  <a href="javascript:void(0);" title="Share" class="share-yellow">Chat</a>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="custom-row tab-pane fade" id="cenceled" role="tabpanel" aria-labelledby="cenceled-tab">
                      <div class="table-responsive">
                        <table class="table custom-table">
                          <thead>
                            <tr>
                              <th scope="col">Tournament Name</th>
                              <th scope="col">Start Time</th>
                              <th scope="col">Skill Level</th>
                              <th scope="col">Format</th>
                              <th scope="col">Entry Fees</th>
                              <th scope="col">Prize Pool</th>
                              <th scope="col">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                <div class="d-flex align-items-center">
                                  <div class="rounded-img-xs me-2">
                                    <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                                  </div>
                                  Sniper <span class="bg-blue-rounder ms-2">Tagg</span>
                                </div>
                              </td>
                              <td>10AM | 21 Apr, 2021</td>
                              <td>Pro</td>
                              <td>
                                3V3
                              </td>
                              <td>
                                $152
                              </td>
                              <td>
                                $3000
                              </td>
                              <td>
                                <div class="d-flex align-items-center">
                                  <a href="javascript:void(0);" title="Share" class="share-yellow">Chat</a>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Tabs section end -->
          </div>
        </div>
      </div>
    </div>
    <!-- Button trigger modal -->
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content bg-color2 modal-box">
          <span class="trianle-tl"></span>
          <span class="trianle-tr"></span>
          <span class="trianle-bl"></span>
          <span class="trianle-br"></span>
          <div class="modal-header cpx-2">
            <h5 class="heading-3 md mb-0 py-2">Add New Friend</h5>
            <button type="button" class="custom-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body cpx-2 py-4">
            <div class="custom-row">
              <span class="search-icon"></span>
              <input type="text" class="form-control input-transparent icon-lp md" placeholder="Search Friends" aria-label="Search" aria-describedby="basic-addon2" name="">
            </div>
            <div class="custom-row mt-md-4 mt-3">
              <ul class="custom-row scroll-y bg-color6 p-3">
                <li class="d-flex align-items-center justify-content-between py-3">
                  <div class="d-flex align-items-center">
                    <div class="rounded-img-xs me-2">
                      <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                    </div>
                    <div>
                      <h4 class="heading-3 mb-2">James Smith</h4>
                      <p class="typo6 md">Jason 11</p>
                    </div>
                  </div>
                  <div>
                    <span class="bg-blue-rounder d-blue px-3 text-uppercase">PSN ID: 58745</span>
                  </div>
                  <div>
                    <a href="javascript:void(0);" title="" class="d-flex align-items-center btn-yellow-border fill-yellow px-3 text-uppercase"><em class="follow-dBlue me-2"></em>Follow</a>
                  </div>
                </li>
                <li class="d-flex align-items-center justify-content-between py-3">
                  <div class="d-flex align-items-center">
                    <div class="rounded-img-xs me-2">
                      <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                    </div>
                    <div>
                      <h4 class="heading-3 mb-2">James Smith</h4>
                      <p class="typo6 md">Jason 11</p>
                    </div>
                  </div>
                  <div>
                    <span class="bg-blue-rounder d-blue px-3 text-uppercase">PSN ID: 58745</span>
                  </div>
                  <div>
                    <a href="javascript:void(0);" title="" class="d-flex align-items-center btn-yellow-border fill-yellow px-3 text-uppercase"><em class="follow-dBlue me-2"></em>Follow</a>
                  </div>
                </li>
                <li class="d-flex align-items-center justify-content-between py-3">
                  <div class="d-flex align-items-center">
                    <div class="rounded-img-xs me-2">
                      <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                    </div>
                    <div>
                      <h4 class="heading-3 mb-2">James Smith</h4>
                      <p class="typo6 md">Jason 11</p>
                    </div>
                  </div>
                  <div>
                    <span class="bg-blue-rounder d-blue px-3 text-uppercase">PSN ID: 58745</span>
                  </div>
                  <div>
                    <a href="javascript:void(0);" title="" class="d-flex align-items-center btn-yellow-border fill-yellow px-3 text-uppercase"><em class="follow-dBlue me-2"></em>Follow</a>
                  </div>
                </li>
                <li class="d-flex align-items-center justify-content-between py-3">
                  <div class="d-flex align-items-center">
                    <div class="rounded-img-xs me-2">
                      <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                    </div>
                    <div>
                      <h4 class="heading-3 mb-2">James Smith</h4>
                      <p class="typo6 md">Jason 11</p>
                    </div>
                  </div>
                  <div>
                    <span class="bg-blue-rounder d-blue px-3 text-uppercase">PSN ID: 58745</span>
                  </div>
                  <div>
                    <a href="javascript:void(0);" title="" class="d-flex align-items-center btn-yellow-border fill-yellow px-3 text-uppercase"><em class="follow-dBlue me-2"></em>Follow</a>
                  </div>
                </li>
                <li class="d-flex align-items-center justify-content-between py-3">
                  <div class="d-flex align-items-center">
                    <div class="rounded-img-xs me-2">
                      <img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}">
                    </div>
                    <div>
                      <h4 class="heading-3 mb-2">James Smith</h4>
                      <p class="typo6 md">Jason 11</p>
                    </div>
                  </div>
                  <div>
                    <span class="bg-blue-rounder d-blue px-3 text-uppercase">PSN ID: 58745</span>
                  </div>
                  <div>
                    <a href="javascript:void(0);" title="" class="d-flex align-items-center btn-yellow-border fill-yellow px-3 text-uppercase"><em class="follow-dBlue me-2"></em>Follow</a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="custom-row">
      <!-- Page content -->
      <div class="container">
        <div class="row">


          @include('dashboards.users.layout.script')
        </div>
      </div>
    </div>
  </div>
  <link rel="stylesheet" href="https://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">

  @include('toaster')

  <script>
    function loadPreview(input, id) {
      id = id || '#preview_img';
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $(id)
            .attr('src', e.target.result)
            .width(270)
            .height(215);
        };

        reader.readAsDataURL(input.files[0]);
        document.getElementById('formpicture').submit();
      }

    }
  </script>


  <script>
    $(document).ready(function() {
      $("#carousel").owlCarousel({
        autoplay: true,
        lazyLoad: true,
        loop: false,
        margin: 20,
        nav: true,
        responsiveClass: true,
        autoHeight: true,
        autoplayTimeout: 7000,
        smartSpeed: 800,
        responsive: {
          0: {
            items: 1
          },

          767: {
            items: 2
          },

          1024: {
            items: 3
          },

          1366: {
            items: 3
          }
        }
      });
      $("#interest").owlCarousel({
        autoplay: true,
        lazyLoad: true,
        loop: false,
        margin: 20,
        nav: true,
        responsiveClass: true,
        autoHeight: true,
        autoplayTimeout: 7000,
        smartSpeed: 800,
        responsive: {
          0: {
            items: 1
          },

          767: {
            items: 2
          },

          1024: {
            items: 3
          },

          1366: {
            items: 3
          }
        }
      });
    });
  </script>
</body>

</html>
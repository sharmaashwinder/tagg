<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('dashboards.users.layout.head')

<body>
    {{-- {{ session()->get( 'data' ) }} --}}
    <div class="outer-container">
        @include('dashboards.users.layout.header')
        <div class="banner-outer banner-bg bg-color4 color2">
            <div class="inner-container-xs">
                <div class="color-box c-my-5">
                    <div class="text-center">
                        <h1 class="heading-1 md text-uppercase"><span class="theme-color md">{{ __('Verify Your Email Address') }}</span></h1>
                        <div class="typo2">
                            @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                {{ __('A fresh verification link has been sent to your email address.') }}
                            </div>
                            @endif

                            {{ __('Before proceeding, please check your email for a verification link.') }}
                            {{ __('If you did not receive the email') }},
                        </div>
                    </div>
                    @if(Session::has('message'))
                    <div class="alert alert-{{ Session::get('alert-type', 'info') }}">
                        <ul>
                            {{ Session::get('message') }}
                        </ul>
                    </div>
                    @endif
                    @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! \Session::get('success') !!}</li>
                        </ul>
                    </div>
                @endif

                    @if ($errors->any())
                    <div class="alert alert-danger mt-4 mb-0">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form class="pt-lg-4 pt-md-3 pt-2" method="POST" action="{{ route('resend_verify') }}">
                        @csrf
                        <input name='email' hidden  value="{{ session()->get( 'data' )}}"/>
                        <button type="submit" class="btn-yellow-lg text-center custom-row">{{ __('click here to request another') }}</button>.
                    </form>
                </div>
            </div>
        </div>
        @include('dashboards.users.layout.footer')
    </div>
</body>

</html>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('dashboards.users.layout.head')

<body>
    <div class="outer-container">
        <div class="custom-row">
            <header class="header py-2 py-lg-3 header-top">
                <div class="inner-container">
                    <div class="py-md-2 py-1">
                        <div class="logo-area admin-logo">
                            <a href="/" title="Tagg Us">
                                <img src="{{asset('frontend/assets/img/tagg-logo.png')}}" width="194" height="82" alt="Tagg Us Logo" />
                            </a>
                        </div>

                    </div>
                </div>
            </header>
            <!-- header end -->
            <!-- banner start -->
            <div class="banner-outer banner-bg bg-color4 color2 full-h">
                <div class="inner-container-xs">
                    <div class="color-box c-my-5">
                        <div class="text-center">
                            <h1 class="heading-1 md"><span class="theme-color md">SIGN IN</span></h1>
                            {{-- <div class="typo2">Already Member? <a class="typo2" href="{{ route('register') }}" title="Sign Up">Sign Up</a>
                        </div> --}}
                    </div>

                    @if ($errors->any())
                    <div class="alert alert-danger mt-4 mb-0">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form id="loginform" method="POST" action="{{ route('admin_login') }}">
                        @csrf
                        <div class="custom-row my-2">
                            <input id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus type="text" class="form-control custom-input-lg" placeholder="Email" />
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <span class="ab-icon d-flex align-items-center justify-content-center"><em class="icon-user"></em></span>
                        </div>
                        <div class="custom-row my-2">
                            <input id="password" name="password" required autocomplete="current-password" type="password" class="form-control custom-input-lg" placeholder="Password" />
                            <span class="ab-icon d-flex align-items-center justify-content-center"><em class="icon-lock"></em></span>
                        </div>
                        @if (Route::has('password.request'))
                        {{-- <div class="custom-row my-3">
                            <a class="typo5" href="{{ route('password.request') }}" title="Forgot Password?">
                        {{ __('Forgot Your Password?') }}
                        </a>
                </div> --}}
                @endif
                {{-- <div class="custom-row my-3">
                            <a class="typo5" href="{{ route('password/reset') }}" title="Forgot Password?">Forgot Password?</a>
            </div> --}}
            <div class="custom-row mt-md-3 mt-2">
                <button type="submit" class="btn-yellow-lg text-center custom-row" title="Sign In">Sign In</button>
            </div>
            </form>
        </div>
    </div>
    </div>
    <!-- banner end -->
    <!-- brand logos -->
    </div>
    <div class="common-space-sm bg-color2">
        <div class="inner-container typo4 text-center">
            © <?php echo date("Y"); ?>
            TAGG. All Rights Reserved.
        </div>
    </div>
    </div>
    <script>
        $(document).ready(function() {
            $("#loginform").validate({
                rules: {
                    email: {
                        // required: true,
                        email: true,
                        //  pattern:"/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/",
                        // regex:'/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/',
                    },
                    password: {
                        required: true,
                        minlength: 6
                    }

                },
                messages: {
                    email: {
                        required: 'Email is required',
                        regex: 'not there',
                        pattern: 'pattern does not matched'
                    },
                    password: {
                        required: 'Password is required',

                    },

                }
            });
        });
        $.validator.methods.email = function(value, element) {
            return this.optional(element) || /[a-z]+@[a-z]+\.[a-z]+/.test(value);
        }

        //         window.onload = () => {
        //  const myInput = document.getElementById('myInput');
        //  myInput.onpaste = e => e.preventDefault();
    </script>

</body>

</html>
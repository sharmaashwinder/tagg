<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('dashboards.users.layout.head')

<body>
    <div class="outer-container">
        @include('dashboards.users.layout.header')
        <!-- header end -->
        <!-- banner start -->
        <div class="banner-outer banner-bg bg-color4 color2">
            <div class="inner-container-xs">
                <div class="color-box c-my-5">
                    <div class="text-center">
                        <h1 class="heading-1 md"><span class="theme-color md">SIGN IN</span></h1>
                        <div class="typo2">Already Member? <a class="typo2" href="{{ route('register') }}" title="Sign Up">Sign Up</a></div>
                    </div>
                    <ul class="py-4">
                        <li class="my-3">
                            <a class="grad-bg d-flex align-items-center" href="{{ url('auth/epic') }}" title="Sign in with Epic Games">
                                <div class="left-logo black d-flex align-items-center justify-content-center"><img src="frontend/assets/img/epic-white.png" width="43" height="46" alt="Epic Games" /></div>
                                sign in with epic games
                            </a>
                        </li>
                        <li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="javascript:void(0);" title="Sign in with Steam">
                                <div class="left-logo s-blue d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/steam.png" width="46" height="46" alt="Steam" /></div>
                                sign in with Steam
                            </a>
                        </li>
                        {{--<li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="{{ url('auth/activision') }}" title="sign in with activision">
                                <div class="left-logo white d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/active-vision.png" width="79" height="21" alt="Activision" /></div>
                                sign in with Activision
                            </a>
                        </li>--}}
                        <li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="{{ url('auth/twitch') }}" title="Sign in with Twitch">
                                <div class="left-logo t-purple d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/twitch.png" width="79" height="21" alt="Twitch" /></div>
                                sign in with Twitch
                            </a>
                        </li>
                        <li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="javascript:void(0);" title="Sign in with Playstation Network">
                                <div class="left-logo l-blue d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/playstation-logo.png" width="51" height="41" alt="Playstation Network" /></div>
                                sign in with Playstation Network
                            </a>
                        </li>
                        <li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="{{ url('auth/xbox') }}" title="Sign In with Xbox live">
                                <div class="left-logo green d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/xbox.png" width="42" height="42" alt="Xbox live" /></div>
                                sign in with Xbox live
                            </a>
                        </li>
                        <li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="{{ url('auth/facebook') }}" title="Sign in with Facebook">
                                <div class="left-logo f-bg d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/facebook-logo.png" width="22" height="43" alt="Facebook" /></div>
                                sign in with Facebook
                            </a>
                        </li>
                        <li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="{{ url('auth/google') }}" title="Sign in with Google">
                                <div class="left-logo white d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/google.png" width="40" height="40" alt="Google" /></div>
                                sign in with Google
                            </a>
                        </li>
                        <li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="{{ url('auth/twitter') }}" title="Sign in with Twitter">
                                <div class="left-logo tweet-bg d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/twitter.png" width="42" height="34" alt="twitter" /></div>
                                sign in with twitter
                            </a>
                        </li>
                        <li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="{{ url('auth/apple') }}" title="Sign in with Apple">
                                <div class="left-logo white d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/apple-black.png" width="34" height="41" alt="Apple" /></div>
                                sign in with Apple
                            </a>
                        </li>
                    </ul>
                    <div class="separator">
                        <span>Or</span>
                    </div>
                     @if(Session::has('message'))
                    <div class="alert alert-{{ Session::get('alert-type', 'danger') }}">
                        <ul>
                            {{ Session::get('message') }}
                        </ul>
                    </div>
                    @endif
                    @if(Session::has('success'))
                    <div class="alert alert-{{ Session::get('alert-type', 'success') }}">
                        <ul>
                            {{ Session::get('success') }}
                        </ul>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger mt-4 mb-0">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <form id="loginform" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="custom-row my-2">
                            <input id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus type="text" class="form-control custom-input-lg" placeholder="Email" />
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <span class="ab-icon d-flex align-items-center justify-content-center"><em class="icon-user"></em></span>
                        </div>
                        <div class="custom-row my-2">
                            <input id="password" name="password" required autocomplete="current-password" type="password" class="form-control custom-input-lg" placeholder="Password" />
                            <span class="ab-icon d-flex align-items-center justify-content-center"><em class="icon-lock"></em></span>
                        </div>
                        @if (Route::has('password.request'))
                        <div class="custom-row my-3">
                            <a class="typo5" href="{{ route('password.request') }}" title="Forgot Password?">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        </div>
                        @endif
                        {{-- <div class="custom-row my-3">
                            <a class="typo5" href="{{ route('password/reset') }}" title="Forgot Password?">Forgot Password?</a>
                </div> --}}
                <div class="custom-row mt-md-3 mt-2">
                    <button type="submit" class="btn-yellow-lg text-center custom-row" title="Sign In">Sign In</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- banner end -->
    <!-- brand logos -->
    @include('dashboards.users.layout.footer')
    </div>
    <script>
        $(document).ready(function() {
            $("#loginform").validate({
                rules: {
                    email: {


                        //  pattern:"/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/",
                        // regex:'/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/',
                    },
                    password: {
                        required: true,
                        minlength: 6
                    }

                },
                messages: {
                    email: {
                        required: 'Email is required',
                        email: 'Please enter valid email'
                    },
                    password: {
                        required: 'Password is required',

                    },

                }
            });
        });
        $.validator.methods.email = function(value, element) {
            //return this.optional(element) || /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value);
            return this.optional(element) || /^[a-z0-9][-a-z0-9.!#$%&'*+-=?^_`{|}~\/]+@([-a-z0-9]+\.)+[a-z]{2,5}$/.test(value);
            // ^[a-z0-9](\.?[a-z0-9]){5,}
        }

        // /[a-z]+@[a-z]+\.[a-z]+/
        //         window.onload = () => {
        //  const myInput = document.getElementById('myInput');
        //  myInput.onpaste = e => e.preventDefault();
    </script>
</body>

</html>

<?php $gamingPlatform = 0;
if (!empty(session('gamingPlatform'))) {
    $gamingPlatform = session('gamingPlatform');
    session()->forget('gamingPlatform');
}
?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('dashboards.users.layout.head')
<body>
    <div class="outer-container">
        @include('dashboards.users.layout.header')
        <!-- header end -->
        <!-- banner start -->
        <div class="banner-outer banner-bg bg-color4 color2">
            <div class="inner-container-xs">
                <div class="color-box c-my-5">
                    <div class="text-center">
                        <h1 class="heading-1 md"><span class="theme-color md">SIGN UP</span></h1>
                        <div class="typo2">Not Member Yet? <a class="typo2" href="{{ route('register') }}" title="Join Free">Join Free</a></div>
                    </div>
                    @if(empty($gamingPlatform))
                    <!-- <div class="alert alert-info mt-3 lh-2 h5 text-center">
                        Hi, we were not able to find a linked Tagg account. Would you like to link this account?
                    </div> -->
                    <ul class="py-lg-4 py-md-3 py-2">
                        <li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="{{ url('auth/epic') }}" title="Sign up with Epic Games">
                                <div class="left-logo black d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/epic-white.png" width="43" height="46" alt="Epic Games" /></div>
                                Sign up with epic games
                            </a>
                        </li>
                        <li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="javascript:void(0);" title="Sign up with Steam">
                                <div class="left-logo s-blue d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/steam.png" width="46" height="46" alt="Steam" /></div>
                                Sign up with Steam
                            </a>
                        </li>
                        {{--<li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="{{ url('auth/activision')}}" title="sign up with Twitch">
                                <div class="left-logo white d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/active-vision.png" width="79" height="21" alt="Activision" /></div>
                                sign up with Activision
                            </a>
                        </li>--}}
                        <li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="{{ url('auth/twitch')}}" title="Sign up with Twitch">
                                <div class="left-logo t-purple d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/twitch.png" width="79" height="21" alt="Twitch" /></div>
                                Sign up with Twitch
                            </a>
                        </li>
                        <li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="javascript:void(0);" title="Sign up with Playstation Network">
                                <div class="left-logo l-blue d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/playstation-logo.png" width="51" height="41" alt="Playstation Network" /></div>
                                Sign up with Playstation Network
                            </a>
                        </li>
                        <li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="{{ url('auth/xbox') }}" title="Sign Up with Xbox live">
                                <div class="left-logo green d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/xbox.png" width="42" height="42" alt="Xbox live" /></div>
                                Sign up with Xbox live
                            </a>
                        </li>
                        <li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="{{ url('auth/facebook') }}" title="Sign up with Facebook">
                                <div class="left-logo f-bg d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/facebook-logo.png" width="22" height="43" alt="Facebook" /></div>
                                Sign up with Facebook
                            </a>
                        </li>
                        <li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="{{ url('auth/google') }}" title="Sign up with Google">
                                <div class="left-logo white d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/google.png" width="40" height="40" alt="Google" /></div>
                                Sign up with Google
                            </a>
                        </li>
                        <li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="{{ url('auth/twitter') }}" title="Sign Up with Twitter">
                                <div class="left-logo tweet-bg d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/twitter.png" width="42" height="34" alt="twitter" /></div>
                                Sign up with twitter
                            </a>
                        </li>
                        <li class="my-md-3 my-2">
                            <a class="grad-bg d-flex align-items-center" href="{{ url('auth/apple') }}" title="Sign up with Apple">
                                <div class="left-logo white d-flex align-items-center justify-content-center"><img class="img-fluid-xy" src="frontend/assets/img/apple-black.png" width="34" height="41" alt="Apple" /></div>
                                Sign up with Apple
                            </a>
                        </li>
                    </ul>
                    <div class="separator">
                        <span>Or</span>
                    </div>
                    @endif

                    @if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif
                    @if(Session::has('message'))
                    <div class="alert alert-{{ Session::get('alert-type', 'info') }}">
                        <ul>
                            {{ Session::get('message') }}
                        </ul>
                    </div>
                    @endif
                    {{-- @if ($errors->any())
                    <div class="alert alert-danger mt-4 mb-0">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif --}}
                    @if ($errors->any())
                    <div class="alert alert-danger mt-4 mb-0">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form class="" method="POST" id="registerForm" action="{{ route('register') }}">
                        @csrf
                        <input type="hidden" name="gamingPlatform" value="{{ json_encode($gamingPlatform) }}">
                        <div class="custom-row my-2">
                            <input class="form-control custom-input-lg" placeholder="Email" id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" />
                            <span class="ab-icon d-flex align-items-center justify-content-center"><em class="icon-email"></em></span>
                        </div>
                        <div class="custom-row my-2">
                            <input type="text" class="form-control custom-input-lg" placeholder="Name" id="name" name="name" value="{{ old('name') }}" required />
                            <span class="ab-icon d-flex align-items-center justify-content-center"><em class="icon-user"></em></span>
                        </div>
                        <div class="custom-row my-2">
                            <input type="text" class="form-control custom-input-lg" placeholder="Gamer Name" id="gamer_name" name="gamer_name" value="{{ old('gamer_name') }}" required />
                            <span class="ab-icon d-flex align-items-center justify-content-center"><em class="icon-user"></em></span>
                        </div>
                        <div class="custom-row my-2">
                            <input type="password" id="password" name="password" class="form-control custom-input-lg" placeholder="Password" required autocomplete="new-password" />
                            <span class="ab-icon d-flex align-items-center justify-content-center"><em class="icon-lock"></em></span>
                        </div>
                        <div class="custom-row my-2">
                            <input type="password" id="password_confirmation" class="form-control custom-input-lg" name="password_confirmation" placeholder="Confirm Password" name="password_confirmation" required autocomplete="new-password" />
                            <span class="ab-icon d-flex align-items-center justify-content-center"><em class="icon-lock"></em></span>
                        </div>
                        {{-- <div class="custom-row my-3 custom-checkbox">
                            <input class="check-input" type="checkbox" value="" id="flexCheckDefault">
                            <label class="check-label" for="flexCheckDefault">
                                Remember me
                            </label>
                        </div> --}}
                        <div class="custom-row mt-md-3 mt-2">
                            <button type="submit" class="btn-yellow-lg text-center custom-row" title="Sign Up">Sign Up</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- banner end -->
        <!-- brand logos -->
        @include('dashboards.users.layout.footer')
    </div>
    <script>
        $(document).ready(function() {
            $("#registerForm").validate({
                rules: {
                    email: {
                        required: true,
                    },
                    gamer_name: {
                        required: true,
                    },
                    name: {
                        required: true,
                    },
                    password: {
                        required: true,
                        minlength: 6
                    },
                    password_confirmation: {
                        required: true,
                        equalTo: "#password",
                        minlength: 6
                    }
                },
                messages: {
                    email: {
                        required: 'Email is required',
                        email: 'Please enter valid email'
                    },
                    name: {
                        required: 'Name is required'
                    },
                    gamer_name: {
                        required: 'Gamer Name is required'
                    },
                    password: {
                        required: 'Password is required',
                    },
                    password_confirmation: {
                        required: 'Confirm  Password is required ',
                        equalTo: 'Password and confirm password does not match'
                    },
                }
            });
        });
        $.validator.methods.email = function(value, element) {
            // return this.optional(element) || /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value);
            return this.optional(element) || /^[a-z0-9][-a-z0-9.!#$%&'*+-=?^_`{|}~\/]+@([-a-z0-9]+\.)+[a-z]{2,5}$/.test(value);
        }
        //         window.onload = () => {
        //  const myInput = document.getElementById('myInput');
        //  myInput.onpaste = e => e.preventDefault();
    </script>
    {{-- <script>
    var password = document.getElementById("password")
  , confirm_password = document.getElementById("password_confirmation");
function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Password and confirm password does not match");
  } else {
    confirm_password.setCustomValidity('');
  }
}
password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
    </script> --}}
</body>

</html>

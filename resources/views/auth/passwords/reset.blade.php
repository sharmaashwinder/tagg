<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('dashboards.users.layout.head')

<body>

    <div class="outer-container">
        @include('dashboards.users.layout.header')
        <div class="banner-outer banner-bg bg-color4 color2">
            <div class="inner-container-xs">
                <div class="color-box c-my-5">
                    <div class="text-center">
                        <h1 class="heading-1 md text-uppercase"><span class="theme-color md">{{ __('Reset Password') }}</span></h1>
                        <div class="typo2">Enter your new password below.</div>
                    </div>
                    <form class="pt-lg-4 pt-md-3 pt-2" method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="custom-row my-2">
                            <input id="email" type="email" class="form-control custom-input-lg @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" placeholder="E-Mail Address" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <span class="ab-icon d-flex align-items-center justify-content-center"><em class="icon-email"></em></span>
                        </div>

                        <div class="custom-row my-2">
                            <input id="password" type="password" class="form-control custom-input-lg @error('password') is-invalid @enderror" name="password" placeholder="{{ __('Password') }}" required autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <span class="ab-icon d-flex align-items-center justify-content-center"><em class="icon-lock"></em></span>
                        </div>

                        <div class="custom-row my-2">

                            <input id="password-confirm" type="password" class="form-control custom-input-lg" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('Confirm Password') }}">
                            <span class="ab-icon d-flex align-items-center justify-content-center"><em class="icon-lock"></em></span>
                        </div>

                        <div class="custom-row mt-md-3 mt-2">
                            <button type="submit" class="btn-yellow-lg text-center custom-row" title="Reset Password">
                                {{ __('Reset Password') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @include('dashboards.users.layout.footer')
    </div>
</body>

</html>
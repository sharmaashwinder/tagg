<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('dashboards.users.layout.head')

<body>
    <div class="outer-container">
        @include('dashboards.users.layout.header')
        <div class="banner-outer banner-bg bg-color4 color2">
            <div class="inner-container-xs">
                <div class="color-box c-my-5">
                    <div class="text-center">
                        <h1 class="heading-1 md text-uppercase"><span class="theme-color md">{{ __('Reset Password') }}</span></h1>
                        <div class="typo2">Enter your email address below and we'll send you a link to get back into your account.</div>
                    </div>
                    @if (session('status'))
                    <div class="alert alert-success mt-4 mb-0" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form class="pt-lg-4 pt-md-3 pt-2" method="POST"  id="emailForm" action="{{ route('password.email') }}">
                        @csrf


                        <div class="custom-row my-2">
                            <input id="email" type="email" class="form-control custom-input-lg @error('email') is-invalid @enderror" placeholder="E-Mail Address" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <span class="ab-icon d-flex align-items-center justify-content-center"><em class="icon-email"></em></span>
                        </div>

                        <div class="custom-row mt-md-3 mt-2">
                            <button type="submit" class="btn-yellow-lg text-center custom-row" title="Send Password Reset Link">
                                {{ __('Send Password Reset Link') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @include('dashboards.users.layout.footer')
    </div>
    <script>
    $(document).ready(function() {
            $("#emailForm").validate({
                rules: {
                    email: {
                        required: true,

                    }

                },
                messages:{
                    email:{
                      required:'Email is required',
                      pattern:'pattern does not matched'
                    },


                }
            });
        });
        $.validator.methods.email = function( value, element ) {
//   return this.optional( element ) ||  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test( value );
return this.optional(element) || /^[a-z0-9][-a-z0-9.!#$%&'*+-=?^_`{|}~\/]+@([-a-z0-9]+\.)+[a-z]{2,5}$/.test(value);
}

// $( "#emailForm" ).submit(function( event ) {
//     var id=$('#email').val();
//     console.log(id);
//   event.preventDefault();
// });

    </script>
</body>

</html>

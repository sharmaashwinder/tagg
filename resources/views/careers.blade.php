<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('dashboards.users.layout.head')

<body>
    <div class="outer-container">
        @include('dashboards.users.layout.header')
        <!-- header end -->
        <!-- banner start -->
        <div class="banner-outer banner-about">
            <div class="inner-container-sm">
                <div class="text-center py-lg-5 py-md-4 py-0 my-lg-5 my-md-4 my-0">
                    <h1 class="heading-1 pt-lg-4 pt-md-2 pt-0 pb-2">Coming Soon...</h1>
                </div>
            </div>
        </div>
        <!-- popular start -->
        @include('dashboards.users.layout.popular')
        <!-- Subscribe start -->
        @include('dashboards.users.layout.subscribe')
        <!-- footer start -->
        @include('dashboards.users.layout.footer')
    </div>

</body>

</html>
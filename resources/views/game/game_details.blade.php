<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('dashboards.users.layout.head')

<body>

    <div class="outer-container">
        @include('dashboards.users.layout.header')
        <!-- header end -->
        <!-- banner start -->
        <div class="banner-outer banner-product">
            <div class="inner-container">
                <div class="custom-row c-my-5">
                    <div class="row align-items-center">
                        <div class="col-lg-3 col-md-4">
                            <figure class="d-flex justify-content-center justify-content-md-start">
                        @if(!empty($data[0]->image))
                                <img class="img-fluid" width="267" height="292" alt="Product image" src="{{asset('storage/'.$data[0]->image)}}">
                            @else
                            <img class="img-fluid" width="267" height="292" alt="Product image" src="{{asset('frontend/assets/img/default-sm.jpg')}}">
                            @endif
                            </figure>
                        </div>

                        <div class="col-lg-9 col-md-8 pt-4 pt-md-0">
                            <div class="d-flex align-items-center flex-wrap">
                                <h1 class="section-title me-3">   {{$data[0]->game_name}}</h1>
                                {{-- <a class="border-btn2 border-sm custom-col" href="javascript:void(0);" title="Add Friend">Add Friend</a> --}}
                            </div>
                            <ul class="list-with-icons d-flex">
                                <li class="d-flex align-items-center"><em class="icon-category me-3"></em>{{$data[0]->genre}}</li>
                                <li class="d-flex align-items-center"><em class="icon-game me-3"></em> {{$data[0]->platform}}</li>
                                <li class="d-flex align-items-center"><em class="icon-users me-3"></em> 87</li>
                                @if(!empty($tournamentCount))
                                <li class="d-flex align-items-center"><em class="icon-tour me-3"></em> {{$tournamentCount}} Active Tournament</li>
                           @else
                           <li class="d-flex align-items-center"><em class="icon-tour me-3"></em> 0 Active Tournament</li>
                               @endif
                            </ul>
                            <p class="typo3 md">
                            {{$data[0]->description}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- banner end -->
        <!-- product detail start  -->
        <div class="common-space bg-color2">
            <div class="full-container">
                <div class="row">
                    <div class="col-lg-3 col-12 mb-lg-0 mb-4">
                        <div class="bg-color3">
                            <h2 class="heading-3 md border-b-title px-3 mb-0">Tournaments</h2>
                            <ul class="sidebar-list px-3">
                               
                              
                        
                               @if(!empty($Tournament) && $Tournament->count())
                               @foreach($Tournament as $Tournaments)
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center me-3">
                                        <div class="rounded-img-xs me-md-3 me-2"><img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}"></div>
                                        <h4 class="heading-5">{{$Tournaments->name}}</h4>
                                    </div>
                                    <a href="javascript:void(0);" title="Share" class="category-share"></a>
                                </li>
                             
                                @endforeach
                                @else
                               
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center me-3">
                                        <div class="rounded-img-xs me-md-3 me-2"><img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}"></div>
                                        <h4 class="heading-5">Snipper Game</h4>
                                    </div>
                                    <a href="javascript:void(0);" title="Share" class="category-share"></a>
                                </li>

                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center me-3">
                                        <div class="rounded-img-xs me-md-3 me-2"><img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}"></div>
                                        <h4 class="heading-5">Snipper Game</h4>
                                    </div>
                                    <a href="javascript:void(0);" title="Share" class="category-share"></a>
                                </li>

                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center me-3">
                                        <div class="rounded-img-xs me-md-3 me-2"><img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}"></div>
                                        <h4 class="heading-5">Snipper Game</h4>
                                    </div>
                                    <a href="javascript:void(0);" title="Share" class="category-share"></a>
                                </li>              
                                @endif


                            </ul>
                            <div class="border-t p-3">
                                <a class="arrow-btn" href="javascript:void(0);" title="View All">View All</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 mb-lg-0 mb-4">
                        <div class="px-md-2">
                            <!-- top search -->
                            <div class="d-flex flex-sm-row flex-column">
                                <form class="input-group order-group" id="" action="" method="get">
                                    <span class="search-icon"></span>
                                    <input type="text" class="form-control input-transparent icon-lp" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2" name="game_name" required="">
                                </form>
                            </div>
                            <!-- add post -->
                            <div class="bg-color3 mt-md-4 mt-3">
                                <div class="p-md-4 p-3">
                                    <div class="d-flex">
                                        <div class="rounded-img-xs me-md-3 me-2"><img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}"></div>
                                        <input type="text" class="form-control input-blue" placeholder="Write Something here" />
                                    </div>
                                </div>
                                <div class="border-t top-1 p-md-4 p-3 d-flex justify-content-between">
                                    <div class="d-flex">
                                        <div class="custom-row me-3">
                                            <a class="btn-yellow-border custom-row text-center text-uppercase px-4" href="javascript:void(0);" title="Invite">Invite</a>
                                        </div>
                                        <div class="custom-row">
                                            <a class="btn-yellow-border custom-row text-center text-uppercase px-4 text-nowrap" href="javascript:void(0);" title="Add Photos/Videos">Add Photos/Videos</a>
                                        </div>
                                    </div>
                                    <div>
                                        <a class="btn-yellow-fill custom-row text-center text-uppercase px-4" href="javascript:void(0);" title="POST">POST</a>
                                    </div>
                                </div>
                            </div>
                            <!-- post list -->
                            <div class="bg-color3 mt-md-4 mt-3">
                                <div class="p-md-4 p-3">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div class="d-flex align-items-center">
                                            <div class="rounded-img-xs me-md-3 me-2"><img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}"></div>
                                            <div>
                                                <h4 class="heading-3 m-0">Mehna Rich</h4>
                                                <div class="typo6 pt-3">4 hours ago</div>
                                            </div>
                                        </div>
                                        <div class="custom-dropdown">
                                            <button class="dropdown-toggle btn-dots" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                                                <span></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-end p-0" aria-labelledby="dropdownMenuButton">
                                                <li><a class="dropdown-item" title="Tag your friend" href="javascript:void(0);">Tag your friend</a></li>
                                                <li><a class="dropdown-item" title="Hide post" href="javascript:void(0);">Hide post</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="pt-md-4 pt-3">
                                        <p class="typo3 md pb-3">
                                            In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available.
                                        </p>
                                        <figure class="custom-row pb-md-4 pb-3">
                                            <img class="img-fluid" src="{{asset('frontend/assets/img/banner-faq.jpg')}}">
                                        </figure>
                                        <div class="cusrom-row">
                                            <div class="cusrom-row">
                                                <button class="icon-like me-md-4 me-3"></button>
                                                <button class="icon-comment me-md-4 me-3"></button>
                                                <button class="icon-reply"></button>
                                            </div>
                                            <p class="typo7 pt-3">Liked by <strong>Millions</strong> and <strong>250 others</strong></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="px-4 px-3 py-3 border-t top-1">
                                    <input type="text" class="form-control input-transparent border-0" placeholder="Write Your Comment...">
                                </div>
                            </div>
                            <div class="bg-color3 mt-md-4 mt-3">
                                <div class="p-md-4 p-3">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div class="d-flex align-items-center">
                                            <div class="rounded-img-xs me-md-3 me-2"><img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}"></div>
                                            <div>
                                                <h4 class="heading-3 m-0">Mehna Rich</h4>
                                                <div class="typo6 pt-3">4 hours ago</div>
                                            </div>
                                        </div>
                                        <div class="custom-dropdown">
                                            <button class="dropdown-toggle btn-dots" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                                                <span></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-end p-0" aria-labelledby="dropdownMenuButton">
                                                <li><a class="dropdown-item" title="Tag your friend" href="javascript:void(0);">Tag your friend</a></li>
                                                <li><a class="dropdown-item" title="Hide post" href="javascript:void(0);">Hide post</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="pt-md-4 pt-3">
                                        <p class="typo3 md pb-3">
                                            In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available.
                                        </p>
                                        <ul class="list-with-icons border-r d-flex mb-md-4 mb-3 mt-1">
                                            <li class="d-flex align-items-center"><em class="icon-game me-3"></em> Skill Level  Pro</li>
                                            <li class="d-flex align-items-center"><em class="icon-users me-3"></em> Num. of Players 3</li>
                                        </ul>
                                        <div class="custom-row mb-md-4 mb-3">
                                            <a href="javascript:void(0);" title="Join Now" class="btn-yellow-fill lg custom-row text-uppercase text-center">Join Now</a>
                                        </div>
                                        <div class="cusrom-row">
                                            <div class="cusrom-row">
                                                <button class="icon-like me-md-4 me-3"></button>
                                                <button class="icon-comment me-md-4 me-3"></button>
                                                <button class="icon-reply"></button>
                                            </div>
                                            <p class="typo7 pt-3">Liked by <strong>Millions</strong> and <strong>250 others</strong></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="px-4 px-3 py-3 border-t top-1">
                                    <input type="text" class="form-control input-transparent border-0" placeholder="Write Your Comment...">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-12">
                        <div class="bg-color3 mb-md-4 mb-3">
                            <div class="row px-3 pt-3">
                                <div class="col">
                                    <a class="btn-yellow-fill custom-row text-center text-uppercase" href="javascript:void(0);" title="Friends">Friends</a>
                                </div>
                                <div class="col">
                                    <a class="btn-yellow-border custom-row text-center text-uppercase" href="javascript:void(0);" title="Players">Players</a>
                                </div>
                            </div>
                            <ul class="sidebar-list px-3">
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center me-3">
                                        <div class="rounded-img-xs me-md-3 me-2"><img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}"></div>
                                        <h4 class="heading-5">Snipper Game</h4>
                                    </div>
                                    <a href="javascript:void(0);" title="Add" class="icon-userAdd"></a>
                                </li>
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center me-3">
                                        <div class="rounded-img-xs me-md-3 me-2"><img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}"></div>
                                        <h4 class="heading-5">Snipper Game</h4>
                                    </div>
                                    <a href="javascript:void(0);" title="Add" class="icon-userAdd"></a>
                                </li>
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center me-3">
                                        <div class="rounded-img-xs me-md-3 me-2"><img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}"></div>
                                        <h4 class="heading-5">Snipper Game</h4>
                                    </div>
                                    <a href="javascript:void(0);" title="Add" class="icon-userAdd"></a>
                                </li>
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center me-3">
                                        <div class="rounded-img-xs me-md-3 me-2"><img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}"></div>
                                        <h4 class="heading-5">Snipper Game</h4>
                                    </div>
                                    <a href="javascript:void(0);" title="Add" class="icon-userAdd"></a>
                                </li>
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center me-3">
                                        <div class="rounded-img-xs me-md-3 me-2"><img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}"></div>
                                        <h4 class="heading-5">Snipper Game</h4>
                                    </div>
                                    <a href="javascript:void(0);" title="Add" class="icon-userAdd"></a>
                                </li>
                            </ul>
                            <div class="border-t p-3">
                                <a class="arrow-btn" href="javascript:void(0);" title="View All">View All</a>
                            </div>
                        </div>
                        <div class="bg-color3">
                            <h2 class="heading-3 md border-b-title px-3 mb-0">Groups</h2>
                            <ul class="sidebar-list px-3">
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center me-3">
                                        <div class="rounded-img-xs me-md-3 me-2"><img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}"></div>
                                        <h4 class="heading-5">Snipper Game</h4>
                                    </div>
                                    <a href="javascript:void(0);" title="Share" class="category-share"></a>
                                </li>
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center me-3">
                                        <div class="rounded-img-xs me-md-3 me-2"><img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}"></div>
                                        <h4 class="heading-5">Snipper Game</h4>
                                    </div>
                                    <a href="javascript:void(0);" title="Share" class="category-share"></a>
                                </li>
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center me-3">
                                        <div class="rounded-img-xs me-md-3 me-2"><img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}"></div>
                                        <h4 class="heading-5">Snipper Game</h4>
                                    </div>
                                    <a href="javascript:void(0);" title="Share" class="category-share"></a>
                                </li>
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center me-3">
                                        <div class="rounded-img-xs me-md-3 me-2"><img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}"></div>
                                        <h4 class="heading-5">Snipper Game</h4>
                                    </div>
                                    <a href="javascript:void(0);" title="Share" class="category-share"></a>
                                </li>
                                <li class="d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center me-3">
                                        <div class="rounded-img-xs me-md-3 me-2"><img class="img-fluid" src="{{asset('frontend/assets/img/about-tagg.jpg')}}"></div>
                                        <h4 class="heading-5">Snipper Game</h4>
                                    </div>
                                    <a href="javascript:void(0);" title="Share" class="category-share"></a>
                                </li>
                            </ul>
                            <div class="border-t p-3">
                                <a class="arrow-btn" href="javascript:void(0);" title="View All">View All</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- detail end  -->
        <!-- footer start -->
        @include('dashboards.users.layout.footer')
    </div>

</body>

</html>

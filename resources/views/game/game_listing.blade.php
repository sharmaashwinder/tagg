<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('dashboards.users.layout.head')

<body>
    <div class="outer-container">
        @include('dashboards.users.layout.header')
        <!-- header end -->
        <!-- banner start -->
        <div class="banner-outer bg-color2">
            <div class="inner-container">
                <div class="custom-row c-my-5">
                    <div class="custom-row">
                        <h1 class="heading-1 pt-lg-4 pt-md-2 pt-0 pb-3"><span class="theme-color">games</span></h1>
                    </div>
                    <div class="d-flex flex-sm-row flex-column py-sm-3 py-2">
                        <form class="input-group order-group me-0 me-sm-4" id="" action="{{url('/game-listing')}}" method="get">
                            <span class="search-icon"></span>
                            <input type="text" class="form-control input-transparent icon-lp" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2" name="game_name" required>
                            <button type="submit" class="input-group-text button-yellow min-w justify-content-center" title="" id="basic-addon2">Search</button>
                        </form>

                        {{-- <button type="submit" class="input-group-text button-yellow button-yellow-border min-w mt-sm-0 mt-3 justify-content-center" title="Filter" id="basic-addon2">Filter</button> --}}
                    </div>
                    <div class="row pt-3 pt-md-4">
                        <!-- this is loop div -->


                        @if(!empty($all_games[0]) && $all_games[0]->count())
                        @foreach($all_games[0] as $all_game)
                       
                        <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                            <div class="custom-row my-2 my-sm-3">
                                <div class="product-img custom-row">
                                    <a href="{{route('game_page',$all_games[1]->encode($all_game->id))}}">
                                 @if(!empty($all_game->image))
                                    <img class="img-fluid" src="{{ asset('storage/' . $all_game->image) }}">
                                        @else
                                        <img class="img-fluid" src="{{asset('frontend/assets/img/default-sm.jpg')}}">
                                        @endif
                                    <!-- <span class="figure-tag">{{$all_game->genre}}</span> -->

                                    <span class="pro-count align-items-center justify-content-center d-flex">Active Tournaments {{$TournamentCount->count()}}</span>
                                       
                                       
                                    </a>
                                </div>
                                <div class="detail-bg">
                                    <h3 class="heading-5 text-truncate pb-1">{{$all_game->game_name}}</h3>
                                    <div class="typo6">{{$all_game->platform}}</div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <div class="col-12">
                            <h3 class="typo3 md text-center text-capitalize">No record found</h3>
                        </div>
                        @endif

                        <!-- loop div end -->

                    </div>
                    <!-- <div class="d-flex justify-content-end">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <li class="page-item border-pre disabled">
                                <a class="page-link" href=""><span class="page-prev"></span></a></li>

                                 <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item border-next"><a class="page-link" href=""><span class="page-next"></span></a></li>
                            </ul>
                        </nav>
                    </div> -->
                </div>
            </div>
        </div>
        <!-- banner end -->


        <!-- footer start -->
        @include('dashboards.users.layout.footer')
    </div>

</body>

</html>

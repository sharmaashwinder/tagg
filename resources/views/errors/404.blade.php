<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('dashboards.users.layout.head')

<body>
    <div class="outer-container">
        <div class="banner-outer banner-bg bg-color4 color2 full-Vheight d-flex align-items-center justify-content-center">
            <div class="inner-container-xs text-center">
                <h1 class="heading-7">404</h1>
                <div class="py-md-3 py-2">
                    <div class="heading-2">Oops! Something is wrong.</div>
                    <p class="typo1 pt-md-3 pt-2 mb-0">The page you are looking for does not exist.</p>
                </div>


                <div class="custom-row pt-lg-4 pt-2">
                    <a class="border-btn2 border-md custom-col" href="/" title="Go Home">Go Home</a>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
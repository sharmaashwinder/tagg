<?php

use App\Events\ChatEvent;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\GoogleController;
use App\Http\Controllers\SocialController;
use App\Http\Controllers\admin\TaggFeeController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\StaticController;
use App\Http\Controllers\User\UserController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});
Route::middleware(['middleware'=>'PreventBackHistory'])->group(function () {
    Auth::routes();
});





Route::get('/admin/login', [LoginController::class, 'display_loginpage'])->middleware('PreventBackHistory')->name('adminlogin');
Route::post('/adminslogin', [LoginController::class, 'admin_login'])->middleware('PreventBackHistory')->name('admin_login');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->middleware('PreventBackHistory')->name('home');
Route::get('/verify', [RegisterController::class, 'verifyuser'])->middleware('PreventBackHistory')->name('verify');
Route::view('/verify_mail','auth.verify')->name('verify_mail');
Route::post('/resend_verify', [RegisterController::class, 'resendverifyuser'])->middleware('PreventBackHistory')->name('resend_verify');




Route::group(['prefix'=>'admin', 'middleware'=>['isAdmin','auth','PreventBackHistory']], function(){

            Route::get('dashboard',[AdminController::class,'index'])->name('admin.dashboard');
            Route::get('change-password',[AdminController::class,'profile'])->name('admin.profile');
            Route::get('settings',[AdminController::class,'settings'])->name('admin.settings');
            Route::get('player',[AdminController::class,'player'])->name('admin.player');
            Route::get('affiliate',[AdminController::class,'affiliates'])->name('admin.affiliate');
            Route::get('player/{id}',[AdminController::class,'playerview'])->name('player.view');
            Route::get('player_edit/{id}',[AdminController::class,'playeredit'])->name('player.edit');
            Route::post('player_update/{id}',[AdminController::class,'updateuser'])->name('player.update');
            Route::post('playerrolesave',[AdminController::class,'playereditrolesave'])->name('playereditrole.save');
            Route::post('player_edit',[AdminController::class,'playereditsave'])->name('playeredit.save');
            Route::get('/player-destroy/{id}',[AdminController::class,'destroy'])->name('player.destroy');
            Route::get('permission',[AdminController::class,'permission'])->name('admin.permission');
            Route::post('permission_save',[AdminController::class,'permission_save'])->name('admin.permission_save');
            Route::post('change-profile-picture',[AdminController::class,'updatePicture'])->name('adminPictureUpdate');
            Route::post('change-password', 'App\Http\Controllers\Admin\ChangePasswordController@store')->name('change.password');
            Route::get('/game', 'App\Http\Controllers\Admin\GameController@create_game')->name('create.game');
            Route::get('/create_game', 'App\Http\Controllers\Admin\GameController@add_game')->name('create.games');
            Route::post('/store', 'App\Http\Controllers\Admin\GameController@store')->name('store.games');
            Route::get('/view_game/{id}', 'App\Http\Controllers\Admin\GameController@gameview')->name('view.games');
            Route::get('/destroy/{id}', 'App\Http\Controllers\Admin\GameController@destroy')->name('destroy.games');
            Route::get('/edit-games/{id}', 'App\Http\Controllers\Admin\GameController@gameedit')->name('edit.games');
            Route::post('/update/{id}', 'App\Http\Controllers\Admin\GameController@updategame')->name('update.games');
            Route::get('/tournament', 'App\Http\Controllers\Admin\TournamentController@tournament')->name('tournament');
            Route::get('/create-tournament', 'App\Http\Controllers\Admin\TournamentController@create_tournament')->name('create.tournament');
            Route::post('/create-store', 'App\Http\Controllers\Admin\TournamentController@store')->name('store.tournament');
            Route::get('/tournament/{id}', 'App\Http\Controllers\Admin\TournamentController@view_tournament')->name('view.tournament');
            Route::get('/tournament-edit/{id}', 'App\Http\Controllers\Admin\TournamentController@tournamentedit')->name('tournament.games');
            Route::post('/update-tournament/{id}', 'App\Http\Controllers\Admin\TournamentController@UpdateTournament')->name('tournament.update');
            Route::get('prizepool', 'App\Http\Controllers\Admin\PrizePollController@prizeindex')->name('prize.index');
            Route::get('/create_prize', 'App\Http\Controllers\Admin\PrizePollController@create_prize')->name('create.prize');
            Route::post('/store_prize', 'App\Http\Controllers\Admin\PrizePollController@store')->name('store.prize');
            Route::get('/edit_prize/{id}/{tournamnet_id?}', 'App\Http\Controllers\Admin\PrizePollController@prizeedit')->name('edit.prize');
            Route::post('/update_prize/{id}', 'App\Http\Controllers\Admin\PrizePollController@updateprize')->name('update.prize');
            Route::get('/prize_destroy/{id}', 'App\Http\Controllers\Admin\PrizePollController@destroy')->name('delete.prize');
            Route::any('/setting', 'App\Http\Controllers\Admin\SettingController@admin_setting')->name('setting');
            Route::post('/set_badge_mode', 'App\Http\Controllers\Admin\SettingController@set_mode')->name('set_badge_mode');
            Route::post('/theme_change', 'App\Http\Controllers\Admin\SettingController@theme_change')->name('theme_change');
            Route::get('/transaction-fee', 'App\Http\Controllers\Admin\TaggFeeController@index')->name('taggfeelist');
});





// user
Route::group(['middleware'=>['auth','PreventBackHistory','isUser']], function(){
    Route::get('share', [UserController::class, 'share']);
    Route::get('profile',[UserController::class,'profile'])->name('users.profile');
    Route::get('editprofile',[UserController::class,'editprofile'])->name('users.editprofile');
    Route::post('profile-save',[UserController::class,'editprofilesave'])->name('profilesave');

    Route::post('image-upload', [ UserController::class, 'imageUploadPost' ])->name('image.upload.post');
    Route::get('user_tournaments',[UserController::class,'user_tournaments'])->name('users.tournaments');
    Route::get('upcoming_tournaments',[UserController::class,'upcoming_tournaments'])->name('upcoming.tournaments');
    Route::get('completed_tournaments',[UserController::class,'completed_tournaments'])->name('completed.tournaments');
    // working on it

    Route::get('/social_permisisons',[UserController::class,'get_socialpermissions'])->name('social.permissions');
    Route::post('social_permission',[UserController::class,'social_permission'])->name('savesocial.permissions');
    Route::get('user-listing',[UserController::class,'guest_listing'])->name('users.list');
    Route::get('gamer-profile/{id}',[UserController::class,'gamer_profile'])->name('gamer.view');
    // Route::get('gamer-profile/{id?}',[UserController::class,'gamer_profile'])->name('gamer.view');

    Route::any('user_search',[UserController::class,'user_search'])->name('usersearch.view');

    Route::post('followrequest',[UserController::class,'follow_request'])->name('followrequest');
    Route::post('unfollowrequest',[UserController::class,'unfollow_request'])->name('unfollowrequest');

});

// Route::get('auth/apple', [GoogleController::class, 'apple_login']);
// Route::get('auth/apple/callback/', [GoogleController::class, 'apple_callback']);
// Route::post('auth/apple/callback/', [GoogleController::class, 'apple_callback']);
Route::get('auth/{provider}', [SocialController::class, 'redirectToProvider']);
Route::any('auth/{provider}/callback', [SocialController::class, 'handleProviderCallback']);
Route::get('/about', 'App\Http\Controllers\StaticController@about')->name('about');
Route::get('/faq', 'App\Http\Controllers\StaticController@faq')->name('faq');
Route::get('/contact-us', 'App\Http\Controllers\StaticController@contactus')->name('contact-us');
Route::get('/privacy', 'App\Http\Controllers\StaticController@privacypolicy')->name('privacy');
Route::get('/article', 'App\Http\Controllers\StaticController@article')->name('article');
Route::get('/become-affiliate', 'App\Http\Controllers\StaticController@becomeaffiliate')->name('become-affiliate');
Route::get('/about-rating', 'App\Http\Controllers\StaticController@aboutrating')->name('about-rating');
Route::get('/press-release', 'App\Http\Controllers\StaticController@pressrelease')->name('press-release');
Route::get('/careers', 'App\Http\Controllers\StaticController@careers')->name('careers');
Route::get('/developers', 'App\Http\Controllers\StaticController@developers')->name('developers');
Route::get('/site-map', 'App\Http\Controllers\StaticController@sitemap')->name('site-map');
Route::post('change_password', 'App\Http\Controllers\Admin\ChangePasswordController@store')->name('password_change');
Route::post('/subscribe', 'App\Http\Controllers\StaticController@store')->name('subscription');
Route::post('/contacts', 'App\Http\Controllers\StaticController@contactussave')->name('contactuss');
Route::get('connections', "App\Http\Controllers\HomeController@gameConnections")->name('connections');
Route::get('disconnect-platform/{platform}', "App\Http\Controllers\HomeController@disconnectPlatform")->name('disconnect-platform');
Route::get('change_password',[StaticController::class,'profile'])->middleware('auth')->name('profile');



// Wallet Routes
Route::any('wallet', 'App\Http\Controllers\PayPalPaymentController@wallet')->name('wallet');
Route::post('createOrder', 'App\Http\Controllers\PayPalPaymentController@createOrder')->name('createOrder');
Route::any('captureOrder/{orderId}/capture', 'App\Http\Controllers\PayPalPaymentController@capturePayment');
Route::get('paypalReturn', 'App\Http\Controllers\PayPalPaymentController@returnpPaypal')->name('paypalReturn');
Route::get('paypalCancel', 'App\Http\Controllers\PayPalPaymentController@cancelPaypal')->name('paypalCancel');
Route::get('donate-token/{id}', 'App\Http\Controllers\WalletController@donateToken')->name('donate-token');
Route::post('transfer-token', 'App\Http\Controllers\WalletController@transferToken')->name('transfer-token');
Route::post('withdraw', 'App\Http\Controllers\PayPalPaymentController@withdrawFunds')->name('withdraw');
Route::post('transaction-info', 'App\Http\Controllers\PayPalPaymentController@transactionInfo')->name('transaction-info');

//logout
 Route::get('/logout', 'App\Http\Controllers\Auth\LoginController@logout');

// Game Routes
Route::any('/game-listing', 'App\Http\Controllers\GameController@gameListing')->name('game-listing');
Route::get('/game/{id}', 'App\Http\Controllers\GameController@gamepage')->name('game_page');
Route::any('/game-details', 'App\Http\Controllers\GameController@gameDetails')->name('game-details');


Route::any('ui', 'App\Http\Controllers\StaticController@ui')->name('ui');
Route::get('chats',function(){
   event(new ChatEvent('hey '));

});


Route::get('chatblade',function(){
    return view('chat');
});

Route::get('chat2',function(){
    return view('chat2');
});


Route::get('chat3',function(){
    return view('chat3');
});
